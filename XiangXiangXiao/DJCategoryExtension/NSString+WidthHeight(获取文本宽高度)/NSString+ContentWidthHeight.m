//
//  NSString+ContentWidthHeight.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "NSString+ContentWidthHeight.h"

@implementation NSString (ContentWidthHeight)
+(CGFloat)getWidthWithContent:(NSString *)content font:(CGFloat)font contentSize:(CGSize)contentSize
{
    if (content == nil){
        return 0;
    }
    
    CGSize measureSize;
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:font]};
    
    if([[UIDevice currentDevice].systemVersion floatValue] < 7.0){
        
        measureSize = [content sizeWithFont:[UIFont systemFontOfSize:font] constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        
    }else{
        
        measureSize = [content boundingRectWithSize:contentSize options:NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
        
    }
    
    return ceil(measureSize.width);
}

//傳字符串文本和字體返回高度
+(CGFloat)getHeightWithContent:(NSString *)content font:(CGFloat)font contentSize:(CGSize)contentSize
{
    if (content == nil){
        return 0;
    }
    
    CGSize measureSize;
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:font]};
    
    if([[UIDevice currentDevice].systemVersion floatValue] < 7.0){
        
        measureSize = [content sizeWithFont:[UIFont systemFontOfSize:font] constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        
    }else{
        
        measureSize = [content boundingRectWithSize:contentSize options:NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
        
    }
    
    return ceil(measureSize.height);
}
@end
