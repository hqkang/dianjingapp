//
//  NSString+ContentWidthHeight.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ContentWidthHeight)
+(CGFloat)getWidthWithContent:(NSString *)content font:(CGFloat)font contentSize:(CGSize)contentSize;
+(CGFloat)getHeightWithContent:(NSString *)content font:(CGFloat)font contentSize:(CGSize)contentSize;
@end
