//
//  NSDictionary+Addtion.m
//  anxindian
//
//  Created by AEF_LUO on 16/7/4.
//  Copyright © 2016年 anerfa. All rights reserved.
//

#import "NSDictionary+Addtion.h"
#import "NSString+Addtion.h"

@implementation NSDictionary (Addtion)

- (BOOL)isNotEmpty
{
    return (![(NSNull *)self isEqual:[NSNull null]]
            && [self isKindOfClass:[NSDictionary class]]
            && self.count > 0);
}

- (NSArray *)arrayForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return nil;
    } else {
        if ([object isKindOfClass:[NSArray class]]) {
            return object;
        }
    }
    return nil;
}

- (NSNumber*)numberForKey:(id)key {
    id s = [self objectForKey:key];
    if (s == [NSNull null] || ![s isKindOfClass:[NSNumber class]]) {
        return @(0);
    }
    return s;
}

- (NSDictionary *)dictForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return nil;
    } else {
        if ([object isKindOfClass:[NSDictionary class]]) {
            return object;
        }
    }
    return nil;
}

- (NSString *)stringForKey:(id)aKey
{
    
    id object = [self objectForKey:aKey];
    
    if ([object isKindOfClass:[NSNull class]] || object == nil) {
        return @"";
    } else {
        if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object stringValue];
        } else if ([object isKindOfClass:[NSString class]]) {
            return (NSString *)object;
        }
    }
    
    return @"";
    
}

- (NSInteger)integerForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return 0;
    } else {
        if ([object isKindOfClass:[NSString class]]) {
            return [(NSString *)object integerValue];
        } else if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object integerValue];
        }
    }
    return 0;
}

- (int)intForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return (int)0;
    } else {
        if ([object isKindOfClass:[NSString class]]) {
            return [(NSString *)object intValue];
        } else if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object intValue];
        }
    }
    return (int)0;
}

- (float)floatForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return (float)0;
    } else {
        if ([object isKindOfClass:[NSString class]]) {
            return [(NSString *)object floatValue];
        } else if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object floatValue];
        }
    }
    return (float)0;
}

- (double)doubleForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return (double)0;
    } else {
        if ([object isKindOfClass:[NSString class]]) {
            return [(NSString *)object doubleValue];
        } else if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object doubleValue];
        }
    }
    return (double)0;
}

- (BOOL)boolForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSNull class]]) {
        return NO;
    } else {
        if ([object isKindOfClass:[NSString class]]) {
            return [(NSString *)object boolValue];
        } else if ([object isKindOfClass:[NSNumber class]]) {
            return [(NSNumber *)object boolValue];
        }
    }
    return NO;
}


- (id)objWithKey:(id)key{
    
    id object = [self objectForKey:key];
    
    if ([object isKindOfClass:[NSNumber class]]) {
        if ([object isKindOfClass:[NSNull class]]) {
            return @0;
        }
        return [(NSNumber *)object stringValue];
    } else if ([object isKindOfClass:[NSString class]]) {
        if ([object isKindOfClass:[NSNull class]]) {
            return @"";
        }
        return (NSString *)object;
    }else if ([object isKindOfClass:[NSNull class]]){
        
        return @"";
    }
    return object;
}

@end
