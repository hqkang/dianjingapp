//
//  UIView+DJCategory.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (DJCategory)

@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;


// view设置圆角
- (void)addRadius:(CGFloat)radius corners:(UIRectCorner)corners
          bgColor:(UIColor *)bgColor ;

/**
 设置大小一致的圆角，可选1-4角
 */
-(void)setCornerWithRadii:(CGFloat)radii corners:(UIRectCorner)corners;

+ (id)loadFromNib;


/**
 设置边框
 
 @param color 颜色
 @param width 边框宽度
 */
- (void)setBorderColor:(UIColor *)color width:(CGFloat)width;



@end

NS_ASSUME_NONNULL_END
