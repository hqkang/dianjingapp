//
//  UIView+DJCategory.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "UIView+DJCategory.h"

@implementation UIView (DJCategory)

#pragma mark - public method
- (void)addRadius:(CGFloat)radius
          corners:(UIRectCorner)corners
          bgColor:(UIColor *)bgColor{
    UIImageView *imgView = [self _addCornerImageView];
    CGRect bounds = self.bounds;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self _addCornerRadius:radius corners:corners imageView:imgView bounds:bounds bgColor:bgColor];
    });
}
#pragma mark - private method
- (UIImageView *)_addCornerImageView{
    UIImageView *imageView = nil;
    for (UIView *subView in self.subviews) {
        if (subView.tag == 1234567) {
            imageView = (UIImageView *)subView;
            break;
        }
    }
    if (!imageView) {
        imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        imageView.userInteractionEnabled = NO;
        imageView.opaque = YES;
        imageView.tag = 1234567;//防重复添加
        //置顶
        imageView.layer.zPosition = 999;
        [self addSubview:imageView];
        [self bringSubviewToFront:imageView];
    }
    
    return imageView;
}
- (void)_addCornerRadius:(CGFloat)radius corners:(UIRectCorner)corners imageView:(UIImageView *)imageView bounds:(CGRect)bounds bgColor:(UIColor *)bgColor{
    
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:bounds];
    UIBezierPath *cornerPath = [[UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)] bezierPathByReversingPath];
    
    [path appendPath:cornerPath];
    //裁剪出圆角路径
    CGContextAddPath(context, path.CGPath);
    //用背景色填充路径
    [bgColor set];
    CGContextFillPath(context);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    dispatch_async(dispatch_get_main_queue(), ^{
        imageView.image = image;
    });
}


/**
 设置大小一致的圆角，可选1-4角
 */
-(void)setCornerWithRadii:(CGFloat)radii corners:(UIRectCorner)corners
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(radii, radii)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}


+ (id)loadFromNib
{
    NSString *nibName = NSStringFromClass([self class]);
    NSArray *elements = [[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:nil
                                                    options:nil];
    for ( NSObject *anObject in elements ) {
        if ( [anObject isKindOfClass:[self class]] ) {
            return anObject;
        }
    }
    return nil;
}


- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (BOOL)isShowingOnKeyWindow
{
    // 主窗口
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    // 以主窗口左上角为坐标原点, 计算self的矩形框
    CGRect newFrame = [keyWindow convertRect:self.frame fromView:self.superview];
    CGRect winBounds = keyWindow.bounds;
    
    // 主窗口的bounds 和 self的矩形框 是否有重叠
    BOOL intersects = CGRectIntersectsRect(newFrame, winBounds);
    
    return !self.isHidden && self.alpha > 0.01 && self.window == keyWindow && intersects;
}


+ (instancetype)showRemindView:(CGRect)rect withImage:(UIImage*)image withText:(NSString *)text
{
    UIView *view = [[UIView alloc] init];
    view.frame = rect;
    CGFloat constant = 100;
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake((rect.size.width - constant) / 2, rect.size.height / 2  - constant, constant, constant);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;
    [view addSubview:imageView];
    CGFloat labelY = CGRectGetMaxY(imageView.frame);
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, labelY + 10, rect.size.width, 30);
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor darkGrayColor];
    [view addSubview:label];
    return view;
}

- (void)setBorderColor:(UIColor *)color width:(CGFloat)width {
    [self.layer setBorderWidth:width];
    [self.layer setBorderColor:color.CGColor];
    /*****注释:锯齿*****/
    self.layer.allowsEdgeAntialiasing = YES;
}


@end
