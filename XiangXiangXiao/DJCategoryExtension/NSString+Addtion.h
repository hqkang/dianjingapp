//
//  NSString+Addtion.h
//  anxindian
//
//  Created by AEF_LUO on 16/7/2.
//  Copyright © 2016年 anerfa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Addtion)

- (BOOL)isNotEmpty;
- (BOOL)isEmpty;
+ (NSString *)firstCharactor:(NSString *)aString;
@end
