//
//  UIColor+Extension.h
//  PointPointOnline
//
//  Created by 点点 on 2018/7/13.
//  Copyright © 2018年 diandianguanjia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

+ (CAGradientLayer *)setGradualChangingColor:(UIView *)view fromColor:(NSString *)fromHexColorStr toColor:(NSString *)toHexColorStr;


+ (UIColor *)colorWithHex:(NSString *)hexColor;

@end
