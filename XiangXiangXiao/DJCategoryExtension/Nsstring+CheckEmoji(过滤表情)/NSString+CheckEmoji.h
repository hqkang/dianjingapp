//
//  NSString+CheckEmoji.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CheckEmoji)

- (BOOL) containEmoji;

@end
