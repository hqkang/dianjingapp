//
//  NSArray+Addtion.m
//  anxindian
//
//  Created by AEF_LUO on 16/7/2.
//  Copyright © 2016年 anerfa. All rights reserved.
//

#import "NSArray+Addtion.h"

@implementation NSArray (Addtion)

/**
 *  @brief 判断是否为空
 */
- (BOOL)isNotEmpty
{
    return (![(NSNull *)self isEqual:[NSNull null]]
            && [self isKindOfClass:[NSArray class]]
            && self.count > 0);
}

- (id)objAtIndex:(NSUInteger)index{
    
    if (index < self.count)	{
        return self[index];
    }
    return nil;
}

@end
