//
//  CALayer+Extension.m
//  PointPointOnline
//
//  Created by 点点 on 2017/12/13.
//  Copyright © 2017年 diandianguanjia. All rights reserved.
//

#import "CALayer+Extension.h"

@implementation CALayer (Extension)

- (void)setBorderColorWithUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

@end
