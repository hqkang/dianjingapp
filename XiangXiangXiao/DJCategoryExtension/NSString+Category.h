//
//  NSString+Category.h
//  ZHBasedFrameWork
//
//  Created by 曾浩 on 2017/1/9.
//  Copyright © 2017年 zenghao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

/**
 *  去掉首尾空字符串
 */
- (NSString *)replaceSpaceOfHeadTail;
- (NSString *)replaceUnicode;

/**
 获取缓存路径

 @return 将当前字符串拼接到cache目录后面
 */
- (NSString *)cacheDic;

/**
 获取document路径

 @return 将当前字符串拼接到document目录后面
 */
- (NSString *)docDic;

/**
 获取tmp路径

 @return 将当前字符串拼接到tmp目录后面
 */
- (NSString *)tmpDic;

/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

/**
 *  添加行间距并返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 *  @param lineSpaceing 行间距
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize andlineSpacing:(CGFloat) lineSpaceing;

/**
 *  自适应图片url
 */
- (NSString *)self_adaptionHost;

- (NSString *)timing;

@end
