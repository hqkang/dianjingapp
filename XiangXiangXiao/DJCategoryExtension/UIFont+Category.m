//
//  UIFont+Category.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "UIFont+Category.h"

@implementation UIFont (Category)

+ (UIFont *)font_PFSC_MediumWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@".PingFangSC-Medium" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)font_PFSC_RegularWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@".PingFangSC-Regular" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)font_PFSC_BoldWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont boldSystemFontOfSize:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)font_PFSC_SemiblodWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"PingFangSC-Semibold" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

@end
