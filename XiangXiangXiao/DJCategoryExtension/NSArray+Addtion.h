//
//  NSArray+Addtion.h
//  anxindian
//
//  Created by AEF_LUO on 16/7/2.
//  Copyright © 2016年 anerfa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Addtion)

- (BOOL)isNotEmpty;

/// 数组越界处理
- (id)objAtIndex:(NSUInteger)index;


@end
