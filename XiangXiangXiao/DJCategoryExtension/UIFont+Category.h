//
//  UIFont+Category.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (Category)

+ (UIFont *)font_PFSC_MediumWithSize:(CGFloat)fontSize;

+ (UIFont *)font_PFSC_RegularWithSize:(CGFloat)fontSize;

+ (UIFont *)font_PFSC_BoldWithSize:(CGFloat)fontSize;

+ (UIFont *)font_PFSC_SemiblodWithSize:(CGFloat)fontSize;

@end

NS_ASSUME_NONNULL_END
