//
//  MBProgressHUD+Custom.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBProgressHUD (Custom)
+(MBProgressHUD *)showHUDAddedTo:(UIView *)view animated:(BOOL)animated;
+ (MBProgressHUD *)bwm_showTitle:(NSString *)title toView:(UIView *)view hideAfter:(NSTimeInterval)afterSecond;
+ (MBProgressHUD *)bwm_showHUDAddedTo:(UIView *)view title:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
