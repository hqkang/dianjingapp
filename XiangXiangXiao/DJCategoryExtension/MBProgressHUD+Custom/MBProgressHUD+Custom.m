//
//  MBProgressHUD+Custom.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import "MBProgressHUD+Custom.h"
#import <UIImage+GIF.h>
#import "HudGifImageView.h"

@implementation MBProgressHUD (Custom)
+(MBProgressHUD *)showHUDAddedTo:(UIView *)view animated:(BOOL)animated
{
    //使用SDWebImage 放入gif 图片
//    NSString *loading = ((kIsIphoneP)||(kIsIPhoneX))?@"loading3":@"loading2";
//    UIImage *image = [UIImage sd_animatedGIFNamed:loading];
    //自定义imageView
//    UIImageView *cusImageV = [[UIImageView alloc] initWithImage:image];
//    cusImageV.backgroundColor = kColorFromRGBAndAlpha(kWhite, 0);
    
    
    HudGifImageView *cusImageV = [[HudGifImageView alloc]initWithFrame:CGRectMake(0, 0, HUDGIFW, HUDGIFH)];
    cusImageV.animationImages =@[[UIImage imageNamed:@"ic_loading_01"],
                                         [UIImage imageNamed:@"ic_loading_02"],
                                         [UIImage imageNamed:@"ic_loading_03"],
                                 [UIImage imageNamed:@"ic_loading_04"],
                                 [UIImage imageNamed:@"ic_loading_05"],
                                 [UIImage imageNamed:@"ic_loading_06"],
                                 [UIImage imageNamed:@"ic_loading_07"],
                                 [UIImage imageNamed:@"ic_loading_08"],
                                 [UIImage imageNamed:@"ic_loading_09"],
                                 [UIImage imageNamed:@"ic_loading_10"],
                                 [UIImage imageNamed:@"ic_loading_11"],
                                 [UIImage imageNamed:@"ic_loading_12"],
                                 [UIImage imageNamed:@"ic_loading_13"],
                                 [UIImage imageNamed:@"ic_loading_14"],
                                 [UIImage imageNamed:@"ic_loading_15"],
                                 [UIImage imageNamed:@"ic_loading_16"],
                                 [UIImage imageNamed:@"ic_loading_17"],
                                 [UIImage imageNamed:@"ic_loading_18"],
                                 [UIImage imageNamed:@"ic_loading_19"]
                                 ];
    cusImageV.animationDuration = 0.84f;
    cusImageV.animationRepeatCount = 0;
    
    
    //设置hud模式
    MBProgressHUD *hud = [[self alloc] initWithView:view];
    hud.mode = MBProgressHUDModeCustomView;
    hud.removeFromSuperViewOnHide = YES;
    hud.customView = cusImageV;
//    hud.customView.frame = CGRectMake(0, 0, 60, 60);
//    hud.minSize = CGSizeMake(60, 60);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = kColorFromRGBAndAlpha(kWhite, 0);
    hud.backgroundColor = kColorFromRGBAndAlpha(kWhite, 0);
    [view addSubview:hud];
    
    [cusImageV startAnimating];

    [hud showAnimated:animated];
    return hud;
}
+ (MBProgressHUD *)bwm_showTitle:(NSString *)title toView:(UIView *)view hideAfter:(NSTimeInterval)afterSecond {
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUD.mode = MBProgressHUDModeText;
    HUD.label.font = [UIFont systemFontOfSize:14.f];
    HUD.label.text = title;
    HUD.label.textColor = kColorFromRGB(kWhite);
    HUD.bezelView.backgroundColor = kColorFromRGBAndAlpha(kBlack, 0.85);
    HUD.backgroundColor = nil;
    [HUD hideAnimated:YES afterDelay:afterSecond];
    return HUD;
}
+(MBProgressHUD *)bwm_showHUDAddedTo:(UIView *)view title:(NSString *)title
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUD.label.font = [UIFont systemFontOfSize:14.f];
    HUD.label.text = title;
    HUD.label.textColor = kColorFromRGB(kWhite);
    HUD.bezelView.backgroundColor = kColorFromRGBAndAlpha(kBlack, 0.85);
    HUD.backgroundColor = nil;
    return HUD;
}
@end
