//
//  NSDictionary+Addtion.h
//  anxindian
//
//  Created by AEF_LUO on 16/7/4.
//  Copyright © 2016年 anerfa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Addtion)

- (BOOL)isNotEmpty;

- (NSArray *)arrayForKey:(id)aKey;
- (NSDictionary *)dictForKey:(id)aKey;
- (NSNumber*) numberForKey:(id)key;
- (NSString *)stringForKey:(id)aKey;
- (NSInteger)integerForKey:(id)aKey;
- (int)intForKey:(id)aKey;
- (float)floatForKey:(id)aKey;
- (double)doubleForKey:(id)aKey;
- (BOOL)boolForKey:(id)aKey;

- (id)objWithKey:(id)key;

@end
