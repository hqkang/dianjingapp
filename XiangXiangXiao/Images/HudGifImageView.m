//
//  HudGifImageView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HudGifImageView.h"

@implementation HudGifImageView

- (CGSize)intrinsicContentSize {
    CGFloat contentViewH = HUDGIFH;
    CGFloat contentViewW = HUDGIFW;
    return CGSizeMake(contentViewW, contentViewH);
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

@end
