//
//  HudGifImageView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define HUDGIFW 121
#define HUDGIFH 51
@interface HudGifImageView : UIImageView

@end

NS_ASSUME_NONNULL_END
