//
//  EduListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "EduListModel.h"

NSString *const kEduTitle = @"title";
NSString *const kEduCover = @"cover";
NSString *const kEduPrice = @"price";

@interface EduListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation EduListModel

@synthesize title = _title;
@synthesize cover = _cover;
@synthesize price = _price;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.title = [self objectOrNilForKey:kEduTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kEduCover fromDictionary:dict];
        self.price = [self objectOrNilForKey:kEduPrice fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kEduTitle];
    [mutableDict setValue:self.cover forKey:kEduCover];
    [mutableDict setValue:self.price forKey:kEduPrice];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.title = [aDecoder decodeObjectForKey:kEduTitle];
    self.cover = [aDecoder decodeObjectForKey:kEduCover];
    self.price = [aDecoder decodeObjectForKey:kEduPrice];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_title forKey:kEduTitle];
    [aCoder encodeObject:_cover forKey:kEduCover];
    [aCoder encodeObject:_price forKey:kEduPrice];
}

- (id)copyWithZone:(NSZone *)zone {
    EduListModel *copy = [[EduListModel alloc] init];
    
    if (copy) {
        
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.price = [self.price copyWithZone:zone];
    }
    
    return copy;
}

@end
