//
//  FamilyEduCategory.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/26.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "FamilyEduCategory.h"

NSString *const kFamilyEduCategoryID = @"id";
NSString *const kFamilyEduCategoryName = @"name";

@interface FamilyEduCategory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FamilyEduCategory

@synthesize categoryID = _categoryID;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.categoryID = [[self objectOrNilForKey:kFamilyEduCategoryID fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kFamilyEduCategoryName fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryID] forKey:kFamilyEduCategoryID];
    [mutableDict setValue:self.name forKey:kFamilyEduCategoryName];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.categoryID = [aDecoder decodeDoubleForKey:kFamilyEduCategoryID];
    self.name = [aDecoder decodeObjectForKey:kFamilyEduCategoryName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_categoryID forKey:kFamilyEduCategoryID];
    [aCoder encodeObject:_name forKey:kFamilyEduCategoryName];
}

- (id)copyWithZone:(NSZone *)zone {
    FamilyEduCategory *copy = [[FamilyEduCategory alloc] init];
    
    if (copy) {
        
        copy.categoryID = self.categoryID;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}

@end
