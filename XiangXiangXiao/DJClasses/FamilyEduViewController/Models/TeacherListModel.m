//
//  TeacherListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TeacherListModel.h"

NSString *const kTeacherName = @"realname";
NSString *const kTeacherPhoto = @"suits_photo";
NSString *const kTeacherContent = @"content";

@interface TeacherListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TeacherListModel

@synthesize realname = _realname;
@synthesize suits_photo = _suits_photo;
@synthesize content = _content;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.realname = [self objectOrNilForKey:kTeacherName fromDictionary:dict];
        self.suits_photo = [self objectOrNilForKey:kTeacherPhoto fromDictionary:dict];
        self.content = [self objectOrNilForKey:kTeacherContent fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.realname forKey:kTeacherName];
    [mutableDict setValue:self.suits_photo forKey:kTeacherPhoto];
    [mutableDict setValue:self.content forKey:kTeacherContent];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.realname = [aDecoder decodeObjectForKey:kTeacherName];
    self.suits_photo = [aDecoder decodeObjectForKey:kTeacherPhoto];
    self.content = [aDecoder decodeObjectForKey:kTeacherContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_realname forKey:kTeacherName];
    [aCoder encodeObject:_suits_photo forKey:kTeacherPhoto];
    [aCoder encodeObject:_content forKey:kTeacherContent];
}

- (id)copyWithZone:(NSZone *)zone {
    TeacherListModel *copy = [[TeacherListModel alloc] init];
    
    if (copy) {
        
        copy.realname = [self.realname copyWithZone:zone];
        copy.suits_photo = [self.suits_photo copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    
    return copy;
}

@end
