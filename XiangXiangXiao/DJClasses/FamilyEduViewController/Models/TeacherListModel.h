//
//  TeacherListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeacherListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *suits_photo;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
