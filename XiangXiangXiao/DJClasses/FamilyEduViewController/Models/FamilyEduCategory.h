//
//  FamilyEduCategory.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/26.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FamilyEduCategory : NSObject<NSCoding, NSCopying>

@property (nonatomic, assign) double categoryID;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
