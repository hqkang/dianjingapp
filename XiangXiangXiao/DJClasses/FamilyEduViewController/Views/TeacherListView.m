//
//  TeacherListView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TeacherListView.h"
#import "TeacherItemCollectionViewCell.h"

@interface TeacherListView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong)UICollectionViewFlowLayout *flowLayout;

@end

@implementation TeacherListView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (NSArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _flowLayout = [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.minimumInteritemSpacing = 2.f;
        _flowLayout.minimumLineSpacing = 0.f;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _flowLayout.sectionInset = UIEdgeInsetsMake(8, kEdgeLeftAndRight, 0, kEdgeLeftAndRight);
        
        CGFloat itemWidth = kScreenWidth/2.5;
        CGFloat itemHeight = itemWidth * 139 / 127 + 60;
        // kScreenWidth/2.5 * 139 / 127 + 60
        _flowLayout.itemSize = CGSizeMake(itemWidth,itemHeight);
        
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(8, 8, kScreenWidth - 16, self.bounds.size.height - 16) collectionViewLayout:_flowLayout];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.bounces = NO;
        collectionView.showsHorizontalScrollIndicator = NO;
        [collectionView setBackgroundColor:kColorFromRGBAndAlpha(kWhite, 0)];
        
        UINib *nibCellStraight = [UINib nibWithNibName:NSStringFromClass([TeacherItemCollectionViewCell class]) bundle:nil];
        [collectionView registerNib:nibCellStraight forCellWithReuseIdentifier:NSStringFromClass([TeacherItemCollectionViewCell class])];
        
        [self addSubview:collectionView];
        _collectionView = collectionView;
    }
    return _collectionView;
}

-(void)setupUI
{
    kWeakSelf(self);
    
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}


#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource****************>
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    number = self.dataArray.count;
    return number;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    insets = UIEdgeInsetsMake(0, 0, 0, 0);
    return insets;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[UICollectionViewCell alloc]init];
    
    TeacherItemCollectionViewCell *cellStraight = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TeacherItemCollectionViewCell class]) forIndexPath:indexPath];
    TeacherListModel *model = [[TeacherListModel alloc] init];
    model = self.dataArray[indexPath.item];
    cellStraight.hideShadow = YES;
    if (indexPath.row < self.dataArray.count) {
        cellStraight.model = model;
        cellStraight.cusNumOfLine = 2;
        cellStraight.showCornerRaius = YES;
        cellStraight.showNameBgViewCornerRaius = YES;
    }
    cell = cellStraight;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectCustomActivityItemAtIndexPath:activitySection:)]) {
        [self.delegate didSelectCustomActivityItemAtIndexPath:indexPath activitySection:indexPath.section];
    }
}

@end
