//
//  TeacherListView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TeacherListViewDelegate<NSObject>
-(void)didSelectCustomActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section;
@end

@interface TeacherListView : UIView

@property (nonatomic, weak)id<TeacherListViewDelegate> delegate;

@property (nonatomic, copy)NSArray *dataArray;

@property (nonatomic, weak)UICollectionView *collectionView;

@end

NS_ASSUME_NONNULL_END
