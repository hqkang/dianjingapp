//
//  NewCustomCategoryItemCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/26.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "NewCustomCategoryItemCell.h"

@implementation NewCustomCategoryItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(EduListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.cover)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.descLabel.text = model.title;
    self.priceLabel.text = model.price;
}



@end
