//
//  FamilyEduBannerView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "FamilyEduBannerView.h"
#import "CustomPageControl.h"

//view
#import "NewPagedFlowView.h"

//models
#import "AdList.h"


@interface FamilyEduBannerView()<NewPagedFlowViewDelegate,NewPagedFlowViewDataSource>

@property (nonatomic, strong) NSMutableArray *imgArray;
@property (nonatomic, strong) NewPagedFlowView *cardBannerScroView;
@property (nonatomic, strong) CustomPageControl *pageControl;

@end

@implementation FamilyEduBannerView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(NewPagedFlowView *)cardBannerScroView
{
    if (!_cardBannerScroView) {
        NewPagedFlowView *tempView = [[NewPagedFlowView alloc]initWithFrame:self.bounds];
        tempView.delegate = self;
        tempView.dataSource = self;
        tempView.autoTime = 4.f;
        _cardBannerScroView = tempView;
        [self addSubview:_cardBannerScroView];
    }
    return _cardBannerScroView;
}

-(NSMutableArray *)imgArray
{
    if (!_imgArray) {
        _imgArray = [NSMutableArray array];
    }
    return _imgArray;
}
-(CustomPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[CustomPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 20, 100, 30)];
        [_pageControl setValue:[UIImage imageNamed:@"icon_pagedot_nor"] forKeyPath:@"_pageImage"];
        [_pageControl setValue:[UIImage imageNamed:@"icon_pagedot_sel"] forKeyPath:@"_currentPageImage"];
        self.cardBannerScroView.pageControl = _pageControl;
        [self.cardBannerScroView addSubview:_pageControl];
        [_pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.bottom.mas_equalTo(-8);
            make.height.mas_equalTo(10);
        }];
    }
    return _pageControl;
}

-(void)setupUI
{
    [self.cardBannerScroView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 0, -10, 0));
    }];
}

-(void)setAdListArray:(NSArray *)adListArray
{
    [self.imgArray removeAllObjects];
    for (AdList *model in adListArray) {
        NSString *tempStr = model.image;
        [self.imgArray addObject:tempStr];
    }
    
    self.pageControl.numberOfPages = self.imgArray.count;
    [self.cardBannerScroView reloadData];
}



#pragma mark - <NewPagedFlowViewDelegate>
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    CGFloat width = self.bounds.size.width-kEdgeLeftAndRight*4;
    CGFloat height = width/2.0;
    return CGSizeMake(width, height);
}

- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    if ([self.delegate respondsToSelector:@selector(didSelectBannerWithIndex:)]) {
        [self.delegate didSelectBannerWithIndex:subIndex];
    }
}

#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return self.imgArray.count;
}
- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = [flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] init];
        bannerView.tag = index;
        //        bannerView.layer.masksToBounds = YES;
        
        // 阴影颜色
        bannerView.layer.shadowColor = kColorFromRGB(kBlack).CGColor;
        // 阴影偏移，默认(0, -3)
        bannerView.layer.shadowOffset = CGSizeMake(0,4);
        // 阴影透明度，默认0
        bannerView.layer.shadowOpacity = 0.3;
        // 阴影半径，默认3
        bannerView.layer.shadowRadius = 5;
        bannerView.layer.cornerRadius = 4;
    }
    //在这里下载网络图片
    NSURL *imgURL = [NSURL URLWithString:self.imgArray[index]];
    [bannerView.mainImageView sd_setImageWithURL:imgURL placeholderImage:kImagePlaceholder];
    
    bannerView.mainImageView.layer.cornerRadius = 4;
    bannerView.mainImageView.layer.masksToBounds = YES;
    
    return bannerView;
}



@end
