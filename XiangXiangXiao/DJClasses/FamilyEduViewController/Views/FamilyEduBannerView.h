//
//  FamilyEduBannerView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BannerViewDelegate<NSObject>
@optional
-(void)didSelectBannerWithIndex:(NSInteger)index;
@end


@interface FamilyEduBannerView : UIView
@property (nonatomic, weak)id<BannerViewDelegate> delegate;
@property (nonatomic, copy)NSArray *adListArray;
@property (nonatomic, assign)NSInteger timeInterval;

@end

NS_ASSUME_NONNULL_END
