//
//  TeacherItemCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TeacherItemCollectionViewCell.h"

@interface TeacherItemCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *teacherImageView;
@property (weak, nonatomic) IBOutlet UILabel *teacherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *nameBgView;

@end
@implementation TeacherItemCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setShowCornerRaius:(BOOL)showCornerRaius
{
    if (showCornerRaius) {
        self.teacherImageView.layer.cornerRadius = 4.0;
        self.teacherImageView.layer.masksToBounds = YES;
    }else{
        self.teacherImageView.layer.cornerRadius = 0;
    }
}

-(void)setShowNameBgViewCornerRaius:(BOOL)showNameBgViewCornerRaius
{
    if (showNameBgViewCornerRaius) {
        self.nameBgView.layer.cornerRadius = 4.0;
        self.nameBgView.layer.masksToBounds = YES;
    }else{
        self.nameBgView.layer.cornerRadius = 0;
    }
}

-(void)setNameColor:(UIColor *)nameColor
{
    self.teacherNameLabel.textColor = nameColor;
}

- (void)setDescColor:(UIColor *)descColor
{
    self.descLabel.textColor = descColor;
}

-(void)setHideShadow:(BOOL)hideShadow
{
    if (hideShadow) {
        self.shadowView.backgroundColor = kColorFromRGBAndAlpha(kWhite, 0);
    }else{
        self.shadowView.backgroundColor = kColorFromRGBAndAlpha(kBlack, 0.02);
        self.shadowView.layer.cornerRadius = 4.f;
        self.shadowView.layer.masksToBounds = YES;
    }
}
-(void)setCusNumOfLine:(NSInteger)cusNumOfLine
{
    self.descLabel.numberOfLines = cusNumOfLine;
}
-(void)setModel:(TeacherListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.teacherImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.suits_photo)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.teacherNameLabel.text = model.realname;
    self.descLabel.text = model.content;
}


@end
