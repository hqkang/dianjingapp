//
//  TeacherListCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TeacherListCollectionViewCellDelegate<NSObject>
-(void)didSelectCustomActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section;
@end

@interface TeacherListCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *teacherImageView;

@property (nonatomic, strong) UILabel *teacherNameLabel;

@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, weak)id<TeacherListCollectionViewCellDelegate> delegate;

@property (nonatomic, strong) TeacherListModel *teacherListModel;

@property (nonatomic, copy)NSArray *dataArray;

@end

NS_ASSUME_NONNULL_END
