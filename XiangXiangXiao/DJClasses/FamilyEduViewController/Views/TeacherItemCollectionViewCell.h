//
//  TeacherItemCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TeacherItemCollectionViewCell : UICollectionViewCell


@property (nonatomic, assign)BOOL hideShadow;
@property (nonatomic, strong)UIColor *nameColor;
@property (nonatomic, strong)UIColor *descColor;
@property (nonatomic, assign)NSInteger cusNumOfLine;

@property (nonatomic, assign)BOOL showCornerRaius;
@property (nonatomic, assign)BOOL showNameBgViewCornerRaius;
@property (nonatomic, strong) TeacherListModel *model;

@end

NS_ASSUME_NONNULL_END
