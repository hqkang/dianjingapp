//
//  TeacherListCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TeacherListCollectionViewCell.h"
#import "TeacherItemCollectionViewCell.h"

@interface TeacherListCollectionViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, weak)UICollectionView *collectionView;
@property (nonatomic, strong)UICollectionViewFlowLayout *flowLayout;

@end

@implementation TeacherListCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (NSArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _flowLayout = [[UICollectionViewFlowLayout alloc]init];
        _flowLayout.minimumInteritemSpacing = 2.f;
        _flowLayout.minimumLineSpacing = 0.f;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _flowLayout.sectionInset = UIEdgeInsetsMake(8, kEdgeLeftAndRight, 0, kEdgeLeftAndRight);
        CGFloat itemHeight = kScreenWidth/2.0;
        CGFloat itemWidth = itemHeight/2.5*2.0;
        _flowLayout.itemSize = CGSizeMake(itemWidth,itemHeight);
        
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.contentView.bounds collectionViewLayout:_flowLayout];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.bounces = NO;
        collectionView.showsHorizontalScrollIndicator = NO;
        [collectionView setBackgroundColor:kColorFromRGBAndAlpha(kWhite, 0)];
        
        UINib *nibCellStraight = [UINib nibWithNibName:NSStringFromClass([TeacherItemCollectionViewCell class]) bundle:nil];
        [collectionView registerNib:nibCellStraight forCellWithReuseIdentifier:NSStringFromClass([TeacherItemCollectionViewCell class])];
        
        [self.contentView addSubview:collectionView];
        _collectionView = collectionView;
    }
    return _collectionView;
}

-(void)setupUI
{
    kWeakSelf(self);
   
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}


#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource****************>
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    number = self.dataArray.count;
    return number;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    insets = UIEdgeInsetsMake(0, 0, 0, 0);
    return insets;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[UICollectionViewCell alloc]init];
    
    TeacherItemCollectionViewCell *cellStraight = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TeacherItemCollectionViewCell class]) forIndexPath:indexPath];
    TeacherListModel *model = [[TeacherListModel alloc] init];
    model = self.dataArray[indexPath.item];
    cellStraight.hideShadow = YES;
    if (indexPath.row < self.dataArray.count) {
        cellStraight.model = model;
        cellStraight.cusNumOfLine = 2;
        cellStraight.showCornerRaius = YES;
        cellStraight.showNameBgViewCornerRaius = YES;
    }
    cell = cellStraight;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectCustomActivityItemAtIndexPath:activitySection:)]) {
        [self.delegate didSelectCustomActivityItemAtIndexPath:indexPath activitySection:indexPath.section];
    }
}

@end
