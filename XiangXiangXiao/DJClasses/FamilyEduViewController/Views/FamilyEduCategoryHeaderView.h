//
//  FamilyEduCategoryHeaderView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/26.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YUHoriView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FamilyEduCategoryHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet YUHoriView *categoryView;

@end

NS_ASSUME_NONNULL_END
