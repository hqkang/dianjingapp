//
//  NewCustomCategoryItemCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/26.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EduListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewCustomCategoryItemCell : UICollectionViewCell

@property (nonatomic, copy) EduListModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end

NS_ASSUME_NONNULL_END
