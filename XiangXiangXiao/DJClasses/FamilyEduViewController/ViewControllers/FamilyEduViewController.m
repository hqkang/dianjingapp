//
//  FamilyEduViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "FamilyEduViewController.h"
#import "SearchViewController.h"
#import "CommonWebViewController.h"

#import "DJCollectionViewFlowLayout.h"

// cells
#import "MoreReusableHeaderView.h"
#import "BannerCollectionViewCell.h"
#import "TeacherListCollectionViewCell.h"
#import "NewCustomCategoryItemCell.h"

// views
#import "FamilyEduCategoryHeaderView.h"
#import "CustomDefaultView.h"
#import "NavigationSearchView.h"

// models
#import "TeacherListModel.h"
#import "AdList.h"
#import "EduListModel.h"
#import "FamilyEduCategory.h"

@interface FamilyEduViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, CustomDefaultViewDelegate, NavigationSearchBarDelegate, BannerCollectionViewCellDelegate, TeacherListCollectionViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *titles;
@property (nonatomic, strong) DJCollectionViewFlowLayout *flowLayout;
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, strong) CustomDefaultView *defaultView;
@property (nonatomic, strong) NavigationSearchView *navigationSearchBar;

@property (nonatomic, strong) NSMutableArray *adListArray;//banner
@property (nonatomic, strong) NSMutableArray *classifyArray;//分类
@property (nonatomic, strong) NSMutableArray *recommedArray;//精选择推荐
@property (nonatomic, strong) NSMutableArray *teacherListArray;//明星老师

@property (nonatomic, assign) int currentIndex;

@property (nonatomic, assign) BOOL isFirst;

@end

@implementation FamilyEduViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFirst = YES;
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self configNavigation];
    [self getData];
}

- (void)getData
{
    // 创建信号量
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // 创建全局并行
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_group_async(group, queue, ^{
        // 轮播图
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSArray *keyValueArray = @[@"index_student"];
        params[@"codes"] = keyValueArray;
        [HttpTool postWithURL:kURL(kAdvList) params:params haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.adListArray removeAllObjects];
                    NSArray *results = dict[@"index_student"][@"ad_content"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.adListArray addObject:[AdList modelObjectWithDictionary:dic]];
                        }
                    }
                }
            }
        } failure:^(NSError *error) {
            [self.collectionView.mj_header endRefreshing];
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 家教类型
    dispatch_group_async(group, queue, ^{
        [HttpTool getWithURL:kURL(kEduCategoryList) params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.classifyArray removeAllObjects];
                    [self.titles removeAllObjects];
                    NSArray *results = [dict arrayForKey:@"data"];
                    if ([results isNotEmpty]) {
                        for (int i = 0; i < results.count; i++) {
                            [self.classifyArray addObject:[FamilyEduCategory modelObjectWithDictionary:results[i]]];
                            [self.titles addObject:results[i][@"name"]];
                        }
                        [self.titles insertObject:@"精选推荐" atIndex:0];
                    }
                }
            }
        } failure:^(NSError *error) {
            [self.collectionView.mj_header endRefreshing];
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 明星老师
    dispatch_group_async(group, queue, ^{
        
        [HttpTool getWithURL:kURL(kTeacherList) params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.teacherListArray removeAllObjects];
                    NSArray *results = [dict arrayForKey:@"data"];
                    if ([results isNotEmpty]) {
                        for (int i = 0; i < results.count; i++) {
                            [self.teacherListArray addObject:[TeacherListModel modelObjectWithDictionary:results[i]]];
                        }
                    }
                }
            }
        } failure:^(NSError *error) {
            [self.collectionView.mj_header endRefreshing];
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 家教列表
    dispatch_group_async(group, queue, ^{
        [HttpTool getWithURL:[NSString stringWithFormat:@"%@?recommend=1", kURL(kEduList)] params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.recommedArray removeAllObjects];
                    NSArray *results = dict[@"data"][@"data"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                        }
                    }
                }
            }
        } failure:^(NSError *error) {
            [self.collectionView.mj_header endRefreshing];
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    
    dispatch_group_notify(group, queue, ^{
        
        // 四个请求对应四次信号等待
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //在这里 进行请求后的方法，回到主线程
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView.mj_header endRefreshing];
            [self setupUIWithRequestError:NO];
            [hud hideAnimated:YES];
            //更新UI操作
            [self.collectionView reloadData];
        });
    });
}



- (NSMutableArray *)titles
{
    if (_titles == nil) {
        _titles = [NSMutableArray array];
    }
    return _titles;
}

- (NSMutableArray *)teacherListArray
{
    if (_teacherListArray == nil) {
        _teacherListArray = [NSMutableArray array];
    }
    return _teacherListArray;
}



#pragma mark - <配置navigation>
- (void)configNavigation
{
    //定位
    UIButton *btnLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLocation setFrame:CGRectMake(0, 0, 30, 30)];
    [btnLocation setTitle:@"中山" forState:UIControlStateNormal];
    [btnLocation setTitleColor:kColorFromRGB(kBlack) forState:UIControlStateNormal];
    btnLocation.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    UIBarButtonItem *barBtnItemLoc = [[UIBarButtonItem alloc]initWithCustomView:btnLocation];
    
    [btnLocation addTarget:self action:@selector(btnLocationAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = @[barBtnItemLoc];
    
    // search
    self.navigationItem.titleView = self.navigationSearchBar;
}

#pragma mark - 导航栏左侧定位按钮响应方法
- (void)btnLocationAction
{
    
}

- (NavigationSearchView *)navigationSearchBar
{
    if (!_navigationSearchBar) {
        NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
        navigationSearchBar.delegate = self;
        navigationSearchBar.isHideBtnSearch = YES;
        _navigationSearchBar = navigationSearchBar;
    }
    return _navigationSearchBar;
}

- (NSMutableArray *)adListArray
{
    if (_adListArray == nil) {
        _adListArray = [NSMutableArray array];
    }
    return _adListArray;
}

- (NSMutableArray *)classifyArray
{
    if (_classifyArray == nil) {
        _classifyArray = [NSMutableArray array];
    }
    return _classifyArray;
}

- (NSMutableArray *)recommedArray
{
    if (_recommedArray == nil) {
        _recommedArray = [NSMutableArray array];
    }
    return _recommedArray;
}

-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        DJCollectionViewFlowLayout *flowLayout = [[DJCollectionViewFlowLayout alloc]initWithType:AlignTypeWithLeft betweenOfCell:0 navigationHeight:0 sectionNeedToHover:2 customAlign:NO sectionNeedToCustomAlign:0];
        _flowLayout = flowLayout;
    }
    return _flowLayout;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:self.flowLayout];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.showsVerticalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self.view addSubview:collectionView];
        
        [collectionView registerClass:[BannerCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([BannerCollectionViewCell class])];
        [collectionView registerClass:[TeacherListCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([TeacherListCollectionViewCell class])];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MoreReusableHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([MoreReusableHeaderView class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([FamilyEduCategoryHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([FamilyEduCategoryHeaderView class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([NewCustomCategoryItemCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([NewCustomCategoryItemCell class])];
        [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self getData];
        }];
        collectionView.mj_header = header;
        
        _collectionView = collectionView;
    }
    return _collectionView;
}


-(void)loadTeacherDataWithReload:(BOOL)isReload
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // two_cat_id
    [HttpTool getWithURL:[NSString stringWithFormat:@"%@?two_cat_id=%d", kURL(kEduList), self.currentIndex] params:nil haveHeader:NO success:^(id json) {
        [self.collectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isEmpty) {
                    [self.recommedArray removeAllObjects];
                    NSArray *results = dict[@"data"][@"data"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                        }
                    }
                }
                [self.collectionView reloadData];
            }else{
                [self setupUIWithRequestError:YES];
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self setupUIWithRequestError:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        [self setupUIWithRequestError:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"出错啦～请稍后重试";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}


#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource*********************************>
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if (section == 0) {
        if (self.adListArray.count>0) {
            number = 1;
        }
    }else if (section == 1){
        if (self.teacherListArray.count>0) {
            number = 1;
        }
    }else if(section == 2){
        if (self.recommedArray.count>0) {
            number = self.recommedArray.count;
        }
    }
    return number;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    BannerCollectionViewCell *cellBanner = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([BannerCollectionViewCell class]) forIndexPath:indexPath];
    TeacherListCollectionViewCell *cellNested = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TeacherListCollectionViewCell class]) forIndexPath:indexPath];
    NewCustomCategoryItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([NewCustomCategoryItemCell class]) forIndexPath:indexPath];
    if (indexPath.section == 0) {
        if (self.adListArray.count > 0) {
            cellBanner.timeInterval = 4;
            cellBanner.delegate = self;
            cellBanner.adListArray = self.adListArray;
            gridcell = cellBanner;
        }
    }else if (indexPath.section == 1){
        if (self.teacherListArray.count > 0) {
            cellNested.dataArray = self.teacherListArray;
            cellNested.delegate = self;
            gridcell = cellNested;
        }
        
    }else if (indexPath.section == 2){
        if (self.recommedArray.count > 0) {
            EduListModel *model = [[EduListModel alloc] init];
            model = self.recommedArray[indexPath.item];
            cell.model = model;
            gridcell = cell;
        }
        
    }
    return gridcell;
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        MoreReusableHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([MoreReusableHeaderView class]) forIndexPath:indexPath];
        FamilyEduCategoryHeaderView *categoryHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([FamilyEduCategoryHeaderView class]) forIndexPath:indexPath];
        if(indexPath.section == 1){
            if (self.teacherListArray.count > 0) {
                headerView.headerViewTitleLabel.text = @"明星老师";
                reusableView = headerView;
            }
        }else if(indexPath.section == 2){
            if (self.titles.count > 0) {
                categoryHeaderView.categoryView.titles = self.titles;
                categoryHeaderView.categoryView.defaultButtonPos = 0;
                // 刷新
                if (self.isFirst) {
                    [categoryHeaderView.categoryView refresh];
                }
                // 位置改变的事件
                WEAKSELF;
                categoryHeaderView.categoryView.onPosChange = ^(YUHoriElementButton *sender, int pos, NSString *title) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.currentIndex = pos;
                        if (pos == 0 && !self.isFirst) {
                            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                            [HttpTool getWithURL:[NSString stringWithFormat:@"%@?recommend=1", kURL(kEduList)] params:nil haveHeader:NO success:^(id json) {
                                [hud hideAnimated:YES];
                                if (json) {
                                    NSDictionary *dict = (NSDictionary *)json;
                                    NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                                    BOOL isEmpty = [codeStr isEmpty];
                                    if (isEmpty) {
                                        [self.recommedArray removeAllObjects];
                                        NSArray *results = dict[@"data"][@"data"];
                                        if ([results isNotEmpty]) {
                                            for (NSDictionary *dic in results) {
                                                [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                                            }
                                            [self.collectionView reloadData];
                                        }
                                    }
                                }
                            } failure:^(NSError *error) {
                                [self.collectionView.mj_header endRefreshing];
                                [hud hideAnimated:YES];
                                [self setupUIWithRequestError:YES];
                            }];
                        }else{
                            self.isFirst = NO;
                            [self loadTeacherDataWithReload:YES];
                        }
                        self.currentIndex = pos;
                    });
                    NSLog(@"pos: %d ,title :%@",pos,title);
                    
                };
                
                reusableView = categoryHeaderView;
            }
        }
    }
    
    return reusableView;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"点击的section: %ld  item: %ld", indexPath.section, indexPath.item);
}

#pragma mark - <UICollectionViewFlowLayout*********************************>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeZero;
    if (indexPath.section == 0) {
        if (self.adListArray.count > 0) {
            itemSize = CGSizeMake(kScreenWidth, (kScreenWidth)/2.0);
        }
    }else if (indexPath.section == 1){
        if (self.teacherListArray.count>0) {
            itemSize = CGSizeMake(kScreenWidth, kScreenWidth / 2.0);
        }
    }else if (indexPath.section == 2){
        if (self.recommedArray.count>0) {
            CGFloat itemW = (kScreenWidth - 20) / 2;
            CGFloat itemH = itemW * 200 / 164;
            itemSize = CGSizeMake(itemW, itemH);
        }
    }
    return itemSize;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize itemSize = CGSizeZero;
    switch (section) {
        case 1:{
            itemSize = CGSizeMake(kScreenWidth, 60);
        }
            break;
        case 2:{
            itemSize = CGSizeMake(kScreenWidth, 60);
        }
            break;
            
        default:
            break;
    }
    return itemSize;
}



-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    if (section == 0) {
        if (self.adListArray.count>0) {
            insets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }else if (section == 1){
        if (self.teacherListArray.count>0) {
            //                insets = UIEdgeInsetsMake(0, 12, 16, 12);
            insets = UIEdgeInsetsMake(0, 0, 0, 0);
            
        }
    }else if (section == 2){
        if (self.recommedArray.count>0) {
            //                insets = UIEdgeInsetsMake(15, kEdgeLeftAndRight, 30, kEdgeLeftAndRight);
            insets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }
    
    return insets;
}

#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    if (isError) {
        [self.view bringSubviewToFront:self.defaultView];
        [self.view sendSubviewToBack:self.collectionView];
    }else{
        [self.view bringSubviewToFront:self.collectionView];
        [self.view sendSubviewToBack:self.defaultView];
    }
}

#pragma mark - <CustomDefaultViewDelegate------->
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView
{
    [self getData];
}

#pragma mark - <NavigationSearchBarDelegate>
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    [self jumpToSearchVCWithRecommdWords:textField.placeholder];
    return NO;
}
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField
{
    return NO;
}

#pragma mark - <跳转搜索页>
-(void)jumpToSearchVCWithRecommdWords:(NSString *)recommdWord
{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.recommdWords = recommdWord;
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark - <SDCycleScrollViewDelegate------->
-(void)didSelectBannerWithIndex:(NSInteger)index
{
    if (index < self.adListArray.count) {
        AdList *adListModel = self.adListArray[index];
        NSString *urlStr = adListModel.href;
        if ([urlStr isEmpty]) {
            return;
        }
        NSURL *url = [NSURL URLWithString:urlStr];
        CommonWebViewController *webVC = [[CommonWebViewController alloc] init];
        webVC.urlStr = urlStr;
        [self.navigationController pushViewController:webVC animated:YES];
    }
}

- (void)didSelectCustomActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section
{
    NSLog(@"section: %ld  item: %ld", section, indexPath.item);
}

@end
