//
//  FirstViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirstViewController : UIViewController

@property (nonatomic, strong) UICollectionView *infoCollectionView;

@end

NS_ASSUME_NONNULL_END
