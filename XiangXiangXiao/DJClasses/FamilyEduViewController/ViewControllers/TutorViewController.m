//
//  TutorViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TutorViewController.h"
#import <YNPageViewController.h>
#import <UIView+YNPageExtend.h>
#import "SearchViewController.h"
#import "CommonViewController.h"
#import "CommonWebViewController.h"

// views
#import "CustomDefaultView.h"
#import "NavigationSearchView.h"
#import "FamilyEduBannerView.h"
#import "TeacherListView.h"
#import "TeacherListHeaderView.h"

// models
#import "TeacherListModel.h"
#import "AdList.h"
#import "EduListModel.h"
#import "FamilyEduCategory.h"


@interface TutorViewController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate, NavigationSearchBarDelegate, CustomDefaultViewDelegate, BannerViewDelegate, TeacherListViewDelegate>
@property (nonatomic, strong) YNPageViewController *vc;

@property (nonatomic, strong) NavigationSearchView *navigationSearchBar;
@property (nonatomic,strong) UIView *headView;
@property (nonatomic,strong) FamilyEduBannerView *bannerView;
@property (nonatomic,strong) TeacherListHeaderView *sectionView;
@property (nonatomic,strong) TeacherListView *teacherListView;
@property (nonatomic, strong) CustomDefaultView *defaultView;
@property (nonatomic, strong) NSMutableArray *adListArray;//banner
@property (nonatomic, strong) NSMutableArray *classifyArray;//分类
@property (nonatomic, strong) NSMutableArray *recommedArray;//精选择推荐
@property (nonatomic, strong) NSMutableArray *teacherListArray;//明星老师
@property (nonatomic, strong) NSMutableArray *titles;

@property (nonatomic, strong) CommonViewController *currentVC;

@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation TutorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self configNavigation];
    //头部view包含view1,view2,view2根据传递过来的值决定显示不显示
    [self.headView addSubview:self.bannerView];
    [self.headView addSubview:self.sectionView];
    [self.headView addSubview:self.teacherListView];
    [self getData];
    [QK_NOTICENTER addObserver:self selector:@selector(headerRefresh) name:@"refreshCommonViewData" object:nil];
}

- (void)headerRefresh
{
    if (self.currentIndex == 0) {
        [HttpTool getWithURL:[NSString stringWithFormat:@"%@?recommend=1", kURL(kEduList)] params:nil haveHeader:NO success:^(id json) {
            [[self.currentVC infoCollectionView].mj_header endRefreshing];
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.recommedArray removeAllObjects];
                    NSArray *results = dict[@"data"][@"data"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                        }
                        self.currentVC.dataArray = self.recommedArray;
                        [[self.currentVC infoCollectionView] reloadData];
                    }
                }
            }
        } failure:^(NSError *error) {
            [[self.currentVC infoCollectionView].mj_header endRefreshing];
            [self setupUIWithRequestError:YES];
        }];
        
    }else{
        FamilyEduCategory *model = [[FamilyEduCategory alloc] init];
        model = self.classifyArray[self.currentIndex - 1];
        [self loadTeacherDataWithIndexID:[NSString stringWithFormat:@"%d", (int)model.categoryID] withVC:self.currentVC reload:YES];
    }
}


#pragma mark - <CustomDefaultViewDelegate------->
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView
{
    [self getData];
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"出错啦～请稍后重试";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}

- (NSMutableArray *)titles
{
    if (_titles == nil) {
        _titles = [NSMutableArray array];
    }
    return _titles;
}

- (NSMutableArray *)teacherListArray
{
    if (_teacherListArray == nil) {
        _teacherListArray = [NSMutableArray array];
    }
    return _teacherListArray;
}

- (NSMutableArray *)recommedArray
{
    if (_recommedArray == nil) {
        _recommedArray = [NSMutableArray array];
    }
    return _recommedArray;
}

- (NSMutableArray *)classifyArray
{
    if (_classifyArray == nil) {
        _classifyArray = [NSMutableArray array];
    }
    return _classifyArray;
}

- (NSMutableArray *)adListArray
{
    if (_adListArray == nil) {
        _adListArray = [NSMutableArray array];
    }
    return _adListArray;
}

- (void)getData
{
    // 创建信号量
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // 创建全局并行
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_group_async(group, queue, ^{
        // 轮播图
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSArray *keyValueArray = @[@"index_student"];
        params[@"codes"] = keyValueArray;
        [HttpTool postWithURL:kURL(kAdvList) params:params haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.adListArray removeAllObjects];
                    NSArray *results = dict[@"index_student"][@"ad_content"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.adListArray addObject:[AdList modelObjectWithDictionary:dic]];
                        }
                        self.bannerView.timeInterval = 4;
                        self.bannerView.delegate = self;
                        self.bannerView.adListArray = self.adListArray;
                    }
                }
            }
        } failure:^(NSError *error) {
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 家教类型
    dispatch_group_async(group, queue, ^{
        [HttpTool getWithURL:kURL(kEduCategoryList) params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.classifyArray removeAllObjects];
                    [self.titles removeAllObjects];
                    NSArray *results = [dict arrayForKey:@"data"];
                    if ([results isNotEmpty]) {
                        for (int i = 0; i < results.count; i++) {
                            [self.classifyArray addObject:[FamilyEduCategory modelObjectWithDictionary:results[i]]];
                            [self.titles addObject:results[i][@"name"]];
                        }
                        [self.titles insertObject:@"精选推荐" atIndex:0];
                    }
                }
            }
        } failure:^(NSError *error) {
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 明星老师
    dispatch_group_async(group, queue, ^{
        
        [HttpTool getWithURL:kURL(kTeacherList) params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.teacherListArray removeAllObjects];
                    NSArray *results = [dict arrayForKey:@"data"];
                    if ([results isNotEmpty]) {
                        for (int i = 0; i < results.count; i++) {
                            [self.teacherListArray addObject:[TeacherListModel modelObjectWithDictionary:results[i]]];
                        }
                        self.teacherListView.dataArray = self.teacherListArray;
                        self.teacherListView.delegate = self;
                        [[self.teacherListView collectionView] reloadData];
                    }
                }
            }
        } failure:^(NSError *error) {
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    // 家教列表
    dispatch_group_async(group, queue, ^{
        [HttpTool getWithURL:[NSString stringWithFormat:@"%@?recommend=1", kURL(kEduList)] params:nil haveHeader:NO success:^(id json) {
            dispatch_semaphore_signal(semaphore);
            if (json) {
                NSDictionary *dict = (NSDictionary *)json;
                NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
                BOOL isEmpty = [codeStr isEmpty];
                if (isEmpty) {
                    [self.recommedArray removeAllObjects];
                    NSArray *results = dict[@"data"][@"data"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                        }
                    }
                }
            }
        } failure:^(NSError *error) {
            [hud hideAnimated:YES];
            [self setupUIWithRequestError:YES];
        }];
    });
    
    dispatch_group_notify(group, queue, ^{
        
        // 四个请求对应四次信号等待
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //在这里 进行请求后的方法，回到主线程
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setupUIWithRequestError:NO];
            [hud hideAnimated:YES];
            [self setupPageVC];
        });
    });
}


#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    if (isError) {
        [self.view bringSubviewToFront:self.defaultView];
    }else{
        [self.view sendSubviewToBack:self.defaultView];
    }
}

#pragma mark - <配置navigation>
- (void)configNavigation
{
    //定位
    UIButton *btnLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLocation setFrame:CGRectMake(0, 0, 30, 30)];
    [btnLocation setTitle:@"中山" forState:UIControlStateNormal];
    [btnLocation setTitleColor:kColorFromRGB(kBlack) forState:UIControlStateNormal];
    btnLocation.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    UIBarButtonItem *barBtnItemLoc = [[UIBarButtonItem alloc]initWithCustomView:btnLocation];
    
    [btnLocation addTarget:self action:@selector(btnLocationAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = @[barBtnItemLoc];
    
    // search
    self.navigationItem.titleView = self.navigationSearchBar;
}

#pragma mark - 导航栏左侧定位按钮响应方法
- (void)btnLocationAction
{
    
}


#pragma mark - <NavigationSearchBarDelegate>
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    [self jumpToSearchVCWithRecommdWords:textField.placeholder];
    return NO;
}
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField
{
    return NO;
}

#pragma mark - <跳转搜索页>
-(void)jumpToSearchVCWithRecommdWords:(NSString *)recommdWord
{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.recommdWords = recommdWord;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (NavigationSearchView *)navigationSearchBar
{
    if (!_navigationSearchBar) {
        NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
        navigationSearchBar.delegate = self;
        navigationSearchBar.isHideBtnSearch = YES;
        _navigationSearchBar = navigationSearchBar;
    }
    return _navigationSearchBar;
}


- (void)setupPageVC{
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleSuspensionCenter;
    configration.headerViewCouldScale = NO;
    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    configration.scrollMenu = YES;
    configration.aligmentModeCenter = NO;
    configration.lineWidthEqualFontWidth = NO;
    configration.showBottomLine = NO;
    configration.menuHeight = 44;
    configration.lineLeftAndRightMargin = 0;
    configration.bottomLineLeftAndRightMargin = 10;
    configration.lineColor = kColorFromRGB(kCateTitleColor);
    configration.selectedItemColor = kColorFromRGB(kCateTitleColor);
    configration.normalItemColor = kColorFromRGB(kTextColor);
    configration.selectedItemFont = [UIFont systemFontOfSize:21];
    /// 设置悬浮停顿偏移量
    //    configration.suspenOffsetY = KNavBarHeight;
    configration.suspenOffsetY = 0;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles config:configration];
    self.vc = vc;
    vc.dataSource = self;
    vc.delegate = self;
    vc.headerView = [self headView];
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
}
#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    self.currentIndex = index;
    CommonViewController *vc = pageViewController.controllersM[index];
    self.currentVC = vc;
    if (index == 0) {
        vc.dataArray = self.recommedArray;
        [[vc infoCollectionView] reloadData];
    }else{
        FamilyEduCategory *model = [[FamilyEduCategory alloc] init];
        model = self.classifyArray[index - 1];
        [self loadTeacherDataWithIndexID:[NSString stringWithFormat:@"%d", (int)model.categoryID] withVC:vc reload:YES];
    }
    
    return [vc infoCollectionView];
    
}

- (void)pageViewController:(YNPageViewController *)pageViewController didScroll:(UIScrollView *)scrollView progress:(CGFloat)progress formIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex
{
    self.currentIndex = toIndex;
    self.currentVC = pageViewController.controllersM[toIndex];
}

-(void)loadTeacherDataWithIndexID:(NSString *)indexID withVC:(CommonViewController *)vc reload:(BOOL)isReload
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // two_cat_id
    [HttpTool getWithURL:[NSString stringWithFormat:@"%@?two_cat_id=%@", kURL(kEduList), indexID] params:nil haveHeader:NO success:^(id json) {
        [[vc infoCollectionView].mj_header endRefreshing];
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isEmpty) {
                    [self.recommedArray removeAllObjects];
                    NSArray *results = dict[@"data"][@"data"];
                    if ([results isNotEmpty]) {
                        for (NSDictionary *dic in results) {
                            [self.recommedArray addObject:[EduListModel modelObjectWithDictionary:dic]];
                        }
                    }
                    vc.dataArray = self.recommedArray;
                    [[vc infoCollectionView] reloadData];
                }
            }else{
                [self setupUIWithRequestError:YES];
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self setupUIWithRequestError:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [[vc infoCollectionView].mj_header endRefreshing];
        [hud hideAnimated:YES];
        [self setupUIWithRequestError:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

- (NSArray *)getArrayVCs {
    NSMutableArray *vcArray = [NSMutableArray array];
    for (int i = 0; i < self.titles.count; i++) {
        CommonViewController *vc = [[CommonViewController alloc] init];
        [vcArray addObject:vc];
    }
    
    return vcArray;
}

- (UIView *)headView{
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth / 2.0 + 60 + kScreenWidth/2.5 * 139 / 127 + 60)];
        _headView.backgroundColor = [UIColor whiteColor];
    }
    return _headView;
}
- (FamilyEduBannerView *)bannerView{
    if (!_bannerView) {
        _bannerView = [[FamilyEduBannerView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth / 2.0)];
    }
    return _bannerView;
}
- (TeacherListHeaderView *)sectionView{
    if (!_sectionView) {
        _sectionView = [TeacherListHeaderView loadFromNib];
        _sectionView.frame = CGRectMake(0, kScreenWidth / 2.0, kScreenWidth, 60);
    }
    return _sectionView;
}

- (TeacherListView *)teacherListView
{
    if (_teacherListView == nil) {
        _teacherListView = [[TeacherListView alloc] initWithFrame:CGRectMake(0, kScreenWidth / 2 + 60, kScreenWidth, kScreenWidth/2.5 * 139 / 127 + 60)];
    }
    return _teacherListView;
}


#pragma mark - <SDCycleScrollViewDelegate------->
-(void)didSelectBannerWithIndex:(NSInteger)index
{
    if (index < self.adListArray.count) {
        AdList *adListModel = self.adListArray[index];
        NSString *urlStr = adListModel.href;
        if ([urlStr isEmpty]) {
            return;
        }
        CommonWebViewController *webVC = [[CommonWebViewController alloc] init];
        webVC.urlStr = urlStr;
        [self.navigationController pushViewController:webVC animated:YES];
    }
}

- (void)didSelectCustomActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section
{
    NSLog(@"section: %ld  item: %ld", section, indexPath.item);
}

- (void)dealloc
{
    [QK_NOTICENTER removeObserver:self name:@"refreshCommonViewData" object:nil];
}

@end
