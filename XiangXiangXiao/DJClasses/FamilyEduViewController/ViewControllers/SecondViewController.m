//
//  SecondViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SecondViewController.h"
@interface SecondViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray *array;
@property (nonatomic,assign) NSInteger num;
@end

@implementation SecondViewController


- (UITableView *)infoTableView {
    if (!_infoTableView) {
        _infoTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _infoTableView.showsVerticalScrollIndicator = NO;
        _infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _infoTableView.delegate = self;
        _infoTableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            _infoTableView.estimatedRowHeight = 0;
            _infoTableView.estimatedSectionHeaderHeight = 0;
            _infoTableView.estimatedSectionFooterHeight = 0;
        }
    }
    return _infoTableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.infoTableView];
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        
        [self.infoTableView reloadData];
        [self.infoTableView.mj_header endRefreshing];
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.text = self.array[indexPath.row];
    return cell;
}
- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray arrayWithCapacity:10];
        for (int i = 0; i < 50; i++) {
            [_array addObject:@"测试"];
        }
    }
    return _array;
}

@end
