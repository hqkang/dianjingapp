//
//  CommonViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/27.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CommonViewController.h"
#import "NewCustomCategoryItemCell.h"
#import "EduListModel.h"

@interface CommonViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, assign)BOOL ableLoadMore;

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self.infoCollectionView reloadData];
}


- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        _flowLayout = flowLayout;
    }
    return _flowLayout;
}
-(UICollectionView *)infoCollectionView
{
    if (!_infoCollectionView) {
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:self.flowLayout];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.showsVerticalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self.view addSubview:collectionView];
        
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([NewCustomCategoryItemCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([NewCustomCategoryItemCell class])];
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [QK_NOTICENTER postNotificationName:@"refreshCommonViewData" object:nil userInfo:nil];
            });
        }];
        collectionView.mj_header = header;
        //上拉加载更多
        MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            if (weakself.ableLoadMore) {
                
            }else{
                [collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }];
        collectionView.mj_footer = footer;
        [footer setTitle:@"" forState:MJRefreshStateIdle];
        [footer setTitle:@"" forState:MJRefreshStateNoMoreData];
        collectionView.mj_footer = footer;
        _infoCollectionView = collectionView;
    }
    return _infoCollectionView;
}




#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource*********************************>
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    number = self.dataArray.count;
    
    return number;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewCustomCategoryItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([NewCustomCategoryItemCell class]) forIndexPath:indexPath];
    EduListModel *model = [[EduListModel alloc] init];
    model = self.dataArray[indexPath.item];
    cell.model = model;
    return cell;
}


#pragma mark - <UICollectionViewFlowLayout*********************************>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeZero;
    
    itemSize = CGSizeMake((kScreenWidth - 20)/2.0, 200);
    
    return itemSize;
}


-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    
    
    return insets;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"点击的section: %ld  item: %ld", indexPath.section, indexPath.item);
}

@end
