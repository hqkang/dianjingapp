//
//  NewLiveRoomViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/10.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "NewLiveRoomViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "LiveRoomCollectionViewCell.h"

#import "NTESGLView.h"
#import "CustomAlert.h"
#import "NTESLiveUtil.h"
#import "LiveRoomTopView.h"
#import "UIAlertController+TapDismiss.h"

#define liveRoomItemW (kScreenWidth / 2)
#define liveRoomItemH (liveRoomItemW * 200 / 164)
@interface NewLiveRoomViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NIMLoginManagerDelegate, NIMNetCallManagerDelegate, NIMSystemNotificationManagerDelegate, LiveRoomTopViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataArray;

///视频配置
@property (nonatomic,copy) NIMNetCallVideoCaptureParam *param;
///视频会议
@property (nonatomic, strong) NIMNetCallMeeting *meeting;

///视频窗口
@property (nonatomic, strong) NTESGLView *remoteGLView;

@property (nonatomic, strong) UIView *localPreView;

@property (nonatomic, strong) UIView *localVideoView;

@property (nonatomic, strong) NSString *currentRoomID;
@end

@implementation NewLiveRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentRoomID = @"ios";
    self.view.backgroundColor = kColorFromRGB(kLiveBGColor);
    [self setupTopView];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"连麦" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.frame = CGRectMake((kScreenWidth - 50) / 2, kScreenHeight - 50, 50, 30);
    [self.view addSubview:btn];
    
    [self setupUI];
    self.meeting = [[NIMNetCallMeeting alloc] init];
    self.meeting.name = @"ios33";
    self.meeting.type = NIMNetCallMediaTypeVideo;
    self.meeting.actor = YES;
    self.meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
    
    NSString *phoneStr = kUserDefaultObject(kUserPhone);
    if ([phoneStr isEqualToString:@"15687629671"]) {
        [[NIMAVChatSDK sharedSDK].netCallManager reserveMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nullable error) {
            if (error) {
                NSLog(@"创建房间失败");
            }else{
                NSLog(@"创建房间成功");
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 2018111418034419
                    [[NIMAVChatSDK sharedSDK].netCallManager joinMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nonnull error) {
                        //加入会议失败
                        if (error) {
                            NSLog(@"加入会议失败");
                        }
                        //加入会议成功
                        else
                        {
                            [[RCIMClient sharedRCIMClient] joinChatRoom:self.currentRoomID messageCount:0 success:^{
                                NSLog(@"加入房间成功");
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    self.param = [[NIMNetCallVideoCaptureParam alloc] init];
                                    //清晰度480P
                                    self.param.preferredVideoQuality = NIMNetCallVideoQuality480pLevel;
                                    //裁剪类型 16:9
                                    self.param.videoCrop  = NIMNetCallVideoCrop16x9;
                                    //打开初始为前置摄像头
                                    self.param.startWithBackCamera = NO;
                                    //开始采集
                                    [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
                                    //视频采集数据回调
                                    self.param.videoHandler =^(CMSampleBufferRef sampleBuffer){
                                        //对采集数据进行外部前处理
                                        //把 sampleBuffer 数据发送给 SDK 进行显示，编码，发送
                                        [[NIMAVChatSDK sharedSDK].netCallManager sendVideoSampleBuffer:sampleBuffer];
                                    };
                                    //若需要开启前处理指定 videoProcessorParam
                                    NIMNetCallVideoProcessorParam *videoProcessorParam = [[NIMNetCallVideoProcessorParam alloc] init];
                                    //若需要通话开始时就带有前处理效果（如美颜自然模式）
                                    videoProcessorParam.filterType = NIMNetCallFilterTypeZiran;
                                    self.param.videoProcessorParam = videoProcessorParam;
                                    meeting.option.videoCaptureParam = self.param;
                                    meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
                                });
                                
                            } error:^(RCErrorCode status) {
                                NSLog(@"加入房间失败");
                            }];
                            self.localPreView.hidden = NO;
                            self.localVideoView.hidden = NO;
                            
                            NIMChatroom *chatRoom = [[NIMChatroom alloc] init];
                            NSString *liveUrl = @"rtmp://p42997226.live.126.net/live/03d870bc00044f88b444b37f5467625a?wsSecret=2ab02198a26190d0ad6677593094a216&wsTime=1561603387";
                            
                            if (liveUrl.length) {
                                chatRoom.broadcastUrl = liveUrl;
                            }
                        }
                    }];
                });
            }
        }];
    }else{
        [[NIMAVChatSDK sharedSDK].netCallManager joinMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nonnull error) {
            
            //加入会议失败
            if (error) {
                NSLog(@"加入会议失败");
            }
            //加入会议成功
            else
            {
                [[RCIMClient sharedRCIMClient] joinChatRoom:self.currentRoomID messageCount:0 success:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        self.param = [[NIMNetCallVideoCaptureParam alloc] init];
                        //清晰度480P
                        self.param.preferredVideoQuality = NIMNetCallVideoQuality480pLevel;
                        //裁剪类型 16:9
                        self.param.videoCrop  = NIMNetCallVideoCrop16x9;
                        //打开初始为前置摄像头
                        self.param.startWithBackCamera = NO;
                        //开始采集
                        [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
                        //视频采集数据回调
                        self.param.videoHandler =^(CMSampleBufferRef sampleBuffer){
                            //对采集数据进行外部前处理
                            //把 sampleBuffer 数据发送给 SDK 进行显示，编码，发送
                            [[NIMAVChatSDK sharedSDK].netCallManager sendVideoSampleBuffer:sampleBuffer];
                        };
                        //若需要开启前处理指定 videoProcessorParam
                        NIMNetCallVideoProcessorParam *videoProcessorParam = [[NIMNetCallVideoProcessorParam alloc] init];
                        //若需要通话开始时就带有前处理效果（如美颜自然模式）
                        videoProcessorParam.filterType = NIMNetCallFilterTypeZiran;
                        self.param.videoProcessorParam = videoProcessorParam;
                        meeting.option.videoCaptureParam = self.param;
                        meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
                        [self.dataArray addObject:@"2"];
                        [self.collectionView reloadData];
                    });
                    NSLog(@"加入房间成功");
                } error:^(RCErrorCode status) {
                    NSLog(@"加入房间失败");
                }];
                NIMChatroom *chatRoom = [[NIMChatroom alloc] init];
                NSString *liveUrl = @"rtmp://p42997226.live.126.net/live/03d870bc00044f88b444b37f5467625a?wsSecret=2ab02198a26190d0ad6677593094a216&wsTime=1561603387";
                
                if (liveUrl.length) {
                    chatRoom.broadcastUrl = liveUrl;
                }
            }
        }];
    }
    
    [[NIMSDK sharedSDK].loginManager addDelegate:self];
    
    [[NIMAVChatSDK sharedSDK].netCallManager addDelegate:self];
    
    [[NIMSDK sharedSDK].systemNotificationManager addDelegate:self];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)setupTopView
{
    LiveRoomTopView *topView = [LiveRoomTopView loadFromNib];
    topView.frame = CGRectMake(0, kStatusBarHeight, kScreenWidth, 40);
    topView.delegate = self;
    [self.view addSubview:topView];
}

#pragma mark - topView Delegate
- (void)liveRoomTopViewButtonDidClick:(UIButton *)button
{
    if (button.tag == 0) {
        
    }else if(button.tag == 1){
        [self closeAction];
    }
}

- (void)closeAction
{
    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"是否退出房间" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"是" actionCancelName:@"否" actionConfirm:^{
        NSDictionary *notificationDic = [NSDictionary dictionary];
        NSDictionary *dic = [NSDictionary dictionary];
        dic = kUserDefaultObject(kUserInfo);
        notificationDic = @{@"id" : kUserDefaultObject(kUserPhone), @"room" : self.currentRoomID, @"uname" : dic[@"nickname"], @"avatar" : dic[@"avatar"], @"code" : @"leave", @"type" : @"1"};
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notificationDic options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NIMCustomSystemNotification *notification = [[NIMCustomSystemNotification alloc] initWithContent:jsonString];
        NIMSession *session = [NIMSession session:@"15687629671" type:NIMSessionTypeP2P];
        [[NIMSDK sharedSDK].systemNotificationManager sendCustomNotification:notification toSession:session completion:^(NSError * _Nullable error) {
            if (error) {
                NSLog(@"失败");
            }else{
                NSLog(@"成功");
            }
        }];
        [[NIMAVChatSDK sharedSDK].netCallManager stopVideoCapture];
        [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
        [[RCIMClient sharedRCIMClient] quitChatRoom:self.currentRoomID success:nil error:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } actionCancel:nil];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (void)btnAction
{
    NSDictionary *notificationDic = [NSDictionary dictionary];
    NSDictionary *dic = [NSDictionary dictionary];
    dic = kUserDefaultObject(kUserInfo);
    notificationDic = @{@"id" : kUserDefaultObject(kUserPhone), @"room" : self.currentRoomID, @"uname" : dic[@"nickname"], @"avatar" : dic[@"avatar"], @"code" : @"apply", @"type" : @"1"};
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notificationDic options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NIMCustomSystemNotification *notification = [[NIMCustomSystemNotification alloc] initWithContent:jsonString];
    NIMSession *session = [NIMSession session:@"15687629671" type:NIMSessionTypeP2P];
    [[NIMSDK sharedSDK].systemNotificationManager sendCustomNotification:notification toSession:session completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"失败");
        }else{
            NSLog(@"成功");
            
        }
    }];
}


- (void)onReceive:(UInt64)callID from:(NSString *)caller type:(NIMNetCallMediaType)type message:(NSString *)extendMessage
{
    NSLog(@"收到音频连线");
}

- (void)onReceiveCustomSystemNotification:(NIMCustomSystemNotification *)notification
{
    NSDictionary *dic = [NSDictionary dictionary];
    dic = [self dictionaryWithJsonString:notification.content];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (dic[@"code"]) {
            if ([dic[@"code"] isEqualToString:@"close_video"]) {
                [[NIMAVChatSDK sharedSDK].netCallManager stopVideoCapture];
            }else if ([dic[@"code"] isEqualToString:@"open_video"]){
                
            }else if ([dic[@"code"] isEqualToString:@"open_audio"]){
                [[NIMAVChatSDK sharedSDK].netCallManager setMute:NO];
            }else if ([dic[@"code"] isEqualToString:@"close_audio"]){
                [[NIMAVChatSDK sharedSDK].netCallManager setMute:YES];
            }else if ([dic[@"code"] isEqualToString:@"kill"]){
                //离开当前多人会议
                [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您已被踢出房间" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }else if ([dic[@"code"] isEqualToString:@"test"]){
                
            }else if ([dic[@"code"] isEqualToString:@"closeRoom"]){
                [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
                
            }else if ([dic[@"code"] isEqualToString:@"apply"]){
                UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"是否接收连麦申请" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"是" actionCancelName:@"否" actionConfirm:^{
                    NSLog(@"sdfsdf");
                    
                } actionCancel:nil];
                [self presentViewController:alertVC animated:YES completion:nil];
            }else if ([dic[@"code"] isEqualToString:@"leave"]){
                
            }
        }
    });
    
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}


- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray arrayWithObjects:@"1", nil];
    }
    return _dataArray;
}


- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)dealloc
{
    //离开当前多人会议
    [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
    [[NIMAVChatSDK sharedSDK].netCallManager removeDelegate:self];
    [[NIMSDK sharedSDK].loginManager removeDelegate:self];
    [[NIMSDK sharedSDK].systemNotificationManager removeDelegate:self];
}

- (UIView *)localPreView
{
    if (_localPreView == nil) {
        _localPreView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth / 2, liveRoomItemH)];
        _localPreView.userInteractionEnabled = NO;
    }
    return _localPreView;
}

- (UIView *)localVideoView
{
    if (_localVideoView == nil) {
        _localVideoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth / 2, liveRoomItemH)];
        _localVideoView.userInteractionEnabled = NO;
    }
    return _localVideoView;
}


#pragma mark - 设置UI
- (void)setupUI
{
    self.collectionView.backgroundColor = kColorFromRGB(kBGColor);
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//    self.localPreView = [[UIView alloc] initWithFrame:CGRectMake(0, (kScreenHeight - 2 * liveRoomItemH) / 2, kScreenWidth, 2 * liveRoomItemH)];
//    [self.view addSubview:self.localPreView];
//
//    self.localVideoView = [[UIView alloc] initWithFrame:CGRectMake(0, (kScreenHeight - 2 * liveRoomItemH) / 2, kScreenWidth, 2 * liveRoomItemH)];
//    [self.view addSubview:self.localVideoView];
    
    //    self.firstLocalPreView = [[UIView alloc] initWithFrame:CGRectMake(liveRoomItemW, (kScreenHeight - 2 * liveRoomItemH) / 2, liveRoomItemW, liveRoomItemH)];
    //    [self.view addSubview:self.localPreView];
    //
    //    self.firstLocalVideoView = [[UIView alloc] initWithFrame:CGRectMake(liveRoomItemW, (kScreenHeight - 2 * liveRoomItemH) / 2, liveRoomItemW, liveRoomItemH)];
    //
    //    [self.view addSubview:self.firstLocalVideoView];
    
    //    self.secondLocalPreView = [[UIView alloc] initWithFrame:CGRectMake(0, (kScreenHeight - 2 * liveRoomItemH) / 2 + liveRoomItemH, liveRoomItemW, liveRoomItemH)];
    //    [self.view addSubview:self.secondLocalPreView];
    //
    //    self.secondLocalVideoView = [[UIView alloc] initWithFrame:CGRectMake(0, (kScreenHeight - 2 * liveRoomItemH) / 2 + liveRoomItemH, liveRoomItemW, liveRoomItemH)];
    //
    //    [self.view addSubview:self.secondLocalVideoView];
    
    //    self.thirdLocalPreView = [[UIView alloc] initWithFrame:CGRectMake(liveRoomItemW, (kScreenHeight - 2 * liveRoomItemH) / 2, liveRoomItemW, liveRoomItemH)];
    //    [self.view addSubview:self.thirdLocalPreView];
    //
    //    self.thirdLocalVideoView = [[UIView alloc] initWithFrame:CGRectMake(liveRoomItemW, (kScreenHeight - 2 * liveRoomItemH) / 2 + liveRoomItemH, liveRoomItemW, liveRoomItemH)];
    //    [self.view addSubview:self.thirdLocalVideoView];
    
    [self remoteGLView];
    //    [self firstGuestRemoteGLView];
    //    [self secondGuestRemoteGLView];
    //    [self thirdGuestRemoteGLView];
}



#pragma mark - UICollectionView懒加载
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, (kScreenHeight - liveRoomItemH * 2) / 2, kScreenWidth, 2 * liveRoomItemH) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        // 注册cell和headerView
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([LiveRoomCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LiveRoomCollectionViewCell class])];
    }
    return _collectionView;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
        reusableview = headerView;
    }else
    {
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
        reusableview = footerView;
    }
    return reusableview;
}

#pragma mark - head宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(liveRoomItemW, liveRoomItemH);
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    LiveRoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LiveRoomCollectionViewCell class]) forIndexPath:indexPath];
    [self setupVideoView:cell indexPath:indexPath];
    
//    self.remoteGLView = [[NTESGLView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
//    [self.remoteGLView setContentMode:UIViewContentModeScaleAspectFill];
//    [self.remoteGLView setBackgroundColor:[UIColor clearColor]];
//    self.remoteGLView = cell.remoteGLView;
    gridcell = cell;
    
    return gridcell;
}

- (void)setupVideoView:(LiveRoomCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        NSString *phoneStr = kUserDefaultObject(kUserPhone);
        if ([phoneStr isEqualToString:@"15687629671"]) {
            [cell.contentView addSubview:self.localPreView];
            [cell.contentView addSubview:self.localVideoView];
        }else{
            [cell.contentView addSubview:self.remoteGLView];
        }
    }else{
        [cell.contentView addSubview:self.localPreView];
        [cell.contentView addSubview:self.localVideoView];
    }
    [cell.contentView addSubview:[UIView new]];
}

#pragma mark - <UICollectionViewDelegateFlowLayout>
#pragma mark - X间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark - Y间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


#pragma mark - 选中item
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet actionName:@[@"打开语音", @"开启视频", @"全屏显示", @"强制退出此人", @"取消"] actionConfirm:^(int index) {
        NSLog(@"当前选择的下标为:%d", index);
        switch (index) {
            case 0:
                {

                }
                break;
            case 1:
                {

                }
                break;
            case 2:
                {

                }
                break;
            case 3:
                {

                }
                break;
            case 4:
                {
                    
                }
                break;
            default:
                break;
        }
    }];
    [self presentViewController:alertVC animated:YES completion:nil];
}

//远程YUV数据就绪回调
- (void)onRemoteYUVReady:(NSData *)yuvData
                   width:(NSUInteger)width
                  height:(NSUInteger)height
                    from:(NSString *)user
{
    // _remoteGLView 是 NTESGLView 类型  DEMO 提供 NTESGLView 类来渲染yuv数据
    [_remoteGLView render:yuvData width:width height:height];
}

- (NTESGLView *)remoteGLView{
    if (!_remoteGLView) {
        _remoteGLView = [[NTESGLView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth / 2, liveRoomItemH)];
        [_remoteGLView setContentMode:UIViewContentModeScaleAspectFill];
        [_remoteGLView setBackgroundColor:[UIColor clearColor]];
        _remoteGLView.userInteractionEnabled = NO;
//        [self.view addSubview:_remoteGLView];
    }
    return _remoteGLView;
}

#pragma mark - 本地预览
- (void)onLocalDisplayviewReady:(UIView *)displayView
{
    if (self.localPreView) {
        [self.localPreView removeFromSuperview];
    }
    self.localPreView = displayView;
    displayView.frame = self.localVideoView.bounds;
    [self.localVideoView addSubview:displayView];
}


@end
