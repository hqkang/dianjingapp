//
//  HomeViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeViewController.h"
#import "ClassifyViewController.h"
#import "SearchViewController.h"
#import "CommonWebViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "InterviewOnlineViewController.h"
#import "LiveRoomViewController.h"
#import "NewLiveRoomViewController.h"

// views
#import "MoreReusableHeaderView.h"
#import "ADFooterReusableView.h"
#import "CustomDefaultView.h"
#import "NavigationSearchView.h"


// cells
#import "BannerCollectionViewCell.h"
#import "HomeCategoryCell.h"
#import "AdCollectionViewCell.h"
#import "HomeAdCell.h"
#import "HomeRecommendClassCell.h"


// models
#import "HomeIndexModel.h"
#import "HomeBannerModel.h"
#import "HomeCategoryModel.h"
#import "HomeRecommendModel.h"
#import "HomeAdModel.h"


@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, CustomDefaultViewDelegate, NavigationSearchBarDelegate, BannerCollectionViewCellDelegate, SDCycleScrollViewDelegate>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, strong) CustomDefaultView *defaultView;
@property (nonatomic, strong) NavigationSearchView *navigationSearchBar;

@property (nonatomic, assign)BOOL ableLoadMore;

@property (nonatomic, strong) NSMutableArray *homeModelArray;
@property (nonatomic, strong) NSMutableArray *bannerArray;//banner
@property (nonatomic, strong) NSMutableArray *classifyArray;//分类
@property (nonatomic, strong) NSMutableArray *adArray;//广告
@property (nonatomic, strong) NSMutableArray *adImageArray;//广告图片url
@property (nonatomic, strong) NSMutableArray *recommedArray;//精选择推荐

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self configNavigation];
    [self loadIndexDataWithReload:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - <配置navigation>
- (void)configNavigation
{
    //分类
    UIButton *btnClass = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClass setImage:[UIImage imageNamed:@"icon_class"] forState:UIControlStateNormal];
    [btnClass setFrame:CGRectMake(0, 0, 30, 30)];
    UIBarButtonItem *barBtnItemClass = [[UIBarButtonItem alloc]initWithCustomView:btnClass];
    
    [btnClass addTarget:self action:@selector(btnClassAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[barBtnItemClass];
    
    // search
    self.navigationItem.titleView = self.navigationSearchBar;
}

#pragma mark - 导航栏右侧分类按钮响应方法
- (void)btnClassAction
{
//    ClassifyViewController *classifyVC = [[ClassifyViewController alloc] init];
//    [self.navigationController pushViewController:classifyVC animated:YES];
    NewLiveRoomViewController *vc = [[NewLiveRoomViewController alloc] init];
//    vc.modelID = @"201906271043072890";
//    [self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

- (NavigationSearchView *)navigationSearchBar
{
    if (!_navigationSearchBar) {
        NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
        navigationSearchBar.delegate = self;
        navigationSearchBar.isHideBtnSearch = YES;
        _navigationSearchBar = navigationSearchBar;
    }
    return _navigationSearchBar;
}


- (NSMutableArray *)homeModelArray
{
    if (_homeModelArray == nil) {
        _homeModelArray = [NSMutableArray array];
    }
    return _homeModelArray;
}

- (NSMutableArray *)bannerArray
{
    if (_bannerArray == nil) {
        _bannerArray = [NSMutableArray array];
    }
    return _bannerArray;
}

- (NSMutableArray *)classifyArray
{
    if (_classifyArray == nil) {
        _classifyArray = [NSMutableArray array];
    }
    return _classifyArray;
}

- (NSMutableArray *)adArray
{
    
    if (_adArray == nil) {
        _adArray = [NSMutableArray array];
    }
    return _adArray;
}

- (NSMutableArray *)adImageArray
{
    if (_adImageArray == nil) {
        _adImageArray = [NSMutableArray array];
    }
    return _adImageArray;
}

- (NSMutableArray *)recommedArray
{
    if (_recommedArray == nil) {
        _recommedArray = [NSMutableArray array];
    }
    return _recommedArray;
}

-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        _flowLayout = flowLayout;
    }
    return _flowLayout;
}
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:self.flowLayout];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.showsVerticalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self.view addSubview:collectionView];
        
        [collectionView registerClass:[BannerCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([BannerCollectionViewCell class])];
        [collectionView registerClass:[AdCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([AdCollectionViewCell class])];
        [collectionView registerClass:[AdCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([AdCollectionViewCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HomeCategoryCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([HomeCategoryCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HomeAdCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([HomeAdCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HomeRecommendClassCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([HomeRecommendClassCell class])];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MoreReusableHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([MoreReusableHeaderView class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ADFooterReusableView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([ADFooterReusableView class])];
        [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself loadIndexDataWithReload:YES];
        }];
        collectionView.mj_header = header;
        //上拉加载更多
        MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            if (weakself.ableLoadMore) {
                
            }else{
                [collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }];
        collectionView.mj_footer = footer;
        [footer setTitle:@"" forState:MJRefreshStateIdle];
        [footer setTitle:@"" forState:MJRefreshStateNoMoreData];
        collectionView.mj_footer = footer;
        _collectionView = collectionView;
    }
    return _collectionView;
}

#pragma mark - 首页数据
-(void)loadIndexDataWithReload:(BOOL)isReload
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool getWithURL:kURL(kHomeData) params:nil haveHeader:NO success:^(id json) {
        [self.collectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isReload) {
                    [self.homeModelArray removeAllObjects];
                    [self.bannerArray removeAllObjects];
                    [self.classifyArray removeAllObjects];
                    [self.adArray removeAllObjects];
                    [self.adImageArray removeAllObjects];
                    [self.recommedArray removeAllObjects];
                    //清除图片缓存
                    //                    [[SDImageCache sharedImageCache] clearDisk];
                }
                
                NSArray *results = [dict arrayForKey:@"data"];
                if ([results isNotEmpty]) {
                    for (int i = 0; i < results.count; i++) {
                        [self.homeModelArray addObject:[HomeIndexModel modelObjectWithDictionary:results[i]]];
                    }
                }
                if ([self.homeModelArray isNotEmpty]) {
                    for (int i = 0; i < self.homeModelArray.count; i++) {
                        HomeIndexModel *model = [[HomeIndexModel alloc] init];
                        model = self.homeModelArray[i];
                        NSArray *tempArray = [NSArray array];
                        tempArray = model.data;
                        // 轮播图
                        if ([model.type isEqualToString:@"banner"]) {
                            if ([tempArray isNotEmpty]) {
                                for (NSDictionary *dic in tempArray) {
                                    [self.bannerArray addObject:[HomeBannerModel modelObjectWithDictionary:dic]];
                                }
                            }
                        }
                        // 分类
                        if ([model.type isEqualToString:@"icon"]) {
                            if ([tempArray isNotEmpty]) {
                                for (NSDictionary *dic in tempArray) {
                                    [self.classifyArray addObject:[HomeCategoryModel modelObjectWithDictionary:dic]];
                                }
                            }
                        }
                        // 广告
                        if ([model.type isEqualToString:@"adv"]) {
                            if ([tempArray isNotEmpty]) {
                                for (NSDictionary *dic in tempArray) {
                                    [self.adArray addObject:[HomeAdModel modelObjectWithDictionary:dic]];
                                    [self.adImageArray addObject:dic[@"image"]];
                                }
                            }
                        }
                        // 推荐
                        if ([model.type isEqualToString:@"eduCourseRecommend"]) {
                            
                            if ([tempArray isNotEmpty]) {
                                self.ableLoadMore = YES;
                                for (NSDictionary *dic in tempArray) {
                                    [self.recommedArray addObject:[HomeRecommendModel modelObjectWithDictionary:dic]];
                                }
                                NSLog(@"123");
                            }else{
                                self.ableLoadMore = NO;
                            }
                        }
                    }
                }
                [self setupUIWithRequestError:NO];
                [self.collectionView reloadData];
            }else{
                [self setupUIWithRequestError:YES];
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self setupUIWithRequestError:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        [self setupUIWithRequestError:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"出错啦～请稍后重试";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}


#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource*********************************>
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.homeModelArray.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    HomeIndexModel *model = [[HomeIndexModel alloc] init];
    model = self.homeModelArray[section];
    if ([model.type isEqualToString:@"banner"]) {
        if (self.bannerArray.count>0) {
            number = 1;
        }
    }else if ([model.type isEqualToString:@"icon"]){
        if (self.classifyArray.count>0) {
            number = self.classifyArray.count;
        }
    }else if ([model.type isEqualToString:@"adv"]){
        if (self.adArray.count>0) {
            number = 1;
        }
    }else if ([model.type isEqualToString:@"eduCourseRecommend"]){
        if (self.recommedArray.count>0) {
            number = self.recommedArray.count;
        }
    }
    
    return number;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    //  轮播图
    BannerCollectionViewCell *cellBanner = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([BannerCollectionViewCell class]) forIndexPath:indexPath];
    // 分类
    HomeCategoryCell *categoryCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([HomeCategoryCell class]) forIndexPath:indexPath];
    // 广播
    AdCollectionViewCell *adCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AdCollectionViewCell class]) forIndexPath:indexPath];
    adCell.backgroundColor = kColorFromRGB(kWhite);
    // 精选推荐
    HomeRecommendClassCell *recommendCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([HomeRecommendClassCell class]) forIndexPath:indexPath];
    HomeIndexModel *model = [[HomeIndexModel alloc] init];
    model = self.homeModelArray[indexPath.section];
    if ([model.type isEqualToString:@"banner"]) {
        cellBanner.timeInterval = 4;
        cellBanner.delegate = self;
        cellBanner.adListArray = self.bannerArray;
        gridcell = cellBanner;
    }else if ([model.type isEqualToString:@"icon"]){
        HomeCategoryModel *categoryModel = [[HomeCategoryModel alloc] init];
        categoryModel = self.classifyArray[indexPath.item];
        categoryCell.model = categoryModel;
        gridcell = categoryCell;
    }else if ([model.type isEqualToString:@"adv"]){
        adCell.adScrollView.imageURLStringsGroup = self.adImageArray;
        adCell.adScrollView.autoScrollTimeInterval = 5;
        if (self.adImageArray.count > 1) {
            adCell.adScrollView.autoScroll = YES;
        }else{
            adCell.adScrollView.autoScroll = NO;
        }
        adCell.adScrollView.delegate = self;
        gridcell = adCell;
    }else if ([model.type isEqualToString:@"eduCourseRecommend"]){
        HomeRecommendModel *recommendModel = [[HomeRecommendModel alloc] init];
        recommendModel = self.recommedArray[indexPath.item];
        recommendCell.model = recommendModel;
        gridcell = recommendCell;
    }
    
    return gridcell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    kWeakSelf(self);
    UICollectionReusableView *reusableView = [[UICollectionReusableView alloc]init];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        MoreReusableHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([MoreReusableHeaderView class]) forIndexPath:indexPath];
//        headerView.delegate = self;
        switch (indexPath.section) {
            case 1:{
                if (self.classifyArray.count > 0) {
                    headerView.headerViewTitleLabel.text = @"精选推荐";
                }
            }
                break;
                
            default:
                break;
        }
        reusableView = headerView;
        
    }else if (kind == UICollectionElementKindSectionFooter) {
        if (self.classifyArray && indexPath.section == 1) {//分类
            ADFooterReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([ADFooterReusableView class]) forIndexPath:indexPath];
//            footerView.imgName = @"";
            reusableView = footerView;
            
            footerView.didClickADImg  = ^{
                // 跳转
            };
        }
    }
    
    return reusableView;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        [self presentLoginVC];
        return;
    }
    NSLog(@"分区:%ld, 块:%ld", indexPath.section, indexPath.item);
    HomeCategoryModel *model = [[HomeCategoryModel alloc] init];
    model = self.classifyArray[indexPath.item];
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    web.urlStr = model.h5_url;
    [self.navigationController pushViewController:web animated:YES];
}

-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}

#pragma mark - <UICollectionViewFlowLayout*********************************>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeZero;
    HomeIndexModel *model = [[HomeIndexModel alloc] init];
    model = self.homeModelArray[indexPath.section];
    if ([model.type isEqualToString:@"banner"]) {
        itemSize = CGSizeMake(kScreenWidth, (kScreenWidth)/2.0);
    }else if ([model.type isEqualToString:@"icon"]){
        if (self.classifyArray.count>0) {
            itemSize = CGSizeMake((kScreenWidth-kEdgeLeftAndRight*8)/5.0, (kScreenWidth-kEdgeLeftAndRight*8)/5.0 + 20);
        }
    }else if ([model.type isEqualToString:@"adv"]){
        if (self.adArray.count>0) {
            itemSize = CGSizeMake(kScreenWidth, 120);
        }
    }else if ([model.type isEqualToString:@"eduCourseRecommend"]){
        if (self.recommedArray.count>0) {
            itemSize = CGSizeMake((kScreenWidth - 20)/2.0, 200);
        }
    }
    return itemSize;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize itemSize = CGSizeZero;
    HomeIndexModel *model = [[HomeIndexModel alloc] init];
    model = self.homeModelArray[section];
    if ([model.type isEqualToString:@"banner"]) {
        
    }else if ([model.type isEqualToString:@"icon"]){
        
    }else if ([model.type isEqualToString:@"adv"]){
        
    }else if ([model.type isEqualToString:@"eduCourseRecommend"]){
        itemSize = CGSizeMake(kScreenWidth, 60);
    }
   
    return itemSize;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    CGSize itemSize = CGSizeZero;
    
    return itemSize;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    HomeIndexModel *model = [[HomeIndexModel alloc] init];
    model = self.homeModelArray[section];
    if ([model.type isEqualToString:@"banner"]) {
        if (self.bannerArray.count > 0) {
            insets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }else if ([model.type isEqualToString:@"icon"]){
        insets = UIEdgeInsetsMake(8, 12, 16, 12);
    }else if ([model.type isEqualToString:@"adv"]){
        
    }else if ([model.type isEqualToString:@"eduCourseRecommend"]){
        insets = UIEdgeInsetsMake(0, 5, 0, 0);
    }
    
    return insets;
}

#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    if (isError) {
        [self.view bringSubviewToFront:self.defaultView];
        [self.view sendSubviewToBack:self.collectionView];
    }else{
        [self.view bringSubviewToFront:self.collectionView];
        [self.view sendSubviewToBack:self.defaultView];
    }
    
}

#pragma mark - <CustomDefaultViewDelegate------->
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView
{
    [self loadIndexDataWithReload:YES];
}

#pragma mark - <NavigationSearchBarDelegate>
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    [self jumpToSearchVCWithRecommdWords:textField.placeholder];
    return NO;
}
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField
{
    return NO;
}

#pragma mark - <跳转搜索页>
-(void)jumpToSearchVCWithRecommdWords:(NSString *)recommdWord
{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.recommdWords = recommdWord;
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark - <SDCycleScrollViewDelegate------->
-(void)didSelectBannerWithIndex:(NSInteger)index
{
    if (index < self.bannerArray.count) {
        HomeBannerModel *bannerModel = self.bannerArray[index];
        NSString *urlStr = bannerModel.href;
        NSURL *url = [NSURL URLWithString:urlStr];
//        [self jumpToADVCWithAdURL:url];
        CommonWebViewController *webVC = [[CommonWebViewController alloc] init];
        webVC.urlStr = urlStr;
        [self.navigationController pushViewController:webVC animated:YES];
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (index < self.adArray.count) {
        HomeAdModel *adModel = self.adArray[index];
        NSString *urlStr = adModel.href;
        NSURL *url = [NSURL URLWithString:urlStr];
        //        [self jumpToADVCWithAdURL:url];
        CommonWebViewController *webVC = [[CommonWebViewController alloc] init];
        webVC.urlStr = urlStr;
        [self.navigationController pushViewController:webVC animated:YES];
    }
}


@end
