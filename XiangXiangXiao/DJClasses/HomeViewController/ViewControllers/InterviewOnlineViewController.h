//
//  InterviewOnlineViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/8.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InterviewOnlineViewController : UIViewController

@property (nonatomic, strong) NSString *modelID;

@end

NS_ASSUME_NONNULL_END
