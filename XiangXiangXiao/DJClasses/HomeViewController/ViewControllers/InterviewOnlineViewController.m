//
//  InterviewOnlineViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/8.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "InterviewOnlineViewController.h"
//thirds
#import <NIMSDK/NIMSDK.h>
#import <NIMAVChat/NIMAVChat.h>
#import <RongIMKit/RongIMKit.h>

#import "NTESGLView.h"


@interface InterviewOnlineViewController ()<NIMNetCallManagerDelegate, NIMChatroomManagerDelegate, NIMChatManagerDelegate, NIMLoginManagerDelegate, NIMSystemNotificationManagerDelegate>

///视频配置
@property (nonatomic,copy) NIMNetCallVideoCaptureParam *param;
///视频会议
@property (nonatomic, strong) NIMNetCallMeeting *meeting;

///视频窗口
@property (nonatomic, strong) NTESGLView *remoteGLView;

@property (nonatomic, strong) UIView *localPreView;

@property (nonatomic, strong) UIView *localVideoView;

@property (nonatomic, strong) NSDictionary *requestDic;

@property (nonatomic, strong) NSString *currentRoomID;

@property (nonatomic, strong) NSString *testID;

@property (nonatomic, assign) BOOL isCloseVideo;

@end

@implementation InterviewOnlineViewController{
    
}


#pragma mark - initial
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"面试间";
    self.isCloseVideo = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
//    [self getRoomsData];
    self.meeting = [[NIMNetCallMeeting alloc] init];
    self.meeting.name = @"ios33";
    self.meeting.type = NIMNetCallMediaTypeVideo;
    self.meeting.actor = YES;
    self.meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
    NSString *phoneStr = kUserDefaultObject(kUserPhone);
    if ([phoneStr isEqualToString:@"15687629671"]) {
        [[NIMAVChatSDK sharedSDK].netCallManager reserveMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nullable error) {
            if (error) {
                NSLog(@"sdfsdf");
            }else{
                NSLog(@"dsfsd");
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 2018111418034419
                    [[NIMAVChatSDK sharedSDK].netCallManager joinMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nonnull error) {
                        
                        self.param = [[NIMNetCallVideoCaptureParam alloc] init];
                        //清晰度480P
                        self.param.preferredVideoQuality = NIMNetCallVideoQuality480pLevel;
                        //裁剪类型 16:9
                        self.param.videoCrop  = NIMNetCallVideoCrop16x9;
                        //打开初始为前置摄像头
                        self.param.startWithBackCamera = NO;
                        //开始采集
                        [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
                        //视频采集数据回调
                        self.param.videoHandler =^(CMSampleBufferRef sampleBuffer){
                            //对采集数据进行外部前处理
                            //把 sampleBuffer 数据发送给 SDK 进行显示，编码，发送
                            [[NIMAVChatSDK sharedSDK].netCallManager sendVideoSampleBuffer:sampleBuffer];
                        };
                        //若需要开启前处理指定 videoProcessorParam
                        NIMNetCallVideoProcessorParam *videoProcessorParam = [[NIMNetCallVideoProcessorParam alloc] init];
                        //若需要通话开始时就带有前处理效果（如美颜自然模式）
                        videoProcessorParam.filterType = NIMNetCallFilterTypeZiran;
                        self.param.videoProcessorParam = videoProcessorParam;
                        meeting.option.videoCaptureParam = self.param;
                        meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
                        
                        //加入会议失败
                        if (error) {
                            NSLog(@"失败");
                            [self dismissVC];
                            
                        }
                        //加入会议成功
                        else
                        {
                            
                            [[RCIMClient sharedRCIMClient] joinChatRoom:self.currentRoomID messageCount:0 success:^{
                                NSLog(@"成功");
                            } error:^(RCErrorCode status) {
                                NSLog(@"失败");
                            }];
                            self.localPreView.hidden = NO;
                            self.localVideoView.hidden = NO;
                            
                            NIMChatroom *chatRoom = [[NIMChatroom alloc] init];
                            NSString *liveUrl = @"rtmp://p42997226.live.126.net/live/03d870bc00044f88b444b37f5467625a?wsSecret=2ab02198a26190d0ad6677593094a216&wsTime=1561603387";
                            
                            if (liveUrl.length) {
                                chatRoom.broadcastUrl = liveUrl;
                            }
                        }
                    }];
                });
            }
        }];
    }else{
        [[NIMAVChatSDK sharedSDK].netCallManager joinMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nonnull error) {
            
            self.param = [[NIMNetCallVideoCaptureParam alloc] init];
            //清晰度480P
            self.param.preferredVideoQuality = NIMNetCallVideoQuality480pLevel;
            //裁剪类型 16:9
            self.param.videoCrop  = NIMNetCallVideoCrop16x9;
            //打开初始为前置摄像头
            self.param.startWithBackCamera = NO;
            //开始采集
            [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
            //视频采集数据回调
            self.param.videoHandler =^(CMSampleBufferRef sampleBuffer){
                //对采集数据进行外部前处理
                //把 sampleBuffer 数据发送给 SDK 进行显示，编码，发送
                [[NIMAVChatSDK sharedSDK].netCallManager sendVideoSampleBuffer:sampleBuffer];
            };
            //若需要开启前处理指定 videoProcessorParam
            NIMNetCallVideoProcessorParam *videoProcessorParam = [[NIMNetCallVideoProcessorParam alloc] init];
            //若需要通话开始时就带有前处理效果（如美颜自然模式）
            videoProcessorParam.filterType = NIMNetCallFilterTypeZiran;
            self.param.videoProcessorParam = videoProcessorParam;
            meeting.option.videoCaptureParam = self.param;
            meeting.option.bypassStreamingMixMode = NIMNetCallBypassStreamingMixModeLatticeAspectFit;
            
            //加入会议失败
            if (error) {
                NSLog(@"失败");
                [self dismissVC];
            }
            //加入会议成功
            else
            {
                
                [[RCIMClient sharedRCIMClient] joinChatRoom:self.currentRoomID messageCount:0 success:^{
                    NSLog(@"成功");
                } error:^(RCErrorCode status) {
                    NSLog(@"失败");
                }];
                self.localPreView.hidden = NO;
                self.localVideoView.hidden = NO;
                
                NIMChatroom *chatRoom = [[NIMChatroom alloc] init];
                NSString *liveUrl = @"rtmp://p42997226.live.126.net/live/03d870bc00044f88b444b37f5467625a?wsSecret=2ab02198a26190d0ad6677593094a216&wsTime=1561603387";
                
                if (liveUrl.length) {
                    chatRoom.broadcastUrl = liveUrl;
                }
            }
        }];
    }
    
    
    [[NIMSDK sharedSDK].loginManager addDelegate:self];
    
    [[NIMAVChatSDK sharedSDK].netCallManager addDelegate:self];
    
    [[NIMSDK sharedSDK].chatroomManager addDelegate:self];
    
    [[NIMSDK sharedSDK].chatManager addDelegate:self];
    
    [[NIMSDK sharedSDK].systemNotificationManager addDelegate:self];
    
}


- (void)onReceiveCustomSystemNotification:(NIMCustomSystemNotification *)notification
{
    NSLog(@"notification:%@", notification);
    NSDictionary *dic = [NSDictionary dictionary];
    dic = [self dictionaryWithJsonString:notification.content];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (dic[@"code"]) {
            if ([dic[@"code"] isEqualToString:@"close_video"]) {
                [[NIMAVChatSDK sharedSDK].netCallManager stopVideoCapture];
                self.localVideoView.hidden = YES;
                self.localPreView.hidden = YES;
                if (self.remoteGLView.width == 80) {
                    self.remoteGLView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
                }
                self.isCloseVideo = YES;
            }else if ([dic[@"code"] isEqualToString:@"open_video"]){
                [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
                self.localVideoView.hidden = NO;
                self.localPreView.hidden = NO;
                self.isCloseVideo = NO;
                self.localVideoView.frame = CGRectMake(kScreenWidth - 88, kScreenHeight - kNavigationBarHeight - 50 - 80, 80, 80);
                [self.view bringSubviewToFront:self.localVideoView];
            }else if ([dic[@"code"] isEqualToString:@"open_audio"]){
                [[NIMAVChatSDK sharedSDK].netCallManager setMute:NO];
            }else if ([dic[@"code"] isEqualToString:@"close_audio"]){
                [[NIMAVChatSDK sharedSDK].netCallManager setMute:YES];
            }else if ([dic[@"code"] isEqualToString:@"kill"]){
                //离开当前多人会议
                [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您已被踢出房间" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissVC];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }else if ([dic[@"code"] isEqualToString:@"test"]){
                self.testID = [NSString stringWithFormat:@"%@", dic[@"test_id"]];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"你收到一份名为%@的面试", dic[@"text_title"]] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"稍后再做" style:UIAlertActionStyleDefault handler:nil]];
                [alert addAction:[UIAlertAction actionWithTitle:@"立即答题" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }else if ([dic[@"code"] isEqualToString:@"closeRoom"]){
                [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
                [self dismissVC];
            }
        }
    });
    
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

- (void)onRecvMessages:(NSArray<NIMMessage *> *)messages
{
    NSLog(@"message:%@", messages);
}

- (NSDictionary *)requestDic
{
    if (_requestDic == nil) {
        _requestDic = [NSDictionary dictionary];
    }
    return _requestDic;
}

- (void)onReceive:(UInt64)callID from:(NSString *)caller type:(NIMNetCallMediaType)type message:(NSString *)extendMessage
{
    NSLog(@"收到音频连线");
}

- (void)chatroomBeKicked:(NIMChatroomBeKickedResult *)result
{
    NSLog(@"%@", result);
}

- (void)onKicked{
    NSLog(@"sdfdsf");
}

- (void)chatroom:(NSString *)roomId connectionStateChanged:(NIMChatroomConnectionState)state
{
    NSLog(@"state:%ld", state);
}

- (void)chatroom:(NSString *)roomId beKicked:(NIMChatroomKickReason)reason
{
    // [roomId isEqualToString:self.chatroom.roomId]
    NSLog(@"sdfsf");
}

#pragma mark - 上麦请求
- (void)speakAction
{
    //发一条自定义通知告诉主播我进队列了，主播最多同一时间接到100条通知，不用担心主播会被撑爆
    
    NSDictionary *notificationDic = [NSDictionary dictionary];
    NSDictionary *dic = [NSDictionary dictionary];
    dic = USERDATA;
    notificationDic = @{@"id" : [USERDATA objectForKey:@"phone"], @"room" : self.currentRoomID, @"uname" : [USERDATA objectForKey:@"nickname"], @"avatar" : @"12321", @"code" : @"apply", @"type" : @"1"};
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notificationDic options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NIMCustomSystemNotification *notification = [[NIMCustomSystemNotification alloc] initWithContent:jsonString];
    NIMSession *session = [NIMSession session:self.requestDic[@"hr"][@"phone"] type:NIMSessionTypeP2P];
    [[NIMSDK sharedSDK].systemNotificationManager sendCustomNotification:notification toSession:session completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"失败");
        }else{
            NSLog(@"成功");
            
        }
    }];
}

#pragma mark - 设置UI
- (void)setupUI
{
    self.localPreView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    self.localPreView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.localPreView];
    self.localPreView.hidden = NO;
    
    self.localVideoView = [[UIView alloc] initWithFrame:CGRectMake(0, kStatusBarHeight + 30, kScreenWidth / 2, kScreenWidth / 2)];
    self.localVideoView.backgroundColor = [UIColor cyanColor];
    self.localVideoView.hidden = NO;
    [self.view addSubview:self.localVideoView];
    [self remoteGLView];
}

#pragma mark - dismiss页面
- (void)dismissVC
{
    [self.navigationController popViewControllerAnimated:YES];
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 本地预览
- (void)onLocalDisplayviewReady:(UIView *)displayView
{
    if (self.localPreView) {
        [self.localPreView removeFromSuperview];
    }
    self.localPreView = displayView;
    displayView.frame = self.localVideoView.bounds;
    [self.localVideoView addSubview:displayView];
}

- (void)onLogin:(NIMLoginStep)step
{
    NSLog(@"step:%ld", step);
}

- (void)onAutoLoginFailed:(NSError *)error
{
    if (error) {
        NSLog(@"登录失败");
    }else{
        NSLog(@"登录成功");
    }
}

#pragma mark - 获取房间信息
- (void)getRoomsData
{
    [HttpTool getWithURL:[NSString stringWithFormat:@"%@/%@", kURL(kVideoRoomsData), self.modelID] params:nil haveHeader:NO success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.meeting = [[NIMNetCallMeeting alloc] init];
                    self.meeting.name = [NSString stringWithFormat:@"%@", dict[@"channel"][@"name"]];
                    self.meeting.type = NIMNetCallMediaTypeVideo;
                    self.meeting.actor = YES;
                    self.currentRoomID = [NSString stringWithFormat:@"%@", dict[@"chat_room_id"]];
                    //加入会议
                    // 2018111418034419
                    [[NIMAVChatSDK sharedSDK].netCallManager joinMeeting:self.meeting completion:^(NIMNetCallMeeting * _Nonnull meeting, NSError * _Nonnull error) {
                        
                        self.param = [[NIMNetCallVideoCaptureParam alloc] init];
                        //清晰度480P
                        self.param.preferredVideoQuality = NIMNetCallVideoQuality480pLevel;
                        //裁剪类型 16:9
                        self.param.videoCrop  = NIMNetCallVideoCrop16x9;
                        //打开初始为前置摄像头
                        self.param.startWithBackCamera = NO;
                        //开始采集
                        [[NIMAVChatSDK sharedSDK].netCallManager startVideoCapture:self.param];
                        //视频采集数据回调
                        self.param.videoHandler =^(CMSampleBufferRef sampleBuffer){
                            //对采集数据进行外部前处理
                            //把 sampleBuffer 数据发送给 SDK 进行显示，编码，发送
                            [[NIMAVChatSDK sharedSDK].netCallManager sendVideoSampleBuffer:sampleBuffer];
                        };
                        //若需要开启前处理指定 videoProcessorParam
                        NIMNetCallVideoProcessorParam *videoProcessorParam = [[NIMNetCallVideoProcessorParam alloc] init];
                        //若需要通话开始时就带有前处理效果（如美颜自然模式）
                        videoProcessorParam.filterType = NIMNetCallFilterTypeZiran;
                        self.param.videoProcessorParam = videoProcessorParam;
                        meeting.option.videoCaptureParam = self.param;
                        
                        //加入会议失败
                        if (error) {
                            NSLog(@"失败");
                            [self dismissVC];
                    
                        }
                        //加入会议成功
                        else
                        {
                            self.currentRoomID = [NSString stringWithFormat:@"%@", dict[@"chat_room_id"]];
                            [[RCIMClient sharedRCIMClient] joinChatRoom:self.currentRoomID messageCount:0 success:^{
                                NSLog(@"成功");
                            } error:^(RCErrorCode status) {
                                NSLog(@"失败");
                            }];
                            self.localPreView.hidden = YES;
                            self.localVideoView.hidden = YES;
                            
                            NIMChatroom *chatRoom = [[NIMChatroom alloc] init];
                            chatRoom.roomId = [NSString stringWithFormat:@"%@", dict[@"chat_room_id"]];
                            NSString *liveUrl = [NSString stringWithFormat:@"%@", dict[@"channel"][@"pushUrl"]];
                            NSString *pullUrl = [NSString stringWithFormat:@"%@", dict[@"channel"][@"rtmpPullUrl"]];
                            NSDictionary *liveDic = @{@"pushUrl" : liveUrl,
                                                      @"pullUrl": pullUrl};
                            
                            if (liveUrl.length) {
                                chatRoom.broadcastUrl = liveUrl;
                            }
                        }
                    }];
                });
            }else{
                
            }
        }
    } failure:^(NSError *error) {
        
    }];
}


- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"控制器:%ld", self.navigationController.viewControllers.count);
    if (self.navigationController.viewControllers.count >= 3) {
        
    }else{
        //离开当前多人会议
        [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
    }
    
}

//远程YUV数据就绪回调
- (void)onRemoteYUVReady:(NSData *)yuvData
                   width:(NSUInteger)width
                  height:(NSUInteger)height
                    from:(NSString *)user
{
    //_remoteGLView 是 NTESGLView 类型  DEMO 提供 NTESGLView 类来渲染yuv数据
    [_remoteGLView render:yuvData width:width height:height];
    if (self.isCloseVideo) {
        [self.view bringSubviewToFront:self.localVideoView];
    }
    
}

- (void)onUserJoined:(NSString *)uid
             meeting:(NIMNetCallMeeting *)meeting
{
    NSLog(@"uid:%@", uid);
}

- (void)onUserLeft:(NSString *)uid meeting:(NIMNetCallMeeting *)meeting
{
    NSLog(@"uid:%@", uid);
}

- (NTESGLView *)remoteGLView{
    if (!_remoteGLView) {
        _remoteGLView = [[NTESGLView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        [_remoteGLView setContentMode:UIViewContentModeScaleAspectFill];
        [_remoteGLView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_remoteGLView];
    }
    return _remoteGLView;
}


#pragma mark - 头部视图协议方法
- (void)chatRoomTopViewButtonDidClick:(UIButton *)button
{
    if (button.tag == 0) {
        //        [QKPublicUtility showAutomaticDisappearAlertViewWithTipInformation:@"关注"];
    }else if (button.tag == 1){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否退出面试间" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //离开当前多人会议
            [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
            [self dismissVC];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)dealloc
{
    //离开当前多人会议
    [[NIMAVChatSDK sharedSDK].netCallManager leaveMeeting:self.meeting];
}

@end
