//
//  HomeAdCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeAdModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeAdCell : UICollectionViewCell

@property (nonatomic, copy) HomeAdModel *model;

@end

NS_ASSUME_NONNULL_END
