//
//  MoreReusableHeaderView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MoreReusableHeaderViewDelegate <NSObject>

- (void)moreReusableHeaderViewButtonDidClick:(UIButton *)button;

@end

@interface MoreReusableHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *headerViewTitleLabel;

@property (nonatomic, weak) id<MoreReusableHeaderViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
