//
//  LiveRoomTopView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/10.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "LiveRoomTopView.h"

@implementation LiveRoomTopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)liveRoomTopViewBtnAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomTopViewButtonDidClick:)]) {
        [self.delegate liveRoomTopViewButtonDidClick:sender];
    }
}

@end
