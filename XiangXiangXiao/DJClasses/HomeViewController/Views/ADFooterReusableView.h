//
//  ADFooterReusableView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ADFooterReusableView : UICollectionReusableView

@property (nonatomic, copy)void(^didClickADImg)(void);

@property (nonatomic, strong)NSString *imgName;

@end

NS_ASSUME_NONNULL_END
