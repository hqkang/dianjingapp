//
//  HomeAdCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeAdCell.h"

@implementation HomeAdCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HomeAdModel *)model
{
    if (_model != model) {
        _model = model;
    }
}

@end
