//
//  HomeCategoryCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeCategoryCell.h"

@implementation HomeCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HomeCategoryModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.image)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.categoryTitleLabel.text = model.name;
}

@end
