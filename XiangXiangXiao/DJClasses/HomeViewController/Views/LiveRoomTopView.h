//
//  LiveRoomTopView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/10.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LiveRoomTopViewDelegate <NSObject>

- (void)liveRoomTopViewButtonDidClick:(UIButton *)button;

@end

@interface LiveRoomTopView : UIView

@property (nonatomic, weak) id<LiveRoomTopViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
