//
//  HomeCategoryCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeCategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryTitleLabel;

@property (nonatomic, copy) HomeCategoryModel *model;

@end

NS_ASSUME_NONNULL_END
