//
//  HomeRecommendClassCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeRecommendClassCell.h"

@implementation HomeRecommendClassCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupBgView];
}

-(void)setupBgView
{
    self.collBGView.backgroundColor = kColorFromRGB(kWhite);
    self.collBGView.layer.borderWidth = 1;
    self.collBGView.layer.borderColor = kColorFromRGB(kWhite).CGColor;
    self.collBGView.layer.shadowColor = kColorFromRGBAndAlpha(kBlack,0.8).CGColor;
    self.collBGView.layer.shadowOffset = CGSizeMake(-3, 3);
    self.collBGView.layer.shadowOpacity = 0.8;
}

- (void)setModel:(HomeRecommendModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.classImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.cover)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.classNameLabel.text = model.title;
    self.priceLabel.text = model.price;
    self.organizationNameLabel.text = model.auth_user[@"realname"];
}

@end
