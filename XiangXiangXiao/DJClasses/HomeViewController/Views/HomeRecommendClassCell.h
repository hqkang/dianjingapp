//
//  HomeRecommendClassCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeRecommendModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeRecommendClassCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *classImageView;
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *organizationNameLabel;
@property (weak, nonatomic) IBOutlet UIView *collBGView;

@property (nonatomic, copy) HomeRecommendModel *model;

@end

NS_ASSUME_NONNULL_END
