//
//  ADFooterReusableView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "ADFooterReusableView.h"

@interface ADFooterReusableView()

@property (weak, nonatomic) IBOutlet UIImageView *imgv;


@end

@implementation ADFooterReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    self.imgv.userInteractionEnabled = YES;
    [self.imgv addGestureRecognizer:tap];
}

- (void)tapAction:(UITapGestureRecognizer *)sender
{
    if (self.didClickADImg) {
        self.didClickADImg();
    }
}


-(void)setImgName:(NSString *)imgName
{
    [self.imgv sd_setImageWithURL:[NSURL URLWithString:kImgURL(imgName)] placeholderImage:[UIImage imageNamed:@"img_score_placeholder"] options:SDWebImageRefreshCached];
}


@end
