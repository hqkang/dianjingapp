//
//  NewnewChildCag.m
//
//  Created by   on 2018/12/21
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "NewnewChildCag.h"


NSString *const kNewnewChildCagName = @"name";
NSString *const kNewnewChildCagId = @"id";
NSString *const kNewnewChildCagPic = @"pic";
NSString *const kNewnewChildCagNewPic = @"newPic";


@interface NewnewChildCag ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NewnewChildCag

@synthesize name = _name;
@synthesize newnewChildCagIdentifier = _newnewChildCagIdentifier;
@synthesize pic = _pic;
@synthesize versionPic = _newPic;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kNewnewChildCagName fromDictionary:dict];
            self.newnewChildCagIdentifier = [[self objectOrNilForKey:kNewnewChildCagId fromDictionary:dict] doubleValue];
            self.pic = [self objectOrNilForKey:kNewnewChildCagPic fromDictionary:dict];
            self.versionPic = [self objectOrNilForKey:kNewnewChildCagNewPic fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kNewnewChildCagName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.newnewChildCagIdentifier] forKey:kNewnewChildCagId];
    [mutableDict setValue:self.pic forKey:kNewnewChildCagPic];
    [mutableDict setValue:self.versionPic forKey:kNewnewChildCagNewPic];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kNewnewChildCagName];
    self.newnewChildCagIdentifier = [aDecoder decodeDoubleForKey:kNewnewChildCagId];
    self.pic = [aDecoder decodeObjectForKey:kNewnewChildCagPic];
    self.versionPic = [aDecoder decodeObjectForKey:kNewnewChildCagNewPic];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kNewnewChildCagName];
    [aCoder encodeDouble:_newnewChildCagIdentifier forKey:kNewnewChildCagId];
    [aCoder encodeObject:_pic forKey:kNewnewChildCagPic];
    [aCoder encodeObject:_newPic forKey:kNewnewChildCagNewPic];

}

- (id)copyWithZone:(NSZone *)zone {
    NewnewChildCag *copy = [[NewnewChildCag alloc] init];
    
    
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.newnewChildCagIdentifier = self.newnewChildCagIdentifier;
        copy.pic = [self.pic copyWithZone:zone];
        copy.versionPic = [self.versionPic copyWithZone:zone];
    }
    
    return copy;
}


@end
