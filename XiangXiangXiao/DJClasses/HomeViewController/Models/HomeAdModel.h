//
//  HomeAdModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeAdModel : NSObject <NSCoding, NSCopying>

/*
 "id": 85,
 "parent_id": 38,
 "name": "app教育首页广告",
 "subtitle": null,
 "image": "http://api.net/images/default-thumb.png",
 "content": null,
 "href": "http://m.haiwai.xiangxiangxiao.com/#/esli",
 "status": 1,
 "created_at": "2019-06-24 17:37:18",
 "updated_at": "2019-06-24 17:40:39",
 "forever": 1,
 "effectived_at": null,
 "show_num": 2
 */

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *href;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
