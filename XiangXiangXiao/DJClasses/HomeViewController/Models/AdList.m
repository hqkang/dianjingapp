//
//  AdList.m
//
//  Created by   on 2018/4/26
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "AdList.h"

NSString *const kAdListImage = @"image";
NSString *const kAdListHref = @"href";


@interface AdList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AdList

@synthesize image = _image;
@synthesize href = _href;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.image = [self objectOrNilForKey:kAdListImage fromDictionary:dict];
        self.href = [self objectOrNilForKey:kAdListHref fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.image forKey:kAdListImage];
    [mutableDict setValue:self.href forKey:kAdListHref];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.image = [aDecoder decodeObjectForKey:kAdListImage];
    self.href = [aDecoder decodeObjectForKey:kAdListHref];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_image forKey:kAdListImage];
    [aCoder encodeObject:_href forKey:kAdListHref];
}

- (id)copyWithZone:(NSZone *)zone {
    AdList *copy = [[AdList alloc] init];
    
    if (copy) {
        copy.image = [self.image copyWithZone:zone];
        copy.href = [self.href copyWithZone:zone];
    }
    return copy;
}


@end
