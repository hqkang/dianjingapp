//
//  HomeCategoryModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCategoryModel : NSObject <NSCoding, NSCopying>

/*
 name": "搜工作",
 "code": "jobs",
 "image": "http://api.net/uploads/images/43e9b76ee2c37db9502477a0bb961933.png",
 "android_uri": "",
 "ios_uri": "NewJobViewController",
 "h5_url": "http://front.xiangxiangxiao.cn/#/joblist",
 "href": "
 */

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *ios_uri;
@property (nonatomic, strong) NSString *h5_url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
