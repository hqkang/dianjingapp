//
//  HomeIndexModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeIndexModel : NSObject <NSCoding, NSCopying>

/*
 "whole_switch": true,
 "whole_h5_url": null,
 "whole_android_uri": null,
 "whole_ios_uri": null
 */

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *en_name;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSDictionary *whole;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
