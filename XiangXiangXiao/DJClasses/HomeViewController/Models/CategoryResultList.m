//
//  CategoryResultList.m
//
//  Created by   on 2018/5/2
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "CategoryResultList.h"


NSString *const kCategoryResultListName = @"name";
NSString *const kCategoryResultListId = @"id";
NSString *const kCategoryResultListPic = @"pic";


@interface CategoryResultList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CategoryResultList

@synthesize name = _name;
@synthesize categoryResultListIdentifier = _categoryResultListIdentifier;
@synthesize pic = _pic;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kCategoryResultListName fromDictionary:dict];
            self.categoryResultListIdentifier = [[self objectOrNilForKey:kCategoryResultListId fromDictionary:dict] doubleValue];
            self.pic = [self objectOrNilForKey:kCategoryResultListPic fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kCategoryResultListName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryResultListIdentifier] forKey:kCategoryResultListId];
    [mutableDict setValue:self.pic forKey:kCategoryResultListPic];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kCategoryResultListName];
    self.categoryResultListIdentifier = [aDecoder decodeDoubleForKey:kCategoryResultListId];
    self.pic = [aDecoder decodeObjectForKey:kCategoryResultListPic];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kCategoryResultListName];
    [aCoder encodeDouble:_categoryResultListIdentifier forKey:kCategoryResultListId];
    [aCoder encodeObject:_pic forKey:kCategoryResultListPic];
}

- (id)copyWithZone:(NSZone *)zone {
    CategoryResultList *copy = [[CategoryResultList alloc] init];
    
    
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.categoryResultListIdentifier = self.categoryResultListIdentifier;
        copy.pic = [self.pic copyWithZone:zone];
    }
    
    return copy;
}


@end
