//
//  HomeAdModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeAdModel.h"

NSString *const kHomeAdName = @"name";
NSString *const kHomeAdImage = @"image";
NSString *const kHomeAdHref = @"href";


@interface HomeAdModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeAdModel

@synthesize name = _name;
@synthesize image = _image;
@synthesize href = _href;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.name = [self objectOrNilForKey:kHomeAdName fromDictionary:dict];
        self.image = [self objectOrNilForKey:kHomeAdImage fromDictionary:dict];
        self.href = [self objectOrNilForKey:kHomeAdHref fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kHomeAdName];
    [mutableDict setValue:self.image forKey:kHomeAdImage];
    [mutableDict setValue:self.href forKey:kHomeAdHref];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.name = [aDecoder decodeObjectForKey:kHomeAdName];
    self.image = [aDecoder decodeObjectForKey:kHomeAdImage];
    self.href = [aDecoder decodeObjectForKey:kHomeAdHref];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_name forKey:kHomeAdName];
    [aCoder encodeObject:_image forKey:kHomeAdImage];
    [aCoder encodeObject:_href forKey:kHomeAdHref];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeAdModel *copy = [[HomeAdModel alloc] init];
    
    if (copy) {
        copy.name = [self.name copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.href = [self.href copyWithZone:zone];
    }
    return copy;
}

@end
