//
//  CategoryList.m
//
//  Created by   on 2018/4/26
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "CategoryList.h"


NSString *const kCategoryListId = @"id";
NSString *const kCategoryListPic = @"pic";
NSString *const kCategoryListName = @"name";
NSString *const kCategoryListRecommendPic = @"recommendPic";


@interface CategoryList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CategoryList

@synthesize categoryListIdentifier = _categoryListIdentifier;
@synthesize pic = _pic;
@synthesize name = _name;
@synthesize recommendPic = _recommendPic;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.categoryListIdentifier = [[self objectOrNilForKey:kCategoryListId fromDictionary:dict] doubleValue];
            self.pic = [self objectOrNilForKey:kCategoryListPic fromDictionary:dict];
            self.name = [self objectOrNilForKey:kCategoryListName fromDictionary:dict];
            self.recommendPic = [self objectOrNilForKey:kCategoryListRecommendPic fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryListIdentifier] forKey:kCategoryListId];
    [mutableDict setValue:self.pic forKey:kCategoryListPic];
    [mutableDict setValue:self.name forKey:kCategoryListName];
    [mutableDict setValue:self.recommendPic forKey:kCategoryListRecommendPic];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.categoryListIdentifier = [aDecoder decodeDoubleForKey:kCategoryListId];
    self.pic = [aDecoder decodeObjectForKey:kCategoryListPic];
    self.name = [aDecoder decodeObjectForKey:kCategoryListName];
    self.recommendPic = [aDecoder decodeObjectForKey:kCategoryListRecommendPic];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_categoryListIdentifier forKey:kCategoryListId];
    [aCoder encodeObject:_pic forKey:kCategoryListPic];
    [aCoder encodeObject:_name forKey:kCategoryListName];
    [aCoder encodeObject:_recommendPic forKey:kCategoryListRecommendPic];
}

- (id)copyWithZone:(NSZone *)zone {
    CategoryList *copy = [[CategoryList alloc] init];
    
    
    
    if (copy) {

        copy.categoryListIdentifier = self.categoryListIdentifier;
        copy.pic = [self.pic copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.recommendPic = [self.recommendPic copyWithZone:zone];
    }
    
    return copy;
}


@end
