//
//  HomeIndexModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeIndexModel.h"

NSString *const kHomeIndexType = @"type";
NSString *const kHomeIndexName = @"name";
NSString *const kHomeIndexEn_name = @"en_name";
NSString *const kHomeIndexData = @"data";
NSString *const kHomeIndexWhole = @"whole";


@interface HomeIndexModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeIndexModel

@synthesize type = _type;
@synthesize name = _name;
@synthesize en_name = _en_name;
@synthesize data = _data;
@synthesize whole = _whole;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.type = [self objectOrNilForKey:kHomeIndexType fromDictionary:dict];
        self.name = [self objectOrNilForKey:kHomeIndexName fromDictionary:dict];
        self.en_name = [self objectOrNilForKey:kHomeIndexEn_name fromDictionary:dict];
        self.data = [self objectOrNilForKey:kHomeIndexData fromDictionary:dict];
        self.whole = [self objectOrNilForKey:kHomeIndexWhole fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.type forKey:kHomeIndexType];
    [mutableDict setValue:self.name forKey:kHomeIndexName];
    [mutableDict setValue:self.en_name forKey:kHomeIndexEn_name];
    [mutableDict setValue:self.data forKey:kHomeIndexData];
    [mutableDict setValue:self.whole forKey:kHomeIndexWhole];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.type = [aDecoder decodeObjectForKey:kHomeIndexType];
    self.name = [aDecoder decodeObjectForKey:kHomeIndexName];
    self.en_name = [aDecoder decodeObjectForKey:kHomeIndexEn_name];
    self.data = [aDecoder decodeObjectForKey:kHomeIndexData];
    self.whole = [aDecoder decodeObjectForKey:kHomeIndexWhole];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_type forKey:kHomeIndexType];
    [aCoder encodeObject:_name forKey:kHomeIndexName];
    [aCoder encodeObject:_en_name forKey:kHomeIndexEn_name];
    [aCoder encodeObject:_data forKey:kHomeIndexData];
    [aCoder encodeObject:_whole forKey:kHomeIndexWhole];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeIndexModel *copy = [[HomeIndexModel alloc] init];
    
    if (copy) {
        copy.type = [self.type copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.en_name = [self.en_name copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
        copy.whole = [self.whole copyWithZone:zone];
    }
    return copy;
}

@end
