//
//  HomeCategoryModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeCategoryModel.h"


NSString *const kHomeCategoryName = @"name";
NSString *const kHomeCategoryCode = @"code";
NSString *const kHomeCategoryImage = @"image";
NSString *const kHomeCategoryIOS_URI = @"ios_uri";
NSString *const kHomeCategoryH5_URL = @"h5_url";


@interface HomeCategoryModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeCategoryModel

@synthesize name = _name;
@synthesize code = _code;
@synthesize image = _image;
@synthesize ios_uri = _ios_uri;
@synthesize h5_url = _h5_url;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.name = [self objectOrNilForKey:kHomeCategoryName fromDictionary:dict];
        self.code = [self objectOrNilForKey:kHomeCategoryCode fromDictionary:dict];
        self.image = [self objectOrNilForKey:kHomeCategoryImage fromDictionary:dict];
        self.ios_uri = [self objectOrNilForKey:kHomeCategoryIOS_URI fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kHomeCategoryH5_URL fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kHomeCategoryName];
    [mutableDict setValue:self.code forKey:kHomeCategoryCode];
    [mutableDict setValue:self.image forKey:kHomeCategoryImage];
    [mutableDict setValue:self.ios_uri forKey:kHomeCategoryIOS_URI];
    [mutableDict setValue:self.h5_url forKey:kHomeCategoryH5_URL];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.name = [aDecoder decodeObjectForKey:kHomeCategoryName];
    self.code = [aDecoder decodeObjectForKey:kHomeCategoryCode];
    self.image = [aDecoder decodeObjectForKey:kHomeCategoryImage];
    self.ios_uri = [aDecoder decodeObjectForKey:kHomeCategoryIOS_URI];
    self.h5_url = [aDecoder decodeObjectForKey:kHomeCategoryH5_URL];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_name forKey:kHomeCategoryName];
    [aCoder encodeObject:_code forKey:kHomeCategoryCode];
    [aCoder encodeObject:_image forKey:kHomeCategoryImage];
    [aCoder encodeObject:_ios_uri forKey:kHomeCategoryIOS_URI];
    [aCoder encodeObject:_h5_url forKey:kHomeCategoryH5_URL];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeCategoryModel *copy = [[HomeCategoryModel alloc] init];
    
    if (copy) {
        copy.name = [self.name copyWithZone:zone];
        copy.code = [self.code copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.ios_uri = [self.ios_uri copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
    }
    return copy;
}

@end
