//
//  HomeBannerModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeBannerModel.h"

NSString *const kHomeBannerImage = @"image";
NSString *const kHomeBannerHref = @"href";


@interface HomeBannerModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeBannerModel

@synthesize image = _image;
@synthesize href = _href;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.image = [self objectOrNilForKey:kHomeBannerImage fromDictionary:dict];
        self.href = [self objectOrNilForKey:kHomeBannerHref fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.image forKey:kHomeBannerImage];
    [mutableDict setValue:self.href forKey:kHomeBannerHref];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.image = [aDecoder decodeObjectForKey:kHomeBannerImage];
    self.href = [aDecoder decodeObjectForKey:kHomeBannerHref];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_image forKey:kHomeBannerImage];
    [aCoder encodeObject:_href forKey:kHomeBannerHref];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeBannerModel *copy = [[HomeBannerModel alloc] init];
    
    if (copy) {
        copy.image = [self.image copyWithZone:zone];
        copy.href = [self.href copyWithZone:zone];
    }
    return copy;
}

@end
