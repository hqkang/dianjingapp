//
//  HomeIcon.m
//
//  Created by   on 2019/1/7
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

#import "HomeIcon.h"


NSString *const kHomeIconId = @"id";
NSString *const kHomeIconShowImage = @"showImage";
NSString *const kHomeIconName = @"name";
NSString *const kHomeIconType = @"type";


@interface HomeIcon ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeIcon

@synthesize homeIconIdentifier = _homeIconIdentifier;
@synthesize showImage = _showImage;
@synthesize name = _name;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.homeIconIdentifier = [[self objectOrNilForKey:kHomeIconId fromDictionary:dict] doubleValue];
            self.showImage = [self objectOrNilForKey:kHomeIconShowImage fromDictionary:dict];
            self.name = [self objectOrNilForKey:kHomeIconName fromDictionary:dict];
            self.type = [[self objectOrNilForKey:kHomeIconType fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.homeIconIdentifier] forKey:kHomeIconId];
    [mutableDict setValue:self.showImage forKey:kHomeIconShowImage];
    [mutableDict setValue:self.name forKey:kHomeIconName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kHomeIconType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.homeIconIdentifier = [aDecoder decodeDoubleForKey:kHomeIconId];
    self.showImage = [aDecoder decodeObjectForKey:kHomeIconShowImage];
    self.name = [aDecoder decodeObjectForKey:kHomeIconName];
    self.type = [aDecoder decodeDoubleForKey:kHomeIconType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_homeIconIdentifier forKey:kHomeIconId];
    [aCoder encodeObject:_showImage forKey:kHomeIconShowImage];
    [aCoder encodeObject:_name forKey:kHomeIconName];
    [aCoder encodeDouble:_type forKey:kHomeIconType];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeIcon *copy = [[HomeIcon alloc] init];
    
    
    
    if (copy) {

        copy.homeIconIdentifier = self.homeIconIdentifier;
        copy.showImage = [self.showImage copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.type = self.type;
    }
    
    return copy;
}


@end
