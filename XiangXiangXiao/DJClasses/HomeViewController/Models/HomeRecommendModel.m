//
//  HomeRecommendModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "HomeRecommendModel.h"

NSString *const kHomeRecommendTitle = @"title";
NSString *const kHomeRecommendPrice = @"price";
NSString *const kHomeRecommendCover = @"cover";
NSString *const kHomeRecommendAuth_user = @"auth_user";

@interface HomeRecommendModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HomeRecommendModel

@synthesize title = _title;
@synthesize price = _price;
@synthesize cover = _cover;
@synthesize auth_user = _auth_user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.title = [self objectOrNilForKey:kHomeRecommendTitle fromDictionary:dict];
        self.price = [self objectOrNilForKey:kHomeRecommendPrice fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kHomeRecommendCover fromDictionary:dict];
        self.auth_user = [self objectOrNilForKey:kHomeRecommendAuth_user fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kHomeRecommendTitle];
    [mutableDict setValue:self.price forKey:kHomeRecommendPrice];
    [mutableDict setValue:self.cover forKey:kHomeRecommendCover];
    [mutableDict setValue:self.auth_user forKey:kHomeRecommendAuth_user];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.title = [aDecoder decodeObjectForKey:kHomeRecommendTitle];
    self.price = [aDecoder decodeObjectForKey:kHomeRecommendPrice];
    self.cover = [aDecoder decodeObjectForKey:kHomeRecommendCover];
    self.auth_user = [aDecoder decodeObjectForKey:kHomeRecommendAuth_user];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_title forKey:kHomeRecommendTitle];
    [aCoder encodeObject:_price forKey:kHomeRecommendPrice];
    [aCoder encodeObject:_cover forKey:kHomeRecommendCover];
    [aCoder encodeObject:_auth_user forKey:kHomeRecommendAuth_user];
}

- (id)copyWithZone:(NSZone *)zone {
    HomeRecommendModel *copy = [[HomeRecommendModel alloc] init];
    
    if (copy) {
        
        copy.title = [self.title copyWithZone:zone];
        copy.price = [self.price copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.auth_user = [self.auth_user copyWithZone:zone];
    }
    
    return copy;
}

@end
