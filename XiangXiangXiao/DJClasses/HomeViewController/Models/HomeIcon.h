//
//  HomeIcon.h
//
//  Created by   on 2019/1/7
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface HomeIcon : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double homeIconIdentifier;
@property (nonatomic, strong) NSString *showImage;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double type;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
