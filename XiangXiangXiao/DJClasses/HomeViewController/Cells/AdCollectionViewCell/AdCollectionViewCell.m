//
//  AdCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "AdCollectionViewCell.h"

@implementation AdCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(void)setupUI
{
    self.adScrollView.backgroundColor = kColorFromRGB(kWhite);
    [self.contentView addSubview:self.adScrollView];
    [self.adScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (SDCycleScrollView *)adScrollView
{
    if (_adScrollView == nil) {
        _adScrollView = [[SDCycleScrollView alloc] init];
    }
    return _adScrollView;
}

@end
