//
//  AdCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/25.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDCycleScrollView.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) SDCycleScrollView *adScrollView;

@end

NS_ASSUME_NONNULL_END
