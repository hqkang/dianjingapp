//
//  BannerCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BannerCollectionViewCellDelegate<NSObject>
@optional
-(void)didSelectBannerWithIndex:(NSInteger)index;
@end


@interface BannerCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak)id<BannerCollectionViewCellDelegate> delegate;
@property (nonatomic, copy)NSArray *adListArray;
@property (nonatomic, assign)NSInteger timeInterval;
@end
