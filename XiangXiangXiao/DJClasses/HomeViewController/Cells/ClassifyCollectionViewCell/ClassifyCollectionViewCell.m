//
//  ClassifyCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "ClassifyCollectionViewCell.h"

//models
#import "CategoryList.h"
#import "CategoryResultList.h"
#import "NewnewChildCag.h"
#import "HomeIcon.h"

//#import "SDWebImageDownloaderOperation.h"

@interface ClassifyCollectionViewCell()
@property (nonatomic, strong)UIImageView *imgv;
@property (nonatomic, strong)UILabel *lbName;
@end

@implementation ClassifyCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(UIImageView *)imgv
{
    if (!_imgv) {
        _imgv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height/3.0*2.1)];
        _imgv.contentMode = UIViewContentModeScaleAspectFit;
        _imgv.layer.masksToBounds = YES;
        UIImage *imgPlace = [UIImage imageNamed:@"img_placeholder_classify"];
        [_imgv setImage:imgPlace];
    }
    return _imgv;
}
-(UILabel *)lbName
{
    if (!_lbName) {
         _lbName = [[UILabel alloc]init];
        _lbName.textAlignment = NSTextAlignmentCenter;
        [_lbName setFont:[UIFont systemFontOfSize:kClassifyFont]];
        [_lbName setTextColor:kColorFromRGB(kCateColor)];
    }
    return _lbName;
}

-(void)setupUI
{
    UIView *bgView = [[UIView alloc]initWithFrame:self.bounds];
    [bgView setBackgroundColor:kColorFromRGB(kWhite)];
    [self.contentView addSubview:bgView];
    
    [bgView addSubview:self.imgv];
    [bgView addSubview:self.lbName];
    
//    if (self.cellType == ClassifyCollectionViewCellTypeHome) {
//        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_equalTo(weakself.contentView.mas_width).multipliedBy(0.5);
//            make.top.mas_equalTo(0);
//            make.centerX.mas_equalTo(0);
//        }];
//    }else if (self.cellType == ClassifyCollectionViewCellTypeCategory){
//        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_equalTo(weakself.contentView.mas_width).multipliedBy(0.3);
//            make.top.mas_equalTo(0);
//            make.centerX.mas_equalTo(0);
//        }];
//    }
    
    [self.lbName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
//        make.top.mas_equalTo(weakself.imgv.mas_bottom).with.offset(5);
        make.height.mas_equalTo(14);
    }];
}

-(void)setClassifyModel:(CategoryList *)classifyModel
{
    NSString *imgNameStr = classifyModel.recommendPic;
    NSString *lbNameStr = classifyModel.name;
    UIImage *imgPlace = [UIImage imageNamed:@"img_placeholder_classify"];
    NSURL *imgURL = [NSURL URLWithString:kImgURL(imgNameStr)];
    self.imgv.image = nil;

    if (imgNameStr.length>0) {
        [self.imgv sd_setImageWithURL:imgURL placeholderImage:imgPlace options:SDWebImageRefreshCached];
    }else{
        [self.imgv setImage:imgPlace];
    }
    
    [self.lbName setText:lbNameStr];
}
-(void)setCategoryResultListModel:(CategoryResultList *)categoryResultListModel
{
    NSString *imgNameStr = categoryResultListModel.pic;
    NSString *lbNameStr = categoryResultListModel.name;
    UIImage *imgPlace = [UIImage imageNamed:@"img_placeholder_classify"];
    NSURL *imgURL = [NSURL URLWithString:kImgURL(imgNameStr)];
    self.imgv.image = nil;
    
    if (imgNameStr.length>0) {
        [self.imgv sd_setImageWithURL:imgURL placeholderImage:imgPlace options:SDWebImageRefreshCached];
    }else{
        [self.imgv setImage:imgPlace];
    }
    
    [self.lbName setText:lbNameStr];
}

// 二级分类
-(void)setNewnewChildCagModel:(NewnewChildCag *)newnewChildCagModel
{
    NSString *imgNameStr = newnewChildCagModel.versionPic; // 旧版是pic
    NSString *lbNameStr = newnewChildCagModel.name;
    UIImage *imgPlace = [UIImage imageNamed:@"bg_fenlei_placehold"];
    NSURL *imgURL = [NSURL URLWithString:kImgURL(imgNameStr)];
    self.imgv.image = nil;
    
    if (imgNameStr.length>0) {
        [self.imgv sd_setImageWithURL:imgURL placeholderImage:imgPlace options:SDWebImageRefreshCached];
    }else{
        [self.imgv setImage:imgPlace];
    }
    
    [self.lbName setText:lbNameStr];
    
    if (self.cellType == ClassifyCollectionViewCellTypeNewnewCategory) {
        [self.lbName setText:@""];
    }
    
}

-(void)setHomeIconModel:(HomeIcon *)homeIconModel
{
    NSString *imgNameStr = homeIconModel.showImage;
    NSString *lbNameStr = homeIconModel.name;
    UIImage *imgPlace = [UIImage imageNamed:@"img_placeholder_classify"];
    NSURL *imgURL = [NSURL URLWithString:kImgURL(imgNameStr)];
    self.imgv.image = nil;
    
    if (imgNameStr.length>0) {
        [self.imgv sd_setImageWithURL:imgURL placeholderImage:imgPlace];
    }else{
        [self.imgv setImage:imgPlace];
    }
    
    [self.lbName setText:lbNameStr];
}
-(void)setCellType:(ClassifyCollectionViewCellType)cellType
{
    _cellType = cellType;
    
    kWeakSelf(self);
    if (cellType == ClassifyCollectionViewCellTypeHome) {
        _imgv.contentMode = UIViewContentModeScaleAspectFit;
        [_lbName setTextColor:kColorFromRGB(kCateColor)];
        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.bottom.mas_equalTo(weakself.lbName.mas_top).with.offset(0);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(weakself.imgv.mas_height);
        }];
    }else if (cellType == ClassifyCollectionViewCellTypeCategory){
        _imgv.contentMode = UIViewContentModeScaleAspectFit;
        [_lbName setTextColor:kColorFromRGB(kCateColor)];
        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(weakself.contentView.mas_width).multipliedBy(0.35);
            make.centerX.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.top.mas_equalTo(20);
        }];
        [self.lbName mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
                    make.top.mas_equalTo(weakself.imgv.mas_bottom).with.offset(5);
        }];
    }else if (cellType == ClassifyCollectionViewCellTypeNewnewCategory){
        // 二级分类！只显示图片
//        _imgv.contentMode = UIViewContentModeScaleAspectFit;
//        [_lbName setTextColor:kColorFromRGB(kGoodShopNameColor)];
//        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(8);
//            make.bottom.mas_equalTo(weakself.lbName.mas_top).with.offset(-10);
//            make.centerX.mas_equalTo(0);
//            make.width.mas_equalTo(weakself.imgv.mas_height);
//        }];
        self.lbName.text = @"";
        _imgv.contentMode = UIViewContentModeScaleAspectFit;
        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakself);
        }];
        
        
    }else if (cellType == ClassifyCollectionViewCellTypeSaleTop){
        _imgv.contentMode = UIViewContentModeScaleToFill;
        _lbName.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
        [_lbName setTextColor:kColorFromRGB(kNameColor)];
        [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(weakself.lbName.mas_top).with.offset(-10);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(weakself.imgv.mas_height);
        }];
    }
}

-(void)setToolDict:(NSDictionary *)toolDict
{
    [self.imgv mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_offset(CGSizeMake(25, 25));
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-15);
    }];
    NSString *imgvName = toolDict[@"imgvName"];
    NSString *titleStr = toolDict[@"titleStr"];
    
    [self.imgv setImage:[UIImage imageNamed:imgvName]];
    [self.lbName setText:titleStr];
}

@end
