//
//  ClassifyCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ClassifyCollectionViewCellType) {
    ClassifyCollectionViewCellTypeHome,
    ClassifyCollectionViewCellTypeCategory,
    ClassifyCollectionViewCellTypeNewnewCategory,
    ClassifyCollectionViewCellTypeSaleTop,
};

@class CategoryList,CategoryResultList,NewnewChildCag,HomeIcon;

@interface ClassifyCollectionViewCell : UICollectionViewCell
@property (nonatomic, assign)ClassifyCollectionViewCellType cellType;
@property (nonatomic, strong)CategoryList *classifyModel;
@property (nonatomic, strong)CategoryResultList *categoryResultListModel;
@property (nonatomic, strong)NSDictionary *toolDict;
@property (nonatomic, strong)NewnewChildCag *newnewChildCagModel;
@property (nonatomic, strong)HomeIcon *homeIconModel;


@end
