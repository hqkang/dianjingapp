//
//  RightNestedCollViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/20.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import "RightNestedCollViewCell.h"

#import "ClassifyCollectionViewCell.h"

#define itemWH kWidth(264)
@interface RightNestedCollViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, weak)UICollectionView *rightCollectionView;
@end

@implementation RightNestedCollViewCell

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}
-(UICollectionView *)rightCollectionView
{
    if (!_rightCollectionView) {
        UICollectionViewFlowLayout *flowLY = [[UICollectionViewFlowLayout alloc]init];
//        flowLY.minimumLineSpacing = 20; // 行间距
        flowLY.minimumLineSpacing = CGFLOAT_MIN; // 行间距
        flowLY.minimumInteritemSpacing = CGFLOAT_MIN;
        
        flowLY.sectionInset = UIEdgeInsetsMake(14,kWidth(6), 0, 0);
        flowLY.itemSize = CGSizeMake(itemWH, kHH(itemWH, 2.18));

        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.contentView.bounds collectionViewLayout:flowLY];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.bounces = NO;
        
        [collectionView registerClass:[ClassifyCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([ClassifyCollectionViewCell class])];
        
        _rightCollectionView = collectionView;
        [self.contentView addSubview:_rightCollectionView];
    }
    return _rightCollectionView;
}
-(void)setupUI
{
    [self.rightCollectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

-(void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self.rightCollectionView reloadData];
}

#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource>

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ClassifyCollectionViewCell *cellClassify = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ClassifyCollectionViewCell class]) forIndexPath:indexPath];
    cellClassify.cellType = ClassifyCollectionViewCellTypeNewnewCategory;
    if (indexPath.item < self.dataArray.count) {
        NewnewChildCag *model = self.dataArray[indexPath.item];
        cellClassify.newnewChildCagModel = model; // 此模型赋值 取 newPic
    }
    return cellClassify;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectCategoryItemWithIndex:)]) {
        [self.delegate didSelectCategoryItemWithIndex:indexPath.item];
    }
}

@end
