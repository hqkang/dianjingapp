//
//  RightNestedCollViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/20.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol RightNestedCollCellDelegate <NSObject>
-(void)didSelectCategoryItemWithIndex:(NSInteger)index;
@end

NS_ASSUME_NONNULL_BEGIN

@interface RightNestedCollViewCell : UICollectionViewCell
@property (nonatomic, weak)id<RightNestedCollCellDelegate> delegate;
@property (nonatomic, copy)NSArray *dataArray;
@end

NS_ASSUME_NONNULL_END
