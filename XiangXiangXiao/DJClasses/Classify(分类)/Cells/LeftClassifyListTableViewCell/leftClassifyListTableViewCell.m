//
//  leftClassifyListTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/20.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "leftClassifyListTableViewCell.h"

@interface leftClassifyListTableViewCell()

@end

@implementation leftClassifyListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected == YES) {
        [UIView animateWithDuration:0.3 animations:^{
//            self.labelName.textColor = kColorFromRGB(kGoodShopNameColor);
//            self.labelName.font = [UIFont systemFontOfSize:15];
//            [self.labelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
            
            [self.labelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
            self.labelName.textColor = RGBS(51);
            self.backView.backgroundColor = kColorFromRGB(kWhite);
            self.guideView.backgroundColor = rgba(255, 217, 67, 1);

        }];
        
    }else{
        [UIView animateWithDuration:0.3 animations:^{
//            self.labelName.textColor = kColorFromRGB(kTabTextColor);
//            [self.labelName setFont:[UIFont fontWithName:@"Helvetica" size:14]];
//            self.backView.backgroundColor = kColorFromRGB(kBGColor);
//            self.guideView.backgroundColor = [UIColor clearColor];
            
            self.labelName.textColor = rgba(153, 153, 153, 1);
            [self.labelName setFont:[UIFont fontWithName:@"Helvetica" size:13]];
            self.backView.backgroundColor = rgba(244, 244, 244, 1);
            self.guideView.backgroundColor = [UIColor clearColor];
            
        }];
        
    }
}

@end
