//
//  leftClassifyListTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/20.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface leftClassifyListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *guideView;

@end
