//
//  NewnewCagResults.h
//
//  Created by   on 2018/12/21
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NewnewCagResults : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double newNotice;
@property (nonatomic, strong) NSString *searchwords;
@property (nonatomic, strong) NSArray *newnewCagChild;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
