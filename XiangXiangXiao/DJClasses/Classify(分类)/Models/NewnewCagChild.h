//
//  NewnewCagChild.h
//
//  Created by   on 2018/12/21
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NewnewCagChild : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double newnewCagChildIdentifier;
@property (nonatomic, strong) NSString *pic;
@property (nonatomic, strong) NSArray *newnewChildCag;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
