//
//  NewnewCagChild.m
//
//  Created by   on 2018/12/21
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "NewnewCagChild.h"
#import "NewnewChildCag.h"


NSString *const kNewnewCagChildId = @"id";
NSString *const kNewnewCagChildPic = @"pic";
NSString *const kNewnewCagChildNewnewChildCag = @"childCategory";
NSString *const kNewnewCagChildName = @"name";


@interface NewnewCagChild ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NewnewCagChild

@synthesize newnewCagChildIdentifier = _newnewCagChildIdentifier;
@synthesize pic = _pic;
@synthesize newnewChildCag = _newnewChildCag;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.newnewCagChildIdentifier = [[self objectOrNilForKey:kNewnewCagChildId fromDictionary:dict] doubleValue];
            self.pic = [self objectOrNilForKey:kNewnewCagChildPic fromDictionary:dict];
    NSObject *receivedNewnewChildCag = [dict objectForKey:kNewnewCagChildNewnewChildCag];
    NSMutableArray *parsedNewnewChildCag = [NSMutableArray array];
    
    if ([receivedNewnewChildCag isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedNewnewChildCag) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedNewnewChildCag addObject:[NewnewChildCag modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedNewnewChildCag isKindOfClass:[NSDictionary class]]) {
       [parsedNewnewChildCag addObject:[NewnewChildCag modelObjectWithDictionary:(NSDictionary *)receivedNewnewChildCag]];
    }

    self.newnewChildCag = [NSArray arrayWithArray:parsedNewnewChildCag];
            self.name = [self objectOrNilForKey:kNewnewCagChildName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.newnewCagChildIdentifier] forKey:kNewnewCagChildId];
    [mutableDict setValue:self.pic forKey:kNewnewCagChildPic];
    NSMutableArray *tempArrayForNewnewChildCag = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.newnewChildCag) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForNewnewChildCag addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForNewnewChildCag addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForNewnewChildCag] forKey:kNewnewCagChildNewnewChildCag];
    [mutableDict setValue:self.name forKey:kNewnewCagChildName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.newnewCagChildIdentifier = [aDecoder decodeDoubleForKey:kNewnewCagChildId];
    self.pic = [aDecoder decodeObjectForKey:kNewnewCagChildPic];
    self.newnewChildCag = [aDecoder decodeObjectForKey:kNewnewCagChildNewnewChildCag];
    self.name = [aDecoder decodeObjectForKey:kNewnewCagChildName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_newnewCagChildIdentifier forKey:kNewnewCagChildId];
    [aCoder encodeObject:_pic forKey:kNewnewCagChildPic];
    [aCoder encodeObject:_newnewChildCag forKey:kNewnewCagChildNewnewChildCag];
    [aCoder encodeObject:_name forKey:kNewnewCagChildName];
}

- (id)copyWithZone:(NSZone *)zone {
    NewnewCagChild *copy = [[NewnewCagChild alloc] init];
    
    
    
    if (copy) {

        copy.newnewCagChildIdentifier = self.newnewCagChildIdentifier;
        copy.pic = [self.pic copyWithZone:zone];
        copy.newnewChildCag = [self.newnewChildCag copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
