//
//  NewnewCagResults.m
//
//  Created by   on 2018/12/21
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "NewnewCagResults.h"
#import "NewnewCagChild.h"


NSString *const kNewnewCagResultsNewNotice = @"newNotice";
NSString *const kNewnewCagResultsSearchwords = @"searchwords";
NSString *const kNewnewCagResultsNewnewCagChild = @"categoryChild";


@interface NewnewCagResults ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NewnewCagResults

@synthesize newNotice = _newNotice;
@synthesize searchwords = _searchwords;
@synthesize newnewCagChild = _newnewCagChild;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.newNotice = [[self objectOrNilForKey:kNewnewCagResultsNewNotice fromDictionary:dict] doubleValue];
            self.searchwords = [self objectOrNilForKey:kNewnewCagResultsSearchwords fromDictionary:dict];
    NSObject *receivedNewnewCagChild = [dict objectForKey:kNewnewCagResultsNewnewCagChild];
    NSMutableArray *parsedNewnewCagChild = [NSMutableArray array];
    
    if ([receivedNewnewCagChild isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedNewnewCagChild) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedNewnewCagChild addObject:[NewnewCagChild modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedNewnewCagChild isKindOfClass:[NSDictionary class]]) {
       [parsedNewnewCagChild addObject:[NewnewCagChild modelObjectWithDictionary:(NSDictionary *)receivedNewnewCagChild]];
    }

    self.newnewCagChild = [NSArray arrayWithArray:parsedNewnewCagChild];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.newNotice] forKey:kNewnewCagResultsNewNotice];
    [mutableDict setValue:self.searchwords forKey:kNewnewCagResultsSearchwords];
    NSMutableArray *tempArrayForNewnewCagChild = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.newnewCagChild) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForNewnewCagChild addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForNewnewCagChild addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForNewnewCagChild] forKey:kNewnewCagResultsNewnewCagChild];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.newNotice = [aDecoder decodeDoubleForKey:kNewnewCagResultsNewNotice];
    self.searchwords = [aDecoder decodeObjectForKey:kNewnewCagResultsSearchwords];
    self.newnewCagChild = [aDecoder decodeObjectForKey:kNewnewCagResultsNewnewCagChild];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_newNotice forKey:kNewnewCagResultsNewNotice];
    [aCoder encodeObject:_searchwords forKey:kNewnewCagResultsSearchwords];
    [aCoder encodeObject:_newnewCagChild forKey:kNewnewCagResultsNewnewCagChild];
}

- (id)copyWithZone:(NSZone *)zone {
    NewnewCagResults *copy = [[NewnewCagResults alloc] init];
    
    
    
    if (copy) {

        copy.newNotice = self.newNotice;
        copy.searchwords = [self.searchwords copyWithZone:zone];
        copy.newnewCagChild = [self.newnewCagChild copyWithZone:zone];
    }
    
    return copy;
}


@end
