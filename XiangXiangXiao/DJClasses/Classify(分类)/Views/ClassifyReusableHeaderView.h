//
//  ClassifyReusableHeaderView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/21.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClassifyReusableHeaderView : UICollectionReusableView

@end

NS_ASSUME_NONNULL_END
