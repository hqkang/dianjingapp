//
//  ClassifyViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/20.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "ClassifyViewController.h"
//controllers
#import "SearchViewController.h"
#import "BaseNavigationController.h"
#import "LoginViewController.h"

//cells
#import "ClassifyCollectionViewCell.h"
//#import "ClassifyReusableHeaderView.h"
#import "leftClassifyListTableViewCell.h"
#import "RightNestedCollViewCell.h"

//views
#import "NavigationSearchView.h"
#import "CustomDefaultView.h"
#import "UIBarButtonItem+Badge.h"
#import "ClassifyReusableHeaderView.h"

//models
#import "NewnewCagResults.h"
#import "NewnewCagChild.h"
#import "NewnewChildCag.h"

static NSString *LEFTCELLID = @"LEFTCELLID";
static NSString *RIGHTCELLID = @"RIGHTCELLID";

//#define kLeftContentWidth kScreenWidth/3.5
#define kLeftContentWidth kWidth(81)

@interface ClassifyViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CustomDefaultViewDelegate,NavigationSearchBarDelegate,UITableViewDelegate,UITableViewDataSource,RightNestedCollCellDelegate>

@property (nonatomic, weak)UICollectionView *mainCollectionView;
@property (nonatomic, strong)UITableView *leftTableView;
//@property (nonatomic, weak)UICollectionView *rightCollectionView;
@property (nonatomic, weak)CustomDefaultView *defaultView;

@property (nonatomic, strong)NSMutableArray *categoryListArray;
@property (nonatomic, strong)NSMutableArray *categoryListSubArray;
@property (nonatomic, strong)NSMutableArray *categoryADListArray;

@property (nonatomic, strong)NavigationSearchView *navigationSearchBar;

@end

@implementation ClassifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:kColorFromRGB(kWhite)];
    [self configNavigation];
    [self setupUIWithRequestError:NO];
//    [self loadNewnewCategoryDataWithIsHud:YES isReload:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - <获取分类页数据新需求，包含二级分类>
-(void)loadNewnewCategoryDataWithIsHud:(BOOL)isHud isReload:(BOOL)isReload
{
    MBProgressHUD *hud = nil;
    if (isHud) {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    NSDictionary *params = @{@"apiVersion":@"2"};
    [HttpTool postWithURL:nil params:params haveHeader:NO success:^(id json) {
        [self.mainCollectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isReload) {
                    [self.categoryListArray removeAllObjects];
                    [self.categoryListSubArray removeAllObjects];
                    [self.categoryADListArray removeAllObjects];
                    //清除图片缓存
                    //                    [[SDImageCache sharedImageCache] clearDisk];
                }
                NewnewCagResults *model = [NewnewCagResults modelObjectWithDictionary:dict[@"results"]];
                //搜索热词
                self.navigationSearchBar.searchRecommendContent = model.searchwords;
                
                [self.categoryListArray addObjectsFromArray:model.newnewCagChild];
                if (self.categoryListArray.count>0) {
                    NewnewCagChild *tempModel = self.categoryListArray[0];
                    self.categoryListSubArray = [NSMutableArray arrayWithArray:tempModel.newnewChildCag];
                }
                
                //                [self.categoryADListArray addObjectsFromArray:model.categoryResult.categoryResultAdList];
                [self setupUIWithRequestError:NO];
                [self.mainCollectionView reloadData];
                [self.leftTableView reloadData];
                //                [self.rightCollectionView reloadData];
                [self.leftTableView setContentOffset:CGPointMake(0, 0) animated:YES];
                //                [self.rightCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
                if (self.categoryListArray.count>0) {
                    //默认选中第一个
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.leftTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                }
            }else{
                [self setupUIWithRequestError:YES];
                [MBProgressHUD bwm_showTitle:dict[@"msg"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self setupUIWithRequestError:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.mainCollectionView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        [self setupUIWithRequestError:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}


#pragma mark - <配置navigation>
-(void)configNavigation
{
    //search
    self.navigationItem.titleView = self.navigationSearchBar;
}


#pragma mark - <搜索通知响应>
-(void)btnNotiAction:(UIButton *)sender
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        [self presentLoginVC];
        return;
    }
}

#pragma mark - <懒加载*****************************************>
-(NSMutableArray *)categoryListArray
{
    if (!_categoryListArray) {
        _categoryListArray = [NSMutableArray array];
    }
    return _categoryListArray;
}
-(NSMutableArray *)categoryListSubArray
{
    if (!_categoryListSubArray) {
        _categoryListSubArray = [NSMutableArray array];
    }
    return _categoryListSubArray;
}
-(NSMutableArray *)categoryADListArray
{
    if (!_categoryADListArray) {
        _categoryADListArray = [NSMutableArray array];
    }
    return _categoryADListArray;
}
-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"出错啦～请稍后重试";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}

-(NavigationSearchView *)navigationSearchBar
{
    if (!_navigationSearchBar) {
        NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
        navigationSearchBar.delegate = self;
        navigationSearchBar.isHideBtnSearch = YES;
        _navigationSearchBar = navigationSearchBar;
    }
    return _navigationSearchBar;
}
-(UITableView *)leftTableView
{
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTableView.bounces = NO;
        _leftTableView.backgroundColor = [UIColor whiteColor];
        //        _leftTableView.rowHeight = (kLeftContentWidth)/2.0;
        _leftTableView.rowHeight = (kLeftContentWidth)/1.5;
        
        UINib *nibLeftClassifyCell = [UINib nibWithNibName:NSStringFromClass([leftClassifyListTableViewCell class]) bundle:nil];
        [_leftTableView registerNib:nibLeftClassifyCell forCellReuseIdentifier:NSStringFromClass([leftClassifyListTableViewCell class])];
        
        //        [self.view addSubview:_leftTableView];
    }
    return _leftTableView;
}
-(UICollectionView *)mainCollectionView
{
    if (!_mainCollectionView) {
        UICollectionViewFlowLayout *flowLY = [[UICollectionViewFlowLayout alloc]init];
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLY];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        //        collectionView.showsVerticalScrollIndicator = NO;
        //        if (@available(iOS 11.0, *)) {
        //            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        //        }
        
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:LEFTCELLID];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:RIGHTCELLID];
        [collectionView registerClass:[RightNestedCollViewCell class] forCellWithReuseIdentifier:NSStringFromClass([RightNestedCollViewCell class])];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ClassifyReusableHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([ClassifyReusableHeaderView class])];
        [self.view addSubview:collectionView];
        
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself loadNewnewCategoryDataWithIsHud:YES isReload:YES];
        }];
        collectionView.mj_header = header;
        
        _mainCollectionView = collectionView;
    }
    return _mainCollectionView;
}

#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    if (isError) {
        //        [self.collectionView removeFromSuperview];
        //        [self.leftTableView removeFromSuperview];
        [self.mainCollectionView removeFromSuperview];
        
        [self.defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
    }else{
        [self.defaultView removeFromSuperview];
        
        [self.mainCollectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        //        kWeakSelf(self);
        //        [self.leftTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        //            make.top.left.bottom.mas_equalTo(0);
        //            make.width.mas_equalTo(kLeftContentWidth);
        //        }];
        //        [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        //            make.left.mas_equalTo(weakself.leftTableView.mas_right);
        //            make.top.right.bottom.mas_equalTo(0);
        //        }];
    }
}

#pragma mark - <跳转商品列表>
//-(void)jumpToGoodsListVCWithType:(VCType)vcType titleStr:(NSString *)titleStr clsID:(NSString *)clsID
//{
//    SearchResultViewController *goodsListVC = [[SearchResultViewController alloc]init];
//    goodsListVC.vcType = vcType;
//    goodsListVC.titleStr = titleStr;
//    goodsListVC.clsID = clsID;
//    [self.navigationController pushViewController:goodsListVC animated:YES];
//}

#pragma mark - <跳转“登录”页面>
-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}
#pragma mark - <跳转搜索页>
-(void)jumpToSearchVCWithRecommdWords:(NSString *)recommdWord
{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.recommdWords = recommdWord;
    [self.navigationController pushViewController:searchVC animated:YES];
}



#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource*********************************>
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger num = 0;
    if (collectionView == self.mainCollectionView) {
        num = 1;
    }
    //    else if (collectionView ==self.rightCollectionView){
    //        num = 1;
    //    }
    return num;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if (collectionView == self.mainCollectionView) {
        number = 2;
    }
    //    else if (collectionView ==self.rightCollectionView){
    //        switch (section) {
    //            case 0:{
    //                number = self.categoryListSubArray.count;
    //            }
    //                break;
    //                //        case 1:{number = self.categoryADListArray.count;}
    //                //            break;
    //
    //            default:
    //                break;
    //        }
    //    }
    
    return number;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.mainCollectionView) {
        switch (indexPath.item) {
            case 0:
            {
                UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LEFTCELLID forIndexPath:indexPath];
                [cell.contentView addSubview:self.leftTableView];
                CGRect frame = self.leftTableView.frame;
                CGRect cellFrame = cell.contentView.frame;
                frame = CGRectMake(0, 0, cellFrame.size.width, cellFrame.size.height);
                self.leftTableView.frame = frame;
                return cell;
            }
                break;
            case 1:
            {
                RightNestedCollViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RightNestedCollViewCell class]) forIndexPath:indexPath];
                cell.delegate = self;
                cell.dataArray = self.categoryListSubArray;
                return cell;
            }
                break;
                
            default:
            {
                UICollectionViewCell *cell = [[UICollectionViewCell alloc]init];
                return cell;
            }
                break;
        }
    }
    
    UICollectionViewCell *cell = [[UICollectionViewCell alloc]init];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeZero;
    if (collectionView == self.mainCollectionView) {
        switch (indexPath.item) {
            case 0:{itemSize = CGSizeMake(kLeftContentWidth, self.view.frame.size.height);}
                break;
            case 1:{itemSize = CGSizeMake(kScreenWidth-kLeftContentWidth, self.view.frame.size.height);}
                break;
                
            default:
                break;
        }
    }
    
    return itemSize;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    kWeakSelf(self);
    UICollectionReusableView *reusableView = [[UICollectionReusableView alloc]init];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        ClassifyReusableHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([ClassifyReusableHeaderView class]) forIndexPath:indexPath];
        reusableView = headerView;
    }
    
    return reusableView;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize itemSize = CGSizeZero;
    itemSize = CGSizeMake(kScreenWidth, 160);
    return itemSize;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = CGFLOAT_MIN;
    //    if (collectionView == self.rightCollectionView) {
    //        space = 20.f;
    //    }
    return space;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    return insets;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (collectionView == self.rightCollectionView) {
    
    //    }
}
#pragma mark - <UITableViewDelegate,UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger number = 0;
    if (tableView == self.leftTableView) {
        number = 1;
    }
    return number;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if (tableView == self.leftTableView) {
        number = self.categoryListArray.count;
    }
    return number;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if (tableView == self.leftTableView) {
        //        height = (kLeftContentWidth)/2.0;
        height = (kLeftContentWidth)/1.5;
    }
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.leftTableView) {
        return 0.1f;
    }else{
        return 30;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (tableView == self.leftTableView) {
        if (indexPath.row<self.categoryListArray.count) {
            leftClassifyListTableViewCell *cellLeftClassify = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([leftClassifyListTableViewCell class])];
            NewnewCagChild *model = self.categoryListArray[indexPath.row];
            NSString *cagNameFirst = model.name;
            cellLeftClassify.labelName.text = cagNameFirst;
            cell = cellLeftClassify;
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row<self.categoryListArray.count) {
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        NewnewCagChild *model = self.categoryListArray[indexPath.row];
        [self.categoryListSubArray removeAllObjects];
        self.categoryListSubArray = [NSMutableArray arrayWithArray:model.newnewChildCag];
        NSIndexPath *tempIndexpath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.mainCollectionView reloadItemsAtIndexPaths:@[tempIndexpath]];
    }
}

#pragma mark - <CustomDefaultViewDelegate---------->
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView
{
    [self loadNewnewCategoryDataWithIsHud:YES isReload:YES];
}

#pragma mark - <NavigationSearchBarDelegate>
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    [self jumpToSearchVCWithRecommdWords:textField.placeholder];
    return NO;
}

#pragma mark - <RightNestedCollViewCellDelegate>
-(void)didSelectCategoryItemWithIndex:(NSInteger)index
{
    if (index<self.categoryListSubArray.count) {
        NewnewChildCag *model = self.categoryListSubArray[index];
        NSString *title = model.name;
        NSString *clsID = [NSString stringWithFormat:@"%.0f",model.newnewChildCagIdentifier];
//        [self jumpToGoodsListVCWithType:VCTypeGoodsList titleStr:title clsID:clsID];
    }
}

@end
