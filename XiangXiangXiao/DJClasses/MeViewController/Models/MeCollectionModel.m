//
//  MeCollectionModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MeCollectionModel.h"
#import "MeCollectionItemModel.h"

NSString *const kMeCollectionCode = @"code";
NSString *const kMeCollectionItem = @"item";

@interface MeCollectionModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MeCollectionModel

@synthesize code = _code;
@synthesize item = _item;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    self.code = [self objectOrNilForKey:kMeCollectionCode fromDictionary:dict];
    // 新增groupInfo
    NSObject *itemObject = [dict objectForKey:kMeCollectionItem];
    NSMutableArray *tempArr = [NSMutableArray array];
    if ([itemObject isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)itemObject) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                MeCollectionItemModel *info = [MeCollectionItemModel modelObjectWithDictionary:item];
                [tempArr addObject:info];
            }
        }
    }
    self.item = [NSArray arrayWithArray:tempArr];
    
    
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.code forKey:kMeCollectionCode];
    NSMutableArray *tempArrayForItemInfo = [NSMutableArray array];
    for (NSObject *subArrayObject in self.item) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForItemInfo addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForItemInfo addObject:subArrayObject];
        }
    }
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.code = [aDecoder decodeObjectForKey:kMeCollectionCode];
    self.item = [aDecoder decodeObjectForKey:kMeCollectionItem];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_code forKey:kMeCollectionCode];
    [aCoder encodeObject:_item forKey:kMeCollectionItem];
}

- (id)copyWithZone:(NSZone *)zone {
    MeCollectionModel *copy = [[MeCollectionModel alloc] init];
    
    if (copy) {
        copy.code = [self.code copyWithZone:zone];
        copy.item = [self.item copyWithZone:zone];
    }
    return copy;
}

@end
