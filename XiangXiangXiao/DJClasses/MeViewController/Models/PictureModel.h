//
//  PictureModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/4.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PictureModel : NSObject

@property (nonatomic, strong) NSString *img;

@property (nonatomic, strong) NSString *is_select;

@end

NS_ASSUME_NONNULL_END
