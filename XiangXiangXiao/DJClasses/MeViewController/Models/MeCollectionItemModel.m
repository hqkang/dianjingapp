//
//  MeCollectionItemModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MeCollectionItemModel.h"

NSString *const kMeCollectionItemH5_url = @"h5_url";
NSString *const kMeCollectionItemIos_url = @"ios_url";
NSString *const kMeCollectionItemIcon = @"icon";
NSString *const kMeCollectionItemTitle = @"title";


@interface MeCollectionItemModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MeCollectionItemModel

@synthesize h5_url = _h5_url;
@synthesize ios_url = _ios_url;
@synthesize icon = _icon;
@synthesize title = _title;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.h5_url = [self objectOrNilForKey:kMeCollectionItemH5_url fromDictionary:dict];
        self.ios_url = [self objectOrNilForKey:kMeCollectionItemIos_url fromDictionary:dict];
        self.icon = [self objectOrNilForKey:kMeCollectionItemIcon fromDictionary:dict];
        self.title = [self objectOrNilForKey:kMeCollectionItemTitle fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.h5_url forKey:kMeCollectionItemH5_url];
    [mutableDict setValue:self.ios_url forKey:kMeCollectionItemIos_url];
    [mutableDict setValue:self.icon forKey:kMeCollectionItemIcon];
    [mutableDict setValue:self.title forKey:kMeCollectionItemTitle];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.h5_url = [aDecoder decodeObjectForKey:kMeCollectionItemH5_url];
    self.ios_url = [aDecoder decodeObjectForKey:kMeCollectionItemIos_url];
    self.icon = [aDecoder decodeObjectForKey:kMeCollectionItemIcon];
    self.title = [aDecoder decodeObjectForKey:kMeCollectionItemTitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_h5_url forKey:kMeCollectionItemH5_url];
    [aCoder encodeObject:_ios_url forKey:kMeCollectionItemIos_url];
    [aCoder encodeObject:_icon forKey:kMeCollectionItemIcon];
    [aCoder encodeObject:_title forKey:kMeCollectionItemTitle];
}

- (id)copyWithZone:(NSZone *)zone {
    MeCollectionItemModel *copy = [[MeCollectionItemModel alloc] init];
    
    if (copy) {
        copy.h5_url = [self.h5_url copyWithZone:zone];
        copy.ios_url = [self.ios_url copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
    }
    return copy;
}

@end
