//
//  MeCollectionModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class MeCollectionItemModel;
@interface MeCollectionModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,strong) NSString *code;//"类型",
@property (nonatomic,strong) NSArray <MeCollectionItemModel *>*item;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
