//
//  MeCollectionItemModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MeCollectionItemModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,strong) NSString *h5_url;//"H5链接",
@property (nonatomic,strong) NSString *icon;//"图标",
@property (nonatomic,strong) NSString *ios_url;//"原生链接",
@property (nonatomic,strong) NSString *title;//"标题",

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
