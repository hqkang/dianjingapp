//
//  MediaModel.h
//  SchoolOnline
//
//  Created by xhkj on 2019/3/22.
//  Copyright © 2019年 DD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MediaModel : NSObject

@property (nonatomic, strong) NSString *img;

@property (nonatomic, strong) NSString *contentStr;

@property (nonatomic, strong) NSString *is_select;

@end

NS_ASSUME_NONNULL_END
