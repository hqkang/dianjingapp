//
//  MeHeaderView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/29.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MeHeaderView.h"
#import "UIColor+Extension.h"

@implementation MeHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.authBgView.layer addSublayer:[UIColor setGradualChangingColor:self.authBgView fromColor:@"FC67CA" toColor:@"FF6161"]];
}

- (IBAction)meHeaderViewBtnAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(meHeaderViewButtonDidClick:)]) {
        [self.delegate meHeaderViewButtonDidClick:sender];
    }
}


@end
