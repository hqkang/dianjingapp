//
//  PicCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/4.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "PicCollectionViewCell.h"

@implementation PicCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(PictureModel *)model
{
    if (_model != model) {
        _model = model;
    }
    self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
    if ([model.img containsString:@"http"]) {
        self.closeBtn.hidden = NO;
        [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:model.img] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
            }else{
                self.bgImageView.image = image;
            }
        }];
    }else{
        if ([model.is_select isEqualToString:@"1"]) {
            self.closeBtn.hidden = NO;
            [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.img)] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (error) {
                    self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
                }else{
                    self.bgImageView.image = image;
                }
            }];
        }else{
            self.closeBtn.hidden = YES;
            self.bgImageView.image = [UIImage imageNamed:model.img];
        }
        
    }
}
- (IBAction)picCollectionViewCellBtnAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(picCollectionViewCellButtonDidClick:)]) {
        [self.delegate picCollectionViewCellButtonDidClick:sender];
    }
}

@end
