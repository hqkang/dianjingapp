//
//  NestedTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, CellType) {
    MeNormalType,
    MeHaveHeaderType,
};

@protocol NestedTableViewCellDelegate<NSObject>
-(void)didSelectCustomItemAtIndexPath:(NSIndexPath *)indexPath cellType:(CellType)cellType;
@end

@interface NestedTableViewCell : UITableViewCell

@property (nonatomic, assign)id<NestedTableViewCellDelegate> delegate;
@property (nonatomic, assign)CellType cellType;
@property (nonatomic, copy)NSArray *tempDataArray;

@end

NS_ASSUME_NONNULL_END
