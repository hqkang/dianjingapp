//
//  MediaCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/2.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MediaCollectionViewCell.h"

@implementation MediaCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(MediaModel *)model
{
    if (_model != model) {
        _model = model;
    }
    self.contentLabel.text = model.contentStr;
    self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
    if ([model.img containsString:@"http"]) {
        self.shutBtn.hidden = NO;
        self.smallImageView.hidden = YES;
        [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:model.img] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
            }else{
                self.bgImageView.image = image;
            }
        }];
    }else{
        if ([model.is_select isEqualToString:@"1"]) {
            self.shutBtn.hidden = NO;
            self.smallImageView.hidden = YES;
            [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:model.img] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (error) {
                    self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
                }else{
                    self.bgImageView.image = image;
                }
            }];
        }else{
            self.shutBtn.hidden = YES;
            self.smallImageView.hidden = NO;
            self.smallImageView.image = [UIImage imageNamed:model.img];
            self.bgImageView.image = [UIImage imageNamed:@"icon_xuxian"];
        }
        
    }
}
- (IBAction)mediaCollectionViewCellBtnAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(mediaCollectionViewCellButtonDidClick:)]) {
        [self.delegate mediaCollectionViewCellButtonDidClick:sender];
    }
}

@end
