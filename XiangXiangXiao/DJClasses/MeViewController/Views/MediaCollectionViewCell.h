//
//  MediaCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/2.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol MediaCollectionViewCellDelegate <NSObject>

- (void)mediaCollectionViewCellButtonDidClick:(UIButton *)button;

@end

@interface MediaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *shutBtn;
@property (weak, nonatomic) IBOutlet UIImageView *smallImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (nonatomic, weak) id<MediaCollectionViewCellDelegate>delegate;

@property (nonatomic, copy) MediaModel *model;

@end

NS_ASSUME_NONNULL_END
