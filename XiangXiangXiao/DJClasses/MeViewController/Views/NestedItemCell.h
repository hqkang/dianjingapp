//
//  NestedItemCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeCollectionItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NestedItemCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *iconTitleLabel;

@property (nonatomic, copy) MeCollectionItemModel *model;

@end

NS_ASSUME_NONNULL_END
