//
//  PicCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/4.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol PicCollectionViewCellDelegate <NSObject>

- (void)picCollectionViewCellButtonDidClick:(UIButton *)button;

@end

@interface PicCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (nonatomic, weak) id<PicCollectionViewCellDelegate>delegate;

@property (nonatomic, copy) PictureModel *model;


@end

NS_ASSUME_NONNULL_END
