//
//  MeHaveHeaderTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MeHaveHeaderTableViewCellDelegate<NSObject>
-(void)didSelectMeHaveHeaderActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section;
@end

@interface MeHaveHeaderTableViewCell : UITableViewCell

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, weak) id<MeHaveHeaderTableViewCellDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
