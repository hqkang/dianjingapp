//
//  MeNormalTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MeNormalTableViewCell.h"
#import "NestedItemCell.h"
#import "MeCollectionItemModel.h"

@interface MeNormalTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong)UICollectionViewFlowLayout *flowLayout;

@end

@implementation MeNormalTableViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (NSArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.contentView.bounds collectionViewLayout:self.flowLayout];
        collectionView.backgroundColor = kColorFromRGB(kWhite);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.showsVerticalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
        UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([NestedItemCell class]) bundle:nil];
        [collectionView registerNib:nibCell forCellWithReuseIdentifier:NSStringFromClass([NestedItemCell class])];
        
        [self.contentView addSubview:collectionView];
        
        _collectionView = collectionView;
        [self setupUI];
    }
    return _collectionView;
}

-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        _flowLayout = flowLayout;
    }
    return _flowLayout;
}

-(void)setupUI
{
    self.contentView.backgroundColor = kColorFromRGB(kBGColor);
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(8);
        make.bottom.right.equalTo(self.contentView).offset(-8);
    }];
    
    self.collectionView.layer.cornerRadius = 8;
    self.collectionView.layer.masksToBounds = YES;
}


#pragma mark - <UICollectionViewFlowLayout*********************************>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeZero;
    itemSize = CGSizeMake((kScreenWidth - 56) / 4.0, self.contentView.frame.size.height - 16);
    return itemSize;
}


#pragma mark - <UICollectionViewDelegate,UICollectionViewDataSource****************>
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    number = self.dataArray.count;
    return number;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat space = 10.f;
    return space;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
//    insets = UIEdgeInsetsMake(8, -8, 0, 0);
//    insets = UIEdgeInsetsMake(0, 0, 0, 0);
    return insets;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[UICollectionViewCell alloc]init];
    
    NestedItemCell *itemCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([NestedItemCell class]) forIndexPath:indexPath];
    MeCollectionItemModel *model = [[MeCollectionItemModel alloc] init];
    model = self.dataArray[indexPath.item];
    itemCell.model = model;
    cell = itemCell;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *view = [[collectionView cellForItemAtIndexPath:indexPath] superview];
    MeNormalTableViewCell *cell = (MeNormalTableViewCell *)[[view superview] superview];
    UITableView *tableView = (UITableView *)[cell superview];
    NSIndexPath *tableViewIndexPath = [tableView indexPathForCell:cell];
    if ([self.delegate respondsToSelector:@selector(didSelectMeNormalActivityItemAtIndexPath:activitySection:)]) {
        [self.delegate didSelectMeNormalActivityItemAtIndexPath:indexPath activitySection:tableViewIndexPath.section];
    }
}

@end
