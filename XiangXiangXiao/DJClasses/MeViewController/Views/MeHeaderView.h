//
//  MeHeaderView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/29.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MeHeaderViewDelegate <NSObject>

- (void)meHeaderViewButtonDidClick:(UIButton *)button;

@end

@interface MeHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIView *authBgView;
@property (weak, nonatomic) IBOutlet UIView *authView;
@property (weak, nonatomic) IBOutlet UIImageView *portraintImageView;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *focusLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIImageView *authImageView;

@property (nonatomic, weak) id<MeHeaderViewDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
