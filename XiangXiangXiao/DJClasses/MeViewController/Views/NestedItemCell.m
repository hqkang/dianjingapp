//
//  NestedItemCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "NestedItemCell.h"

@implementation NestedItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(MeCollectionItemModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.icon)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.iconTitleLabel.text = [NSString stringWithFormat:@"%@", model.title];
}

@end
