//
//  MeNormalTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MeNormalTableViewCellDelegate<NSObject>
-(void)didSelectMeNormalActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section;
@end

@interface MeNormalTableViewCell : UITableViewCell

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, weak) id<MeNormalTableViewCellDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
