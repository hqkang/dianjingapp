//
//  EditViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/6.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *editTextField;

@property (nonatomic, strong) NSString *editStr;

@property (nonatomic, strong) NSString *identify;

@end

NS_ASSUME_NONNULL_END
