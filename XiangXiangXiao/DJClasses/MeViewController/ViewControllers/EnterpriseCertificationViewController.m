//
//  EnterpriseCertificationViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/7.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "EnterpriseCertificationViewController.h"
#import <TZImagePickerController.h>
#import "MediaModel.h"
#import "MediaCollectionViewCell.h"
#import "CommonWebViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "SDPhotoBrowser.h"

#define ITEMSPACE (8)
#define imageItemW ((kScreenWidth - 2 * ITEMSPACE) / 4)
@interface EnterpriseCertificationViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TZImagePickerControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate, SDPhotoBrowserDelegate, MediaCollectionViewCellDelegate>

@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) UICollectionView *collectionView;

// mediaViewConstraint
@property (weak, nonatomic) IBOutlet UITextField *enterpriseNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *enterpriseContactTextField;
@property (weak, nonatomic) IBOutlet UITextField *enterprisePersonTextField;
@property (weak, nonatomic) IBOutlet UIView *mediaView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mediaViewConstraint;

@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (weak, nonatomic) IBOutlet UIView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstraint;

@property (nonatomic, assign) int selectImageCount;

@property (nonatomic, assign) CGFloat originCollectionH;

@property (nonatomic, assign) CGFloat originScrollH;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *imageDataArray;

@property (nonatomic, strong) NSMutableArray *imageKeyArray;

@property (nonatomic, strong) NSMutableArray *imageUrlArray;

@end

@implementation EnterpriseCertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"企业认证";
    self.selectImageCount = 0;
    [self setupOriginData];
    [self setupUI];
    [self setupNavigationBarItem];
    if (imageItemW + 16 > 90) {
        self.originScrollH += imageItemW + 16 - 90;
        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90;
        self.collectionView.height += imageItemW + 16 - 90;
        self.originCollectionH = self.collectionView.height;
    }
    
    if (self.originScrollH + kNavigationBarHeight > kScreenHeight) {
        self.scrollViewConstraint.constant = self.originScrollH + kNavigationBarHeight - kScreenHeight;
    }
}

#pragma mark - 设置原始数据
- (void)setupOriginData
{
    NSArray *array = [NSArray array];
    array = @[@{@"img" : @"icon_tupian", @"contentStr" : @"添加图片", @"is_select" : @"0"}];
    for (int i = 0; i < array.count; i++) {
        MediaModel *model = [[MediaModel alloc] init];
        [model setValuesForKeysWithDictionary:array[i]];
        [self.dataArray addObject:model];
    }
}

#pragma mark - 设置UI
- (void)setupUI
{
    self.collectionView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - 懒加载
- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)imageDataArray
{
    if (_imageDataArray == nil) {
        _imageDataArray = [NSMutableArray array];
    }
    return _imageDataArray;
}

- (NSMutableArray *)imageKeyArray
{
    if (_imageKeyArray == nil) {
        _imageKeyArray = [NSMutableArray array];
    }
    return _imageKeyArray;
}

- (NSMutableArray *)imageUrlArray
{
    if (_imageUrlArray == nil) {
        _imageUrlArray = [NSMutableArray array];
    }
    return _imageUrlArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


#pragma mark - 创建导航栏右侧提交按钮
- (void)setupNavigationBarItem{
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"去认证" forState:0];
    [self.rightBtn setTitleColor:kColorFromRGB(kNameColor) forState:UIControlStateNormal];
    self.rightBtn.frame = CGRectMake(0, 0, 70, 44);
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.rightBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.rightBtn addTarget:self action:@selector(enterpriseCertificationAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
}

#pragma mark - 企业去认证
- (void)enterpriseCertificationAction
{
    if ([self.enterpriseNameTextField.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"企业名称不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.enterprisePersonTextField.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"企业联系人不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    
    if ([self.enterpriseContactTextField.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"企业联系方式不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.enterpriseContactTextField.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"请输入正确的联系人电话" toView:self.view hideAfter:kDelay];
        return;
    }
    if (self.imageKeyArray.count == 0) {
        [MBProgressHUD bwm_showTitle:@"营业执照不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if (!self.selectedBtn.selected) {
        [MBProgressHUD bwm_showTitle:@"请先阅读并同意企业认证说明" toView:self.view hideAfter:kDelay];
        return;
    }
    if (self.imageKeyArray.count == 0) {
        [MBProgressHUD bwm_showTitle:@"请先上传企业证件" toView:self.view hideAfter:kDelay];
        return;
    }
    
    self.rightBtn.enabled = NO;
    [self goCertificationAction];
    
}

#pragma mark - 去认证
- (void)goCertificationAction
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"company";
    params[@"name"] = self.enterpriseNameTextField.text;
    params[@"phone"] = self.enterpriseContactTextField.text;
    params[@"rep"] = self.enterprisePersonTextField.text;
    if (self.imageKeyArray.count > 0) {
        params[@"business"] = self.imageKeyArray;
    }
    [HttpTool postWithURL:kURL(kAuthUrl) params:params haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - 是否同意认证说明
- (IBAction)agreeBtnAction:(UIButton *)sender {
    self.selectedBtn.selected = !self.selectedBtn.selected;
}

#pragma mark - 展示认证说明
- (IBAction)showInstructionAction:(UIButton *)sender {
    self.selectedBtn.selected = YES;
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    web.params = LOGINDATA;
    web.userDic = USERDATA;
    if (kUserDefaultObject(kProtocol)) {
        NSDictionary *urlDic = [NSDictionary dictionary];
        urlDic = kUserDefaultObject(kProtocol);
        web.urlStr = urlDic[@"company_auth"];
        [self.navigationController pushViewController:web animated:YES];
    }
}

#pragma mark - UICollectionView懒加载
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(ITEMSPACE, ITEMSPACE, kScreenWidth - 2 * ITEMSPACE, self.mediaView.frame.size.height - 2 * ITEMSPACE) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MediaCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([MediaCollectionViewCell class])];
        [self.mediaView addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemW = (kScreenWidth - 2 * ITEMSPACE) / 4;
    return CGSizeMake(itemW, itemW);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    MediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MediaCollectionViewCell class]) forIndexPath:indexPath];
    MediaModel *model = [[MediaModel alloc] init];
    model = self.dataArray[indexPath.item];
    cell.model = model;
    cell.delegate = self;
    cell.shutBtn.tag = indexPath.item;
    gridcell = cell;
    return gridcell;
}

#pragma mark - <UICollectionViewDelegateFlowLayout>
#pragma mark - X间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark - Y间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return ITEMSPACE;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaModel *model = [[MediaModel alloc] init];
    model = self.dataArray[indexPath.item];
    if (indexPath.item == 0){
        if ([model.is_select isEqualToString:@"0"]) {
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = 0;
            browser.delegate = self;
            [browser show];
        }
        
    }else{
        if ([model.is_select isEqualToString:@"0"]) {
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 - self.selectImageCount delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = indexPath.item;
            browser.delegate = self;
            [browser show];
        }
    }
}

- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}

- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.imageUrlArray[index]]];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    self.selectImageCount += (int)photos.count;
    self.imageDataArray = nil;
    for (int i = 0; i < photos.count; i++) {
        NSData *imageData = UIImageJPEGRepresentation(photos[i], 0.1);
        [self.imageDataArray addObject:imageData];
    }
    [self uploadImage];
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < self.imageDataArray.count; i++) {
            [formData appendPartWithFileData:self.imageDataArray[i] name:[NSString stringWithFormat:@"img%d", i ] fileName:[NSString stringWithFormat:@"img%d.png", i] mimeType:@"image/png"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                for (int i = 0; i < self.dataArray.count; i++) {
                    MediaModel *model = [[MediaModel alloc] init];
                    model = self.dataArray[i];
                    if ([model.is_select isEqualToString:@"0"]) {
                        [self.dataArray removeObject:model];
                    }
                }
                for (int i = 0; i < [dic allKeys].count; i++) {
                    NSString *key = [NSString stringWithFormat:@"img%d", i];
                    MediaModel *model = [[MediaModel alloc] init];
                    model.img = dic[key][@"url"];
                    [self.imageKeyArray addObject:dic[key][@"key"]];
                    [self.imageUrlArray addObject:dic[key][@"url"]];
                    model.contentStr = @"";
                    model.is_select = @"1";
                    [self.dataArray addObject:model];
                }
                if (self.selectImageCount == 5) {
                    
                }else{
                    MediaModel *model = [[MediaModel alloc] init];
                    model.img = @"icon_tupian";
                    model.contentStr = [NSString stringWithFormat:@"%d / 3", self.selectImageCount];
                    model.is_select = @"0";
                    [self.dataArray addObject:model];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.dataArray.count > 4) {
                        self.scrollViewConstraint.constant = self.originScrollH + imageItemW + ITEMSPACE;
                        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90 + imageItemW + ITEMSPACE;
                        self.collectionView.height = self.originCollectionH + imageItemW + ITEMSPACE;
                    }
                    [self.collectionView reloadData];
                });
                
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos
{
    if (isSelectOriginalPhoto) {
        [[TZImageManager manager] getOriginalPhotoWithAsset:assets[0] completion:^(UIImage *photo, NSDictionary *info) {
            
        }];
    }
    NSLog(@"选择的是: %@", infos);
}




#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}


- (void)mediaCollectionViewCellButtonDidClick:(UIButton *)button
{
    [self.dataArray removeObjectAtIndex:button.tag];
    if (self.imageKeyArray.count > 0) {
        [self.imageKeyArray removeObjectAtIndex:button.tag];
    }
    if (self.imageUrlArray.count > 0) {
        [self.imageUrlArray removeObjectAtIndex:button.tag];
    }
    
    self.selectImageCount--;
    if (self.dataArray.count <= 3) {
        self.scrollViewConstraint.constant = self.originScrollH;
        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90;
        self.collectionView.height = self.originCollectionH;
    }
    if (self.selectImageCount == 0) {
        [self.dataArray removeObjectAtIndex:0];
        MediaModel *model = [[MediaModel alloc] init];
        model.img = @"icon_tupian";
        model.contentStr = @"添加图片";
        model.is_select = @"0";
        [self.dataArray addObject:model];
    }else if (self.selectImageCount == 4){
        MediaModel *model = [[MediaModel alloc] init];
        model.img = @"icon_tupian";
        model.contentStr = [NSString stringWithFormat:@"%d / 3", self.selectImageCount];
        model.is_select = @"0";
        [self.dataArray addObject:model];
    }else{
        MediaModel *model = [[MediaModel alloc] init];
        model = self.dataArray[self.dataArray.count - 1];
        model.img = @"icon_tupian";
        model.contentStr = [NSString stringWithFormat:@"%d / 3", self.selectImageCount];;
        model.is_select = @"0";
        [self.dataArray replaceObjectAtIndex:self.dataArray.count - 1 withObject:model];
    }
    [self.collectionView reloadData];
}


@end
