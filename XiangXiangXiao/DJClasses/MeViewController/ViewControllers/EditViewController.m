//
//  EditViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/6.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "EditViewController.h"
#import <RongIMKit/RongIMKit.h>

@interface EditViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UIButton *doneBtn;

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBarButton];
    self.editTextField.text = self.editStr;
    [self.editTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - 动态监测textfield
- (void)textFieldDidChange
{
    if (![self.editTextField.text isEqualToString:self.editStr] && self.editTextField.text.length > 0) {
        // 128 91 204
        self.doneBtn.backgroundColor = kColorFromRGB(kCateTitleColor);
        self.doneBtn.enabled = YES;
        [self.doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else
    {
        self.doneBtn.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
        self.doneBtn.enabled = NO;
        [self.doneBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1] forState:UIControlStateNormal];
    }
}

#pragma mark - 视图即将出现
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    // 让输入框成为第一响应者
    [self.editTextField becomeFirstResponder];
}

#pragma mark - 视图即将消失
- (void)viewWillDisappear:(BOOL)animated
{
    // 回收键盘
    [self.editTextField resignFirstResponder];
}

#pragma mark - 设置左右导航栏按钮
- (void)setupNavigationBarButton
{
    // 左边取消按钮
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:NSLocalizedString(@"cancel", nil) forState:0];
    [cancelBtn setTitleColor:kColorFromRGB(kTextColor) forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(0, 0, 60, 44);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    
    cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cancelBtn addTarget:self action:@selector(dismissVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelBtn];
    
    // 右边完成按钮
    self.doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.doneBtn setTitle:NSLocalizedString(@"done", nil) forState:0];
    [self.doneBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1] forState:UIControlStateNormal];
    self.doneBtn.frame = CGRectMake(0, 0, 60, 30);
    self.doneBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.doneBtn.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    self.doneBtn.layer.cornerRadius = 5;
    self.doneBtn.layer.masksToBounds = YES;
    [self.doneBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.doneBtn];
    self.doneBtn.enabled = NO;
}

#pragma mark - 取消按钮响应方法
- (void)dismissVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - 完成按钮响应方法
- (void)doneAction
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[self.identify] = self.editTextField.text;
    [HttpTool postWithURL:kURL(kUpdateProfile) params:params haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                // 获取更新后的用户信息
                [self getUserData];
            }else if([codeStr isEqualToString:@"401"]){
                
            }else{
                
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - 获取用户信息
- (void)getUserData
{
    [HttpTool postWithURL:kURL(kPersonData) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dic = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dic[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                // 头像
                NSString *avatar = nil;
                if (dic[@"avatar"] == [NSNull null]) {
                    avatar = @"";
                }else{
                    avatar = [NSString stringWithFormat:@"%@", dic[@"avatar"]];
                }
                // 电话
                NSString *phone = nil;
                if (dic[@"phone"] == [NSNull null]) {
                    phone = @"";
                }else{
                    phone = [NSString stringWithFormat:@"%@", dic[@"phone"]];
                }
                // 昵称 nickname
                NSString *nickname = nil;
                if (dic[@"nickname"] == [NSNull null]) {
                    nickname = @"";
                }else{
                    nickname = [NSString stringWithFormat:@"%@", dic[@"nickname"]];
                }
                // 用户名 username
                NSString *username = nil;
                if (dic[@"username"] == [NSNull null]) {
                    username = @"";
                }else{
                    username = [NSString stringWithFormat:@"%@", dic[@"username"]];
                }
                // 用户id
                NSString *userID = nil;
                if (dic[@"id"] == [NSNull null]) {
                    userID = @"";
                }else{
                    userID = [NSString stringWithFormat:@"%@", dic[@"id"]];
                }
                // balance 金钱
                NSString *balanceStr = nil;
                if (dic[@"balance"] == [NSNull null]) {
                    balanceStr = @"";
                }else{
                    balanceStr = [NSString stringWithFormat:@"%@", dic[@"balance"]];
                }
                // subscrib_count 关注数
                NSString *subscrib_count = nil;
                if (dic[@"subscrib_count"] == [NSNull null]) {
                    subscrib_count = @"";
                }else{
                    subscrib_count = [NSString stringWithFormat:@"%@", dic[@"subscrib_count"]];
                }
                // fans_count 粉丝数
                NSString *fans_count = nil;
                if (dic[@"fans_count"] == [NSNull null]) {
                    fans_count = @"";
                }else{
                    fans_count = [NSString stringWithFormat:@"%@", dic[@"fans_count"]];
                }
                // 融云token rongcloud_token
                NSString *rongcloud_token = nil;
                if (dic[@"rongcloud_token"] == [NSNull null]) {
                    rongcloud_token = @"";
                }else{
                    rongcloud_token = [NSString stringWithFormat:@"%@", dic[@"rongcloud_token"]];
                }
                // 云信token yunxin_token
                NSString *yunxin_token = nil;
                if (dic[@"yunxin_token"] == [NSNull null]) {
                    yunxin_token = @"";
                }else{
                    yunxin_token = [NSString stringWithFormat:@"%@", dic[@"yunxin_token"]];
                }
                /*
                 "is_auth" = @"student";
                 "is_merchant" = 0;
                 "is_sign_teacher" = 0;
                 "is_student" = 0;
                 "is_teacher" = 0;
                 */
                NSString *is_enterprise = [NSString stringWithFormat:@"%@", dic[@"is_enterprise"]];
                NSString *is_merchant = [NSString stringWithFormat:@"%@", dic[@"is_merchant"]];
                NSString *is_sign_teacher = [NSString stringWithFormat:@"%@", dic[@"is_sign_teacher"]];
                NSString *is_student = [NSString stringWithFormat:@"%@", dic[@"is_student"]];
                NSString *is_teacher = [NSString stringWithFormat:@"%@", dic[@"is_teacher"]];
                // 登录token logintoken
                NSString *loginToken = kUserDefaultObject(kUserLoginToken);
                NSDictionary *dictInfo = @{
                                           @"avatar":avatar,
                                           @"phone":phone,
                                           @"nickname":nickname,
                                           @"username":username,
                                           @"userID":userID,
                                           @"subscrib_count":subscrib_count,
                                           @"fans_count":fans_count,
                                           @"rongcloud_token":rongcloud_token,
                                           @"yunxin_token":yunxin_token,
                                           @"loginToken":loginToken,
                                           @"is_enterprise":is_enterprise,
                                           @"is_sign_teacher":is_sign_teacher,
                                           @"is_student":is_student,
                                           @"is_teacher":is_teacher,
                                           @"balance":balanceStr
                                           };
                
                NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dic];
                kUserDefaultRemoveObject(kTotalUserInfo);
                kUserDefaultSetObject(userEncodedObject, kTotalUserInfo);
                
                //保存用户信息
                kUserDefaultSetObject(dictInfo, kUserInfo);
                kUserDefaultSynchronize;
                RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
                userInfo.name = dictInfo[@"nickname"];
                [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:[NSString stringWithFormat:@"%@", kUserDefaultObject(kUserID)]];
                [RCIM sharedRCIM].currentUserInfo = userInfo;
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                [MBProgressHUD bwm_showTitle:dic[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.editTextField) {
        //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else if (self.editTextField.text.length >= 16) {
            self.editTextField.text = [textField.text substringToIndex:16];
            return NO;
        }
    }
    return YES;
}

@end
