//
//  StudentDataEditViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "StudentDataEditViewController.h"
#import "QKProfessionalPickerView.h"
#import "QKSexPickerView.h"
#import <RongIMKit/RongIMKit.h>
#import "UIColor+Extension.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import <TZImagePickerController.h>

@interface StudentDataEditViewController ()<UITextFieldDelegate, TZImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *userNickNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userAgeTextField;
@property (weak, nonatomic) IBOutlet UILabel *professionalLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;

@property (nonatomic, strong) UIButton *saveBtn;

@property (nonatomic, strong) UIAlertController *alert;

@property (nonatomic, strong) NSMutableArray *professionalArray;

@property (nonatomic, strong) NSString *selectProfessionalID;

@property (nonatomic, strong) NSMutableArray *sexArray;

@property (nonatomic, strong) NSString *sexID;

@property (nonatomic, strong) NSString *uploadKeyStr;

@property (nonatomic, strong) UIImage *selectedImage;

@property (nonatomic, strong) NSData *selectedImageData;

@property (nonatomic, strong) NSURL *selectedImageUrl;

@property (nonatomic, strong) NSString *nickName;

@property (nonatomic, strong) NSString *age;

@property (nonatomic, assign) BOOL isImageChange;

@property (nonatomic, assign) BOOL isProfessionalChange;

@property (nonatomic, assign) BOOL isSexChange;

@end

@implementation StudentDataEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人信息";
    [self setupNavigationBarItem];
    self.isImageChange = NO;
    self.isProfessionalChange = NO;
    self.isSexChange = NO;
    [self startAllRequest];
}

- (NSMutableArray *)sexArray
{
    if (_sexArray == nil) {
        _sexArray = [NSMutableArray arrayWithObjects:@{@"text" : @"男", @"value" : @"1"}, @{@"text" : @"女", @"value" : @"2"}, @{@"text" : @"保密", @"value" : @"0"}, nil];
    }
    return _sexArray;
}

- (NSMutableArray *)professionalArray
{
    if (_professionalArray == nil) {
        _professionalArray = [NSMutableArray array];
    }
    return _professionalArray;
}

#pragma mark - 创建导航栏右侧保存按钮
- (void)setupNavigationBarItem{
    self.saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.saveBtn setTitle:@"保存" forState:0];
    [self.saveBtn setTitleColor:kColorFromRGB(kNameColor) forState:UIControlStateNormal];
    self.saveBtn.frame = CGRectMake(0, 0, 60, 44);
    self.saveBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.saveBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.saveBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.saveBtn addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.saveBtn];
}


#pragma mark - 最终更新信息
- (void)saveAction
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (!self.isImageChange && !self.isProfessionalChange && !self.isSexChange && [self.userNickNameTextField.text isEqualToString:self.nickName] && [self.userAgeTextField.text isEqualToString:self.age]) {
        [MBProgressHUD bwm_showTitle:@"请先修改信息" toView:self.view hideAfter:kDelay];
        return;
    }
    if (self.isImageChange) {
        params[@"headimg"] = self.uploadKeyStr;
    }
    if (![self.userNickNameTextField.text isEqualToString:self.nickName]) {
        params[@"nickname"] = self.userNickNameTextField.text;
    }
    if ([self.userAgeTextField.text doubleValue] == 0) {
        [MBProgressHUD bwm_showTitle:@"请输入年龄数字格式" toView:self.view hideAfter:kDelay];
        return;
    }
    if (![self.userAgeTextField.text isEqualToString:self.age]) {
        params[@"age"] = self.userAgeTextField.text;
    }
    if (self.isProfessionalChange) {
        params[@"major"] = self.selectProfessionalID;
    }
    if (self.isSexChange) {
        params[@"sex"] = self.sexID;
    }
    self.saveBtn.enabled = NO;
    //
    [HttpTool postWithURL:kURL(kUpdateProfile) params:params haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   [self getPersonalData];
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}


- (void)getPersonalData
{
    [HttpTool postWithURL:kURL(kPersonData) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 头像
                    NSString *avatar = nil;
                    if (dict[@"avatar"] == [NSNull null]) {
                        avatar = @"";
                    }else{
                        avatar = [NSString stringWithFormat:@"%@", dict[@"avatar"]];
                    }
                    // 电话
                    NSString *phone = nil;
                    if (dict[@"phone"] == [NSNull null]) {
                        phone = @"";
                    }else{
                        phone = [NSString stringWithFormat:@"%@", dict[@"phone"]];
                    }
                    // 昵称 nickname
                    NSString *nickname = nil;
                    if (dict[@"nickname"] == [NSNull null]) {
                        nickname = @"";
                    }else{
                        nickname = [NSString stringWithFormat:@"%@", dict[@"nickname"]];
                    }
                    // 用户名 username
                    NSString *username = nil;
                    if (dict[@"username"] == [NSNull null]) {
                        username = @"";
                    }else{
                        username = [NSString stringWithFormat:@"%@", dict[@"username"]];
                    }
                    // 用户id
                    NSString *userID = nil;
                    if (dict[@"id"] == [NSNull null]) {
                        userID = @"";
                    }else{
                        userID = [NSString stringWithFormat:@"%@", dict[@"id"]];
                    }
                    // balance 金钱
                    NSString *balanceStr = nil;
                    if (dict[@"balance"] == [NSNull null]) {
                        balanceStr = @"";
                    }else{
                        balanceStr = [NSString stringWithFormat:@"%@", dict[@"balance"]];
                    }
                    // subscrib_count 关注数
                    NSString *subscrib_count = nil;
                    if (dict[@"subscrib_count"] == [NSNull null]) {
                        subscrib_count = @"";
                    }else{
                        subscrib_count = [NSString stringWithFormat:@"%@", dict[@"subscrib_count"]];
                    }
                    // fans_count 粉丝数
                    NSString *fans_count = nil;
                    if (dict[@"fans_count"] == [NSNull null]) {
                        fans_count = @"";
                    }else{
                        fans_count = [NSString stringWithFormat:@"%@", dict[@"fans_count"]];
                    }
                    // 融云token rongcloud_token
                    NSString *rongcloud_token = nil;
                    if (dict[@"rongcloud_token"] == [NSNull null]) {
                        rongcloud_token = @"";
                    }else{
                        rongcloud_token = [NSString stringWithFormat:@"%@", dict[@"rongcloud_token"]];
                    }
                    // 云信token yunxin_token
                    NSString *yunxin_token = nil;
                    if (dict[@"yunxin_token"] == [NSNull null]) {
                        yunxin_token = @"";
                    }else{
                        yunxin_token = [NSString stringWithFormat:@"%@", dict[@"yunxin_token"]];
                    }
                    /*
                     "is_auth" = @"student";
                     "is_merchant" = 0;
                     "is_sign_teacher" = 0;
                     "is_student" = 0;
                     "is_teacher" = 0;
                     */
                    NSString *is_enterprise = [NSString stringWithFormat:@"%@", dict[@"is_enterprise"]];
                    NSString *is_merchant = [NSString stringWithFormat:@"%@", dict[@"is_merchant"]];
                    NSString *is_sign_teacher = [NSString stringWithFormat:@"%@", dict[@"is_sign_teacher"]];
                    NSString *is_student = [NSString stringWithFormat:@"%@", dict[@"is_student"]];
                    NSString *is_teacher = [NSString stringWithFormat:@"%@", dict[@"is_teacher"]];
                    // 登录token logintoken
                    NSString *loginToken = kUserDefaultObject(kUserLoginToken);
                    NSDictionary *dictInfo = @{
                                               @"avatar":avatar,
                                               @"phone":phone,
                                               @"nickname":nickname,
                                               @"username":username,
                                               @"userID":userID,
                                               @"subscrib_count":subscrib_count,
                                               @"fans_count":fans_count,
                                               @"rongcloud_token":rongcloud_token,
                                               @"yunxin_token":yunxin_token,
                                               @"loginToken":loginToken,
                                               @"is_enterprise":is_enterprise,
                                               @"is_sign_teacher":is_sign_teacher,
                                               @"is_student":is_student,
                                               @"is_teacher":is_teacher,
                                               @"balance":balanceStr
                                               };
                    
                    NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dict];
                    kUserDefaultRemoveObject(kTotalUserInfo);
                    kUserDefaultSetObject(userEncodedObject, kTotalUserInfo);
                    
                    //保存用户信息
                    kUserDefaultSetObject(dictInfo, kUserInfo);
                    kUserDefaultSynchronize;
                    if (self.isImageChange) {
                        RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
                        userInfo.portraitUri = [NSString stringWithFormat:@"%@", kImgURL(self.uploadKeyStr)];
                        [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:[NSString stringWithFormat:@"%@", kUserDefaultObject(kUserID)]];
                        [RCIM sharedRCIM].currentUserInfo = userInfo;
                    }
                    if (![self.nickName isEqualToString:self.userNickNameTextField.text]) {
                        RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
                        userInfo.name =  self.nickName;
                        [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:[NSString stringWithFormat:@"%@", kUserDefaultObject(kUserID)]];
                        [RCIM sharedRCIM].currentUserInfo = userInfo;
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}



#pragma mark - 发起请求
- (void)startAllRequest
{
    // 创建信号量
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    // 创建全局并行
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    
    // 学生信息
    dispatch_group_async(group, queue, ^{
        [self getStudentData];
    });
    // 专业信息
    dispatch_group_async(group, queue, ^{
        [self getProfessionalData];
    });
    
    dispatch_group_notify(group, queue, ^{
        
        // 两个请求对应两次信号等待
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //在这里 进行请求后的方法，回到主线程
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    });
}

#pragma mark - 获取学生信息
- (void)getStudentData{
    [HttpTool postWithURL:kURL(kStudentInfo) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *headimgStr = [NSString stringWithFormat:@"%@", dict[@"headimg"]];
                    // 头像
                    if (dict[@"headimg"] && ![headimgStr isEqualToString:@"<null>"]) {
                        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", kImgURL(dict[@"headimg"])]] placeholderImage:nil options:SDWebImageRefreshCached];
                        
                    }else{
                        self.userImageView.image = [UIImage imageNamed:@"ic_image_head"];
                    }
                    // 姓名
                    self.userNameLabel.text = [NSString stringWithFormat:@"%@", dict[@"name"]];;
                    // 昵称
                    NSString *nicknameStr = [NSString stringWithFormat:@"%@", dict[@"nickname"]];
                    if (dict[@"nickname"] && ![nicknameStr isEqualToString:@"<null>"]) {
                        self.userNickNameTextField.text = nicknameStr;
                        
                    }else{
                        self.userNickNameTextField.text = @"kong";
                    }
                    //            self.userNickNameTextField.text = [NSString stringWithFormat:@"%@", dic[@"nickname"]];
                    self.nickName = nicknameStr;
                    // 年龄
                    self.userAgeTextField.text = [NSString stringWithFormat:@"%@", dict[@"age"]];
                    self.age = [NSString stringWithFormat:@"%@", dict[@"age"]];
                    // 专业
                    NSString *collegeindtypeStr = [NSString stringWithFormat:@"%@", dict[@"collegeindtype"][@"name"]];
                    if (dict[@"collegeindtype"][@"name"] && ![collegeindtypeStr isEqualToString:@"<null>"]) {
                        self.professionalLabel.text = [NSString stringWithFormat:@"%@", dict[@"collegeindtype"][@"name"]];
                        
                    }else{
                        self.professionalLabel.text = @"";
                    }
                    // 性别
                    NSString *sexStr = [NSString stringWithFormat:@"%@", dict[@"sex"]];
                    if ([sexStr isEqualToString:@"<null>"] || [sexStr isEqualToString:@"0"]) {
                        self.sexLabel.text = @"保密";
                    }else{
                        if ([sexStr isEqualToString:@"1"]) {
                            self.sexLabel.text = @"男";
                        }else if ([sexStr isEqualToString:@"2"]){
                            self.sexLabel.text = @"女";
                        }
                        
                    }
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 上传图片
- (void)uploadRequest
{
    NSString *headerStr = [NSString stringWithFormat:@"%@", kUserDefaultObject(kUserLoginToken)];
    
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.selectedImageData name:@"img" fileName:@"img" mimeType:@"multipart/form-data"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [NSNotificationCenter.defaultCenter postNotificationName:@"userUploadAvatarSuccess" object:nil];
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                self.uploadKeyStr = dic[@"img"][@"key"];
                self.isImageChange = YES;
                
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [self presentLoginVC];
            }else{
                
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - 获取专业信息
- (void)getProfessionalData{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"is_condition";
    [HttpTool postWithURL:kURL(kMajorInfo) params:params haveHeader:NO success:^(id json) {
        if (json) {
            NSArray *array = [NSArray array];
            array = (NSArray *)json;
            self.professionalArray = [NSMutableArray arrayWithArray:array];
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 头像选择
- (IBAction)showImageSelect:(UIButton *)sender {
    [self changeUserImage];
}

#pragma mark -- 修改头像方法
- (void)changeUserImage
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    // 图片选择器状态栏字体颜色
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
    imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
    // 图片选择器返回/取消字体颜色
    imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
    // 导航栏返回按钮图标颜色
    imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
    
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    self.selectedImageData = UIImageJPEGRepresentation(photos[0], 0.1);
    [self uploadImage];
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.selectedImageData name:@"img" fileName:@"img.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                self.uploadKeyStr = dic[@"img"][@"url"];
                [self.userImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.uploadKeyStr)] placeholderImage:nil options:SDWebImageRefreshCached];
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - 专业选择
- (IBAction)showProfessionalSelect:(UIButton *)sender {
//    [QKPublicUtility showAutomaticDisappearAlertViewWithTipInformation:@"专业选择"];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.professionalArray.count > 0) {
        QKProfessionalPickerView *pickView = [[QKProfessionalPickerView alloc] initWithFrame:[UIScreen mainScreen].bounds withDataArray:self.professionalArray];
        pickView.hideWhenTapGrayView = YES;
        pickView.columns = 2;    //
        [pickView showInView:self.view];
        pickView.pickBlock = ^(NSDictionary * _Nonnull dic) {
            NSLog(@"%@", dic);
            self.isProfessionalChange = YES;
            self.professionalLabel.text = dic[@"secondProfessional"];
            self.selectProfessionalID = dic[@"secondProfessionalID"];
        };
    }
}

#pragma mark - 性别选择
- (IBAction)showSexSelect:(UIButton *)sender {
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.sexArray.count > 0) {
        QKSexPickerView *pickView = [[QKSexPickerView alloc] initWithFrame:[UIScreen mainScreen].bounds withDataArray:self.sexArray];
        pickView.hideWhenTapGrayView = YES;
        pickView.columns = 1;
        [pickView showInView:self.view];
        pickView.pickBlock = ^(NSDictionary * _Nonnull dic) {
            NSLog(@"%@", dic);
            self.isSexChange = YES;
            self.sexLabel.text = dic[@"text"];
            self.sexID = dic[@"textValue"];
        };
    }
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}

@end
