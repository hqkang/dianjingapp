//
//  EnterpriseDataEditViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/3.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EnterpriseDataEditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UITextField *enterpriseNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UITextField *ownerNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *enterprisePhoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UIView *pictureView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoViewConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *videoBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *addVideoIconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *videoPlayIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end

NS_ASSUME_NONNULL_END
