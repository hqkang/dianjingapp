//
//  FeedBackViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/2.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "FeedBackViewController.h"
#import <TZImagePickerController.h>
#import "MediaModel.h"
#import "MediaCollectionViewCell.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "SDPhotoBrowser.h"

#define ITEMSPACE (8)
#define imageItemW ((kScreenWidth - 2 * ITEMSPACE) / 4)
@interface FeedBackViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TZImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate, MediaCollectionViewCellDelegate, SDPhotoBrowserDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign) int selectImageCount;

@property (nonatomic, assign) CGFloat originCollectionH;

@property (nonatomic, assign) CGFloat originScrollH;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *imageDataArray;

@property (nonatomic, strong) NSMutableArray *imageKeyArray;

@property (nonatomic, strong) NSMutableArray *imageUrlArray;

@property (nonatomic, strong) NSMutableDictionary *publishParams;

@property (nonatomic, strong) UIButton *publishBtn;

@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"意见反馈";
    self.selectImageCount = 0;
    [self setupOriginData];
    [self setupUI];
    [self setupNavigationBarItem];
    self.originScrollH = self.scrollViewConstraint.constant;
    if (imageItemW + 16 > 90) {
        self.originScrollH += imageItemW + 16 - 90;
        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90;
        self.collectionView.height += imageItemW + 16 - 90;
        self.originCollectionH = self.collectionView.height;
    }
    
    if (self.originScrollH + kNavigationBarHeight > kScreenHeight) {
        self.scrollViewConstraint.constant = self.originScrollH + kNavigationBarHeight - kScreenHeight;
    }
    [self getFeedBackData];
}

#pragma mark - 导航栏右侧发送按钮
- (void)setupNavigationBarItem{
    self.publishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.publishBtn setTitle:@"发送" forState:0];
    [self.publishBtn setTitleColor:kColorFromRGB(kNameColor) forState:UIControlStateNormal];
    self.publishBtn.frame = CGRectMake(0, 0, 60, 44);
    self.publishBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.publishBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.publishBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.publishBtn addTarget:self action:@selector(feedBackAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.publishBtn];
}

#pragma mark - 发送反馈
- (void)feedBackAction
{
    if ([self.feedBackTextView.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"反馈内容不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    self.publishBtn.enabled = NO;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"content"] = [NSString stringWithFormat:@"%@", self.feedBackTextView.text];
    if (self.imageKeyArray.count > 0) {
        params[@"pic"] = self.imageKeyArray;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool postWithURL:kURL(kFeedBackUrl) params:params haveHeader:YES success:^(id json) {
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
                self.publishBtn.enabled = YES;
            }else{
                self.publishBtn.enabled = YES;
            }
        }
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        self.publishBtn.enabled = YES;
    }];
}

#pragma mark - 获取客服电话和工作时间
- (void)getFeedBackData
{
    [HttpTool getWithURL:kURL(kGetFeedBackData) params:nil haveHeader:NO success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.phoneLabel.text = [NSString stringWithFormat:@"%@", dict[@"service_phone"]];
                    self.workTimeLabel.text = [NSString stringWithFormat:@"%@", dict[@"service_worktime"]];
                });
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 设置原始数据
- (void)setupOriginData
{
    NSArray *array = [NSArray array];
    array = @[@{@"img" : @"icon_tupian", @"contentStr" : @"添加图片", @"is_select" : @"0"}];
    for (int i = 0; i < array.count; i++) {
        MediaModel *model = [[MediaModel alloc] init];
        [model setValuesForKeysWithDictionary:array[i]];
        [self.dataArray addObject:model];
    }
}

#pragma mark - 设置UI
- (void)setupUI
{
    self.collectionView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - 懒加载
- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)imageDataArray
{
    if (_imageDataArray == nil) {
        _imageDataArray = [NSMutableArray array];
    }
    return _imageDataArray;
}

- (NSMutableArray *)imageKeyArray
{
    if (_imageKeyArray == nil) {
        _imageKeyArray = [NSMutableArray array];
    }
    return _imageKeyArray;
}

- (NSMutableArray *)imageUrlArray
{
    if (_imageUrlArray == nil) {
        _imageUrlArray = [NSMutableArray array];
    }
    return _imageUrlArray;
}


#pragma mark - UICollectionView懒加载
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(ITEMSPACE, ITEMSPACE, kScreenWidth - 2 * ITEMSPACE, self.mediaView.frame.size.height - 2 * ITEMSPACE) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MediaCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([MediaCollectionViewCell class])];
        [self.mediaView addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemW = (kScreenWidth - 2 * ITEMSPACE) / 4;
    return CGSizeMake(itemW, itemW);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    MediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MediaCollectionViewCell class]) forIndexPath:indexPath];
    MediaModel *model = [[MediaModel alloc] init];
    model = self.dataArray[indexPath.item];
    cell.model = model;
    cell.delegate = self;
    cell.shutBtn.tag = indexPath.item;
    gridcell = cell;
    return gridcell;
}

#pragma mark - <UICollectionViewDelegateFlowLayout>
#pragma mark - X间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark - Y间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return ITEMSPACE;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaModel *model = [[MediaModel alloc] init];
    model = self.dataArray[indexPath.item];
    if (indexPath.item == 0){
        if ([model.is_select isEqualToString:@"0"]) {
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:5 delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = 0;
            browser.delegate = self;
            [browser show];
        }
        
    }else{
        if ([model.is_select isEqualToString:@"0"]) {
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:5 - self.selectImageCount delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = indexPath.item;
            browser.delegate = self;
            [browser show];
        }
    }
}

- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}

- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.imageUrlArray[index]]];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    self.selectImageCount += (int)photos.count;
    self.imageDataArray = nil;
    for (int i = 0; i < photos.count; i++) {
        NSData *imageData = UIImageJPEGRepresentation(photos[i], 0.1);
        [self.imageDataArray addObject:imageData];
    }
    [self uploadImage];
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < self.imageDataArray.count; i++) {
            [formData appendPartWithFileData:self.imageDataArray[i] name:[NSString stringWithFormat:@"img%d", i ] fileName:[NSString stringWithFormat:@"img%d.png", i] mimeType:@"image/png"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                for (int i = 0; i < self.dataArray.count; i++) {
                    MediaModel *model = [[MediaModel alloc] init];
                    model = self.dataArray[i];
                    if ([model.is_select isEqualToString:@"0"]) {
                        [self.dataArray removeObject:model];
                    }
                }
                for (int i = 0; i < [dic allKeys].count; i++) {
                    NSString *key = [NSString stringWithFormat:@"img%d", i];
                    MediaModel *model = [[MediaModel alloc] init];
                    model.img = dic[key][@"url"];
                    [self.imageKeyArray addObject:dic[key][@"key"]];
                    [self.imageUrlArray addObject:dic[key][@"url"]];
                    model.contentStr = @"";
                    model.is_select = @"1";
                    [self.dataArray addObject:model];
                    
                }
                if (self.selectImageCount == 5) {
                    
                }else{
                    MediaModel *model = [[MediaModel alloc] init];
                    model.img = @"icon_tupian";
                    model.contentStr = [NSString stringWithFormat:@"%d / 5", self.selectImageCount];
                    model.is_select = @"0";
                    [self.dataArray addObject:model];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.dataArray.count > 4) {
                        self.scrollViewConstraint.constant = self.originScrollH + imageItemW + ITEMSPACE;
                        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90 + imageItemW + ITEMSPACE;
                        self.collectionView.height = self.originCollectionH + imageItemW + ITEMSPACE;
                    }
                    [self.collectionView reloadData];
                });
                
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos
{
    if (isSelectOriginalPhoto) {
        [[TZImageManager manager] getOriginalPhotoWithAsset:assets[0] completion:^(UIImage *photo, NSDictionary *info) {
            
        }];
    }
    NSLog(@"选择的是: %@", infos);
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.placeHolderLabel.hidden = YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length == 0) {
        if (textView.isFirstResponder) {
            self.placeHolderLabel.hidden = YES;
        }else
        {
            self.placeHolderLabel.hidden = NO;
        }
        
    }else
    {
        self.placeHolderLabel.hidden = YES;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        self.placeHolderLabel.hidden = NO;
    }else
    {
        self.placeHolderLabel.hidden = YES;
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}


#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)mediaCollectionViewCellButtonDidClick:(UIButton *)button
{
    [self.dataArray removeObjectAtIndex:button.tag];
    if (self.imageKeyArray.count > 0) {
        [self.imageKeyArray removeObjectAtIndex:button.tag];
    }
    if (self.imageUrlArray.count > 0) {
        [self.imageUrlArray removeObjectAtIndex:button.tag];
    }
    
    self.selectImageCount--;
    if (self.dataArray.count <= 3) {
        self.scrollViewConstraint.constant = self.originScrollH;
        self.mediaViewConstraint.constant = 108 + imageItemW + 16 - 90;
        self.collectionView.height = self.originCollectionH;
    }
    if (self.selectImageCount == 0) {
        [self.dataArray removeObjectAtIndex:0];
        MediaModel *model = [[MediaModel alloc] init];
        model.img = @"icon_tupian";
        model.contentStr = @"添加图片";
        model.is_select = @"0";
        [self.dataArray addObject:model];
    }else if (self.selectImageCount == 4){
        MediaModel *model = [[MediaModel alloc] init];
        model.img = @"icon_tupian";
        model.contentStr = [NSString stringWithFormat:@"%d / 5", self.selectImageCount];
        model.is_select = @"0";
        [self.dataArray addObject:model];
    }else{
        MediaModel *model = [[MediaModel alloc] init];
        model = self.dataArray[self.dataArray.count - 1];
        model.img = @"icon_tupian";
        model.contentStr = [NSString stringWithFormat:@"%d / 5", self.selectImageCount];;
        model.is_select = @"0";
        [self.dataArray replaceObjectAtIndex:self.dataArray.count - 1 withObject:model];
    }
    [self.collectionView reloadData];
}

#pragma mark - 致电客服
- (IBAction)phoneAction:(UIButton *)sender {
    if (self.phoneLabel.text.length > 0) {
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"tel:%@", self.phoneLabel.text];
        UIWebView *callWebview = [[UIWebView alloc] init];
        [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    }
}




@end
