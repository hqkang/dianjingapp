//
//  SettingViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/11/23.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "SettingViewController.h"
#import <XHNetworkCache.h>
#import "FeedBackViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "CommonWebViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "CustomAlert.h"
#import "StudentDataEditViewController.h"
#import "OrdinaryDataEditViewController.h"
#import "EnterpriseDataEditViewController.h"

@interface SettingViewController ()
@property (weak, nonatomic) IBOutlet UILabel *cacheSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
//    float cacheSize = [XHNetworkCache cacheSize];
//    self.cacheSizeLabel.text = [NSString stringWithFormat:@"%.2f", cacheSize];
    self.cacheSizeLabel.text = [NSString stringWithFormat:@"%.2fM", [self readCacheSize]];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // app名称
    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    // app build版本
    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
    self.versionLabel.text = [NSString stringWithFormat:@"%@", app_Version];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - 进入个人信息编辑
- (IBAction)pushEditPersonalInfoVC:(UIButton *)sender {
    if (kUserDefaultObject(kUserInfo)) {
        NSDictionary *userInfo = [NSDictionary dictionary];
        userInfo = kUserDefaultObject(kUserInfo);
        if ([userInfo[@"is_student"] isEqualToString:@"1"]) {
            StudentDataEditViewController *stuDataEditVC = [[StudentDataEditViewController alloc] init];
            [self.navigationController pushViewController:stuDataEditVC animated:YES];
        }else if ([userInfo[@"is_enterprise"] isEqualToString:@"1"]){
            EnterpriseDataEditViewController *enterpriseDataEditVC = [[EnterpriseDataEditViewController alloc] init];
            [self.navigationController pushViewController:enterpriseDataEditVC animated:YES];
        }else{
            OrdinaryDataEditViewController *ordinaryDataEditVC = [[OrdinaryDataEditViewController alloc] init];
            [self.navigationController pushViewController:ordinaryDataEditVC animated:YES];
        }
    }
}

#pragma mark - 清除缓存
- (IBAction)clearCacheData:(UIButton *)sender {
    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"是否清除缓存" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"是" actionCancelName:@"否" actionConfirm:^{
        [self clearFile];
    } actionCancel:nil];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - 用户反馈
- (IBAction)feedBackAction:(UIButton *)sender {
    FeedBackViewController *feedbackVC = [[FeedBackViewController alloc] init];
    [self.navigationController pushViewController:feedbackVC animated:YES];
    
}
#pragma mark - 退出账号
- (IBAction)logoutAction:(UIButton *)sender {
    
    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"是否要退出" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"是" actionCancelName:@"否" actionConfirm:^{
        kUserDefaultRemoveObject(kTotalUserInfo);
        kUserDefaultRemoveObject(kUserInfo);
        kUserDefaultRemoveObject(kLoginData);
        kUserDefaultRemoveObject(kUserLoginToken);
        kUserDefaultRemoveObject(kYunXinToken);
        kUserDefaultRemoveObject(kRCTOKEN);
        kUserDefaultRemoveObject(kUserID);
        kUserDefaultRemoveObject(kRefreshToken);
        [QK_NOTICENTER postNotificationName:kLoginStatusChange object:nil userInfo:nil];
        [self.navigationController popViewControllerAnimated:YES];
    } actionCancel:nil];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - 关于我们
- (IBAction)aboutUsAction:(UIButton *)sender {
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    if (kUserDefaultObject(kProtocol)) {
        NSDictionary *protocolDic = kUserDefaultObject(kProtocol);
        web.urlStr = protocolDic[@"about"];
        [self.navigationController pushViewController:web animated:YES];
    }
}


#pragma mark - 获取缓存大小
- (float)readCacheSize
{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    return [self folderSizeAtPath:cachePath];
}


// 由于缓存文件存在沙箱中，我们可以通过NSFileManager API来实现对缓存文件大小的计算。
// 遍历文件夹获得文件夹大小，返回多少 M
- (float)folderSizeAtPath:(NSString *)folderPath{
    
    NSFileManager *manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0 ;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString *fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil ){
        //获取文件全路径
        NSString *fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    
    return folderSize / (1024.0 * 1024.0);
    
}



// 计算 单个文件的大小
- (long long)fileSizeAtPath:(NSString *)filePath{
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error: nil] fileSize];
    }
    return 0;
}



// 清除缓存
- (void)clearFile
{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachePath];
    //NSLog ( @"cachpath = %@" , cachePath);
    for ( NSString *p in files) {
        
        NSError *error = nil ;
        //获取文件全路径
        NSString *fileAbsolutePath = [cachePath stringByAppendingPathComponent:p];
        
        if ([[NSFileManager defaultManager ] fileExistsAtPath:fileAbsolutePath]) {
            [[NSFileManager defaultManager ] removeItemAtPath:fileAbsolutePath error:&error];
        }
    }
    
    //读取缓存大小
    float cacheSize = [self readCacheSize] * 1024;
    self.cacheSizeLabel.text = [NSString stringWithFormat:@"%.2fM", cacheSize / 1024.0];
    
}


@end
