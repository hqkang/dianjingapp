//
//  StudentCertificationViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/6.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "StudentCertificationViewController.h"
#import "QKSchoolPickerView.h"
#import "CommonWebViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AFNetworking.h>
#import "UIColor+Extension.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import <TZImagePickerController.h>

@interface StudentCertificationViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, TZImagePickerControllerDelegate>

@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) UIAlertController *alert;
@property (weak, nonatomic) IBOutlet UIView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (weak, nonatomic) IBOutlet UIView *frontIdentityCardView;
@property (weak, nonatomic) IBOutlet UIView *backIdentityCardView;
@property (weak, nonatomic) IBOutlet UIView *stuCertificationCardView;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (weak, nonatomic) IBOutlet UIView *uploadFrontView;
@property (weak, nonatomic) IBOutlet UIView *uploadBackView;
@property (weak, nonatomic) IBOutlet UIView *uploadCertificationView;
@property (weak, nonatomic) IBOutlet UIImageView *frontIdentityCardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backIdentityCardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *stuCertificationImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstrait;

@property (nonatomic, strong) NSMutableArray *schoolArray;

// 照片类型
@property (nonatomic, strong) NSString *identifyStr;

@property (nonatomic, strong) UIImage *selectedImage;

@property (nonatomic, strong) NSData *selectedImageData;

@property (nonatomic, strong) NSString *selectedImageUrl;

@property (nonatomic, strong) NSString *uploadFrontKey;
@property (nonatomic, strong) NSString *uploadBackKey;
@property (nonatomic, strong) NSString *uploadStuKey;

@property (nonatomic, strong) NSNumber *schoolID;

@end

@implementation StudentCertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"学生认证";
    [self setupNavigationBarItem];
    if (self.scrollView.frame.size.height + kNavigationBarHeight > kScreenHeight) {
        self.scrollViewConstrait.constant = self.scrollView.frame.size.height + kNavigationBarHeight - kScreenHeight;
    }
}

- (NSMutableArray *)schoolArray
{
    if (_schoolArray == nil) {
        _schoolArray = [NSMutableArray array];
    }
    return _schoolArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self getSchoolData];
}

- (void)getSchoolData
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"no_condition";
    [HttpTool postWithURL:kURL(kSchoolDataUrl) params:params haveHeader:NO success:^(id json) {
        if (json) {
            NSArray *array = [NSArray array];
            array = (NSArray *)json;
            self.schoolArray = nil;
            self.schoolArray = [NSMutableArray arrayWithArray:array];
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - 创建导航栏右侧提交按钮
- (void)setupNavigationBarItem{
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"去认证" forState:0];
    [self.rightBtn setTitleColor:kColorFromRGB(kNameColor) forState:UIControlStateNormal];
    self.rightBtn.frame = CGRectMake(0, 0, 60, 44);
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    self.rightBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.rightBtn addTarget:self action:@selector(stuCertificationAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
}

#pragma mark - 导航栏右侧去认证按钮响应方法
- (void)stuCertificationAction
{
    if ([self.nameTextField.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"名字不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.schoolNameLabel.text isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"学校不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.uploadFrontKey isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"身份证正面照片不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.uploadBackKey isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"身份证反面照片不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([self.uploadStuKey isEmpty]) {
        [MBProgressHUD bwm_showTitle:@"学生证照片不能为空" toView:self.view hideAfter:kDelay];
        return;
    }
    if (!self.selectedBtn.selected) {
        [MBProgressHUD bwm_showTitle:@"请先阅读并同意享享校学生认证说明" toView:self.view hideAfter:kDelay];
        return;
    }
    self.rightBtn.enabled = NO;
    [self goCertificationAction];
}



#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - 认证
- (void)goCertificationAction
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"student";
    params[@"realname"] = self.nameTextField.text;
    params[@"school_id"] = [NSString stringWithFormat:@"%@", self.schoolID];
    params[@"idcard1"] = self.uploadFrontKey;
    params[@"idcard2"] = self.uploadBackKey;
    params[@"studentcard"] = self.uploadStuKey;
    [HttpTool postWithURL:kURL(kAuthUrl) params:params haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                [self.navigationController popViewControllerAnimated:YES];
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
                self.rightBtn.enabled = YES;
            }else
            {
                self.rightBtn.enabled = YES;
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        self.rightBtn.enabled = YES;
    }];
}


#pragma mark - 是否勾选认证说明
- (IBAction)agreeBtnAction:(UIButton *)sender {
    self.selectedBtn.selected = !self.selectedBtn.selected;
}

#pragma mark - 展示享享校学生认证说明
- (IBAction)showInstructionAction:(UIButton *)sender {
    self.selectedBtn.selected = YES;
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    web.params = LOGINDATA;
    web.userDic = USERDATA;
    if (kUserDefaultObject(kProtocol)) {
        NSDictionary *urlDic = [NSDictionary dictionary];
        urlDic = kUserDefaultObject(kProtocol);
        web.urlStr = urlDic[@"student_auth"];
        [self.navigationController pushViewController:web animated:YES];
    }
}

#pragma mark - 学校选择
- (IBAction)selectSchoolAction:(UIButton *)sender {
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.schoolArray.count > 0) {
        QKSchoolPickerView *pickView = [[QKSchoolPickerView alloc] initWithFrame:[UIScreen mainScreen].bounds withDataArray:self.schoolArray];
        pickView.hideWhenTapGrayView = YES;
        pickView.columns = 2;
        [pickView showInView:self.view];
        pickView.pickBlock = ^(NSDictionary * _Nonnull dic) {
            NSLog(@"%@", dic);
            self.schoolNameLabel.text = dic[@"school"];
            self.schoolID = dic[@"schoolID"];
        };
    }
}

#pragma mark - 删除身份证正面照
- (IBAction)deleteFrontImage:(UIButton *)sender {
    self.uploadFrontView.hidden = YES;
    self.frontIdentityCardView.hidden = NO;
}

#pragma mark - 删除身份证反面照
- (IBAction)deleteBackImage:(UIButton *)sender {
    self.uploadBackView.hidden = YES;
    self.backIdentityCardView.hidden = NO;
}

#pragma mark - 删除学生证照
- (IBAction)deleteStuCertificationImage:(UIButton *)sender {
    self.uploadCertificationView.hidden = YES;
    self.stuCertificationCardView.hidden = NO;
}

#pragma mark - 选择身份证正面照
- (IBAction)selectFrontImage:(UIButton *)sender {
    self.identifyStr = @"front";
    [self selectImage];
}
#pragma mark - 选择身份证反面照
- (IBAction)selectBackImage:(id)sender {
    self.identifyStr = @"back";
    [self selectImage];
}
#pragma mark - 选择学生证
- (IBAction)selectStuImage:(UIButton *)sender {
    self.identifyStr = @"student";
    [self selectImage];
}

#pragma mark - 选择图片
- (void)selectImage
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    // 图片选择器状态栏字体颜色
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
    imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
    // 图片选择器返回/取消字体颜色
    imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
    // 导航栏返回按钮图标颜色
    imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
    
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    self.selectedImageData = UIImageJPEGRepresentation(photos[0], 0.1);
    [self uploadImage];
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.selectedImageData name:@"img" fileName:@"img.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                if ([self.identifyStr isEqualToString:@"front"]) {
                    self.uploadFrontKey = dic[@"img"][@"url"];
                    [self.frontIdentityCardImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.uploadFrontKey)] placeholderImage:nil options:SDWebImageRefreshCached];
                    self.frontIdentityCardView.hidden = YES;
                    self.uploadFrontView.hidden = NO;
                }else if ([self.identifyStr isEqualToString:@"back"]){
                    self.uploadBackKey = dic[@"img"][@"url"];
                    [self.backIdentityCardImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.uploadBackKey)] placeholderImage:nil options:SDWebImageRefreshCached];
                    self.backIdentityCardView.hidden = YES;
                    self.uploadBackView.hidden = NO;
                }else if ([self.identifyStr isEqualToString:@"student"]){
                    self.uploadStuKey = dic[@"img"][@"url"];
                    [self.stuCertificationImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.uploadStuKey)] placeholderImage:nil options:SDWebImageRefreshCached];
                    self.stuCertificationCardView.hidden = YES;
                    self.uploadCertificationView.hidden = NO;
                }
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}

@end
