//
//  EnterpriseDataEditViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/3.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "EnterpriseDataEditViewController.h"
#import "PictureModel.h"
#import "PicCollectionViewCell.h"
#import "SDPhotoBrowser.h"
#import <TZImagePickerController.h>
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "ArrayPickerView.h"

#define ITEMSPACE (8)
#define imageItemW ((kScreenWidth - 32 - 2 * ITEMSPACE) / 3)
@interface EnterpriseDataEditViewController ()<TZImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SDPhotoBrowserDelegate, PicCollectionViewCellDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign) int selectImageCount;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *imageDataArray;

@property (nonatomic, strong) NSMutableArray *imageKeyArray;

@property (nonatomic, strong) NSMutableArray *imageUrlArray;

@property (nonatomic, strong) NSMutableArray *enterprisCountArray;

@property (nonatomic, strong) NSString *identifyStr;

@property (nonatomic, strong) NSData *logoData;

@property (nonatomic, strong) NSString *logoKey;

@property (nonatomic, strong) NSString *videoType;

@property (nonatomic, strong) NSData *videoImageData;

@property (nonatomic, strong) NSURL *videoUrl;

@property (nonatomic, strong) NSString *videoUrlStr;

@property (nonatomic, strong) NSString *fileName;

@property (nonatomic, strong) NSString *videoKey;

@end

@implementation EnterpriseDataEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"企业信息";
    self.selectImageCount = 0;
    if (self.scrollView.frame.size.height + kNavigationBarHeight > kScreenHeight) {
        self.scrollViewConstraint.constant = self.scrollView.frame.size.height + kNavigationBarHeight - kScreenHeight;
    }
    self.pictureViewConstraint.constant = imageItemW + 16;
    self.scrollViewConstraint.constant += imageItemW - 90;
    [self getEnterpriseData];
}

#pragma mark - 获取企业信息
- (void)getEnterpriseData{
    [HttpTool postWithURL:kURL(kCompanyInfo) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                
                if (dict[@"model"][@"banner"]) {
                    NSArray *imageArray = [NSArray array];
                    imageArray = dict[@"model"][@"banner"];
                    if ([imageArray isNotEmpty]) {
                        [self.dataArray removeAllObjects];
                        [self.imageKeyArray removeAllObjects];
                        [self.imageUrlArray removeAllObjects];
                        self.imageKeyArray = [NSMutableArray arrayWithArray:imageArray];
                        for (int i = 0; i < imageArray.count; i++) {
                            PictureModel *model = [[PictureModel alloc] init];
                            model.img = imageArray[i];
                            model.is_select = @"1";
                            [self.dataArray addObject:model];
                            [self.imageUrlArray addObject:kImgURL(model.img)];
                        }
                        self.selectImageCount = (int)self.dataArray.count;
                        if (self.dataArray.count == 3) {
                        }else{
                            PictureModel *model = [[PictureModel alloc] init];
                            model.img = @"icon_xuxian";
                            model.is_select = @"0";
                            [self.dataArray addObject:model];
                        }
                        [self.collectionView reloadData];
                    }
                }
                // 企业logo
                NSString *logoStr = [NSString stringWithFormat:@"%@", dict[@"model"][@"headimg"]];
                if (dict[@"model"][@"headimg"] && ![logoStr isEqualToString:@"<null>"]) {
                    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(dict[@"model"][@"headimg"])] placeholderImage:[UIImage imageNamed:@"default-thumb"] options:SDWebImageRefreshCached];
                }
                // 联系人
                if (dict[@"model"][@"contacts"]) {
                    self.ownerNameLabel.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"contacts"]];
                }
                // 企业名称
                if (dict[@"model"][@"name"]) {
                    self.enterpriseNameTextField.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"name"]];
                }
                // 联系电话
                if (dict[@"model"][@"phone"]) {
                    self.enterprisePhoneTextField.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"phone"]];
                }
                // 公司规模
                if (dict[@"model"][@"employee"]) {
                    self.countLabel.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"employee"]];
                }
               
                
                // 企业邮箱
                if (dict[@"model"][@"email"]) {
                    self.emailTextField.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"email"]];
                }
                
                
                // 地址
                if (dict[@"model"][@"address"]) {
                    self.addressTextField.text = [NSString stringWithFormat:@"%@", dict[@"model"][@"address"]];
                }
                
                // 获取公司规模可选数组
                if (dict[@"scale"]) {
                    NSArray *array = [NSArray array];
                    array = dict[@"scale"];
                    if (array.count > 0) {
                        self.enterprisCountArray = [NSMutableArray arrayWithArray:array];
                    }
                }
                if (dict[@"model"][@"video"] && ![dict[@"model"][@"video"] isEqual:[NSNull null]]) {
                    self.videoType = @"play";
                    self.deleteBtn.hidden = NO;
                    self.videoPlayIconImageView.hidden = NO;
                    self.addVideoIconImageView.hidden = YES;
                    NSDictionary *videoDic = [NSDictionary dictionary];
                    videoDic = dict[@"model"][@"video"];
                    if (videoDic[@"cover"] && ![videoDic[@"cover"] isEqual:[NSNull null]]) {
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:kImgURL(videoDic[@"cover"])]];
                        UIImage *image = [[UIImage alloc] initWithData:imageData];
                    }
                }else{
                    self.videoType = @"add";
                    self.deleteBtn.hidden = YES;
                    self.videoPlayIconImageView.hidden = YES;
                    self.addVideoIconImageView.hidden = NO;
                }
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
            }else{
                
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - 懒加载
- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)imageDataArray
{
    if (_imageDataArray == nil) {
        _imageDataArray = [NSMutableArray array];
    }
    return _imageDataArray;
}

- (NSMutableArray *)imageKeyArray
{
    if (_imageKeyArray == nil) {
        _imageKeyArray = [NSMutableArray array];
    }
    return _imageKeyArray;
}

- (NSMutableArray *)imageUrlArray
{
    if (_imageUrlArray == nil) {
        _imageUrlArray = [NSMutableArray array];
    }
    return _imageUrlArray;
}

- (NSMutableArray *)enterprisCountArray
{
    if (_enterprisCountArray == nil) {
        _enterprisCountArray = [NSMutableArray array];
    }
    return _enterprisCountArray;
}

#pragma mark - UICollectionView懒加载
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(ITEMSPACE, ITEMSPACE, self.pictureView.frame.size.width - 2 * ITEMSPACE, self.pictureView.frame.size.height - 2 * ITEMSPACE) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.backgroundColor = kColorFromRGB(kColBGColor);
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PicCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([PicCollectionViewCell class])];
        [self.pictureView addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat itemW = (kScreenWidth - 2 * ITEMSPACE) / 3;
    return CGSizeMake(imageItemW, imageItemW);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    PicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PicCollectionViewCell class]) forIndexPath:indexPath];
    PictureModel *model = [[PictureModel alloc] init];
    model = self.dataArray[indexPath.item];
    cell.model = model;
    cell.delegate = self;
    cell.closeBtn.tag = indexPath.item;
    gridcell = cell;
    return gridcell;
}

#pragma mark - <UICollectionViewDelegateFlowLayout>
#pragma mark - X间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark - Y间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return ITEMSPACE;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PictureModel *model = [[PictureModel alloc] init];
    model = self.dataArray[indexPath.item];
    if (indexPath.item == 0){
        if ([model.is_select isEqualToString:@"0"]) {
            self.identifyStr = @"pic";
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = 0;
            browser.delegate = self;
            [browser show];
        }
        
    }else{
        if ([model.is_select isEqualToString:@"0"]) {
            self.identifyStr = @"pic";
            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 - self.selectImageCount delegate:self];
            // 图片选择器状态栏字体颜色
            imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
            imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
            imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
            // 图片选择器返回/取消字体颜色
            imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
            // 导航栏返回按钮图标颜色
            imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
            
            imagePickerVc.allowTakeVideo = NO;
            imagePickerVc.allowPickingImage = YES;
            imagePickerVc.allowPickingVideo = NO;
            imagePickerVc.allowPickingOriginalPhoto = NO;
            [self presentViewController:imagePickerVc animated:YES completion:nil];
        }else{
            SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
            browser.imageCount = self.imageUrlArray.count;
            browser.sourceImagesContainerView = collectionView;
            browser.currentImageIndex = indexPath.item;
            browser.delegate = self;
            [browser show];
        }
    }
}

- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}

- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.imageUrlArray[index]]];
}

#pragma mark - 选择头像
- (IBAction)selectImageAction:(UIButton *)sender {
    self.identifyStr = @"portrait";
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    // 图片选择器状态栏字体颜色
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
    imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
    // 图片选择器返回/取消字体颜色
    imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
    // 导航栏返回按钮图标颜色
    imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
    
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    if ([self.identifyStr isEqualToString:@"portrait"]) {
        self.logoData = UIImageJPEGRepresentation(photos[0], 0.1);
        [self uploadLogoImage];
    }else{
        self.selectImageCount += (int)photos.count;
        self.imageDataArray = nil;
        for (int i = 0; i < photos.count; i++) {
            NSData *imageData = UIImageJPEGRepresentation(photos[i], 0.1);
            [self.imageDataArray addObject:imageData];
        }
        [self uploadImage];
    }
    
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < self.imageDataArray.count; i++) {
            [formData appendPartWithFileData:self.imageDataArray[i] name:[NSString stringWithFormat:@"img%d", i ] fileName:[NSString stringWithFormat:@"img%d.png", i] mimeType:@"image/png"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                for (int i = 0; i < self.dataArray.count; i++) {
                    PictureModel *model = [[PictureModel alloc] init];
                    model = self.dataArray[i];
                    if ([model.is_select isEqualToString:@"0"]) {
                        [self.dataArray removeObject:model];
                    }
                }
                for (int i = 0; i < [dic allKeys].count; i++) {
                    NSString *key = [NSString stringWithFormat:@"img%d", i];
                    PictureModel *model = [[PictureModel alloc] init];
                    model.img = dic[key][@"url"];
                    [self.imageKeyArray addObject:dic[key][@"key"]];
                    [self.imageUrlArray addObject:dic[key][@"url"]];
                    model.is_select = @"1";
                    [self.dataArray addObject:model];
                    
                }
                if (self.selectImageCount == 3) {
                    
                }else{
                    PictureModel *model = [[PictureModel alloc] init];
                    model.img = @"icon_xuxian";
                    model.is_select = @"0";
                    [self.dataArray addObject:model];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                });
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - 上传logo
- (void)uploadLogoImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.logoData name:@"img" fileName:@"img.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                self.logoKey = dic[@"img"][@"key"];
                [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.logoKey)] placeholderImage:nil options:SDWebImageRefreshCached];
                
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)picCollectionViewCellButtonDidClick:(UIButton *)button
{
    [self.dataArray removeObjectAtIndex:button.tag];
    if (self.imageKeyArray.count > 0) {
        [self.imageKeyArray removeObjectAtIndex:button.tag];
    }
    if (self.imageUrlArray.count > 0) {
        [self.imageUrlArray removeObjectAtIndex:button.tag];
    }
    
    self.selectImageCount--;
    if (self.dataArray.count == 0) {
        PictureModel *model = [[PictureModel alloc] init];
        model.img = @"icon_xuxian";
        model.is_select = @"0";
        [self.dataArray addObject:model];
    }
    
    [self.collectionView reloadData];
}

#pragma mark - 播放或者添加视频
- (IBAction)playOrSelectVideoAction:(UIButton *)sender {
    if ([self.videoType isEqualToString:@"add"]) {
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        // 图片选择器状态栏字体颜色
        imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
        imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
        imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
        // 图片选择器返回/取消字体颜色
        imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
        // 导航栏返回按钮图标颜色
        imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
        
        imagePickerVc.allowTakeVideo = YES;
        imagePickerVc.allowPickingImage = NO;
        imagePickerVc.allowPickingVideo = YES;
        // You can get the photos by block, the same as by delegate.
        // 你可以通过block或者代理，来得到用户选择的照片.
        [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
            NSLog(@"%@", coverImage);
            self.videoImageData = UIImageJPEGRepresentation(coverImage, 0.1);
            [[TZImageManager manager] getVideoWithAsset:asset completion:^(AVPlayerItem *playerItem, NSDictionary *info) {
                AVAsset *avasset = playerItem.asset;
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    self.videoPlayer.assetURL = ((AVURLAsset *)avasset).URL;
                    self.videoUrl = ((AVURLAsset *)avasset).URL;
                    self.videoUrlStr = [((AVURLAsset *)avasset).URL path];
                    NSString *fileName = [((AVURLAsset *)avasset).URL lastPathComponent];
                    NSLog(@"视频文件名: %@",fileName);
                    self.fileName = [((AVURLAsset *)avasset).URL lastPathComponent];
                    [self uploadVideoImage];
                });
            }];
        }];
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }else{
        NSLog(@"播放视频");
    }
}

#pragma mark - 删除视频
- (IBAction)deleteVideoAction:(UIButton *)sender {
    self.videoPlayIconImageView.hidden = YES;
    self.addVideoIconImageView.hidden = NO;
    self.deleteBtn.hidden = YES;
    self.videoBgImageView.image = [UIImage imageNamed:@""];
}



#pragma mark - 上传视频图片
- (void)uploadVideoImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.videoImageData name:@"img" fileName:@"img.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                self.videoKey = dic[@"img"][@"key"];
                [self.videoBgImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.videoKey)] placeholderImage:nil options:SDWebImageRefreshCached];
                self.deleteBtn.hidden = NO;
                self.videoPlayIconImageView.hidden = NO;
                self.addVideoIconImageView.hidden = YES;
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}

#pragma mark - 公司规模选择
- (IBAction)showCompanyCountSelect:(UIButton *)sender {
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    if (self.enterprisCountArray.count > 0) {
        ArrayPickerView *pickView = [[ArrayPickerView alloc] initWithFrame:[UIScreen mainScreen].bounds withDataArray:self.enterprisCountArray];
        pickView.hideWhenTapGrayView = YES;
        pickView.columns = 1;
        [pickView showInView:self.view];
        pickView.pickBlock = ^(NSDictionary * _Nonnull dic) {
            NSLog(@"%@", dic);
            self.countLabel.text = dic[@"text"];
        };
    }
}

@end
