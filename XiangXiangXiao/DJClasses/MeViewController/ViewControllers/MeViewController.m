//
//  MeViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MeViewController.h"
#import "MeNormalTableViewCell.h"
#import "MeHaveHeaderTableViewCell.h"
#import "MeCollectionModel.h"
#import "MeCollectionItemModel.h"
#import "CommonWebViewController.h"
#import "MeHeaderView.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"

static CGFloat headerViewHeight = 160;
static CGFloat authHeaderViewHeight = 100;
@interface MeViewController ()<UITableViewDelegate, UITableViewDataSource, MeNormalTableViewCellDelegate, MeHaveHeaderTableViewCellDelegate, MeHeaderViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *sectionCodeArray;

@property (nonatomic, strong) MeHeaderView *meHeaderView;

@end

@implementation MeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self configNavigation];
    self.tableView.backgroundColor = kColorFromRGB(kBGColor);
    [self setupUIData];
    [QK_NOTICENTER addObserver:self selector:@selector(setupUIData) name:kLoginStatusChange object:nil];
}

#pragma mark - 设置UI数据
- (void)setupUIData
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        [self.sectionCodeArray removeAllObjects];
        self.meHeaderView.loginBtn.hidden = NO;
        self.meHeaderView.nicknameLabel.hidden = YES;
        self.meHeaderView.focusLabel.hidden = YES;
        self.meHeaderView.authImageView.hidden = YES;
        self.meHeaderView.authView.hidden = YES;
        [self.tableView reloadData];
    }else{
        self.meHeaderView.loginBtn.hidden = YES;
        self.meHeaderView.nicknameLabel.hidden = NO;
        self.meHeaderView.focusLabel.hidden = NO;
        NSDictionary *userInfo = [NSDictionary dictionary];
        userInfo = kUserDefaultObject(kUserInfo);
        self.meHeaderView.authImageView.hidden = NO;
        self.meHeaderView.authView.hidden = YES;
        if ([userInfo[@"is_enterprise"] isEqualToString:@"1"]) {
            self.meHeaderView.authImageView.image = [UIImage imageNamed:@"student_auth"];
        }else if ([userInfo[@"is_sign_teacher"] isEqualToString:@"1"]) {
            self.meHeaderView.authImageView.image = [UIImage imageNamed:@"student_auth"];
        }else if ([userInfo[@"is_student"] isEqualToString:@"1"]) {
            self.meHeaderView.authImageView.image = [UIImage imageNamed:@"student_auth"];
        }else if ([userInfo[@"is_teacher"] isEqualToString:@"1"]) {
            self.meHeaderView.authImageView.image = [UIImage imageNamed:@"student_auth"];
        }else{
            self.meHeaderView.authImageView.hidden = YES;
            self.meHeaderView.authView.hidden = NO;
            self.meHeaderView.height = headerViewHeight;
        }
        [self.meHeaderView.portraintImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(userInfo[@"avatar"])] placeholderImage:nil options:SDWebImageRefreshCached];
        self.meHeaderView.nicknameLabel.text = userInfo[@"nickname"];
        self.meHeaderView.focusLabel.text = [NSString stringWithFormat:@"关注%@ | 粉丝%@", userInfo[@"subscrib_count"], userInfo[@"fans_count"]];
        [self loadMyDataInfo:YES];
    }
}

#pragma mark - 个人页面数据
- (void)loadMyDataInfo:(BOOL)isReload
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"type"] = @"user";
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool getWithURL:kURL(kMePageData) params:params haveHeader:YES success:^(id json) {
        [self.tableView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isReload) {
                    [self.sectionCodeArray removeAllObjects];
                }
                NSArray *results = dict[@"data"][@"icon_list"];
                if ([results isNotEmpty]) {
                    for (NSDictionary *dic in results) {
                        MeCollectionModel *model = [MeCollectionModel modelObjectWithDictionary:dic];
                        [self.sectionCodeArray addObject:model];
                    }
                    [self.tableView reloadData];
                }
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [hud hideAnimated:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

- (NSMutableArray *)sectionCodeArray
{
    if (_sectionCodeArray == nil) {
        _sectionCodeArray = [NSMutableArray array];
    }
    return _sectionCodeArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tempTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kTabBarHeight - kNavigationBarHeight - kStatusBarHeight - kTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        tempTableView.delegate = self;
        tempTableView.dataSource = self;
        [tempTableView setBackgroundColor:kColorFromRGB(kBGColor)];
        [tempTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        //        tempTableView.estimatedRowHeight = 80;
        // iOS11默认开启Self-Sizing，必须关闭Self-Sizing才能使设置分区高度生效
        tempTableView.estimatedRowHeight = 0;
        tempTableView.estimatedSectionHeaderHeight = 0;
        tempTableView.estimatedSectionFooterHeight = 0;
        
        [tempTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [tempTableView registerClass:[MeNormalTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MeNormalTableViewCell class])];
        [tempTableView registerClass:[MeHaveHeaderTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MeHaveHeaderTableViewCell class])];
        tempTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
        
        self.meHeaderView = [MeHeaderView loadFromNib];
        self.meHeaderView.frame = CGRectMake(0, 0, kScreenWidth, authHeaderViewHeight);
        self.meHeaderView.delegate = self;
        tempTableView.tableHeaderView = self.meHeaderView;
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self loadMyDataInfo:YES];
        }];
        tempTableView.mj_header = header;
        
        [self.view addSubview:tempTableView];
        _tableView = tempTableView;
    }
    return _tableView;
}


#pragma mark - <配置navigation>
- (void)configNavigation
{
    //我的
    UIButton *btnMe = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMe setFrame:CGRectMake(0, 0, 60, 30)];
    [btnMe setTitle:@"我的" forState:UIControlStateNormal];
    [btnMe setTitleColor:kColorFromRGB(kBlack) forState:UIControlStateNormal];
    btnMe.titleLabel.font = [UIFont boldSystemFontOfSize:21];
    UIBarButtonItem *barBtnItemMe = [[UIBarButtonItem alloc]initWithCustomView:btnMe];
    self.navigationItem.leftBarButtonItems = @[barBtnItemMe];
    
    //消息
    UIButton *btnMsg = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMsg setImage:[UIImage imageNamed:@"me_msg_gray"] forState:UIControlStateNormal];
    [btnMsg setFrame:CGRectMake(0, 0, 30, 30)];
    UIBarButtonItem *barBtnItemMsg = [[UIBarButtonItem alloc]initWithCustomView:btnMsg];
    
    [btnMsg addTarget:self action:@selector(btnMsgAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[barBtnItemMsg];
}

#pragma mark - 导航栏右侧消息按钮响应方法
- (void)btnMsgAction
{
    
}


#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat sectionHeight = 0;
    MeCollectionModel *model = self.sectionCodeArray[indexPath.section];
    if ([model.code isEqualToString:@"top"]) {
        sectionHeight = kScreenWidth / 4.0;
    }else if ([model.code isEqualToString:@"order"]) {
        sectionHeight = kScreenWidth / 2.0;
    }else if ([model.code isEqualToString:@"me"]) {
        sectionHeight = kScreenWidth / 4.0;
    }else if ([model.code isEqualToString:@"other"]) {
        sectionHeight = kScreenWidth / 4.0;
    }
    return sectionHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerH = 0.1;
    
    return headerH;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionCodeArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    MeNormalTableViewCell *normalCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MeNormalTableViewCell class])];
    normalCell.selectionStyle = UITableViewCellSelectionStyleNone;
    normalCell.delegate = self;
    MeHaveHeaderTableViewCell *specilCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MeHaveHeaderTableViewCell class])];
    specilCell.selectionStyle = UITableViewCellSelectionStyleNone;
    specilCell.delegate = self;
    MeCollectionModel *model = [[MeCollectionModel alloc] init];
    model = self.sectionCodeArray[indexPath.section];
    if ([model.code isEqualToString:@"top"]) {
        normalCell.dataArray = [NSMutableArray arrayWithArray:model.item];
        [[normalCell collectionView] reloadData];
        cell = normalCell;
    }else if ([model.code isEqualToString:@"order"]) {
        specilCell.dataArray = [NSMutableArray arrayWithArray:model.item];
        [[specilCell collectionView] reloadData];
        cell = specilCell;
    }else if ([model.code isEqualToString:@"me"]) {
        normalCell.dataArray = [NSMutableArray arrayWithArray:model.item];
        [[normalCell collectionView] reloadData];
        cell = normalCell;
    }else if ([model.code isEqualToString:@"other"]) {
        normalCell.dataArray = [NSMutableArray arrayWithArray:model.item];
        [[normalCell collectionView] reloadData];
        cell = normalCell;
    }
    return cell;
}

- (void)didSelectMeNormalActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section
{
    MeCollectionModel *model = [[MeCollectionModel alloc] init];
    model = self.sectionCodeArray[section];
    MeCollectionItemModel *itemModel = [[MeCollectionItemModel alloc] init];
    itemModel = model.item[indexPath.item];
    
    if (itemModel.ios_url) {
        [self.navigationController pushViewController:[[NSClassFromString(itemModel.ios_url) alloc] init] animated:YES];
    }else if ([itemModel.h5_url isNotEmpty]) {
        CommonWebViewController *web = [[CommonWebViewController alloc] init];
        web.urlStr = itemModel.h5_url;
        [self.navigationController pushViewController:web animated:YES];
    }
//    web.urlStr = itemModel.h5_url;
//    [self.navigationController pushViewController:web animated:YES];
    NSLog(@"section: %ld, item: %ld", section, indexPath.item);
}

- (void)didSelectMeHaveHeaderActivityItemAtIndexPath:(NSIndexPath *)indexPath activitySection:(NSInteger)section
{
    MeCollectionModel *model = [[MeCollectionModel alloc] init];
    model = self.sectionCodeArray[section];
    MeCollectionItemModel *itemModel = [[MeCollectionItemModel alloc] init];
    itemModel = model.item[indexPath.item];
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
//    if (itemModel.ios_url) {
//        [self.navigationController pushViewController:[[NSClassFromString(itemModel.ios_url) alloc] init] animated:YES];
//    }else if ([itemModel.h5_url isNotEmpty]) {
//        web.urlStr = itemModel.h5_url;
//        [self.navigationController pushViewController:web animated:YES];
//    }
    
    web.urlStr = itemModel.h5_url;
    [self.navigationController pushViewController:web animated:YES];
    NSLog(@"分区: %ld, 块: %ld", section, indexPath.item);
}

- (void)meHeaderViewButtonDidClick:(UIButton *)button
{
    if (button.tag == 0) {
        [self presentLoginVC];
    }else if (button.tag == 1){
        
    }
}

#pragma mark - <跳转“登录”页面>
-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}

- (void)dealloc
{
    [QK_NOTICENTER removeObserver:self name:kLoginStatusChange object:nil];
}

@end
