//
//  OrdinaryDataEditViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2019/1/3.
//  Copyright © 2019年 DD. All rights reserved.
//

#import "OrdinaryDataEditViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "UIColor+Extension.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import <TZImagePickerController.h>
@interface OrdinaryDataEditViewController ()<UITextFieldDelegate, TZImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *userImageBtn;
@property (weak, nonatomic) IBOutlet UITextField *userNickNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@property (nonatomic, assign) BOOL isImageChange;

@property (nonatomic, strong) NSString *uploadKeyStr;

@property (nonatomic, strong) NSString *nickName;

@property (nonatomic, strong) UIImage *selectedImage;

@property (nonatomic, strong) NSData *selectedImageData;

@property (nonatomic, strong) NSURL *selectedImageUrl;

@end

@implementation OrdinaryDataEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isImageChange = NO;
    self.title = @"个人信息";
    [self getPersonalData];
    
}

#pragma mark - 获取个人信息
- (void)getPersonalData
{
    [HttpTool postWithURL:kURL(kPersonData) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 头像
                    NSString *avatar = nil;
                    if (dict[@"avatar"] == [NSNull null]) {
                        avatar = @"";
                    }else{
                        avatar = [NSString stringWithFormat:@"%@", dict[@"avatar"]];
                    }
                    // 电话
                    NSString *phone = nil;
                    if (dict[@"phone"] == [NSNull null]) {
                        phone = @"";
                    }else{
                        phone = [NSString stringWithFormat:@"%@", dict[@"phone"]];
                    }
                    // 昵称 nickname
                    NSString *nickname = nil;
                    if (dict[@"nickname"] == [NSNull null]) {
                        nickname = @"";
                    }else{
                        nickname = [NSString stringWithFormat:@"%@", dict[@"nickname"]];
                    }
                    // 用户名 username
                    NSString *username = nil;
                    if (dict[@"username"] == [NSNull null]) {
                        username = @"";
                    }else{
                        username = [NSString stringWithFormat:@"%@", dict[@"username"]];
                    }
                    // 用户id
                    NSString *userID = nil;
                    if (dict[@"id"] == [NSNull null]) {
                        userID = @"";
                    }else{
                        userID = [NSString stringWithFormat:@"%@", dict[@"id"]];
                    }
                    // balance 金钱
                    NSString *balanceStr = nil;
                    if (dict[@"balance"] == [NSNull null]) {
                        balanceStr = @"";
                    }else{
                        balanceStr = [NSString stringWithFormat:@"%@", dict[@"balance"]];
                    }
                    // subscrib_count 关注数
                    NSString *subscrib_count = nil;
                    if (dict[@"subscrib_count"] == [NSNull null]) {
                        subscrib_count = @"";
                    }else{
                        subscrib_count = [NSString stringWithFormat:@"%@", dict[@"subscrib_count"]];
                    }
                    // fans_count 粉丝数
                    NSString *fans_count = nil;
                    if (dict[@"fans_count"] == [NSNull null]) {
                        fans_count = @"";
                    }else{
                        fans_count = [NSString stringWithFormat:@"%@", dict[@"fans_count"]];
                    }
                    // 融云token rongcloud_token
                    NSString *rongcloud_token = nil;
                    if (dict[@"rongcloud_token"] == [NSNull null]) {
                        rongcloud_token = @"";
                    }else{
                        rongcloud_token = [NSString stringWithFormat:@"%@", dict[@"rongcloud_token"]];
                    }
                    // 云信token yunxin_token
                    NSString *yunxin_token = nil;
                    if (dict[@"yunxin_token"] == [NSNull null]) {
                        yunxin_token = @"";
                    }else{
                        yunxin_token = [NSString stringWithFormat:@"%@", dict[@"yunxin_token"]];
                    }
                    /*
                     "is_auth" = @"student";
                     "is_merchant" = 0;
                     "is_sign_teacher" = 0;
                     "is_student" = 0;
                     "is_teacher" = 0;
                     */
                    NSString *is_enterprise = [NSString stringWithFormat:@"%@", dict[@"is_enterprise"]];
                    NSString *is_merchant = [NSString stringWithFormat:@"%@", dict[@"is_merchant"]];
                    NSString *is_sign_teacher = [NSString stringWithFormat:@"%@", dict[@"is_sign_teacher"]];
                    NSString *is_student = [NSString stringWithFormat:@"%@", dict[@"is_student"]];
                    NSString *is_teacher = [NSString stringWithFormat:@"%@", dict[@"is_teacher"]];
                    // 登录token logintoken
                    NSString *loginToken = kUserDefaultObject(kUserLoginToken);
                    NSDictionary *dictInfo = @{
                                               @"avatar":avatar,
                                               @"phone":phone,
                                               @"nickname":nickname,
                                               @"username":username,
                                               @"userID":userID,
                                               @"subscrib_count":subscrib_count,
                                               @"fans_count":fans_count,
                                               @"rongcloud_token":rongcloud_token,
                                               @"yunxin_token":yunxin_token,
                                               @"loginToken":loginToken,
                                               @"is_enterprise":is_enterprise,
                                               @"is_sign_teacher":is_sign_teacher,
                                               @"is_student":is_student,
                                               @"is_teacher":is_teacher,
                                               @"balance":balanceStr
                                               };
                    
                    NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dict];
                    kUserDefaultRemoveObject(kTotalUserInfo);
                    kUserDefaultSetObject(userEncodedObject, kTotalUserInfo);
                    
                    //保存用户信息
                    kUserDefaultSetObject(dictInfo, kUserInfo);
                    kUserDefaultSynchronize;
                    if (self.isImageChange) {
                        RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
                        userInfo.portraitUri = [NSString stringWithFormat:@"%@", kImgURL(self.uploadKeyStr)];
                        [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:[NSString stringWithFormat:@"%@", kUserDefaultObject(kUserID)]];
                        [RCIM sharedRCIM].currentUserInfo = userInfo;
                    }
                    if (![self.nickName isEqualToString:self.userNickNameTextField.text]) {
                        RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
                        userInfo.name =  self.nickName;
                        [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:[NSString stringWithFormat:@"%@", kUserDefaultObject(kUserID)]];
                        [RCIM sharedRCIM].currentUserInfo = userInfo;
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 推出登录页面
- (void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark -- 修改头像方法
- (void)changeUserImage
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    // 图片选择器状态栏字体颜色
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.naviBgColor = kColorFromRGB(kNavColor);
    imagePickerVc.naviTitleColor = kColorFromRGB(kNameColor);
    // 图片选择器返回/取消字体颜色
    imagePickerVc.barItemTextColor = kColorFromRGB(kNameColor);
    // 导航栏返回按钮图标颜色
    imagePickerVc.navigationBar.tintColor = kColorFromRGB(kNameColor);
    
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}



- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"选择的图片数组为:%@  原图:%@", photos, assets);
    self.selectedImageData = UIImageJPEGRepresentation(photos[0], 0.1);
    [self uploadImage];
}

#pragma mark - 上传图片
- (void)uploadImage
{
    NSString *headerStr = [NSString stringWithFormat:@"Bearer %@", kUserDefaultObject(kUserLoginToken)];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    [session.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:headerStr forHTTPHeaderField:@"Authorization"];
    
    [session POST:kURL(kUploadUrl) parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.selectedImageData name:@"img" fileName:@"img.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功%@",responseObject);
        NSDictionary *dic = [NSDictionary dictionary];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            dic = responseObject;
            if (!dic[@"errcode"]) {
                self.uploadKeyStr = dic[@"img"][@"url"];
                [self.userImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(self.uploadKeyStr)] placeholderImage:nil options:SDWebImageRefreshCached];
            }else if([[NSString stringWithFormat:@"%@", dic[@"errcode"]] isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                NSString *tipStr = [NSString stringWithFormat:@"%@", dic[@"message"]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败%@", error);
    }];
}

#pragma mark - 用户头像选择
- (IBAction)showImageSelect:(UIButton *)sender {
    [self changeUserImage];
}
- (IBAction)saveAction:(UIButton *)sender {
    
    [self resultSaveAction];
}

- (void)resultSaveAction
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (!self.isImageChange && [self.userNickNameTextField.text isEqualToString:self.nickName]) {
        [MBProgressHUD bwm_showTitle:@"请先修改信息" toView:self.view hideAfter:kDelay];
        return;
    }
    if (self.isImageChange) {
        params[@"headimg"] = self.uploadKeyStr;
    }
    if (![self.userNickNameTextField.text isEqualToString:self.nickName]) {
        params[@"nickname"] = self.userNickNameTextField.text;
    }
    [HttpTool postWithURL:kURL(kUpdateProfile) params:params haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   [self getPersonalData];
                });
            }else if ([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [MBProgressHUD bwm_showTitle:[NSString stringWithFormat:@"%@", dict[@"message"]] toView:self.view hideAfter:kDelay];
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}

@end
