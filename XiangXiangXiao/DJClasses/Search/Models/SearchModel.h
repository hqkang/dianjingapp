//
//  SearchModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *name;//名称
@property (nonatomic, strong) NSNumber *searchID;//id

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
