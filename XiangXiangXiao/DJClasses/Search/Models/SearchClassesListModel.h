//
//  SearchClassesListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class ClassesUserModel;
@interface SearchClassesListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double searchClassID;
@property (nonatomic,strong) NSString *title;//"标题",
@property (nonatomic,strong) NSString *cover;//"图片",
@property (nonatomic,strong) NSString *h5_url;//h5链接
@property (nonatomic,strong) NSString *price;//价格
@property (nonatomic,strong) ClassesUserModel *user;//机构

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
