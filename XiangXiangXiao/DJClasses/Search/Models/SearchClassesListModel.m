//
//  SearchClassesListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchClassesListModel.h"
#import "ClassesUserModel.h"

NSString *const kSearchClassID = @"id";
NSString *const kSearchClassTitle = @"title";
NSString *const kSearchClassCover = @"cover";
NSString *const kSearchClassH5_url = @"h5_url";
NSString *const kSearchClassPrice = @"price";
NSString *const kSearchClassUser = @"user";

@interface SearchClassesListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SearchClassesListModel

@synthesize searchClassID = _searchClassID;
@synthesize title = _title;
@synthesize cover = _cover;
@synthesize h5_url = _h5_url;
@synthesize price = _price;
@synthesize user = _user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.searchClassID = [[self objectOrNilForKey:kSearchClassID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kSearchClassTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kSearchClassCover fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kSearchClassH5_url fromDictionary:dict];
        self.price = [self objectOrNilForKey:kSearchClassPrice fromDictionary:dict];
        self.user = [ClassesUserModel modelObjectWithDictionary:[dict objectForKey:kSearchClassUser]];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.searchClassID] forKey:kSearchClassID];
    [mutableDict setValue:self.title forKey:kSearchClassTitle];
    [mutableDict setValue:self.cover forKey:kSearchClassCover];
    [mutableDict setValue:self.h5_url forKey:kSearchClassH5_url];
    [mutableDict setValue:self.price forKey:kSearchClassPrice];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kSearchClassUser];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.searchClassID = [aDecoder decodeDoubleForKey:kSearchClassID];
    self.title = [aDecoder decodeObjectForKey:kSearchClassTitle];
    self.cover = [aDecoder decodeObjectForKey:kSearchClassCover];
    self.h5_url = [aDecoder decodeObjectForKey:kSearchClassH5_url];
    self.price = [aDecoder decodeObjectForKey:kSearchClassPrice];
    self.user = [aDecoder decodeObjectForKey:kSearchClassUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_searchClassID forKey:kSearchClassID];
    [aCoder encodeObject:_title forKey:kSearchClassTitle];
    [aCoder encodeObject:_cover forKey:kSearchClassCover];
    [aCoder encodeObject:_h5_url forKey:kSearchClassH5_url];
    [aCoder encodeObject:_price forKey:kSearchClassPrice];
    [aCoder encodeObject:_user forKey:kSearchClassUser];
}

- (id)copyWithZone:(NSZone *)zone {
    SearchClassesListModel *copy = [[SearchClassesListModel alloc] init];
    
    if (copy) {
        copy.searchClassID = self.searchClassID;
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
        copy.price = [self.price copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
    }
    return copy;
}

@end
