//
//  SearchInstitutionsListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchInstitutionsListModel.h"

NSString *const kSearchInstitutionsID = @"id";
NSString *const kSearchInstitutionsTitle = @"title";
NSString *const kSearchInstitutionsCover = @"cover";
NSString *const kSearchInstitutionsH5_url = @"h5_url";

@interface SearchInstitutionsListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SearchInstitutionsListModel

@synthesize searchInstitutionsID = _searchInstitutionsID;
@synthesize title = _title;
@synthesize cover = _cover;
@synthesize h5_url = _h5_url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.searchInstitutionsID = [[self objectOrNilForKey:kSearchInstitutionsID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kSearchInstitutionsTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kSearchInstitutionsCover fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kSearchInstitutionsH5_url fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.searchInstitutionsID] forKey:kSearchInstitutionsID];
    [mutableDict setValue:self.title forKey:kSearchInstitutionsTitle];
    [mutableDict setValue:self.cover forKey:kSearchInstitutionsCover];
    [mutableDict setValue:self.h5_url forKey:kSearchInstitutionsH5_url];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.searchInstitutionsID = [aDecoder decodeDoubleForKey:kSearchInstitutionsID];
    self.title = [aDecoder decodeObjectForKey:kSearchInstitutionsTitle];
    self.cover = [aDecoder decodeObjectForKey:kSearchInstitutionsCover];
    self.h5_url = [aDecoder decodeObjectForKey:kSearchInstitutionsH5_url];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_searchInstitutionsID forKey:kSearchInstitutionsID];
    [aCoder encodeObject:_title forKey:kSearchInstitutionsTitle];
    [aCoder encodeObject:_cover forKey:kSearchInstitutionsCover];
    [aCoder encodeObject:_h5_url forKey:kSearchInstitutionsH5_url];
}

- (id)copyWithZone:(NSZone *)zone {
    SearchInstitutionsListModel *copy = [[SearchInstitutionsListModel alloc] init];
    
    if (copy) {
        copy.searchInstitutionsID = self.searchInstitutionsID;
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
    }
    return copy;
}

@end
