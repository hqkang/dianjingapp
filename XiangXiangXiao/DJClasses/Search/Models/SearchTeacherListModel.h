//
//  SearchTeacherListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchTeacherListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double searchTeacherID;
@property (nonatomic,strong) NSString *realname;//"真实姓名",
@property (nonatomic,strong) NSString *suits_photo;//"头像",
@property (nonatomic,strong) NSString *content;//"标题",
@property (nonatomic,assign) double sell_count;//"购买数",
@property (nonatomic,strong) NSString *h5_url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
