//
//  SearchInstitutionsListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchInstitutionsListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double searchInstitutionsID;
@property (nonatomic,strong) NSString *title;//"标题",
@property (nonatomic,strong) NSString *cover;//"图片",
@property (nonatomic,strong) NSString *h5_url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
