//
//  SearchModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchModel.h"

NSString *const kSearchId = @"id";
NSString *const kSearchName = @"name";

@interface SearchModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SearchModel

@synthesize name = _name;
@synthesize searchID = _searchID;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.name = [self objectOrNilForKey:kSearchName fromDictionary:dict];
        
        self.searchID = [self objectOrNilForKey:kSearchId fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kSearchName];
    [mutableDict setValue:self.searchID forKey:kSearchId];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.name = [aDecoder decodeObjectForKey:kSearchName];
    self.searchID = [aDecoder decodeObjectForKey:kSearchId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_name forKey:kSearchName];
    [aCoder encodeObject:_searchID forKey:kSearchId];
}

- (id)copyWithZone:(NSZone *)zone {
    SearchModel *copy = [[SearchModel alloc] init];
    
    if (copy) {
        copy.name = [self.name copyWithZone:zone];
        copy.searchID = self.searchID;
    }
    
    return copy;
}
@end
