//
//  TutorUserModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TutorUserModel.h"

NSString *const kTutorUserID = @"id";
NSString *const kTutorUserNickname = @"nickname";
NSString *const kTutorUserAvatar = @"avatar";


@interface TutorUserModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TutorUserModel

@synthesize userID = _userID;
@synthesize nickname = _nickname;
@synthesize avatar = _avatar;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.userID = [[self objectOrNilForKey:kTutorUserID fromDictionary:dict] doubleValue];
        self.nickname = [self objectOrNilForKey:kTutorUserNickname fromDictionary:dict];
        self.avatar = [self objectOrNilForKey:kTutorUserAvatar fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kTutorUserID];
    [mutableDict setValue:self.nickname forKey:kTutorUserNickname];
    [mutableDict setValue:self.avatar forKey:kTutorUserAvatar];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.userID = [aDecoder decodeDoubleForKey:kTutorUserID];
    self.nickname = [aDecoder decodeObjectForKey:kTutorUserNickname];
    self.avatar = [aDecoder decodeObjectForKey:kTutorUserAvatar];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_userID forKey:kTutorUserID];
    [aCoder encodeObject:_nickname forKey:kTutorUserNickname];
    [aCoder encodeObject:_avatar forKey:kTutorUserAvatar];
}

- (id)copyWithZone:(NSZone *)zone {
    TutorUserModel *copy = [[TutorUserModel alloc] init];
    
    if (copy) {
        copy.userID = self.userID;
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.avatar = [self.avatar copyWithZone:zone];
    }
    return copy;
}

@end
