//
//  ClassesUserModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "ClassesUserModel.h"

NSString *const kClassesUserID = @"id";
NSString *const kClassesUserNickname = @"nickname";
NSString *const kClassesUserAvatar = @"avatar";


@interface ClassesUserModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ClassesUserModel

@synthesize userID = _userID;
@synthesize nickname = _nickname;
@synthesize avatar = _avatar;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.userID = [[self objectOrNilForKey:kClassesUserID fromDictionary:dict] doubleValue];
        self.nickname = [self objectOrNilForKey:kClassesUserNickname fromDictionary:dict];
        self.avatar = [self objectOrNilForKey:kClassesUserAvatar fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userID] forKey:kClassesUserID];
    [mutableDict setValue:self.nickname forKey:kClassesUserNickname];
    [mutableDict setValue:self.avatar forKey:kClassesUserAvatar];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.userID = [aDecoder decodeDoubleForKey:kClassesUserID];
    self.nickname = [aDecoder decodeObjectForKey:kClassesUserNickname];
    self.avatar = [aDecoder decodeObjectForKey:kClassesUserAvatar];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_userID forKey:kClassesUserID];
    [aCoder encodeObject:_nickname forKey:kClassesUserNickname];
    [aCoder encodeObject:_avatar forKey:kClassesUserAvatar];
}

- (id)copyWithZone:(NSZone *)zone {
    ClassesUserModel *copy = [[ClassesUserModel alloc] init];
    
    if (copy) {
        copy.userID = self.userID;
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.avatar = [self.avatar copyWithZone:zone];
    }
    return copy;
}


@end
