//
//  SearchTeacherListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchTeacherListModel.h"

NSString *const kSearchTeacherID = @"id";
NSString *const kSearchTeacherRealname = @"realname";
NSString *const kSearchTeacherContent = @"content";
NSString *const kSearchTeacherSuits_photo = @"suits_photo";
NSString *const kSearchTeacherSell_count = @"sell_count";
NSString *const kSearchTeacherH5_url = @"h5_url";

@interface SearchTeacherListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SearchTeacherListModel

@synthesize searchTeacherID = _searchTeacherID;
@synthesize realname = _realname;
@synthesize content = _content;
@synthesize suits_photo = _suits_photo;
@synthesize sell_count = _sell_count;
@synthesize h5_url = _h5_url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.searchTeacherID = [[self objectOrNilForKey:kSearchTeacherID fromDictionary:dict] doubleValue];
        self.realname = [self objectOrNilForKey:kSearchTeacherRealname fromDictionary:dict];
        self.content = [self objectOrNilForKey:kSearchTeacherContent fromDictionary:dict];
        self.suits_photo = [self objectOrNilForKey:kSearchTeacherSuits_photo fromDictionary:dict];
        self.sell_count = [[self objectOrNilForKey:kSearchTeacherSell_count fromDictionary:dict] doubleValue];
        self.h5_url = [self objectOrNilForKey:kSearchTeacherH5_url fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.searchTeacherID] forKey:kSearchTeacherID];
    [mutableDict setValue:self.content forKey:kSearchTeacherContent];
    [mutableDict setValue:self.realname forKey:kSearchTeacherRealname];
    [mutableDict setValue:self.suits_photo forKey:kSearchTeacherSuits_photo];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sell_count] forKey:kSearchTeacherSell_count];
    [mutableDict setValue:self.h5_url forKey:kSearchTeacherH5_url];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.searchTeacherID = [aDecoder decodeDoubleForKey:kSearchTeacherID];
    self.realname = [aDecoder decodeObjectForKey:kSearchTeacherRealname];
    self.content = [aDecoder decodeObjectForKey:kSearchTeacherContent];
    self.suits_photo = [aDecoder decodeObjectForKey:kSearchTeacherSuits_photo];
    self.sell_count = [aDecoder decodeDoubleForKey:kSearchTeacherSell_count];
    self.h5_url = [aDecoder decodeObjectForKey:kSearchTeacherH5_url];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_searchTeacherID forKey:kSearchTeacherID];
    [aCoder encodeObject:_realname forKey:kSearchTeacherRealname];
    [aCoder encodeObject:_content forKey:kSearchTeacherContent];
    [aCoder encodeObject:_suits_photo forKey:kSearchTeacherSuits_photo];
    [aCoder encodeDouble:_sell_count forKey:kSearchTeacherSell_count];
    [aCoder encodeObject:_h5_url forKey:kSearchTeacherH5_url];
}

- (id)copyWithZone:(NSZone *)zone {
    SearchTeacherListModel *copy = [[SearchTeacherListModel alloc] init];
    
    if (copy) {
        copy.searchTeacherID = self.searchTeacherID;
        copy.realname = [self.realname copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
        copy.suits_photo = [self.suits_photo copyWithZone:zone];
        copy.sell_count = self.sell_count;
        copy.h5_url = [self.h5_url copyWithZone:zone];
    }
    return copy;
}

@end
