//
//  SearchTutorListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchTutorListModel.h"
#import "TutorUserModel.h"

NSString *const kSearchTutorID = @"id";
NSString *const kSearchTutorTitle = @"title";
NSString *const kSearchTutorCover = @"cover";
NSString *const kSearchTutorH5_url = @"h5_url";
NSString *const kSearchTutorPrice = @"price";
NSString *const kSearchTutorUser = @"user";

@interface SearchTutorListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SearchTutorListModel

@synthesize searchTutorID = _searchTutorID;
@synthesize title = _title;
@synthesize cover = _cover;
@synthesize h5_url = _h5_url;
@synthesize price = _price;
@synthesize user = _user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.searchTutorID = [[self objectOrNilForKey:kSearchTutorID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kSearchTutorTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kSearchTutorCover fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kSearchTutorH5_url fromDictionary:dict];
        self.price = [self objectOrNilForKey:kSearchTutorPrice fromDictionary:dict];
        self.user = [TutorUserModel modelObjectWithDictionary:[dict objectForKey:kSearchTutorUser]];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.searchTutorID] forKey:kSearchTutorID];
    [mutableDict setValue:self.title forKey:kSearchTutorTitle];
    [mutableDict setValue:self.cover forKey:kSearchTutorCover];
    [mutableDict setValue:self.h5_url forKey:kSearchTutorH5_url];
    [mutableDict setValue:self.price forKey:kSearchTutorPrice];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kSearchTutorUser];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.searchTutorID = [aDecoder decodeDoubleForKey:kSearchTutorID];
    self.title = [aDecoder decodeObjectForKey:kSearchTutorTitle];
    self.cover = [aDecoder decodeObjectForKey:kSearchTutorCover];
    self.h5_url = [aDecoder decodeObjectForKey:kSearchTutorH5_url];
    self.price = [aDecoder decodeObjectForKey:kSearchTutorPrice];
    self.user = [aDecoder decodeObjectForKey:kSearchTutorUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_searchTutorID forKey:kSearchTutorID];
    [aCoder encodeObject:_title forKey:kSearchTutorTitle];
    [aCoder encodeObject:_cover forKey:kSearchTutorCover];
    [aCoder encodeObject:_h5_url forKey:kSearchTutorH5_url];
    [aCoder encodeObject:_price forKey:kSearchTutorPrice];
    [aCoder encodeObject:_user forKey:kSearchTutorUser];
}

- (id)copyWithZone:(NSZone *)zone {
    SearchTutorListModel *copy = [[SearchTutorListModel alloc] init];
    
    if (copy) {
        copy.searchTutorID = self.searchTutorID;
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
        copy.price = [self.price copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
    }
    return copy;
}
@end
