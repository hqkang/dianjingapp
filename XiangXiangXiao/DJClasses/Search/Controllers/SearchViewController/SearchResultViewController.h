//
//  SearchResultViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <WMPageController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchResultViewController : WMPageController

@property (nonatomic, strong) NSString *searchStr;

@end

NS_ASSUME_NONNULL_END
