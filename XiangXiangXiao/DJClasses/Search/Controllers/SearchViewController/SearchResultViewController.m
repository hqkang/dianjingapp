//
//  SearchResultViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SearchListCommonViewController.h"

// models
#import "SearchTutorListModel.h"
#import "SearchClassesListModel.h"
#import "SearchTeacherListModel.h"
#import "SearchInstitutionsListModel.h"

// views
#import "CustomDefaultView.h"
#import "NavigationSearchView.h"

@interface SearchResultViewController ()<CustomDefaultViewDelegate, NavigationSearchBarDelegate>

@property (nonatomic, strong) CustomDefaultView *defaultView;

@property (nonatomic, strong) NavigationSearchView *navigationSearchBar;

@property (nonatomic, strong) NSMutableArray *tutorArray;

@property (nonatomic, strong) NSMutableArray *classesArray;

@property (nonatomic, strong) NSMutableArray *teacherArray;

@property (nonatomic, strong) NSMutableArray *institutionsArray;

@property (nonatomic, strong) NSMutableArray *titleArray;

@property (nonatomic, strong) NSMutableArray *titleCodeArray;

@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kBGColor);
    [self settingNavigation];
}

#pragma mark - <配置navigation>
-(void)settingNavigation
{
    NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
    navigationSearchBar.delegate = self;
    navigationSearchBar.searchBar.text = self.searchStr;
    self.navigationItem.titleView = navigationSearchBar;
    self.navigationSearchBar = navigationSearchBar;
    
    //    UIButton *btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnSearch setTitle:@"搜索" forState:UIControlStateNormal];
    //    [btnSearch setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
    //    [btnSearch.titleLabel setFont:[UIFont systemFontOfSize:16]];
    //
    //    [btnSearch addTarget:self action:@selector(btnSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_search_default";
        tempDefaultView.warnTitle = @"找不到相关数据";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}




#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    if (isError) {
        [self.view bringSubviewToFront:self.defaultView];
    }else{
        [self.view sendSubviewToBack:self.defaultView];
    }
}

#pragma mark - <配置navigation>
- (void)configNavigation
{
    // search
    self.navigationItem.titleView = self.navigationSearchBar;
}


#pragma mark - <跳转搜索页>
-(void)jumpToSearchVCWithRecommdWords:(NSString *)recommdWord
{
    
}


- (instancetype)init
{
    if (self = [super init]) {
        
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
//        self.menuViewLayoutMode = WMMenuViewLayoutModeCenter;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.menuView.lineColor = kColorFromRGB(kCateTitleColor);
        self.menuHeight = 50;
        self.menuBGColor = [UIColor clearColor];
        self.titleColorNormal = kColorFromRGB(kTextColor);
        self.titleColorSelected = kColorFromRGB(kCateTitleColor);
        
    }
    return self;
    
}

#pragma mark - 设置MenuView item间距
- (CGFloat)menuView:(WMMenuView *)menu itemMarginAtIndex:(NSInteger)index
{
    return 50.f;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self getSearchListData];
}


#pragma mark - Datasource & Delegate

#pragma mark 返回子页面的个数
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titleArray.count;
}

#pragma mark 返回某个index对应的页面
- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    SearchListCommonViewController *vc = [SearchListCommonViewController new];
    vc.searchStr = self.searchStr;
    vc.identifyStr = self.titleCodeArray[index];
    vc.tutorArray = self.tutorArray;
    vc.classesArray = self.classesArray;
    vc.teacherArray = self.teacherArray;
    vc.institutionsArray = self.institutionsArray;
    return vc;
}

#pragma mark 返回index对应的标题
- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    
    NSLog(@"index: %ld", index);
    return self.titleArray[index];
}


#pragma mark - 获取搜索数据
- (void)getSearchListData
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool getWithURL:[NSString stringWithFormat:@"%@?keyword=%@", kURL(kSearchUrl), self.searchStr] params:nil haveHeader:NO success:^(id json) {
        [hud hideAnimated:YES];
        if (json) {
            NSArray *array = (NSArray *)json;
            if (array.count > 0) {
                self.titleArray = nil;
                self.titleCodeArray = nil;
                self.tutorArray = nil;
                self.classesArray = nil;
                self.teacherArray = nil;
                self.institutionsArray = nil;
                for (int i = 0; i < array.count; i++) {
                    [self.titleArray addObject:array[i][@"name"]];
                    [self.titleCodeArray addObject:array[i][@"code"]];
                    if ([array[i][@"code"] isEqualToString:@"tutor"]) { // 家教
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.tutorArray addObject:[SearchTutorListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"course"]) { // 课程
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.classesArray addObject:[SearchClassesListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"teacher"]) { // 老师
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.teacherArray addObject:[SearchTeacherListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"group"]) { // 机构
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.institutionsArray addObject:[SearchInstitutionsListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                            }
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self reloadData];
                    });
                }
            }else{
                [self setupUIWithRequestError:YES];
            }
        }else{
            [self setupUIWithRequestError:YES];
        }
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        [self setupUIWithRequestError:YES];
    }];
    
}

- (void)setupMenu
{
    self.titleSizeNormal = 15;
    self.titleSizeSelected = 15;
    self.menuViewLayoutMode = WMMenuViewLayoutModeCenter;
    //        self.menuItemWidth = [UIScreen mainScreen].bounds.size.width / 10;
    self.menuHeight = 50;
    self.menuBGColor = [UIColor whiteColor];
    self.titleColorNormal = [UIColor blackColor];
    self.titleColorSelected = [UIColor blueColor];
    self.selectIndex = 0;
    self.delegate = self;
}

#pragma mark - 数组懒加载
- (NSMutableArray *)tutorArray
{
    if (_tutorArray == nil) {
        _tutorArray = [NSMutableArray array];
    }
    return _tutorArray;
}

- (NSMutableArray *)classesArray
{
    if (_classesArray == nil) {
        _classesArray = [NSMutableArray array];
    }
    return _classesArray;
}

- (NSMutableArray *)teacherArray
{
    if (_teacherArray == nil) {
        _teacherArray = [NSMutableArray array];
    }
    return _teacherArray;
}

- (NSMutableArray *)institutionsArray
{
    if (_institutionsArray == nil) {
        _institutionsArray = [NSMutableArray array];
    }
    return _institutionsArray;
}


- (NSMutableArray *)titleArray
{
    if (_titleArray == nil) {
        _titleArray = [NSMutableArray array];
    }
    return _titleArray;
}

- (NSMutableArray *)titleCodeArray
{
    if (_titleCodeArray == nil) {
        _titleCodeArray = [NSMutableArray array];
    }
    return _titleCodeArray;
}

#pragma mark - <CustomDefaultViewDelegate---------->
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView
{
    [self getSearchListData];
}

#pragma mark - ****** NavigationSearchBarDelegate *****
-(void)navigationSearchBarSearchActionWith:(UIButton *)button textField:(UITextField *)textField
{
    NSLog(@"开始搜索");
    [textField resignFirstResponder];
    
}

-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"开始编辑");
    [self jumpToSearchVCWithRecommdWords:textField.placeholder];
    return YES;
}
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField
{
    NSLog(@"开始搜索");
    [textField resignFirstResponder];
    
    return NO;
}

- (void)dealloc
{
    NSLog(@"dealloc");
}
@end
