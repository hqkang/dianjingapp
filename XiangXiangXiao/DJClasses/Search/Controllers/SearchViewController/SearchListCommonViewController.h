//
//  SearchListCommonViewController.h
//  SchoolOnline
//
//  Created by xhkj on 2019/1/12.
//  Copyright © 2019年 DD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchListCommonViewController : UIViewController

@property (nonatomic, strong) NSString *identifyStr;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *tutorArray;

@property (nonatomic, strong) NSMutableArray *classesArray;

@property (nonatomic, strong) NSMutableArray *teacherArray;

@property (nonatomic, strong) NSMutableArray *institutionsArray;

@property (nonatomic, strong) NSString *searchStr;

@end

NS_ASSUME_NONNULL_END
