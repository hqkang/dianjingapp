//
//  SearchViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultViewController.h"

//tools
#import "NSString+Predicate.h"

//views
#import "SearchHeaderView.h"

//cells
#import "LabelCollectionViewCell.h"
#import "SearchLabelTableViewCell.h"

//controllers
#import "CustomAlert.h"


@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource,SearchHeaderViewDelegate,SearchLabelTableViewCellDelegate>

@property (nonatomic, weak)UITableView *tableView;

//dataSource
@property (nonatomic, strong)NSMutableArray *dataArraySearchHistory;//搜索历史
@property (nonatomic, strong)NSMutableArray *dataArraySearchRecommend;//推荐搜索

//关键字
@property (nonatomic, copy)NSString *keyword;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getHotwords];
    [self dataSource];
    [self setupUI];
    
    if (![NSString isEmpty:self.recommdWords]) {
        self.keyword = self.recommdWords;
        //        self.navigationSearchBar.searchContent = self.recommdWords;
        self.navigationSearchBar.searchBar.placeholder = self.recommdWords;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - <获取搜索热词>
-(void)getHotwords
{
    [HttpTool getWithURL:kURL(kHotSearch) params:nil haveHeader:NO success:^(id json) {
        if (json) {
            NSArray *array = (NSArray *)json;
            if (array.count > 0) {
                NSArray *tempArray = array;
                for (NSDictionary *tempDict in tempArray) {
                    if (tempDict[@"name"]) {
                        NSString *title = tempDict[@"name"];
                        [self.dataArraySearchRecommend addObject:title];
                        [self.tableView reloadData];
                    }
                }
            }
        }
    } failure:nil];
}

#pragma mark - <视图即将出现>
-(void)viewWillAppear:(BOOL)animated
{
    //去除导航栏下边框线
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - <视图即将消失>
-(void)viewWillDisappear:(BOOL)animated
{
    //设置navigationBar的shadowImage
    UIImage *defaultShadowImage = kUserDefaultObject(kNavigationBarDefaultShadowImage);
    [self.navigationController.navigationBar setShadowImage:defaultShadowImage];
    
    [self.navigationSearchBar endEditing:YES];
}




#pragma mark - <懒加载>
-(NSMutableArray *)dataArraySearchHistory
{
    if (!_dataArraySearchHistory) {
        _dataArraySearchHistory = [NSMutableArray array];
    }
    return _dataArraySearchHistory;
}
-(NSMutableArray *)dataArraySearchRecommend
{
    if (!_dataArraySearchRecommend) {
        _dataArraySearchRecommend = [NSMutableArray array];
    }
    return _dataArraySearchRecommend;
}
-(UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tempTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        tempTableView.delegate = self;
        tempTableView.dataSource = self;
        tempTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [tempTableView setBackgroundColor:kColorFromRGB(kWhite)];
        tempTableView.estimatedRowHeight = 50;
        
        [tempTableView registerClass:[SearchLabelTableViewCell class] forCellReuseIdentifier:NSStringFromClass([SearchLabelTableViewCell class])];
        
        [self.view addSubview:tempTableView];
        _tableView = tempTableView;
    }
    return _tableView;
}





#pragma mark - <搭建UI>
-(void)setupUI
{
    [self.view setBackgroundColor:kColorFromRGB(kBGColor)];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark - <测试数据>
-(void)dataSource
{
    self.keyword = @"";
    
    if (kUserDefaultObject(kUserID)) {
        NSString *currentUserID = kUserDefaultObject(kUserID);
        if (kUserDefaultObject(currentUserID)) {
            NSArray *array = kUserDefaultObject(currentUserID);
            self.dataArraySearchHistory = [NSMutableArray arrayWithArray:array];
        }
        
        //清空搜索临时s记录
        //判断是否存在关键字数组
        if (kUserDefaultObject(kTempSearchRecord)) {
            //删除数组并同步
            kUserDefaultRemoveObject(kTempSearchRecord);
            kUserDefaultSynchronize;
        }
    }else{
        if (kUserDefaultObject(kTempSearchRecord)) {
            NSArray *array = kUserDefaultObject(kTempSearchRecord);
            self.dataArraySearchHistory = [NSMutableArray arrayWithArray:array];
        }
    }
}

#pragma mark - <跳转“搜索结果显示”页面>
-(void)jumpToSearchResultVCWithKeyContent:(NSString *)keyContent
{
    SearchResultViewController *searchResultVC = [[SearchResultViewController alloc] init];
    searchResultVC.searchStr = keyContent;
    [self.navigationController pushViewController:searchResultVC animated:YES];
}

#pragma mark - <保存最近搜索数据>
-(void)saveSearchKeywordsWithKeyword:(NSString *)keyword
{
    if (keyword.length > 0) {
        //判断用户是否已经登录
        if (kUserDefaultObject(kUserID)) {
            NSString *currentUserID = kUserDefaultObject(kUserID);
            //判断是否存在关键字数组
            if (kUserDefaultObject(currentUserID)) {
                NSArray *array = kUserDefaultObject(currentUserID);
                NSMutableArray *keywordArray = [[NSMutableArray alloc]initWithArray:array];
                //判断关键字数据里面是否已经含有该关键字
                if (![keywordArray containsObject:keyword]) {
                    [keywordArray insertObject:keyword atIndex:0];
                    //保存关键字
                    kUserDefaultSetObject(keywordArray, currentUserID);
                    kUserDefaultSynchronize;
                    
                    [self.dataArraySearchHistory insertObject:keyword atIndex:0];
                }else{//如果已经含有该关键字，则排序
                    //获取该关键字所在的index
                    NSInteger index = [self.dataArraySearchHistory indexOfObject:keyword];
                    [self.dataArraySearchHistory exchangeObjectAtIndex:0 withObjectAtIndex:index];
                }
            }else{
                NSMutableArray *keywordArray = [NSMutableArray array];
                [keywordArray insertObject:keyword atIndex:0];
                //保存关键字
                kUserDefaultSetObject(keywordArray, currentUserID);
                kUserDefaultSynchronize;
                
                [self.dataArraySearchHistory insertObject:keyword atIndex:0];
            }
            //刷新ui
            [self.tableView reloadData];
        }else{
            //判断是否存在关键字数组
            if (kUserDefaultObject(kTempSearchRecord)) {
                NSArray *array = kUserDefaultObject(kTempSearchRecord);
                NSMutableArray *keywordArray = [[NSMutableArray alloc]initWithArray:array];
                //判断关键字数据里面是否已经含有该关键字
                if (![keywordArray containsObject:keyword]) {
                    [keywordArray insertObject:keyword atIndex:0];
                    //保存关键字
                    kUserDefaultSetObject(keywordArray, kTempSearchRecord);
                    kUserDefaultSynchronize;
                    
                    [self.dataArraySearchHistory insertObject:keyword atIndex:0];
                }else{//如果已经含有该关键字，则排序
                    //获取该关键字所在的index
                    NSInteger index = [self.dataArraySearchHistory indexOfObject:keyword];
                    [self.dataArraySearchHistory exchangeObjectAtIndex:0 withObjectAtIndex:index];
                }
            }else{
                NSMutableArray *keywordArray = [NSMutableArray array];
                [keywordArray insertObject:keyword atIndex:0];
                //保存关键字
                kUserDefaultSetObject(keywordArray, kTempSearchRecord);
                kUserDefaultSynchronize;
                
                [self.dataArraySearchHistory insertObject:keyword atIndex:0];
            }
            //刷新ui
            [self.tableView reloadData];
        }
    }
}

#pragma mark - <删除最近搜索数据>
-(void)deleteSearchKeywords
{
    //判断用户是否已经登录
    if (kUserDefaultObject(kUserID)) {
        NSString *currentUserID = kUserDefaultObject(kUserID);
        //判断是否存在关键字数组
        if (kUserDefaultObject(currentUserID)) {
            //删除数组并同步
            kUserDefaultRemoveObject(currentUserID);
            kUserDefaultSynchronize;
            
            //更新当前数据
            [self.dataArraySearchHistory removeAllObjects];
            //刷新ui
            [self.tableView reloadData];
        }
    }else{
        //判断是否存在关键字数组
        if (kUserDefaultObject(kTempSearchRecord)) {
            //删除数组并同步
            kUserDefaultRemoveObject(kTempSearchRecord);
            kUserDefaultSynchronize;
            
            //更新当前数据
            [self.dataArraySearchHistory removeAllObjects];
            //刷新ui
            [self.tableView reloadData];
        }
    }
}

#pragma mark - <删除搜索历史>
-(void)deleteSearchHistoryAction
{
    [self deleteSearchKeywords];
}


#pragma mark - <UITableViewDelegate,UITableViewDataSource------------------->
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger number = 0;
    if (self.dataArraySearchHistory.count>0) {
        number += 1;
    }
    if (self.dataArraySearchRecommend.count>0) {
        number += 1;
    }
    return number;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 1;
    return number;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    switch (indexPath.section) {
        case 0:{
            if (self.dataArraySearchHistory.count > 0) {
                SearchLabelTableViewCell *cellLB = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchLabelTableViewCell class])];
                cellLB.cellType = CellLBTypeSearchRecord;
                cellLB.tag = indexPath.section;
                cellLB.delegate = self;
                cellLB.labelArray = self.dataArraySearchHistory;
                cell = cellLB;
            }else if (self.dataArraySearchRecommend.count > 0){
                SearchLabelTableViewCell *cellLB = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchLabelTableViewCell class])];
                cellLB.cellType = CellLBTypeSearchRecord;
                cellLB.tag = indexPath.section;
                cellLB.delegate = self;
                cellLB.labelArray = self.dataArraySearchRecommend;
                cell = cellLB;
            }
            
        }
            break;
        case 1:{
            if (self.dataArraySearchRecommend.count > 0) {
                SearchLabelTableViewCell *cellLB = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchLabelTableViewCell class])];
                cellLB.cellType = CellLBTypeSearchRecord;
                cellLB.tag = indexPath.section;
                cellLB.delegate = self;
                cellLB.labelArray = self.dataArraySearchRecommend;
                cell = cellLB;
            }
        }
            break;
            
        default:
            break;
    }
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    if (section == 0) {
        if (self.dataArraySearchHistory.count > 0) {
            SearchHeaderView *headerView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SearchHeaderView class]) owner:nil options:nil].firstObject;
            headerView.titleStr = @"历史搜索";
            headerView.hideBtnDelete = NO;
            headerView.delegate = self;
            view = headerView;
        }else{
            if (self.dataArraySearchRecommend.count > 0){
                SearchHeaderView *headerView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SearchHeaderView class]) owner:nil options:nil].firstObject;
                headerView.titleStr = @"热门搜索";
                headerView.hideBtnDelete = YES;
                view = headerView;
            }
        }
    }
    if (section == 1) {
        if (self.dataArraySearchRecommend.count > 0){
            SearchHeaderView *headerView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SearchHeaderView class]) owner:nil options:nil].firstObject;
            headerView.titleStr = @"热门搜索";
            headerView.hideBtnDelete = YES;
            view = headerView;
        }
    }
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        if (self.dataArraySearchHistory.count > 0) {
            return 60;
        }else if (self.dataArraySearchRecommend.count > 0) {
            return 60;
        }
    }else if (section == 1){
        if (self.dataArraySearchRecommend.count > 0) {
            return 60;
        }
    }
    return CGFLOAT_MIN;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.dataArraySearchRecommend.count > 0 && self.dataArraySearchHistory.count > 0) {
        return 1;
    }
    return CGFLOAT_MIN;
}

#pragma mark - <SearchHeaderViewDelegate----------------->
-(void)deleteSearchHistoryAction:(UIButton *)button
{
    kWeakSelf(self);
    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"您确定要清空历史记录？" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"确定" actionCancelName:@"取消" actionConfirm:^{
        NSLog(@"确定");
        [weakself deleteSearchHistoryAction];
    } actionCancel:^{
        NSLog(@"取消");
    }];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - ****** NavigationSearchBarDelegate *****
-(void)navigationSearchBarSearchActionWith:(UIButton *)button textField:(UITextField *)textField
{
    NSLog(@"开始搜索");
    [textField resignFirstResponder];
    if (![NSString isEmpty:textField.placeholder]) {
        self.keyword = textField.placeholder;
    }
    if (![NSString isEmpty:textField.text]) {
        self.keyword = textField.text;
    }
    
    //保存最近搜索的关键字
    [self saveSearchKeywordsWithKeyword:self.keyword];
    [self jumpToSearchResultVCWithKeyContent:self.keyword];
}
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"开始编辑");
    return YES;
}
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField
{
    NSLog(@"开始搜索");
    [textField resignFirstResponder];
    if (![NSString isEmpty:textField.placeholder]) {
        self.keyword = textField.placeholder;
    }
    if (![NSString isEmpty:textField.text]) {
        self.keyword = textField.text;
    }
    //    if ([NSString isEmpty:textField.text]) {
    //        [MBProgressHUD bwm_showTitle:@"请输入搜索内容" toView:self.view hideAfter:kDelay];
    //        return NO;
    //    }
    //    self.keyword = textField.text;
    //保存最近搜索的关键字
    [self saveSearchKeywordsWithKeyword:self.keyword];
    [self jumpToSearchResultVCWithKeyContent:self.keyword];
    return NO;
}

#pragma mark - <SearchLabelTableViewCellDelegate>
-(void)didSelectLBWith:(SearchLabelTableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    NSInteger tag = cell.tag;
    if (tag == 0) {
        if (indexPath.row < self.dataArraySearchHistory.count) {
            NSString *keyword = self.dataArraySearchHistory[indexPath.row];
            self.keyword = keyword;
            self.navigationSearchBar.searchContent = self.keyword;
            [self saveSearchKeywordsWithKeyword:self.keyword];
            [self jumpToSearchResultVCWithKeyContent:self.keyword];
        }else if (indexPath.row < self.dataArraySearchRecommend.count) {
            NSString *keyword = self.dataArraySearchRecommend[indexPath.row];
            self.keyword = keyword;
            self.navigationSearchBar.searchContent = self.keyword;
            [self saveSearchKeywordsWithKeyword:self.keyword];
            [self jumpToSearchResultVCWithKeyContent:self.keyword];
        }
    }else if (tag == 1){
        if (indexPath.row < self.dataArraySearchRecommend.count) {
            NSString *keyword = self.dataArraySearchRecommend[indexPath.row];
            self.keyword = keyword;
            self.navigationSearchBar.searchContent = self.keyword;
            [self saveSearchKeywordsWithKeyword:self.keyword];
            [self jumpToSearchResultVCWithKeyContent:self.keyword];
        }
    }
}


-(void)dealloc
{
    NSLog(@"销毁");
}



@end
