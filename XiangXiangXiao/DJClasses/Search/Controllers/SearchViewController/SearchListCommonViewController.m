//
//  SearchListCommonViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2019/1/12.
//  Copyright © 2019年 DD. All rights reserved.
//

#import "SearchListCommonViewController.h"
#import "CommonWebViewController.h"

// models
#import "SearchTutorListModel.h"//家教
#import "SearchClassesListModel.h"//课程
#import "SearchTeacherListModel.h"//老师
#import "SearchInstitutionsListModel.h"//机构

// views
#import "SearchTutorListCell.h"
#import "SearchClassesListCell.h"
#import "SearchTeacherListCell.h"
#import "SearchInstitutionsListCell.h"
#import "CustomDefaultView.h"

static int page = 2;
@interface SearchListCommonViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) CustomDefaultView *defaultView;


@end

@implementation SearchListCommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kColorFromRGB(kBGColor);
    [self setupUI];
    if ([self.identifyStr isEqualToString:@"tutor"]) {
        if (self.tutorArray.count > 0) {
            self.dataArray = self.tutorArray;
            [self.collectionView reloadData];
        }else{
            [self setupUIWithRequestError:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"course"]) {
        if (self.classesArray.count > 0) {
            self.dataArray = self.classesArray;
            [self.collectionView reloadData];
        }else{
            [self setupUIWithRequestError:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"teacher"]) {
        if (self.teacherArray.count > 0) {
            self.dataArray = self.teacherArray;
            [self.collectionView reloadData];
        }else{
            [self setupUIWithRequestError:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"group"]) {
        if (self.institutionsArray.count > 0) {
            self.dataArray = self.institutionsArray;
            [self.collectionView reloadData];
        }else{
            [self setupUIWithRequestError:YES];
        }
    }
    
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_search_default";
        tempDefaultView.warnTitle = @"找不到相关数据";
        tempDefaultView.hideButton = YES;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}

#pragma mark - <搭建UI*****************************************>
-(void)setupUIWithRequestError:(BOOL)isError
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    if (isError) {
        [self.view bringSubviewToFront:self.defaultView];
    }else{
        [self.view sendSubviewToBack:self.defaultView];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)dealloc
{
    NSLog(@"dealloc2222");
}

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)tutorArray
{
    if (_tutorArray == nil) {
        _tutorArray = [NSMutableArray array];
    }
    return _tutorArray;
}

- (NSMutableArray *)classesArray
{
    if (_classesArray == nil) {
        _classesArray = [NSMutableArray array];
    }
    return _classesArray;
}

- (NSMutableArray *)teacherArray
{
    if (_teacherArray == nil) {
        _teacherArray = [NSMutableArray array];
    }
    return _teacherArray;
}

- (NSMutableArray *)institutionsArray
{
    if (_institutionsArray == nil) {
        _institutionsArray = [NSMutableArray array];
    }
    return _institutionsArray;
}


#pragma mark - 设置UI
- (void)setupUI
{
    self.collectionView.backgroundColor = kColorFromRGB(kBGColor);
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    //上拉加载更多
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        [self loadIndexDataWithType:self.identifyStr];
    }];
    self.collectionView.mj_footer = footer;
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"" forState:MJRefreshStateNoMoreData];
    self.collectionView.mj_footer = footer;
}

#pragma mark - 上拉加载更多
- (void)loadIndexDataWithType:(NSString *)type
{
    [HttpTool getWithURL:[NSString stringWithFormat:@"%@?keyword=%@&code=%@&page=%d", kURL(kSearchUrl), self.searchStr, self.identifyStr, page] params:nil haveHeader:NO success:^(id json) {
        [self.collectionView.mj_footer endRefreshing];
        if (json) {
            NSArray *array = (NSArray *)json;
            if (array.count > 0) {
                for (int i = 0; i < array.count; i++) {
                    if ([array[i][@"code"] isEqualToString:@"tutor"]) { // 家教
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.dataArray addObject:[SearchTutorListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                                page++;
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"course"]) { // 课程
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.dataArray addObject:[SearchClassesListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                                page++;
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"teacher"]) { // 老师
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.dataArray addObject:[SearchTeacherListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                                page++;
                            }
                        }
                    }else if ([array[i][@"code"] isEqualToString:@"group"]) { // 机构
                        if (array[i][@"data"]) {
                            NSArray *tempArray = array[i][@"data"][@"data"];
                            if (tempArray.count > 0) {
                                for (int j = 0 ; j < tempArray.count; j++) {
                                    [self.dataArray addObject:[SearchInstitutionsListModel modelObjectWithDictionary:tempArray[j]]];
                                }
                                page++;
                            }
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                    });
                }
            }else{
                
            }
        }else{
            
        }
    } failure:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

#pragma mark - UICollectionView懒加载
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavigationBarHeight - kStatusBarHeight - kTabbarSafeBottomMargin - 50) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        // 注册cell和headerView
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
        
        // 家教
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTutorListCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([SearchTutorListCell class])];
        // 课程
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchClassesListCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([SearchClassesListCell class])];
        // 老师
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTeacherListCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([SearchTeacherListCell class])];
        // 机构
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchInstitutionsListCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([SearchInstitutionsListCell class])];
        
    }
    return _collectionView;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
        reusableview = headerView;
    }else
    {
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
        reusableview = footerView;
    }
    return reusableview;
}

#pragma mark - head宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.identifyStr isEqualToString:@"tutor"]) {
        CGFloat itemW = (kScreenWidth) / 2;
        return CGSizeMake(itemW, itemW * 200 / 164);
    }else if ([self.identifyStr isEqualToString:@"course"]) {
        CGFloat itemW = (kScreenWidth) / 2;
        return CGSizeMake(itemW, itemW * 200 / 164);
    }else if ([self.identifyStr isEqualToString:@"teacher"]) {
        return CGSizeMake(kScreenWidth, 160);
    }else if ([self.identifyStr isEqualToString:@"group"]) {
        return CGSizeMake(kScreenWidth, 133);
    }else{
        return CGSizeZero;
    }
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *gridcell = nil;
    //  家教
    SearchTutorListCell *tutorCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SearchTutorListCell class]) forIndexPath:indexPath];
    // 课程
    SearchClassesListCell *classesCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SearchClassesListCell class]) forIndexPath:indexPath];
    // 老师
    SearchTeacherListCell *teacherCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SearchTeacherListCell class]) forIndexPath:indexPath];
    // 机构
    SearchInstitutionsListCell *institutionsCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SearchInstitutionsListCell class]) forIndexPath:indexPath];
    
    if ([self.identifyStr isEqualToString:@"tutor"]) {
        SearchTutorListModel *model = [[SearchTutorListModel alloc] init];
        model = self.dataArray[indexPath.item];
        tutorCell.model = model;
        gridcell = tutorCell;
    }else if ([self.identifyStr isEqualToString:@"course"]) {
        SearchClassesListModel *model = [[SearchClassesListModel alloc] init];
        model = self.dataArray[indexPath.item];
        classesCell.model = model;
        gridcell = classesCell;
    }else if ([self.identifyStr isEqualToString:@"teacher"]) {
        SearchTeacherListModel *model = [[SearchTeacherListModel alloc] init];
        model = self.dataArray[indexPath.item];
        teacherCell.model = model;
        gridcell = teacherCell;
    }else if ([self.identifyStr isEqualToString:@"group"]) {
        SearchInstitutionsListModel *model = [[SearchInstitutionsListModel alloc] init];
        model = self.dataArray[indexPath.item];
        institutionsCell.model = model;
        gridcell = institutionsCell;
    }
    
    return gridcell;
}

#pragma mark - <UICollectionViewDelegateFlowLayout>
#pragma mark - X间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark - Y间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}


#pragma mark - 选中item
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     SearchTutorListModel
     SearchClassesListModel
     SearchTeacherListModel
     SearchInstitutionsListModel
     */
    if ([self.identifyStr isEqualToString:@"tutor"]) {
        SearchTutorListModel *model = [[SearchTutorListModel alloc] init];
        model = self.dataArray[indexPath.item];
        if ([model.h5_url isNotEmpty]) {
            CommonWebViewController *web = [[CommonWebViewController alloc] init];
            web.urlStr = model.h5_url;
            if (LOGINDATA) {
                web.params = LOGINDATA;
            }
            if (USERDATA) {
                web.userDic = USERDATA;
            }
            [self.navigationController pushViewController:web animated:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"course"]) {
        SearchClassesListModel *model = [[SearchClassesListModel alloc] init];
        model = self.dataArray[indexPath.item];
        if ([model.h5_url isNotEmpty]) {
            CommonWebViewController *web = [[CommonWebViewController alloc] init];
            web.urlStr = model.h5_url;
            if (LOGINDATA) {
                web.params = LOGINDATA;
            }
            if (USERDATA) {
                web.userDic = USERDATA;
            }
            [self.navigationController pushViewController:web animated:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"teacher"]) {
        SearchTeacherListModel *model = [[SearchTeacherListModel alloc] init];
        model = self.dataArray[indexPath.item];
        if ([model.h5_url isNotEmpty]) {
            CommonWebViewController *web = [[CommonWebViewController alloc] init];
            web.urlStr = model.h5_url;
            if (LOGINDATA) {
                web.params = LOGINDATA;
            }
            if (USERDATA) {
                web.userDic = USERDATA;
            }
            [self.navigationController pushViewController:web animated:YES];
        }
    }else if ([self.identifyStr isEqualToString:@"group"]) {
        SearchInstitutionsListModel *model = [[SearchInstitutionsListModel alloc] init];
        model = self.dataArray[indexPath.item];
        if ([model.h5_url isNotEmpty]) {
            CommonWebViewController *web = [[CommonWebViewController alloc] init];
            web.urlStr = model.h5_url;
            if (LOGINDATA) {
                web.params = LOGINDATA;
            }
            if (USERDATA) {
                web.userDic = USERDATA;
            }
            [self.navigationController pushViewController:web animated:YES];
        }
    }else{
        
    }
}


@end
