//
//  SearchViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "BaseSearchViewController.h"

@interface SearchViewController : BaseSearchViewController
@property (nonatomic, strong) NSString *recommdWords;
@end
