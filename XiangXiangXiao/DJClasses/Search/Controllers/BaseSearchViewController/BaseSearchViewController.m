//
//  BaseSearchViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "BaseSearchViewController.h"

@interface BaseSearchViewController ()<NavigationSearchBarDelegate>

//搜索button
//@property (nonatomic, weak)UIButton *btnSearch;
@end

@implementation BaseSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self settingNavigation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - <配置navigation>
-(void)settingNavigation
{
    NavigationSearchView *navigationSearchBar = [[NavigationSearchView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
    navigationSearchBar.delegate = self;
    self.navigationItem.titleView = navigationSearchBar;
    self.navigationSearchBar = navigationSearchBar;
    
//    UIButton *btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnSearch setTitle:@"搜索" forState:UIControlStateNormal];
//    [btnSearch setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
//    [btnSearch.titleLabel setFont:[UIFont systemFontOfSize:16]];
//
//    [btnSearch addTarget:self action:@selector(btnSearchAction:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
}

//-(UIButton *)btnSearch
//{
//    if (!_btnSearch) {
//        UIButton *btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btnSearch setTitle:@"搜索" forState:UIControlStateNormal];
//        [btnSearch setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
//        [btnSearch.titleLabel setFont:[UIFont systemFontOfSize:16]];
//
//        [btnSearch addTarget:self action:@selector(btnSearchAction:) forControlEvents:UIControlEventTouchUpInside];
//
//        _btnSearch = btnSearch;
//    }
//    return _btnSearch;
//}
//
//#pragma mark - <搜索按钮响应>
//-(void)btnSearchAction:(UIButton *)sender
//{
//    if ([self.delegate respondsToSelector:@selector(navigationSearchBarSearchActionWith:textField:)]) {
//        [self.delegate navigationSearchBarSearchActionWith:sender textField:self.searchBar];
//    }
//}












@end
