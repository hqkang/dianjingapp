//
//  BaseSearchViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>


//views
#import "NavigationSearchView.h"

@interface BaseSearchViewController : UIViewController

@property (nonatomic, weak)NavigationSearchView *navigationSearchBar;

@end
