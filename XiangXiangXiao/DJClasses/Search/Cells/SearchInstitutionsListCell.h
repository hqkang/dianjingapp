//
//  SearchInstitutionsListCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchInstitutionsListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchInstitutionsListCell : UICollectionViewCell

@property (nonatomic, copy) SearchInstitutionsListModel *model;

@end

NS_ASSUME_NONNULL_END
