//
//  LabelCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy)NSString *content;
@property (nonatomic, strong)UIColor *bgColor;
@property (nonatomic, assign)CGFloat textFont;
@property (nonatomic, assign)CGFloat corner;

@end
