//
//  LabelCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import "LabelCollectionViewCell.h"

@interface LabelCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;

@end

@implementation LabelCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setContent:(NSString *)content
{
    self.labelContent.text = content;
}

-(void)setTextFont:(CGFloat)textFont
{
    [self.labelContent setFont:[UIFont systemFontOfSize:textFont]];
}

-(void)setBgColor:(UIColor *)bgColor
{
    [self.bgView setBackgroundColor:bgColor];
}

-(void)setCorner:(CGFloat)corner
{
    [self.bgView.layer setCornerRadius:corner];
    [self.bgView.layer setMasksToBounds:YES];
}

@end
