//
//  SearchLabelTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchLabelTableViewCell;

typedef NS_ENUM(NSUInteger, CellLBType) {
    CellLBTypeSearchRecord,
    CellLBTypeSpec,
};

@protocol SearchLabelTableViewCellDelegate<NSObject>
@optional
-(void)didSelectLBWith:(SearchLabelTableViewCell *)cell indexPath:(NSIndexPath *)indexPath;
@end

@interface SearchLabelTableViewCell : UITableViewCell
@property (nonatomic, weak)id<SearchLabelTableViewCellDelegate> delegate;
@property (nonatomic, assign)CellLBType cellType;
@property (nonatomic, copy)NSString *specSelected;
@property (nonatomic, copy)NSArray *labelArray;

//@property (nonatomic, strong)JAGoodDetailModel *goodsDetailModel;
@end
