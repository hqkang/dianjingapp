//
//  SearchLabelTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchLabelTableViewCell.h"
#import "SearchModel.h"

//cells
#import "LabelTxtCollectionViewCell.h"

//tools
#import "NSString+ContentWidthHeight.h"
#import "QKCustomFlowLayout.h"

@interface SearchLabelTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong)UICollectionView *collectionView;
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation SearchLabelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.labelArray = [NSMutableArray arrayWithObjects:@"水杯",@"抱枕",@"家用电器真皮沙发电热壶海尔冰箱汽车座套床上用品",@"大喊傻逼",@"贼喊捉贼", nil];
        [self setupUI];
    }
    return self;
}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        QKCustomFlowLayout *flowLayout = [[QKCustomFlowLayout alloc]initWithType:AlignTypeWithLeft betweenOfCell:10];
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 12, 20, 12);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, self.contentView.bounds.size.height) collectionViewLayout:flowLayout];
        [_collectionView setBackgroundColor:kColorFromRGB(kWhite)];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([LabelTxtCollectionViewCell class]) bundle:nil];
        [_collectionView registerNib:nib forCellWithReuseIdentifier:NSStringFromClass([LabelTxtCollectionViewCell class])];
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}
-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
//-(NSMutableArray *)disableSpecArray
//{
//    if (!_disableSpecArray) {
//        _disableSpecArray = [NSMutableArray array];
//    }
//    return _disableSpecArray;
//}

-(void)setupUI
{
    kWeakSelf(self);
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
        make.height.mas_equalTo(weakself.collectionView.contentSize.height);
    }];
}

-(void)setSpecSelected:(NSString *)specSelected
{
    _specSelected = specSelected;
}
-(void)setLabelArray:(NSArray *)labelArray
{
    _labelArray = labelArray;
    if (self.cellType == CellLBTypeSearchRecord || self.cellType == CellLBTypeSpec) {
        [self.dataArray removeAllObjects];
    }
    for (NSString *txt in labelArray) {
        CGFloat width = [NSString getWidthWithContent:txt font:13 contentSize:CGSizeMake(0, 25)];
        CGFloat height = 0;
        NSDictionary *dict = [NSDictionary dictionary];
        if (width > kScreenWidth-kEdgeLeftAndRight*4) {
            height = [NSString getHeightWithContent:txt font:13 contentSize:CGSizeMake(kScreenWidth-kEdgeLeftAndRight*4, 0)];
            dict = @{@"text":txt,
                     @"width":@(kScreenWidth-kEdgeLeftAndRight*2),
                     @"height":@(height+10)};
        }else{
            dict = @{@"text":txt,
                     @"width":@(width+kEdgeLeftAndRight*2),
                     @"height":@(30)};
        }
        [self.dataArray addObject:dict];
    }
    
    self.frame = [UIScreen mainScreen].bounds;
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self.collectionView reloadData];
    kWeakSelf(self);
    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(weakself.collectionView.collectionViewLayout.collectionViewContentSize.height);
    }];
    
    if (self.cellType == CellLBTypeSpec) {
        if (self.specSelected) {
            NSUInteger index = [labelArray indexOfObject:self.specSelected];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        }
    }
}
-(void)setCellType:(CellLBType)cellType
{
    _cellType = cellType;
}
//-(void)setGoodsDetailModel:(JAGoodDetailModel *)goodsDetailModel
//{
//    _goodsDetailModel = goodsDetailModel;
//    [self.disableSpecArray removeAllObjects];
//
//    if (!self.specSelected) {
//        return;
//    }
//    NSArray *array = self.goodsDetailModel.goodDetail.goodsSpecParam;
//    for (GoodsSpecParam *tempModel in array) {
//        if (tempModel.stock == 0 || tempModel.status == -1) {
//            if ([self.specSelected isEqualToString:tempModel.name]) {
//                [self.disableSpecArray addObject:tempModel.nameTwo];
//            }else if ([self.specSelected isEqualToString:tempModel.nameTwo]){
//                [self.disableSpecArray addObject:tempModel.name];
//            }
//        }
//    }
//
//    if (self.disableSpecArray.count>0) {
//        [self.collectionView reloadData];
//    }
//}








#pragma mark - <------------------------------------->
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    LabelTxtCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LabelTxtCollectionViewCell class]) forIndexPath:indexPath];
//    cell.tag = self.tag;
    if (self.cellType == CellLBTypeSpec) {
        cell.cellType = CellLBTxtTypeSpec;
    }else if (self.cellType == CellLBTypeSearchRecord){
        cell.cellType = CellLBTxtTypeSearchRecord;
    }
    NSDictionary *dict = self.dataArray[indexPath.row];
    NSString *txt = dict[@"text"];
    cell.txt = txt;
//    cell.isDisable = NO;
//    for (NSString *tempTxt in self.disableSpecArray) {
//        if ([txt isEqualToString:tempTxt]) {
//            cell.isDisable = YES;
//            break;
//        }
//    }
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectLBWith:indexPath:)]) {
        [self.delegate didSelectLBWith:self indexPath:indexPath];
    }
}

#pragma mark - <UICollectionViewDelegateFlowLayout----------------->
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.dataArray[indexPath.row];
    NSNumber *widthNum = dict[@"width"];
    NSNumber *heightNum = dict[@"height"];
    CGFloat width = widthNum.floatValue;;
    CGFloat height = heightNum.floatValue;;
    CGSize size = CGSizeMake(width, height);

    return size;
}






@end
