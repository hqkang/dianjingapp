//
//  SearchClassesListCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchClassesListCell.h"

@implementation SearchClassesListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SearchClassesListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.cover)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.priceLabel.text = model.price;
    self.classesNameLabel.text = model.title;
}

@end
