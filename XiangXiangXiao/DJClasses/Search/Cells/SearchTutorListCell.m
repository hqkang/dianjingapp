//
//  SearchTutorListCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchTutorListCell.h"
#import "TutorUserModel.h"

@implementation SearchTutorListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SearchTutorListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.cover)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.tutorTitleLabel.text = model.title;
    self.priceLabel.text = model.price;
    TutorUserModel *userModel = [[TutorUserModel alloc] init];
    userModel = model.user;
    self.userNameLabel.text = userModel.nickname;
}

@end
