//
//  SearchClassesListCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchClassesListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchClassesListCell : UICollectionViewCell

@property (nonatomic, copy) SearchClassesListModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *classesNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

NS_ASSUME_NONNULL_END
