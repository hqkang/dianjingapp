//
//  SearchTeacherListCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchTeacherListCell.h"

@implementation SearchTeacherListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SearchTeacherListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    [self.leftImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(model.suits_photo)] placeholderImage:nil options:SDWebImageRefreshCached];
    self.sellCountLabel.text = [NSString stringWithFormat:@"%d人学习", (int)model.sell_count];
    self.contentLabel.text = model.content;
    self.realNameLabel.text = model.realname;
    
}

@end
