//
//  LabelTxtCollectionViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "LabelTxtCollectionViewCell.h"
#import "SearchModel.h"


@interface LabelTxtCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIView *bgv;
@property (weak, nonatomic) IBOutlet UILabel *lbTxt;

//@property (nonatomic, assign)BOOL isCellSelected;
@end

@implementation LabelTxtCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.mas_equalTo(0);
//        make.width.mas_lessThanOrEqualTo(kScreenWidth);
//        make.height.mas_greaterThanOrEqualTo(0);
//    }];
    
//    self.bgv.layer.borderColor = kColorFromRGB(kShadowColor).CGColor;
//    self.bgv.layer.borderWidth = 0.5;
    
    [self.bgv.layer setCornerRadius:3];
    [self.bgv.layer setMasksToBounds:YES];
}

-(void)setSelected:(BOOL)selected
{
    if (self.cellType == CellLBTxtTypeSpec) {
//        if (selected) {
//            [self.bgv setBackgroundColor:kColorFromRGB(kScoreColor)];
//            self.bgv.layer.borderColor = kColorFromRGB(kWhite).CGColor;
//            self.bgv.layer.borderWidth = 0.5;
//
//            self.lbTxt.textColor = kColorFromRGB(kGoodShopNameColor);
//        }else{
//            [self.bgv setBackgroundColor:kColorFromRGB(kWhite)];
//            self.bgv.layer.borderColor = kColorFromRGB(kShadowColor).CGColor;
//            self.bgv.layer.borderWidth = 0.5;
//
//            self.lbTxt.textColor = kColorFromRGB(kGoodShopNameColor);
//        }
        
        
//        if (self.isDisable) {
//            [self.bgv setBackgroundColor:kColorFromRGB(kWhite)];
//            self.bgv.layer.borderColor = kColorFromRGB(kTabSeperatorColor).CGColor;
//            self.bgv.layer.borderWidth = 0.5;
//
//            self.lbTxt.textColor = kColorFromRGB(kTabSeperatorColor);
//        }else{
//            if (selected) {
//                [self.bgv setBackgroundColor:kColorFromRGB(kScoreColor)];
//                self.bgv.layer.borderColor = kColorFromRGB(kWhite).CGColor;
//                self.bgv.layer.borderWidth = 0.5;
//
//                self.lbTxt.textColor = kColorFromRGB(kGoodShopNameColor);
//            }else{
//                [self.bgv setBackgroundColor:kColorFromRGB(kWhite)];
//                self.bgv.layer.borderColor = kColorFromRGB(kShadowColor).CGColor;
//                self.bgv.layer.borderWidth = 0.5;
//
//                self.lbTxt.textColor = kColorFromRGB(kGoodShopNameColor);
//            }
//
//        }
    }
    
}

-(void)setTxt:(NSString *)txt
{
    _txt = txt;
    self.lbTxt.text = txt;
}
-(void)setCellType:(CellLBTxtType)cellType
{
    _cellType = cellType;
    
    if (cellType == CellLBTxtTypeSearchRecord) {
        [self.bgv setBackgroundColor:kColorFromRGB(kBGColor)];
    }
}
-(void)setActiveSearchModel:(SearchModel *)activeGoodsSpecModel
{
    NSString *spec1 = activeGoodsSpecModel.name;
    NSNumber *spec2 = activeGoodsSpecModel.searchID;
    [self.bgv setBackgroundColor:kColorFromRGB(kBGColor)];
    self.lbTxt.textColor = kColorFromRGB(kTabSeperatorColor);
}



@end
