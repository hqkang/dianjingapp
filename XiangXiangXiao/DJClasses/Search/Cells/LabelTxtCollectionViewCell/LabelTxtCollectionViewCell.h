//
//  LabelTxtCollectionViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchModel;

typedef NS_ENUM(NSUInteger, CellLBTxtType) {
    CellLBTxtTypeSearchRecord,
    CellLBTxtTypeSpec,
};

@interface LabelTxtCollectionViewCell : UICollectionViewCell
@property (nonatomic, assign)CellLBTxtType cellType;
@property (nonatomic, copy)NSString *txt;

//@property (nonatomic, assign)BOOL isDisable;
@property (nonatomic, strong)SearchModel *activeSearchModel;
@end
