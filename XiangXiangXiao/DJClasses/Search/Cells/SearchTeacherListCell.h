//
//  SearchTeacherListCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTeacherListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchTeacherListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *sellCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *realNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *recommendLabel;

@property (nonatomic, copy) SearchTeacherListModel *model;

@end

NS_ASSUME_NONNULL_END
