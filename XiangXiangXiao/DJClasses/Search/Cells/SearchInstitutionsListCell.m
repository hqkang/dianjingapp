//
//  SearchInstitutionsListCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/5.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchInstitutionsListCell.h"

@implementation SearchInstitutionsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SearchInstitutionsListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    
}

@end
