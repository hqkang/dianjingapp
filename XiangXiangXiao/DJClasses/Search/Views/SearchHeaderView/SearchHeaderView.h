//
//  SearchHeaderView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchHeaderViewDelegate<NSObject>
@optional
-(void)deleteSearchHistoryAction:(UIButton *)button;
@end

@interface SearchHeaderView : UIView
@property (nonatomic, weak)id<SearchHeaderViewDelegate> delegate;
@property (nonatomic, assign)BOOL hideBtnDelete;
@property (nonatomic, copy)NSString *titleStr;
@property (nonatomic, assign)CGFloat fontValue;
@end
