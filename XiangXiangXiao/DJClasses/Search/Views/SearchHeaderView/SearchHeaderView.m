//
//  SearchHeaderView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SearchHeaderView.h"

@interface SearchHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchDelete;
@end

@implementation SearchHeaderView

- (IBAction)btnSearchDeleteAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(deleteSearchHistoryAction:)]) {
        [self.delegate deleteSearchHistoryAction:sender];
    }
}

-(void)setHideBtnDelete:(BOOL)hideBtnDelete
{
    [self.btnSearchDelete setHidden:hideBtnDelete];
}
-(void)setTitleStr:(NSString *)titleStr
{
    self.lbTitle.text = titleStr;
}
-(void)setFontValue:(CGFloat)fontValue
{
    self.lbTitle.font = [UIFont systemFontOfSize:fontValue];
}


@end
