//
//  NavigationSearchView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavigationSearchBarDelegate <NSObject>

@optional
-(void)navigationSearchBarSearchActionWith:(UIButton *)button textField:(UITextField *)textField;
-(BOOL)navigationSearchBarShouldBeginEditing:(UITextField *)textField;
-(BOOL)navigationSearchBarShouldReturn:(UITextField *)textField;


@end

@interface NavigationSearchView : UIView

@property (nonatomic, weak)id<NavigationSearchBarDelegate> delegate;

//搜索框的文本内容
@property (nonatomic, copy)NSString *searchContent;
//搜索框内推荐搜索内容
@property (nonatomic, copy)NSString *searchRecommendContent;
//隐藏搜索按钮
@property (nonatomic, assign)BOOL isHideBtnSearch;
//使用textField充当searchBar
@property (nonatomic, weak)UITextField *searchBar;

@end
