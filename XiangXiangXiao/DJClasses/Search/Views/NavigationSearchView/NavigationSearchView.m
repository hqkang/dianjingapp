//
//  NavigationSearchView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import "NavigationSearchView.h"

//tools
#import "NSString+ContentWidthHeight.h"
#import "NSString+CheckEmoji.h"

@interface NavigationSearchView()<UITextFieldDelegate>

//搜索边框的view
@property (nonatomic, weak)UIView *borderView;
//搜索button
@property (nonatomic, weak)UIButton *btnSearch;
//清除搜索内容button
@property (nonatomic, weak)UIButton *btnClear;


@end

@implementation NavigationSearchView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setupUI];
    }
    return self;
}

#pragma mark - <懒加载>
-(UIView *)borderView
{
    if (!_borderView) {
        UIView *borderView = [[UIView alloc]initWithFrame:self.bounds];
        [borderView setBackgroundColor:kColorFromRGB(kSearchBGColor)];
        [borderView.layer setCornerRadius:self.bounds.size.height/2.0];
        [borderView.layer setMasksToBounds:YES];

        _borderView = borderView;
        [self addSubview:_borderView];
    }
    return _borderView;
}
-(UITextField *)searchBar
{
    if (!_searchBar) {
        UITextField *searchBar = [[UITextField alloc]initWithFrame:self.borderView.bounds];
        [searchBar setFont:[UIFont systemFontOfSize:14]];
        searchBar.delegate = self;
        searchBar.returnKeyType = UIReturnKeySearch;
        searchBar.placeholder = @"IELTS 雅思";
        searchBar.tintColor = [UIColor blueColor];
        searchBar.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_search"]];
        searchBar.leftViewMode = UITextFieldViewModeAlways;
        
        [searchBar addTarget:self action:@selector(tfContentShowBtnClear:) forControlEvents:UIControlEventEditingChanged | UIControlEventEditingDidBegin];
        
        _searchBar = searchBar;
        [self.borderView addSubview:_searchBar];
    }
    return _searchBar;
}
-(UIButton *)btnSearch
{
    if (!_btnSearch) {
        UIButton *btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnSearch setTitle:@"搜索" forState:UIControlStateNormal];
        [btnSearch setTitleColor:kColorFromRGB(kNameColor) forState:UIControlStateNormal];
        [btnSearch.titleLabel setFont:[UIFont systemFontOfSize:16]];

        [btnSearch addTarget:self action:@selector(btnSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnSearch];

        _btnSearch = btnSearch;
    }
    return _btnSearch;
}
-(UIButton *)btnClear
{
    if (!_btnClear) {
        UIButton *btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClear setImage:[UIImage imageNamed:@"icon_close_search"] forState:UIControlStateNormal];
        [btnClear setHidden:YES];
        
        [btnClear addTarget:self action:@selector(btnClearAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.borderView addSubview:btnClear];
        _btnClear = btnClear;
    }
    return _btnClear;
}

-(void)setIsHideBtnSearch:(BOOL)isHideBtnSearch
{
    if (isHideBtnSearch) {
        [self.btnSearch setHidden:YES];
        [self.borderView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(UIEdgeInsetsMake(0, 10, 0, 10));
        }];
    }
}

#pragma mark - <显示隐藏内容清除按钮>
-(void)tfContentShowBtnClear:(UITextField *)sender
{
    if (sender.text.length > 0) {
        [self setBtnClearShow:YES];
    }else{
        [self setBtnClearShow:NO];
    }
}
-(void)setBtnClearShow:(BOOL)isShow
{
    if (isShow) {
        [self.btnClear setHidden:NO];
    }else{
        [self.btnClear setHidden:YES];
    }
}


#pragma mark - <搭建UI>
-(void)setupUI
{
    [self setBackgroundColor:kColorFromRGB(kNavColor)];
    
    [self.borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 10, 0, 50));
    }];
    
    [self.btnSearch mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(50);
    }];
    
    [self.searchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(0);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-25);
    }];
    
    [self.btnClear mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(5);
//        make.bottom.mas_equalTo(-5);
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(0);
    }];
}

/*
 1.如果设置了titleView，titleView原来是直接添加到UINavigationBar上，iOS11后，titleView会加在
 UITAMICAdaptorView上，UITAMICAdaptorView这个图层是添加在UINavigationBarContentView上的，
 UINavigationBarContentView之上才是UINavigationBar。
 
 2.titleView支持autolayout，你可能会发现你的titleView变窄了，你可以通过重写 intrinsicContentSize方法来解决这个问题
 3.解决方法：在自定义titleView里面重写intrinsicContentSize。
 */
#pragma mark - <适配iOS11的titleView>
-(CGSize)intrinsicContentSize
{
    CGSize size = self.bounds.size;
    return size;
}

#pragma mark - <搜索按钮响应>
-(void)btnSearchAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(navigationSearchBarSearchActionWith:textField:)]) {
        [self.delegate navigationSearchBarSearchActionWith:sender textField:self.searchBar];
    }
    
    //搜索时隐藏内容清除按钮
    [self.btnClear setHidden:YES];
}

#pragma mark - <清除按钮响应>
-(void)btnClearAction:(UIButton *)sender
{
    NSLog(@"清除搜索内容");
    self.searchBar.text = @"";
    [self.btnClear setHidden:YES];
}
-(void)btnDeleteAction:(UIButton *)sender
{
    [self.searchBar becomeFirstResponder];
}

-(void)setSearchContent:(NSString *)searchContent
{
    self.searchBar.text = searchContent;
}

#pragma mark - <搭建搜索框推荐UI>
-(void)setupSearchBarRecommendUI
{
    
}

#pragma mark - <有搜索栏推荐项时搭建该UI>
-(void)setSearchRecommendContent:(NSString *)searchRecommendContent
{
    self.searchBar.placeholder = searchRecommendContent;
}












#pragma mark - ***** UITextFieldDelegate ****
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(navigationSearchBarShouldBeginEditing:)]) {
        BOOL shouldEdit = [self.delegate navigationSearchBarShouldBeginEditing:textField];
        return shouldEdit;
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(navigationSearchBarShouldReturn:)]) {
        [self.delegate navigationSearchBarShouldReturn:textField];
    }
    
    [self.btnClear setHidden:YES];

    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0) {
    }
    
    [self.btnClear setHidden:YES];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isFirstResponder]) {
        //禁止系统键盘emoji输入
        if ([textField.textInputMode.primaryLanguage isEqualToString:@"emoji"] || textField.textInputMode.primaryLanguage == nil) {
            return NO;
        }
        //过滤文本中的emoji
        if ([string containEmoji]) {
            return NO;
        }
    }
    return YES;
}






@end
