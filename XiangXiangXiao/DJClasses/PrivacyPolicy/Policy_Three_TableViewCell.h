//
//  Policy_Three_TableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef void(^BlockSelectLinkAction)(void);

NS_ASSUME_NONNULL_BEGIN

@interface Policy_Three_TableViewCell : UITableViewCell
@property (nonatomic, copy)BlockSelectLinkAction selectLinkActionBlock;
@end

NS_ASSUME_NONNULL_END
