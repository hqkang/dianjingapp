//
//  PrivacyPolicyViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import "CommonWebViewController.h"
#import "BaseNavigationController.h"
#import "PolicyWarningViewController.h"

#import "Policy_One_TableViewCell.h"
#import "Policy_Two_TableViewCell.h"
#import "Policy_Three_TableViewCell.h"

#import "CustomAlert.h"

@interface PrivacyPolicyViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIImageView *headerImgView;
@property (nonatomic, strong)UILabel *headerLB;
@property (nonatomic, strong)UIView *selectionView;
@property (nonatomic, strong)UIButton *btnNO;
@property (nonatomic, strong)UIButton *btnYES;
@end

@implementation PrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = kColorFromRGBAndAlpha(kBlack, 0.5);
    [self setupUI];
}

-(UIView *)selectionView
{
    if (!_selectionView) {
        _selectionView = [[UIView alloc]initWithFrame:self.view.bounds];
        _selectionView.backgroundColor = kColorFromRGB(kWhite);
        [self.view addSubview:_selectionView];
        
        _selectionView.layer.cornerRadius = 3;
        _selectionView.layer.masksToBounds = YES;
    }
    return _selectionView;
}
-(UIButton *)btnYES
{
    if (!_btnYES) {
        _btnYES = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnYES setTitle:@"同意" forState:UIControlStateNormal];
        [_btnYES setBackgroundColor:kColorFromRGB(kScoreColor)];
        [_btnYES setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
        _btnYES.titleLabel.font = [UIFont systemFontOfSize:16];
        
        _btnYES.layer.cornerRadius = 2;
        _btnYES.layer.masksToBounds = YES;
        [self.selectionView addSubview:_btnYES];
        
        [_btnYES addTarget:self action:@selector(btnYESAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnYES;
}
-(UIButton *)btnNO
{
    if (!_btnNO) {
        _btnNO = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnNO setTitle:@"不同意" forState:UIControlStateNormal];
        [_btnNO setBackgroundColor:kColorFromRGB(kWhite)];
        [_btnNO setTitleColor:kColorFromRGB(kTextColor) forState:UIControlStateNormal];
        _btnNO.titleLabel.font = [UIFont systemFontOfSize:16];
        
        _btnNO.layer.borderWidth = 0.5;
        _btnNO.layer.borderColor = kColorFromRGB(kTextColor).CGColor;
        _btnNO.layer.cornerRadius = 2;
        _btnNO.layer.masksToBounds = YES;
        [self.selectionView addSubview:_btnNO];
        
        [_btnNO addTarget:self action:@selector(btnNOAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnNO;;
}
-(UIImageView *)headerImgView
{
    if (!_headerImgView) {
        _headerImgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
        _headerImgView.image = [UIImage imageNamed:@"img_privacy"];
        [self.view addSubview:_headerImgView];
    }
    return _headerImgView;
}
-(UILabel *)headerLB
{
    if (!_headerLB) {
        _headerLB = [[UILabel alloc]initWithFrame:self.view.bounds];
        _headerLB.text = @"XXX隐私政策";
        _headerLB.font = [UIFont systemFontOfSize:18];
        _headerLB.textAlignment = NSTextAlignmentCenter;
        _headerLB.textColor = kColorFromRGB(kNameColor);
        _headerLB.backgroundColor = kColorFromRGB(kWhite);
        [self.view addSubview:_headerLB];
    }
    return _headerLB;
}
-(UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tempTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        tempTableView.delegate = self;
        tempTableView.dataSource = self;
        [tempTableView setBackgroundColor:kColorFromRGB(kWhite)];
        [tempTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        tempTableView.estimatedRowHeight = 100;
        
        UINib *nibCell_One = [UINib nibWithNibName:NSStringFromClass([Policy_One_TableViewCell class]) bundle:nil];
        [tempTableView registerNib:nibCell_One forCellReuseIdentifier:NSStringFromClass([Policy_One_TableViewCell class])];
        UINib *nibCell_Two = [UINib nibWithNibName:NSStringFromClass([Policy_Two_TableViewCell class]) bundle:nil];
        [tempTableView registerNib:nibCell_Two forCellReuseIdentifier:NSStringFromClass([Policy_Two_TableViewCell class])];
        UINib *nibCell_Three = [UINib nibWithNibName:NSStringFromClass([Policy_Three_TableViewCell class]) bundle:nil];
        [tempTableView registerNib:nibCell_Three forCellReuseIdentifier:NSStringFromClass([Policy_Three_TableViewCell class])];
        
        [self.view addSubview:tempTableView];
        _tableView = tempTableView;
    }
    return _tableView;
}
-(void)setupUI
{
    kWeakSelf(self);
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(200, 28, 180, 28));
    }];
    [self.headerLB mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(weakself.tableView);
        make.bottom.mas_equalTo(weakself.tableView.mas_top).with.offset(3);
    }];
    [self.headerImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(weakself.tableView);
        make.bottom.mas_equalTo(weakself.headerLB.mas_top);
    }];
    [self.selectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakself.tableView.mas_bottom).with.offset(-3);
        make.height.mas_equalTo(60);
        make.left.right.equalTo(weakself.tableView);
    }];
    [self.btnNO mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(weakself.selectionView.mas_centerX).with.offset(-5);
        make.height.mas_equalTo(45);
        make.centerY.mas_equalTo(0);
    }];
    [self.btnYES mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.left.mas_equalTo(weakself.selectionView.mas_centerX).with.offset(5);
        make.height.mas_equalTo(45);
        make.centerY.mas_equalTo(0);
    }];
}

-(void)btnYESAction:(UIButton *)sender
{
    //同意隐私政策后不再提示改隐私政策
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kShowPrivacyPolicy];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)btnNOAction:(UIButton *)sender
{
    //    kWeakSelf(self);
    
    //    UIAlertController *alertVC = [CustomAlert createAlertWithTitle:nil message:@"您需要同意《XXX隐私政策》方可使用本软件" preferredStyle:UIAlertControllerStyleAlert actionConfirmName:@"我知道了" actionCancelName:nil actionConfirm:nil actionCancel:nil];
    //    [self presentViewController:alertVC animated:YES completion:nil];
    
    //建议使用上面的方法
    //该做法待改进，目前为了解决，alert消失后，“《XXX隐私政策》”这几个字，字体颜色会变色问题
    PolicyWarningViewController *poliwarnVC = [[PolicyWarningViewController alloc]initWithNibName:NSStringFromClass([PolicyWarningViewController class]) bundle:nil];
    poliwarnVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    poliwarnVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:poliwarnVC animated:YES completion:nil];
}


#pragma mark - <模态扫描结果>
-(void)presentScanResultVCWithTitle:(NSString *)title urlStr:(NSString *)urlStr token:(NSString *)token
{
    //内嵌webView打开
    CommonWebViewController *webVC = [[CommonWebViewController alloc]init];
    webVC.urlStr = urlStr;
    webVC.titleStr = title;
    BaseNavigationController *navi = [[BaseNavigationController alloc]initWithRootViewController:webVC];
    
    kWeakSelf(self);
    [self presentViewController:navi animated:YES completion:^{
        [weakself.navigationController popToRootViewControllerAnimated:YES];
    }];
}






#pragma mark - <UITableViewDelegate,UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (indexPath.row == 0) {
        Policy_One_TableViewCell *tempCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([Policy_One_TableViewCell class])];
        cell = tempCell;
    }else if (indexPath.row == 1){
        Policy_Two_TableViewCell *tempCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([Policy_Two_TableViewCell class])];
        cell = tempCell;
    }else if (indexPath.row == 2){
        Policy_Three_TableViewCell *tempCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([Policy_Three_TableViewCell class])];
        cell = tempCell;
        
        kWeakSelf(self);
        tempCell.selectLinkActionBlock = ^{
            [weakself presentScanResultVCWithTitle:@"XXX隐私政策" urlStr:kURL(kPrivacyPolicy) token:nil];
        };
    }
    return cell;
}



-(void)dealloc
{
    NSLog(@"销毁");
}

@end
