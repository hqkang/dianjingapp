//
//  Policy_Three_TableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "Policy_Three_TableViewCell.h"
#import "DJRichText.h"

#define PRIVACY_POLICY @"PRIVACY_POLICY"

@interface Policy_Three_TableViewCell ()<TTTAttributedLabelDelegate>
@property (nonatomic, strong)TTTAttributedLabel *lbContent;
@end

@implementation Policy_Three_TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(TTTAttributedLabel *)lbContent
{
    if (!_lbContent) {
        NSArray *textArray = @[@"如您同意",@"《XXXX隐私政策》",@"请点击“同意”开始使用我们的产品和服务，我们将尽全力保护您的个人信息安全"];
        NSArray *richTxtArray = @[@1];
        NSURL *url = [NSURL URLWithString:PRIVACY_POLICY];
        NSArray *urls = @[url];
        _lbContent = [DJRichText createRichTextLabelWithFrame:self.contentView.bounds delegate:self font:14 textAlignment:NSTextAlignmentLeft normalTextRGBColor:kTextColor richTextRGBColor:kPriceColor textArray:textArray richTextIndex:richTxtArray underLineStyle:kCTUnderlineStyleSingle url:urls];
        [self.contentView addSubview:_lbContent];
    }
    return _lbContent;
}
-(void)setupUI
{
    [self.lbContent mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 15, 10, 10));
    }];
}







#pragma mark - <TTTAttributedLabelDelegate>
-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *urlStr = [url absoluteString];
    if ([urlStr isEqualToString:PRIVACY_POLICY]) {
        if (self.selectLinkActionBlock != nil) {
            self.selectLinkActionBlock();
        }
    }
}


@end
