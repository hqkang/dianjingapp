//
//  MyCourseBaseViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/19.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JXCategoryView.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCourseBaseViewController : UIViewController

@property (nonatomic, strong) JXCategoryBaseView *categoryView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) BOOL shouldHandleScreenEdgeGesture;


- (Class)preferredCategoryViewClass;

- (NSUInteger)preferredListViewCount;

- (CGFloat)preferredCategoryViewHeight;

- (void)configListViewController:(UIViewController *)controller index:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
