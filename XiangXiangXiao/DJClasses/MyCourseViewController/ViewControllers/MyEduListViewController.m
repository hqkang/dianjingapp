//
//  MyEduListViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/19.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyEduListViewController.h"
#import "BaseNavigationController.h"
#import "LoginViewController.h"
#import "MyTeacherListTableViewCell.h"
#import "NewMyTeacherListTableViewCell.h"

#import "CustomDefaultView.h"
#import "LogoutView.h"

#import "MyTeacherListModel.h"

@interface MyEduListViewController ()<UITableViewDelegate,UITableViewDataSource,CustomDefaultViewDelegate>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic, strong)CustomDefaultView *defaultView;
@property (nonatomic, strong) LogoutView *logoutView;

@property (nonatomic, assign)int currentPageNo;
@property (nonatomic, assign)BOOL ableLoadMore;

@property (nonatomic, strong)NSMutableArray *dataArray;// 列表数据信息

@end

@implementation MyEduListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self setupLogoutView];
    [self setupEduUIData];
    [QK_NOTICENTER addObserver:self selector:@selector(setupEduUIData) name:kLoginStatusChange object:nil];
}

#pragma mark - 设置页面数据
- (void)setupEduUIData
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        self.logoutView.hidden = NO;
        [self.view bringSubviewToFront:self.logoutView];
    }else{
        self.logoutView.hidden = YES;
        [self loadOrderTutorListInfo:YES];
    }
}

#pragma mark - 未登录占位
- (void)setupLogoutView
{
    self.logoutView = [LogoutView loadFromNib];
    self.logoutView.frame = self.view.bounds;
    [self.logoutView.loginBtn addTarget:self action:@selector(presentLoginVC) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logoutView];
}

#pragma mark - <loadOrderTutorListInfo>
// 列表数据接口
- (void)loadOrderTutorListInfo:(BOOL)isReload{
    MBProgressHUD *hud = nil;
    if (isReload) {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    // user_type type ing
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"user_type"] = @"user";
    params[@"type"] = @"ing";
    [HttpTool getWithURL:kURL(kOrderTutorInfo) params:params haveHeader:YES success:^(id json) {
        [hud hideAnimated:YES];
        [self.tableView.mj_header endRefreshing];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isReload) {
                    [self.dataArray removeAllObjects];
                }
                
                NSArray *tutorListInfo = dict[@"data"][@"data"];
                for (NSDictionary *dic in tutorListInfo) {
                    MyTeacherListModel *model = [MyTeacherListModel modelObjectWithDictionary:dic];
                    [self.dataArray addObject:model];
                }
                [self.tableView reloadData];
                [self setupUIWithEmptyData:(self.dataArray.count==0)];
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [self setupUIWithEmptyData:YES];
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self setupUIWithEmptyData:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
            
        }
    } failure:^(NSError *error) {
        
        [self setupUIWithEmptyData:YES];
        [hud hideAnimated:YES];
        [self.tableView.mj_header endRefreshing];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        
    }];
}

#pragma mark - <UI>
- (void)setupUIWithEmptyData:(BOOL)isEmptyData
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    if (isEmptyData) {
        [self.view bringSubviewToFront:self.defaultView];
    }else{
        [self.view bringSubviewToFront:self.tableView];
    }
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewMyTeacherListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NewMyTeacherListTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MyTeacherListModel *model = [self.dataArray objAtIndex:indexPath.row];
    cell.model = model;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 213;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tempTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kTabBarHeight - kNavigationBarHeight - kStatusBarHeight - kTabbarSafeBottomMargin) style:UITableViewStylePlain];
        tempTableView.delegate = self;
        tempTableView.dataSource = self;
        [tempTableView setBackgroundColor:kColorFromRGB(kBGColor)];
        [tempTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        tempTableView.estimatedSectionFooterHeight = 0;
        
        [tempTableView registerClass:[MyTeacherListTableViewCell class] forCellReuseIdentifier:NSStringFromClass([MyTeacherListTableViewCell class])];
        [tempTableView registerNib:[UINib nibWithNibName:NSStringFromClass([NewMyTeacherListTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([NewMyTeacherListTableViewCell class])];
        tempTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
        
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself loadOrderTutorListInfo:YES];
        }];
        tempTableView.mj_header = header;
        
        [self.view addSubview:tempTableView];
        _tableView = tempTableView;
    }
    return _tableView;
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"暂无数据";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}

- (void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView{
    [self loadOrderTutorListInfo:YES];
}

#pragma mark - <跳转“登录”页面>
-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}

- (void)dealloc
{
    [QK_NOTICENTER removeObserver:self name:kLoginStatusChange object:nil];
}

@end
