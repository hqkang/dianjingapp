//
//  MyCourseViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/19.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyCourseViewController.h"
#import <JXCategoryView.h>
#import "LoginViewController.h"
#import "BaseNavigationController.h"

@interface MyCourseViewController ()

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;

@end

@implementation MyCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigation];
    
}

- (NSArray *)titles
{
    if (_titles == nil) {
        _titles = @[@"我的课程", @"我的家教"];
    }
    return _titles;
}

#pragma mark - <配置navigation>
- (void)configNavigation
{
    self.categoryView.frame = CGRectMake(0, 0, 186, 40);
    self.myCategoryView = (JXCategoryTitleView *)self.categoryView;
    self.myCategoryView.titleLabelZoomEnabled = YES;
    self.myCategoryView.titleLabelZoomScale = 1.6;
    self.myCategoryView.titleColor = kColorFromRGB(kTextColor);
    self.myCategoryView.titleSelectedColor = kColorFromRGB(kCateTitleColor);
    JXCategoryIndicatorBallView *ballView = [[JXCategoryIndicatorBallView alloc] init];
    ballView.ballViewSize = CGSizeMake(8, 8);
    ballView.ballViewColor = kColorFromRGB(kCateTitleColor);
    self.myCategoryView.indicators = @[ballView];
    self.myCategoryView.titles = self.titles;
    self.myCategoryView.cellSpacing = 0;
    self.myCategoryView.cellWidth = 186/2;
    [self.myCategoryView removeFromSuperview];
    UIBarButtonItem *barBtnItemMsg = [[UIBarButtonItem alloc]initWithCustomView:self.myCategoryView];
    self.navigationItem.leftBarButtonItems = @[barBtnItemMsg];
    
    //客服
    UIButton *btnContact = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnContact setImage:[UIImage imageNamed:@"icon_contact"] forState:UIControlStateNormal];
    [btnContact setFrame:CGRectMake(0, 0, 30, 30)];
    UIBarButtonItem *barBtnItemContact = [[UIBarButtonItem alloc]initWithCustomView:btnContact];
    
    [btnContact addTarget:self action:@selector(btnContactAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[barBtnItemContact];
}

#pragma mark - <跳转“客服”页面>
- (void)btnContactAction
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        [self presentLoginVC];
        return;
    }
}

#pragma mark - <跳转“登录”页面>
-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}

- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (NSUInteger)preferredListViewCount {
    return self.titles.count;
}

- (Class)preferredCategoryViewClass {
    return [JXCategoryTitleView class];
}

- (CGFloat)preferredCategoryViewHeight {
    return 0;
}

@end
