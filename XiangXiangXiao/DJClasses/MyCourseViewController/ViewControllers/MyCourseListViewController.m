//
//  MyCourseListViewController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/19.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyCourseListViewController.h"
#import "BaseNavigationController.h"
#import "LoginViewController.h"
#import "CourseListTableViewCell.h"
#import "CommonWebViewController.h"

#import "CustomDefaultView.h"
#import "LogoutView.h"

#import "MyCourseListModel.h"
#import "CourseModel.h"

@interface MyCourseListViewController ()<UITableViewDelegate,UITableViewDataSource,CustomDefaultViewDelegate>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic, strong)CustomDefaultView *defaultView;
@property (nonatomic, strong) LogoutView *logoutView;
@property (nonatomic, assign)int currentPageNo;
@property (nonatomic, assign)BOOL ableLoadMore;

@property (nonatomic, strong)NSMutableArray *dataArray;// 列表数据信息

@end

@implementation MyCourseListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLogoutView];
    self.view.backgroundColor = kColorFromRGB(kWhite);
    [self setupUIData];
    
    [QK_NOTICENTER addObserver:self selector:@selector(setupUIData) name:kLoginStatusChange object:nil];
}

#pragma mark - 设置页面数据
- (void)setupUIData
{
    if (!kUserDefaultObject(kUserLoginToken)) {
        self.logoutView.hidden = NO;
        [self.view bringSubviewToFront:self.logoutView];
    }else{
        self.logoutView.hidden = YES;
        [self loadMyCourseListInfo:YES];
    }
}

#pragma mark - 未登录占位
- (void)setupLogoutView
{
    self.logoutView = [LogoutView loadFromNib];
    self.logoutView.frame = self.view.bounds;
    [self.logoutView.loginBtn addTarget:self action:@selector(presentLoginVC) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logoutView];
}

#pragma mark - <loadNotiList>
// 列表数据接口
- (void)loadMyCourseListInfo:(BOOL)isReload{
    MBProgressHUD *hud = nil;
    if (isReload) {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    [HttpTool getWithURL:kURL(kClassOrder) params:nil haveHeader:YES success:^(id json) {
        [hud hideAnimated:YES];
        [self.tableView.mj_header endRefreshing];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (isReload) {
                    [self.dataArray removeAllObjects];
                }
                
                NSArray *courseListInfo = dict[@"data"];
                for (NSDictionary *dic in courseListInfo) {
                    MyCourseListModel *model = [MyCourseListModel modelObjectWithDictionary:dic];
                    [self.dataArray addObject:model];
                }
                [self.tableView reloadData];
                [self setupUIWithEmptyData:(self.dataArray.count==0)];
            }else if([codeStr isEqualToString:@"401"]){
                [QKUserDefaultTool removeAllUserDefault];
                [self presentLoginVC];
            }else{
                [self setupUIWithEmptyData:YES];
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
                
            }
        }else{
            [self setupUIWithEmptyData:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
            
        }
    } failure:^(NSError *error) {
        
        [self setupUIWithEmptyData:YES];
        [hud hideAnimated:YES];
        [self.tableView.mj_header endRefreshing];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        
    }];
}

#pragma mark - <UI>
- (void)setupUIWithEmptyData:(BOOL)isEmptyData
{
    [self.defaultView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    if (isEmptyData) {
        [self.view bringSubviewToFront:self.defaultView];
    }else{
        [self.view bringSubviewToFront:self.tableView];
    }
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CourseListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CourseListTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MyCourseListModel *model = [self.dataArray objAtIndex:indexPath.section];
    cell.model = model;
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyCourseListModel *model = [[MyCourseListModel alloc] init];
    model = self.dataArray[indexPath.section];
    CourseModel *courseModel = [[CourseModel alloc] init];
    courseModel = model.course;
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    web.urlStr = courseModel.h5_url;
    [self.navigationController pushViewController:web animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (kScreenWidth-10)/4.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tempTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kTabBarHeight - kNavigationBarHeight - kStatusBarHeight - kTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        tempTableView.delegate = self;
        tempTableView.dataSource = self;
        [tempTableView setBackgroundColor:kColorFromRGB(kBGColor)];
        [tempTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        tempTableView.estimatedRowHeight = 80;
        // iOS11默认开启Self-Sizing，必须关闭Self-Sizing才能使设置分区高度生效
        tempTableView.estimatedRowHeight = 0;
        tempTableView.estimatedSectionHeaderHeight = 0;
        tempTableView.estimatedSectionFooterHeight = 0;
        
        [tempTableView registerClass:[CourseListTableViewCell class] forCellReuseIdentifier:NSStringFromClass([CourseListTableViewCell class])];
        
        tempTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
        
        //下拉刷新
        kWeakSelf(self);
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself loadMyCourseListInfo:YES];
        }];
        tempTableView.mj_header = header;
        
        [self.view addSubview:tempTableView];
        _tableView = tempTableView;
    }
    return _tableView;
}

-(CustomDefaultView *)defaultView
{
    if (!_defaultView) {
        CustomDefaultView *tempDefaultView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([CustomDefaultView class]) owner:nil options:nil].firstObject;
        tempDefaultView.imgName = @"img_error";
        tempDefaultView.warnTitle = @"暂无数据";
        tempDefaultView.buttonName = @"点击刷新";
        tempDefaultView.delegate = self;
        [self.view addSubview:tempDefaultView];
        _defaultView = tempDefaultView;
    }
    return _defaultView;
}

- (void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView{
    [self loadMyCourseListInfo:YES];
}

#pragma mark - <跳转“登录”页面>
-(void)presentLoginVC
{
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    BaseNavigationController *loginNavigationController = [[BaseNavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:loginNavigationController animated:YES completion:nil];
}

- (void)dealloc
{
    [QK_NOTICENTER removeObserver:self name:kLoginStatusChange object:nil];
}

@end
