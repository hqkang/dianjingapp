//
//  MyCourseListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CourseModel;
@class LecturerModel;
@interface MyCourseListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double listID;
@property (nonatomic,strong) NSString *title;//"标题",
@property (nonatomic,strong) NSString *updated_at;//"时间",
@property (nonatomic,strong) NSString *progress;//"进度条",
@property (nonatomic,strong) LecturerModel *seller;
@property (nonatomic,strong) CourseModel *course;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
