//
//  MyCourseListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyCourseListModel.h"
#import "LecturerModel.h"
#import "CourseModel.h"

NSString *const kMyCourseListID = @"id";
NSString *const kMyCourseListTitle = @"title";
NSString *const kMyCourseListProgress = @"progress";
NSString *const kMyCourseListUpdated_at = @"updated_at";
NSString *const kMyCourseListCourse = @"course";
NSString *const kMyCourseListLecturer = @"seller";

@interface MyCourseListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MyCourseListModel

@synthesize listID = _listID;
@synthesize title = _title;
@synthesize progress = _progress;
@synthesize updated_at = _updated_at;
@synthesize course = _course;
@synthesize seller = _seller;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.listID = [[self objectOrNilForKey:kMyCourseListID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kMyCourseListTitle fromDictionary:dict];
        self.updated_at = [self objectOrNilForKey:kMyCourseListUpdated_at fromDictionary:dict];
        self.progress = [self objectOrNilForKey:kMyCourseListID fromDictionary:dict];
        self.course = [CourseModel modelObjectWithDictionary:[dict objectForKey:kMyCourseListCourse]];
        self.seller = [LecturerModel modelObjectWithDictionary:[dict objectForKey:kMyCourseListLecturer]];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.listID] forKey:kMyCourseListID];
    [mutableDict setValue:self.title forKey:kMyCourseListTitle];
    [mutableDict setValue:self.updated_at forKey:kMyCourseListUpdated_at];
    [mutableDict setValue:self.progress forKey:kMyCourseListProgress];
    [mutableDict setValue:[self.course dictionaryRepresentation] forKey:kMyCourseListCourse];
    [mutableDict setValue:[self.seller dictionaryRepresentation] forKey:kMyCourseListLecturer];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.listID = [aDecoder decodeDoubleForKey:kMyCourseListID];
    self.title = [aDecoder decodeObjectForKey:kMyCourseListTitle];
    self.updated_at = [aDecoder decodeObjectForKey:kMyCourseListUpdated_at];
    self.progress = [aDecoder decodeObjectForKey:kMyCourseListProgress];
    self.course = [aDecoder decodeObjectForKey:kMyCourseListCourse];
    self.seller = [aDecoder decodeObjectForKey:kMyCourseListLecturer];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_listID forKey:kMyCourseListID];
    [aCoder encodeObject:_title forKey:kMyCourseListTitle];
    [aCoder encodeObject:_updated_at forKey:kMyCourseListUpdated_at];
    [aCoder encodeObject:_progress forKey:kMyCourseListProgress];
    [aCoder encodeObject:_course forKey:kMyCourseListCourse];
    [aCoder encodeObject:_seller forKey:kMyCourseListLecturer];
}

- (id)copyWithZone:(NSZone *)zone {
    MyCourseListModel *copy = [[MyCourseListModel alloc] init];
    
    if (copy) {
        copy.listID = self.listID;
        copy.title = [self.title copyWithZone:zone];
        copy.updated_at = [self.updated_at copyWithZone:zone];
        copy.progress = [self.progress copyWithZone:zone];
        copy.course = [self.course copyWithZone:zone];
        copy.seller = [self.seller copyWithZone:zone];
    }
    return copy;
}

@end
