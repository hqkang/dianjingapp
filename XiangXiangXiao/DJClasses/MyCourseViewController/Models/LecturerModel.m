//
//  LecturerModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "LecturerModel.h"

NSString *const kLecturerID = @"id";
NSString *const kLecturerNickname = @"nickname";
NSString *const kLecturerAvatar = @"avatar";


@interface LecturerModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LecturerModel

@synthesize lecturerID = _lecturerID;
@synthesize nickname = _nickname;
@synthesize avatar = _avatar;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.lecturerID = [[self objectOrNilForKey:kLecturerID fromDictionary:dict] doubleValue];
        self.nickname = [self objectOrNilForKey:kLecturerNickname fromDictionary:dict];
        self.avatar = [self objectOrNilForKey:kLecturerAvatar fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lecturerID] forKey:kLecturerID];
    [mutableDict setValue:self.nickname forKey:kLecturerNickname];
    [mutableDict setValue:self.avatar forKey:kLecturerAvatar];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.lecturerID = [aDecoder decodeDoubleForKey:kLecturerID];
    self.nickname = [aDecoder decodeObjectForKey:kLecturerNickname];
    self.avatar = [aDecoder decodeObjectForKey:kLecturerAvatar];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_lecturerID forKey:kLecturerID];
    [aCoder encodeObject:_nickname forKey:kLecturerNickname];
    [aCoder encodeObject:_avatar forKey:kLecturerAvatar];
}

- (id)copyWithZone:(NSZone *)zone {
    LecturerModel *copy = [[LecturerModel alloc] init];
    
    if (copy) {
        copy.lecturerID = self.lecturerID;
        copy.nickname = [self.nickname copyWithZone:zone];
        copy.avatar = [self.avatar copyWithZone:zone];
    }
    return copy;
}

@end
