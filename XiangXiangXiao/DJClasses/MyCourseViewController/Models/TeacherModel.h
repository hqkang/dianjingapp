//
//  TeacherModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeacherModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double teacherID;
@property (nonatomic,strong) NSString *avatar;//"老师头像",
@property (nonatomic,strong) NSString *username;//"老师姓名",

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
