//
//  CourseModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CourseModel.h"

NSString *const kCourseID = @"id";
NSString *const kCourseTitle = @"title";
NSString *const kCourseCover = @"cover";
NSString *const kCourseH5_url = @"h5_url";

@interface CourseModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CourseModel

@synthesize courseID = _courseID;
@synthesize title = _title;
@synthesize cover = _cover;
@synthesize h5_url = _h5_url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.courseID = [[self objectOrNilForKey:kCourseID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kCourseTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kCourseCover fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kCourseH5_url fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.courseID] forKey:kCourseID];
    [mutableDict setValue:self.title forKey:kCourseTitle];
    [mutableDict setValue:self.cover forKey:kCourseCover];
    [mutableDict setValue:self.h5_url forKey:kCourseH5_url];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.courseID = [aDecoder decodeDoubleForKey:kCourseID];
    self.title = [aDecoder decodeObjectForKey:kCourseTitle];
    self.cover = [aDecoder decodeObjectForKey:kCourseCover];
    self.h5_url = [aDecoder decodeObjectForKey:kCourseH5_url];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_courseID forKey:kCourseID];
    [aCoder encodeObject:_title forKey:kCourseTitle];
    [aCoder encodeObject:_cover forKey:kCourseCover];
    [aCoder encodeObject:_h5_url forKey:kCourseH5_url];
}

- (id)copyWithZone:(NSZone *)zone {
    CourseModel *copy = [[CourseModel alloc] init];
    
    if (copy) {
        copy.courseID = self.courseID;
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
    }
    return copy;
}

@end
