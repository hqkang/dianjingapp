//
//  TutorModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TutorModel.h"

NSString *const kTutorID = @"id";
NSString *const kTutorTitle = @"title";
NSString *const kTutorCover = @"cover";
NSString *const kTutorH5_url = @"h5_url";

@interface TutorModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TutorModel

@synthesize tutorID = _tutorID;
@synthesize title = _title;
@synthesize cover = _cover;
@synthesize h5_url = _h5_url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.tutorID = [[self objectOrNilForKey:kTutorID fromDictionary:dict] doubleValue];
        self.title = [self objectOrNilForKey:kTutorTitle fromDictionary:dict];
        self.cover = [self objectOrNilForKey:kTutorCover fromDictionary:dict];
        self.h5_url = [self objectOrNilForKey:kTutorH5_url fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tutorID] forKey:kTutorID];
    [mutableDict setValue:self.title forKey:kTutorTitle];
    [mutableDict setValue:self.cover forKey:kTutorCover];
    [mutableDict setValue:self.h5_url forKey:kTutorH5_url];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.tutorID = [aDecoder decodeDoubleForKey:kTutorID];
    self.title = [aDecoder decodeObjectForKey:kTutorTitle];
    self.cover = [aDecoder decodeObjectForKey:kTutorCover];
    self.h5_url = [aDecoder decodeObjectForKey:kTutorH5_url];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_tutorID forKey:kTutorID];
    [aCoder encodeObject:_title forKey:kTutorTitle];
    [aCoder encodeObject:_cover forKey:kTutorCover];
    [aCoder encodeObject:_h5_url forKey:kTutorH5_url];
}

- (id)copyWithZone:(NSZone *)zone {
    TutorModel *copy = [[TutorModel alloc] init];
    
    if (copy) {
        copy.tutorID = self.tutorID;
        copy.title = [self.title copyWithZone:zone];
        copy.cover = [self.cover copyWithZone:zone];
        copy.h5_url = [self.h5_url copyWithZone:zone];
    }
    return copy;
}

@end
