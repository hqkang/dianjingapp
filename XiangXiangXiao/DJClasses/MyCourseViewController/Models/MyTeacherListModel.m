//
//  MyTeacherListModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyTeacherListModel.h"
#import "TutorModel.h"
#import "TeacherModel.h"

NSString *const kMyTeacherListID = @"id";
NSString *const kMyTeacherListProgress = @"progress";
NSString *const kMyTeacherListUpdated_at = @"updated_at";
NSString *const kMyTeacherListTeacher = @"teacher";
NSString *const kMyTeacherListTutorship = @"tutorship";
NSString *const kMyTeacherListSubject_data = @"subject_data";

@interface MyTeacherListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MyTeacherListModel

@synthesize listID = _listID;
@synthesize progress = _progress;
@synthesize updated_at = _updated_at;
@synthesize teacher = _teacher;
@synthesize tutorship = _tutorship;
@synthesize subject_data = _subject_data;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.listID = [[self objectOrNilForKey:kMyTeacherListID fromDictionary:dict] doubleValue];
        self.updated_at = [self objectOrNilForKey:kMyTeacherListUpdated_at fromDictionary:dict];
        self.progress = [self objectOrNilForKey:kMyTeacherListProgress fromDictionary:dict];
        self.teacher = [TeacherModel modelObjectWithDictionary:[dict objectForKey:kMyTeacherListTeacher]];
        self.tutorship = [TutorModel modelObjectWithDictionary:[dict objectForKey:kMyTeacherListTutorship]];
        self.subject_data = [self objectOrNilForKey:kMyTeacherListSubject_data fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.listID] forKey:kMyTeacherListID];
    [mutableDict setValue:self.updated_at forKey:kMyTeacherListUpdated_at];
    [mutableDict setValue:self.progress forKey:kMyTeacherListProgress];
    [mutableDict setValue:[self.teacher dictionaryRepresentation] forKey:kMyTeacherListTeacher];
    [mutableDict setValue:[self.tutorship dictionaryRepresentation] forKey:kMyTeacherListTutorship];
    [mutableDict setValue:self.subject_data forKey:kMyTeacherListSubject_data];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.listID = [aDecoder decodeDoubleForKey:kMyTeacherListID];
    self.updated_at = [aDecoder decodeObjectForKey:kMyTeacherListUpdated_at];
    self.progress = [aDecoder decodeObjectForKey:kMyTeacherListProgress];
    self.teacher = [aDecoder decodeObjectForKey:kMyTeacherListTeacher];
    self.tutorship = [aDecoder decodeObjectForKey:kMyTeacherListTutorship];
    self.subject_data = [aDecoder decodeObjectForKey:kMyTeacherListSubject_data];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_listID forKey:kMyTeacherListID];
    [aCoder encodeObject:_updated_at forKey:kMyTeacherListUpdated_at];
    [aCoder encodeObject:_progress forKey:kMyTeacherListProgress];
    [aCoder encodeObject:_teacher forKey:kMyTeacherListTeacher];
    [aCoder encodeObject:_tutorship forKey:kMyTeacherListTutorship];
    [aCoder encodeObject:_subject_data forKey:kMyTeacherListSubject_data];
}

- (id)copyWithZone:(NSZone *)zone {
    MyTeacherListModel *copy = [[MyTeacherListModel alloc] init];
    
    if (copy) {
        copy.listID = self.listID;
        copy.updated_at = [self.updated_at copyWithZone:zone];
        copy.progress = [self.progress copyWithZone:zone];
        copy.teacher = [self.teacher copyWithZone:zone];
        copy.tutorship = [self.tutorship copyWithZone:zone];
        self.subject_data = [self.subject_data copyWithZone:zone];
    }
    return copy;
}

@end
