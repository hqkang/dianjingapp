//
//  LecturerModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/28.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LecturerModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double lecturerID;
@property (nonatomic,strong) NSString *nickname;//"昵称",
@property (nonatomic,strong) NSString *avatar;//"头像",

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
