//
//  TeacherModel.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "TeacherModel.h"

NSString *const kTeacherID = @"id";
NSString *const kTeacherAvatar = @"avatar";
NSString *const kTeacherUsername = @"username";

@interface TeacherModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TeacherModel

@synthesize teacherID = _teacherID;
@synthesize avatar = _avatar;
@synthesize username = _username;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.teacherID = [[self objectOrNilForKey:kTeacherID fromDictionary:dict] doubleValue];
        self.avatar = [self objectOrNilForKey:kTeacherAvatar fromDictionary:dict];
        self.username = [self objectOrNilForKey:kTeacherUsername fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.teacherID] forKey:kTeacherID];
    [mutableDict setValue:self.avatar forKey:kTeacherAvatar];
    [mutableDict setValue:self.username forKey:kTeacherUsername];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.teacherID = [aDecoder decodeDoubleForKey:kTeacherID];
    self.avatar = [aDecoder decodeObjectForKey:kTeacherAvatar];
    self.username = [aDecoder decodeObjectForKey:kTeacherUsername];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_teacherID forKey:kTeacherID];
    [aCoder encodeObject:_avatar forKey:kTeacherAvatar];
    [aCoder encodeObject:_username forKey:kTeacherUsername];
}

- (id)copyWithZone:(NSZone *)zone {
    TeacherModel *copy = [[TeacherModel alloc] init];
    
    if (copy) {
        copy.teacherID = self.teacherID;
        copy.avatar = [self.avatar copyWithZone:zone];
        copy.username = [self.username copyWithZone:zone];
    }
    return copy;
}

@end
