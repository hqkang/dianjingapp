//
//  MyTeacherListModel.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TutorModel;
@class TeacherModel;
@interface MyTeacherListModel : NSObject<NSCoding, NSCopying>

@property (nonatomic,assign) double listID;
@property (nonatomic,strong) NSString *updated_at;//"时间",
@property (nonatomic,strong) NSString *progress;//"进度条",
@property (nonatomic,strong) TutorModel *tutorship;
@property (nonatomic,strong) TeacherModel *teacher;
@property (nonatomic,strong) NSDictionary *subject_data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
