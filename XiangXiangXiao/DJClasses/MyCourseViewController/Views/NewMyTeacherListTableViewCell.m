//
//  NewMyTeacherListTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "NewMyTeacherListTableViewCell.h"
#import "TutorModel.h"
#import "TeacherModel.h"

@implementation NewMyTeacherListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MyTeacherListModel *)model
{
    if (_model != model) {
        _model = model;
    }
    TutorModel *tutorModel = [[TutorModel alloc] init];
    TeacherModel *teacherModel = [[TeacherModel alloc] init];
    NSDictionary *subject_dataDic = [NSDictionary dictionary];
    subject_dataDic = model.subject_data;
    tutorModel = model.tutorship;
    teacherModel = model.teacher;
    self.classTitleLabel.text = tutorModel.title;
    self.classNameLabel.text = [NSString stringWithFormat:@"授课课程: %@", subject_dataDic[@"name"]];
    self.timeLabel.text = [NSString stringWithFormat:@"预约时间: %@", model.updated_at];
    self.teacherNameLabel.text = [NSString stringWithFormat:@"授课老师: %@", teacherModel.username];
    [self.teacherImageView sd_setImageWithURL:[NSURL URLWithString:kImgURL(teacherModel.avatar)] placeholderImage:nil options:SDWebImageRefreshCached];
}

@end
