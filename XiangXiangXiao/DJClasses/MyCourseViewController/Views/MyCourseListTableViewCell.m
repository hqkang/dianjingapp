//
//  MyCourseListTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyCourseListTableViewCell.h"

@implementation MyCourseListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.progressView.backgroundColor = [UIColor whiteColor];
    [self.progressView setProgressColor:[UIColor redColor]];
    [self.progressView setProgressTextColor:kColorFromRGB(kWhite)];
    self.progressView.lineDashPattern = @[@(0.1),@(0.1)];
    self.progressView.progressFont = [UIFont systemFontOfSize:20];
//    [self.progressView updateProgress:valueNum];
}

- (void)setModel:(MyCourseListModel *)model
{
    if (_model != model) {
        _model = model;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
