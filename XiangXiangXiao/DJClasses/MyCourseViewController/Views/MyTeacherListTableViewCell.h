//
//  MyTeacherListTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTeacherListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyTeacherListTableViewCell : UITableViewCell

@property (nonatomic, copy) MyTeacherListModel *model;

@end

NS_ASSUME_NONNULL_END
