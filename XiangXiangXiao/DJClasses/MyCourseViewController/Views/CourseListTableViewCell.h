//
//  CourseListTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MyCourseListModel;
@interface CourseListTableViewCell : UITableViewCell

@property (nonatomic, copy) MyCourseListModel *model;

@end

NS_ASSUME_NONNULL_END
