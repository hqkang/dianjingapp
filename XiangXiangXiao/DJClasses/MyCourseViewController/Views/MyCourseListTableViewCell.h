//
//  MyCourseListTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundProgressView.h"
#import "MyCourseListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCourseListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RoundProgressView *progressView;

@property (nonatomic, copy) MyCourseListModel *model;

@end

NS_ASSUME_NONNULL_END
