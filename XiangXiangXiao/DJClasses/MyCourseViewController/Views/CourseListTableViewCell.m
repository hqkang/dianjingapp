//
//  CourseListTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/24.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CourseListTableViewCell.h"
#import "MyCourseListModel.h"
#import "RoundProgressView.h"
#import "CourseModel.h"

@interface CourseListTableViewCell ()

@property (strong,nonatomic) UIImageView *classImgV;

@property (strong,nonatomic) UILabel *classTitleLab;
@property (strong,nonatomic) UILabel *classTimeLab;
@property (strong,nonatomic) RoundProgressView *progressView;

@end

@implementation CourseListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = kColorFromRGB(kBGColor);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
        
    }
    return self;
}


- (void)setModel:(MyCourseListModel *)model{
    _model = model;
    self.classTitleLab.text = model.title;
    self.classTimeLab.text = model.updated_at;
    self.progressView.progressLabel.text = @"25";
    [self.progressView updateProgress:25];
    CourseModel *courseModel = [[CourseModel alloc] init];
    courseModel = model.course;
    [self.classImgV sd_setImageWithURL:[NSURL URLWithString:kImgURL(courseModel.cover)] placeholderImage:nil options:SDWebImageRefreshCached];
    
}

- (void)setupUI{
    kWeakSelf(self);
    
    UIImageView *imgv = [[UIImageView alloc]init];
    imgv.image = [UIImage imageNamed:@"bg_spell_group"];
    [self.contentView addSubview:imgv];
    
    [imgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    
    self.classImgV = [[UIImageView alloc]init];
    self.classImgV.layer.cornerRadius = 4;
    self.classImgV.layer.masksToBounds = YES;
    self.classImgV.contentMode = UIViewContentModeScaleAspectFill;
    self.classImgV.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.classImgV];
    
    [self.classImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakself.contentView).offset(24);
        make.width.mas_equalTo(110);
        make.height.mas_equalTo(60);
        make.centerY.equalTo(weakself.contentView);
    }];
    
    self.progressView = [[RoundProgressView alloc] initWithFrame:CGRectMake(kScreenWidth - 58, (self.contentView.height - 50) / 2, 50, 50)];
    self.progressView.backgroundColor = [UIColor whiteColor];
    [self.progressView setProgressColor:[UIColor redColor]];
    [self.progressView setProgressTextColor:[UIColor redColor]];
    self.progressView.lineDashPattern = @[@(0.1),@(0.1)];
    self.progressView.progressFont = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:self.progressView];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakself.contentView).offset(-18);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(50);
        make.centerY.equalTo(weakself.contentView);
    }];
    
    
    self.classTitleLab = [[UILabel alloc]init];
    self.classTitleLab.textColor = kColorFromRGB(kNameColor);
    self.classTitleLab.numberOfLines = 2;
    self.classTitleLab.font = [UIFont font_PFSC_BoldWithSize:14];
    [self.contentView addSubview:self.classTitleLab];
    
    [self.classTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.classImgV.mas_right).offset(8);
        make.right.equalTo(self.progressView.mas_left).offset(-8);
        make.top.equalTo(self.classImgV).offset(3);
    }];
    
    self.classTimeLab = [[UILabel alloc]init];
    self.classTimeLab.backgroundColor = [UIColor whiteColor];
    self.classTimeLab.textColor = kColorFromRGB(kTabTextColor);
    self.classTimeLab.font = [UIFont font_PFSC_MediumWithSize:10];
    
    [self.contentView addSubview:self.classTimeLab];
    
    [self.classTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.classTitleLab);
        make.right.mas_equalTo(self.progressView.mas_left).offset(-8);
        make.top.equalTo(self.classImgV.mas_bottom).offset(-18);
        make.height.mas_equalTo(18);
    }];
    
}

@end
