//
//  MyTeacherListTableViewCell.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "MyTeacherListTableViewCell.h"
#import "TutorModel.h"
#import "TeacherModel.h"

@interface MyTeacherListTableViewCell ()

@property (strong,nonatomic) UIImageView *teacherImgV;

@property (strong,nonatomic) UILabel *tutorTitleLab;
@property (strong,nonatomic) UILabel *teacherNameLab;
@property (strong,nonatomic) UILabel *tutorTimeLab;
@property (strong,nonatomic) UILabel *classNameLab;
@property (strong,nonatomic) UILabel *progressLab;
@property (strong,nonatomic) UIButton *appointmentBtn;
@property (strong,nonatomic) UIButton *signBtn;
@property (strong,nonatomic) UIButton *contactBtn;
@end

@implementation MyTeacherListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = kColorFromRGB(kBGColor);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
        
    }
    return self;
}


- (void)setModel:(MyTeacherListModel *)model{
    _model = model;
    TutorModel *tutorModel = [[TutorModel alloc] init];
    TeacherModel *teacherModel = [[TeacherModel alloc] init];
    NSDictionary *subject_dataDic = [NSDictionary dictionary];
    subject_dataDic = model.subject_data;
    tutorModel = model.tutorship;
    teacherModel = model.teacher;
    self.tutorTitleLab.text = tutorModel.title;
    self.classNameLab.text = [NSString stringWithFormat:@"授课课程: %@", subject_dataDic[@"category"][@"name"]];
    self.tutorTimeLab.text = [NSString stringWithFormat:@"预约时间: %@", model.updated_at];
    self.teacherNameLab.text = [NSString stringWithFormat:@"授课老师: %@", teacherModel.username];
    [self.teacherImgV sd_setImageWithURL:[NSURL URLWithString:kImgURL(teacherModel.avatar)] placeholderImage:nil options:SDWebImageRefreshCached];
}

- (void)setupUI{
    kWeakSelf(self);
    // 背景
    UIImageView *imgv = [[UIImageView alloc]init];
    imgv.image = [UIImage imageNamed:@"bg_spell_group"];
    [self.contentView addSubview:imgv];

    [imgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(-5);
        make.bottom.equalTo(self.contentView).offset(16);
    }];
    
    // 老师头像
    self.teacherImgV = [[UIImageView alloc]init];
    self.teacherImgV.layer.cornerRadius = 4;
    self.teacherImgV.layer.masksToBounds = YES;
    self.teacherImgV.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.teacherImgV];
    
    [self.teacherImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakself.contentView).offset(24);
        make.top.equalTo(weakself.contentView).offset(16);
        make.width.mas_equalTo(121);
        make.height.mas_equalTo(68);
    }];
    
    // 标题
    self.tutorTitleLab = [[UILabel alloc]init];
    self.tutorTitleLab.textColor = kColorFromRGB(kNameColor);
    self.tutorTitleLab.numberOfLines = 0;
    self.tutorTitleLab.font = [UIFont font_PFSC_BoldWithSize:14];
    [self.contentView addSubview:self.tutorTitleLab];
    
    [self.tutorTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.teacherImgV.mas_right).offset(8);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.teacherImgV).offset(3);
        make.height.mas_equalTo(60);
    }];
    
    // 课程名
    self.classNameLab = [[UILabel alloc]init];
    self.classNameLab.textColor = kColorFromRGB(kTabTextColor);
    self.classNameLab.font = [UIFont font_PFSC_MediumWithSize:14];
    [self.contentView addSubview:self.classNameLab];
    
    [self.classNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.teacherImgV.mas_right).offset(-24);
        make.right.mas_equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.teacherImgV.mas_bottom).offset(8);
        make.height.mas_equalTo(18);
    }];
    
    // 老师名
    self.teacherNameLab = [[UILabel alloc]init];
    self.teacherNameLab.textColor = kColorFromRGB(kTabTextColor);
    self.teacherNameLab.font = [UIFont font_PFSC_MediumWithSize:14];
    [self.contentView addSubview:self.teacherNameLab];
    
    [self.teacherNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.classNameLab);
        make.top.equalTo(self.classNameLab.mas_bottom).offset(8);
        make.width.mas_equalTo(160);
        make.height.mas_equalTo(18);
    }];
    
    // 沟通
    self.contactBtn = [[UIButton alloc]init];
    self.contactBtn.titleLabel.textColor = kColorFromRGB(kTabTextColor);
    self.contactBtn.titleLabel.font = [UIFont font_PFSC_MediumWithSize:14];
    self.contactBtn.backgroundColor = [UIColor greenColor];
    [self.contentView addSubview:self.contactBtn];
    
    [self.contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.teacherNameLab.mas_right).offset(5);
        make.width.height.mas_equalTo(16);
        make.centerY.mas_equalTo(self.teacherNameLab);
    }];
    
    // 上课时间
    self.tutorTimeLab = [[UILabel alloc]init];
    self.tutorTimeLab.backgroundColor = [UIColor whiteColor];
    self.tutorTimeLab.textColor = kColorFromRGB(kTabTextColor);
    self.tutorTimeLab.font = [UIFont font_PFSC_MediumWithSize:14];
    
    [self.contentView addSubview:self.tutorTimeLab];
    
    [self.tutorTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.classNameLab);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.teacherNameLab.mas_bottom).offset(8);
        make.height.mas_equalTo(18);
    }];
    
    // 家教进度
    self.progressLab = [[UILabel alloc] init];
    self.progressLab.backgroundColor = [UIColor whiteColor];
    self.progressLab.textColor = kColorFromRGB(kTabTextColor);
    self.progressLab.font = [UIFont font_PFSC_MediumWithSize:15];
    self.progressLab.text = @"家教进度";
    [self.contentView addSubview:self.progressLab];
    
    [self.progressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.teacherImgV);
        make.top.equalTo(self.tutorTimeLab.mas_bottom).offset(16);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(80);
    }];
}

@end
