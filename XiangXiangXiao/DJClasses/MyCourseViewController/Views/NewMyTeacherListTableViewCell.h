//
//  NewMyTeacherListTableViewCell.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/1.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTeacherListModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol NewMyTeacherListTableViewDelegate <NSObject>

- (void)newMyTeacherListTableViewBtnClick:(UIButton *)button;

@end

@interface NewMyTeacherListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *teacherImageView;
@property (weak, nonatomic) IBOutlet UILabel *classTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *teacherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;

@property (nonatomic, weak) id<NewMyTeacherListTableViewDelegate>delegate;

@property (nonatomic, copy) MyTeacherListModel *model;

@end

NS_ASSUME_NONNULL_END
