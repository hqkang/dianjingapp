//
//  LoginViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/11/24.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ResetPasswordViewController.h"
#import <RongIMKit/RongIMKit.h>
#import <NIMSDK/NIMSDK.h>
#import "NTESService.h"

@interface LoginViewController ()<UITextFieldDelegate, RCIMUserInfoDataSource>

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIImageView *eyeImageView;
@property (weak, nonatomic) IBOutlet UILabel *protocolLabel;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIView *resetPswView;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;

@property (nonatomic, strong) NSString *friendNickName;

@property (nonatomic, strong) NSString *friendAvatar;

@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"登录";
    [self setupNavigationBarItem];
    [self.phoneTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    
}



#pragma mark - 动态监测textfield
- (void)textFieldDidChange
{
    if (self.phoneTextField.text.length > 0 && self.passwordTextField.text.length > 0) {
        // 128 91 204
        self.loginBtn.backgroundColor = [UIColor colorWithRed:128.0/255.0 green:91.0/255.0 blue:204.0/255.0 alpha:1];
        [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else
    {
        self.loginBtn.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
        [self.loginBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1] forState:UIControlStateNormal];
    }
}

- (void)setupNavigationBarItem{
    //设置返回按钮
    UIButton *btnBack = [[UIButton alloc]init];
    btnBack = [self createBackButtonWithImage:@"nav_btn_back"];
    UIBarButtonItem *itemBackButton = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = itemBackButton;
}

#pragma mark - <设置返回按钮>
-(UIButton *)createBackButtonWithImage:(NSString *)imageName
{
    UIImage *imgBack = [UIImage imageNamed:imageName];
    imgBack = [imgBack imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 0, 30, 30);
    [btnBack setImage:imgBack forState:UIControlStateNormal];
    //    [btnBack.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [btnBack addTarget:self action:@selector(backNormalAction) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setContentEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    return btnBack;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (kUserDefaultObject(kUserPhone)) {
        self.phoneTextField.text = kUserDefaultObject(kUserPhone);
    }
    if (kUserDefaultObject(kUserPsw)) {
        self.passwordTextField.text = kUserDefaultObject(kUserPsw);
    }
    if (self.phoneTextField.text.length > 0 && self.passwordTextField.text.length > 0) {
        self.loginBtn.backgroundColor = [UIColor colorWithRed:128.0/255.0 green:91.0/255.0 blue:204.0/255.0 alpha:1];
        [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - 密码是否可见
- (IBAction)passwordSecureAction:(UIButton *)sender {
    self.eyeImageView.image = sender.selected ? [UIImage imageNamed:@"eye_close"] : [UIImage imageNamed:@"eye_open"];
    self.passwordTextField.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
}

#pragma mark - 登录操作
- (IBAction)loginAction:(UIButton *)sender {
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (![self.phoneTextField.text isVaildElevenNum]) {
        [MBProgressHUD bwm_showTitle:@"请输入有效手机号码" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([NSString isEmpty:self.passwordTextField.text]) {
        [MBProgressHUD bwm_showTitle:@"请输入6到20位密码!" toView:self.view hideAfter:kDelay];
        return;
    }
    
    params[@"password"] = self.passwordTextField.text;
    params[@"phone"] = self.phoneTextField.text;
    params[@"grant_type"] = @"password";
    params[@"client_id"] = CLIENT_ID;
    params[@"client_secret"] = CLIENT_SECRET;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool postWithURL:kURL(kLogin) params:params haveHeader:NO success:^(id json) {
        if (json) {
            NSDictionary *dic = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dic[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                NSString *loginToken = [NSString stringWithFormat:@"%@", dic[@"access_token"]];
                NSString *refreshToken = [NSString stringWithFormat:@"%@", dic[@"refresh_token"]];
                NSString *userID = [NSString stringWithFormat:@"%@", dic[@"user_id"]];
                NSString *rongcloud_token = [NSString stringWithFormat:@"%@", dic[@"rongcloud_token"]];
                NSString *yunxin_token = [NSString stringWithFormat:@"%@", dic[@"yunxin_token"]];
                NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dic];
                kUserDefaultRemoveObject(kLoginData);
                kUserDefaultSetObject(userEncodedObject, kLoginData);
                
                //保存token
                kUserDefaultSetObject(self.phoneTextField.text, kUserPhone);
                kUserDefaultSetObject(self.passwordTextField.text, kUserPsw);
                kUserDefaultSetObject(loginToken, kUserLoginToken);
                kUserDefaultSetObject(yunxin_token, kYunXinToken);
                kUserDefaultSetObject(rongcloud_token, kRCTOKEN);
                kUserDefaultSetObject(userID, kUserID);
                kUserDefaultSetObject(refreshToken, kRefreshToken);
                kUserDefaultSynchronize;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getPersonalDataAction];
                });
            }else{
                [self.hud hideAnimated:YES];
                [MBProgressHUD bwm_showTitle:dic[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self.hud hideAnimated:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.hud hideAnimated:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
    
    
}


#pragma mark - 登录云信
- (void)loginYunXin
{
    NSString *account = kUserDefaultObject(kUserPhone);
    NSString *token   = kUserDefaultObject(kYunXinToken);
    [[[NIMSDK sharedSDK] loginManager] autoLogin:account
                                           token:token];
    [[NTESServiceManager sharedManager] start];
}

#pragma mark - 登录融云服务器
- (void)loginRC
{
    //登录融云服务器
    NSString *rcTokenStr = kUserDefaultObject(kRCTOKEN);
    [[RCIM sharedRCIM] connectWithToken:rcTokenStr     success:^(NSString *userId) {

        NSLog(@"登陆成功。当前登录的用户ID：%@", userId);
        dispatch_async(dispatch_get_main_queue(), ^{
            kUserDefaultSetObject(userId, kUserID);
            kUserDefaultSynchronize;
            NSDictionary *userDic = [NSDictionary dictionary];
            userDic = kUserDefaultObject(kUserInfo);
            // 设置消息体内是否携带用户信息
            [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;
            RCUserInfo *currentUserInfo = [[RCUserInfo alloc] initWithUserId:kUserDefaultObject(kUserID) name:[userDic objectForKey:@"nickname"] portrait:kImgURL(@"avatar")];
            [RCIM sharedRCIM].currentUserInfo = currentUserInfo;
        });
        
    } error:^(RCConnectErrorCode status) {
        if (status == RC_CONNECTION_EXIST) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [RCIM sharedRCIM].userInfoDataSource = self;
//            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.hud hideAnimated:YES];
            [QK_NOTICENTER postNotificationName:kLoginStatusChange object:nil userInfo:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"登陆的错误码为:%ld", (long)status);
        
    } tokenIncorrect:^{
        [self.hud hideAnimated:YES];
        //token过期或者不正确。
        //如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
        //如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。
        NSLog(@"token错误");
    }];
}

- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion
{
   
}

#pragma mark - 获取个人信息
- (void)getPersonalDataAction
{
    [HttpTool postWithURL:kURL(kPersonData) params:nil haveHeader:YES success:^(id json) {
        if (json) {
            NSDictionary *dic = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dic[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                // 头像
                NSString *avatar = nil;
                if (dic[@"avatar"] == [NSNull null]) {
                    avatar = @"";
                }else{
                    avatar = [NSString stringWithFormat:@"%@", dic[@"avatar"]];
                }
                // 电话
                NSString *phone = nil;
                if (dic[@"phone"] == [NSNull null]) {
                    phone = @"";
                }else{
                    phone = [NSString stringWithFormat:@"%@", dic[@"phone"]];
                }
                // 昵称 nickname
                NSString *nickname = nil;
                if (dic[@"nickname"] == [NSNull null]) {
                    nickname = @"";
                }else{
                    nickname = [NSString stringWithFormat:@"%@", dic[@"nickname"]];
                }
                // 用户名 username
                NSString *username = nil;
                if (dic[@"username"] == [NSNull null]) {
                    username = @"";
                }else{
                    username = [NSString stringWithFormat:@"%@", dic[@"username"]];
                }
                // 用户id
                NSString *userID = nil;
                if (dic[@"id"] == [NSNull null]) {
                    userID = @"";
                }else{
                    userID = [NSString stringWithFormat:@"%@", dic[@"id"]];
                }
                // balance 金钱
                NSString *balanceStr = nil;
                if (dic[@"balance"] == [NSNull null]) {
                    balanceStr = @"";
                }else{
                    balanceStr = [NSString stringWithFormat:@"%@", dic[@"balance"]];
                }
                // subscrib_count 关注数
                NSString *subscrib_count = nil;
                if (dic[@"subscrib_count"] == [NSNull null]) {
                    subscrib_count = @"";
                }else{
                    subscrib_count = [NSString stringWithFormat:@"%@", dic[@"subscrib_count"]];
                }
                // fans_count 粉丝数
                NSString *fans_count = nil;
                if (dic[@"fans_count"] == [NSNull null]) {
                    fans_count = @"";
                }else{
                    fans_count = [NSString stringWithFormat:@"%@", dic[@"fans_count"]];
                }
                // 融云token rongcloud_token
                NSString *rongcloud_token = nil;
                if (dic[@"rongcloud_token"] == [NSNull null]) {
                    rongcloud_token = @"";
                }else{
                    rongcloud_token = [NSString stringWithFormat:@"%@", dic[@"rongcloud_token"]];
                }
                // 云信token yunxin_token
                NSString *yunxin_token = nil;
                if (dic[@"yunxin_token"] == [NSNull null]) {
                    yunxin_token = @"";
                }else{
                    yunxin_token = [NSString stringWithFormat:@"%@", dic[@"yunxin_token"]];
                }
                /*
                 "is_auth" = @"student";
                 "is_merchant" = 0;
                 "is_sign_teacher" = 0;
                 "is_student" = 0;
                 "is_teacher" = 0;
                 */
                NSString *is_enterprise = [NSString stringWithFormat:@"%@", dic[@"is_enterprise"]];
                NSString *is_merchant = [NSString stringWithFormat:@"%@", dic[@"is_merchant"]];
                NSString *is_sign_teacher = [NSString stringWithFormat:@"%@", dic[@"is_sign_teacher"]];
                NSString *is_student = [NSString stringWithFormat:@"%@", dic[@"is_student"]];
                NSString *is_teacher = [NSString stringWithFormat:@"%@", dic[@"is_teacher"]];
                // 登录token logintoken
                NSString *loginToken = kUserDefaultObject(kUserLoginToken);
                NSDictionary *dictInfo = @{
                                           @"avatar":avatar,
                                           @"phone":phone,
                                           @"nickname":nickname,
                                           @"username":username,
                                           @"userID":userID,
                                           @"subscrib_count":subscrib_count,
                                           @"fans_count":fans_count,
                                           @"rongcloud_token":rongcloud_token,
                                           @"yunxin_token":yunxin_token,
                                           @"loginToken":loginToken,
                                           @"is_enterprise":is_enterprise,
                                           @"is_sign_teacher":is_sign_teacher,
                                           @"is_student":is_student,
                                           @"is_teacher":is_teacher,
                                           @"balance":balanceStr
                                           };
                
                NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dic];
                kUserDefaultRemoveObject(kTotalUserInfo);
                kUserDefaultSetObject(userEncodedObject, kTotalUserInfo);
                
                //保存用户信息
                kUserDefaultSetObject(dictInfo, kUserInfo);
                kUserDefaultSynchronize;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 创建信号量
                    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                    // 创建全局并行
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_group_t group = dispatch_group_create();
                    
                    // 登录融云
                    dispatch_group_async(group, queue, ^{
                        [self loginRC];
                    });
                    // 登录云信
                    dispatch_group_async(group, queue, ^{
                        
                        [self loginYunXin];
                    });
                    
                    dispatch_group_notify(group, queue, ^{
                        
                        // 两个请求对应两次信号等待
                        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                        //在这里 进行请求后的方法，回到主线程
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.hud hideAnimated:YES];
                            [QK_NOTICENTER postNotificationName:kLoginStatusChange object:nil userInfo:nil];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        });
                    });
                });
            }else{
                [self.hud hideAnimated:YES];
                [MBProgressHUD bwm_showTitle:dic[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [self.hud hideAnimated:YES];
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [self.hud hideAnimated:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}


#pragma mark - 推出注册页面
- (IBAction)pushRegisterVC:(UIButton *)sender {
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

#pragma mark - 推出重设密码页面
- (IBAction)pushResetPswVC:(UIButton *)sender {
    ResetPasswordViewController *resetPswVC = [[ResetPasswordViewController alloc] init];
    [self.navigationController pushViewController:resetPswVC animated:YES];
}

- (void)backNormalAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.phoneTextField) {
        //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else if (self.phoneTextField.text.length >= 11) {
            self.phoneTextField.text = [textField.text substringToIndex:11];
            return NO;
        }
    }else if (textField == self.passwordTextField){
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else{
            if (self.passwordTextField.text.length >= 12) {
                self.passwordTextField.text = [textField.text substringToIndex:12];
                return NO;
            }
           
        }
    }
    return YES;
}

#pragma mark - 查看协议
- (IBAction)showProtocolAction:(UIButton *)sender {
}

#pragma mark - 短信/密码方式切换
- (IBAction)changeBtnAction:(UIButton *)sender {
    self.resetPswView.hidden = sender.selected;
    self.codeView.hidden = !sender.selected;
    sender.selected = !sender.selected;
}

@end
