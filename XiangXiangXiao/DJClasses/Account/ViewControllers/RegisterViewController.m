//
//  RegisterViewController.m
//  SchoolOnline
//
//  Created by xhkj on 2018/11/24.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "RegisterViewController.h"
#import "CommonWebViewController.h"

static int count = 0;
@interface RegisterViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) NSTimer *timer2;
@property (nonatomic, strong) NSTimer *timer1;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *eyeImageView;


@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    self.timeLabel.hidden = YES;
    self.verificationCodeBtn.hidden = NO;
    [self.phoneTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.verificationCodeTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [_timer2 invalidate];
    [_timer1 invalidate];
    count = 0;
}


#pragma mark - 动态监测textfield
- (void)textFieldDidChange
{
    if (self.phoneTextField.text.length > 0 && self.passwordTextField.text.length > 0 && self.verificationCodeTextField.text.length > 0) {
        // 128 91 204
        self.registerBtn.backgroundColor = [UIColor colorWithRed:128.0/255.0 green:91.0/255.0 blue:204.0/255.0 alpha:1];
        [self.registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else
    {
        self.registerBtn.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
        [self.registerBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


- (IBAction)getVerificationCodeAction:(UIButton *)sender {
    WEAKSELF;
    if (![self.phoneTextField.text isVaildElevenNum]) {
        [MBProgressHUD bwm_showTitle:@"请输入有效手机号码" toView:self.view hideAfter:kDelay];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneTextField.text;
    params[@"action_id"] = @"sms_setpassword";
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool postWithURL:kURL(kSendSMS) params:params haveHeader:NO success:^(id json) {
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                self.timeLabel.text = @"剩余60秒";
                [weakSelf.timer2 invalidate];
                [weakSelf.timer1 invalidate];
                count = 0;
                NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(showRepeatButton) userInfo:nil repeats:YES];
                NSTimer *timer2 = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
                weakSelf.timer1 = timer;
                weakSelf.timer2 = timer2;
                
                self.timeLabel.hidden = NO;
                self.verificationCodeBtn.hidden = YES;
            }else{
                [MBProgressHUD bwm_showTitle:dict[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}

#pragma mark - 重发验证码
- (void)showRepeatButton
{
    self.timeLabel.hidden = YES;
    self.verificationCodeBtn.hidden = NO;
    [self.verificationCodeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
    [_timer1 invalidate];
    return;
}

#pragma mark - 定时器更新时间
- (void)updateTime
{
    count++;
    if (count >= 60) {
        [_timer2 invalidate];
        return;
    }
    self.timeLabel.text = [NSString stringWithFormat:@"剩余%i秒",60 - count];
}

#pragma mark - 注册操作
- (IBAction)registerAction:(UIButton *)sender {
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.verificationCodeTextField resignFirstResponder];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (![self.phoneTextField.text isVaildElevenNum]) {
        [MBProgressHUD bwm_showTitle:@"请输入有效手机号码" toView:self.view hideAfter:kDelay];
        return;
    }
    if ([NSString isEmpty:self.passwordTextField.text]) {
        [MBProgressHUD bwm_showTitle:@"请输入6到20位密码!" toView:self.view hideAfter:kDelay];
        return;
    }
    if (![self.verificationCodeTextField.text isValidVerifyCode]) {
        [MBProgressHUD bwm_showTitle:@"请输入6位验证码!" toView:self.view hideAfter:kDelay];
        return;
    }
    
    
    if (!self.agreeBtn.selected) {
        [MBProgressHUD bwm_showTitle:@"请先同意并接受享享校用户注册协议!" toView:self.view hideAfter:kDelay];
        return;
    }
    if (self.recommendCodeTextField.text.length > 0) {
        params[@"recommendCode"] = self.recommendCodeTextField.text;
    }
    params[@"password"] = self.passwordTextField.text;
    params[@"phone"] = self.phoneTextField.text;
    params[@"code"] = self.verificationCodeTextField.text;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpTool postWithURL:kURL(kRegister) params:params haveHeader:NO success:^(id json) {
        [hud hideAnimated:YES];
        if (json) {
            NSDictionary *dic = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dic[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD bwm_showTitle:kRequestSuccess toView:self.view hideAfter:kDelay];
                    [QK_NOTICENTER postNotificationName:@"registerSuccess" object:nil userInfo:nil];
                    kUserDefaultSetObject(self.passwordTextField.text, kUserPsw);
                    kUserDefaultSetObject(self.phoneTextField.text, kUserPhone);
                    kUserDefaultSynchronize;
                    [self.navigationController popViewControllerAnimated:YES];
                    
                });
            }else{
                [MBProgressHUD bwm_showTitle:dic[@"message"] toView:self.view hideAfter:kDelay];
            }
        }else{
            [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
        }
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        [MBProgressHUD bwm_showTitle:kRequestError toView:self.view hideAfter:kDelay];
    }];
}


#pragma mark - 密码是否可见
- (IBAction)passwordSecureAction:(UIButton *)sender {
    self.eyeImageView.image = sender.selected ? [UIImage imageNamed:@"eye_close"] : [UIImage imageNamed:@"eye_open"];
    self.passwordTextField.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
}

#pragma mark - ***** UITextField Delegate *******
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.phoneTextField) {
        //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else if (self.phoneTextField.text.length >= 11) {
            self.phoneTextField.text = [textField.text substringToIndex:11];
            return NO;
        }
    }else if (textField == self.passwordTextField){
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else{
            if (self.passwordTextField.text.length >= 12) {
                self.passwordTextField.text = [textField.text substringToIndex:12];
                return NO;
            }
            
        }
    }else if (textField == self.verificationCodeTextField){
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else{
            if (self.verificationCodeTextField.text.length >= 6) {
                self.verificationCodeTextField.text = [textField.text substringToIndex:6];
                return NO;
            }
            
        }
    }
    return YES;
}

#pragma mark - 协议
- (IBAction)showProtocolVC:(UIButton *)sender {
    self.agreeBtn.selected = YES;
    CommonWebViewController *web = [[CommonWebViewController alloc] init];
    web.titleStr = @"条款内容";
    [self.navigationController pushViewController:web animated:YES];
    
}

- (IBAction)agreeBtnAction:(UIButton *)sender {
    self.agreeBtn.selected = !self.agreeBtn.selected;
}

@end
