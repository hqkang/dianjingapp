//
//  AppDelegate.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "AppDelegate.h"
#import "BaseTabBarController.h"
#import "QKTabBarController.h"
#import "BaseNavigationController.h"
#import "PrivacyPolicyViewController.h"
#import "ADWindowViewController.h"
#import "DJVersion.h"
#import <WebKit/WebKit.h>
#import "NTESService.h"
#import <RongIMKit/RongIMKit.h>

#import "HomeViewController.h"

//pods
#import <IQKeyboardManager.h>
#import <SDCycleScrollView.h>
#import <WRNavigationBar.h>
//SDKs
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>
#import "TabBarModel.h"


UIColor *MainNavBarColor = nil;
UIColor *MainViewColor = nil;
@interface AppDelegate ()<WXApiDelegate, SDCycleScrollViewDelegate, RCIMUserInfoDataSource, RCIMGroupInfoDataSource, RCIMReceiveMessageDelegate>

@property (nonatomic, strong) NSMutableArray *guideArray;

@property (nonatomic, strong) BaseTabBarController *baseTabBarController;

@property (nonatomic, strong) SDCycleScrollView *guideView;

@property (nonatomic, strong) NSMutableArray *tabBarDataArray;

@property (nonatomic, strong) NSString *friendAvatar;

@property (nonatomic, strong) NSString *friendNickName;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:kScreenBounds];
//    self.window.backgroundColor = kColorFromRGB(kWhite);
    [self.window makeKeyAndVisible];
    
    [self configPanBack];
    [self setupRootVC];
    [self showCustomWindow];
    [self configIQKeyBoardManager];
    [self registerPlatform];
    [self setNavBarAppearence];
    // 修改webView的UserAgent
    WKWebView *web = [[WKWebView alloc] initWithFrame:CGRectZero];
    [web evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        NSString *userAgent = result;
        NSString *newUserAgent = [userAgent stringByAppendingString:@"djing-ios"];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [web setCustomUserAgent:newUserAgent];
    }];
    [self getAllProtocolUrl];
    [self initNIM];
    [self initRC];
    return YES;
}

- (void)initRC
{
    // 注册融云
    [[RCIM sharedRCIM] initWithAppKey:RC_APPKEY];
    [RCIM sharedRCIM].userInfoDataSource = self;
    [RCIM sharedRCIM].groupInfoDataSource = self;
    //    [RCIM sharedRCIM].enablePersistentUserInfoCache = YES;
    [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;
    [RCIM sharedRCIM].receiveMessageDelegate = self;
    //    [[RCIM sharedRCIM] setConnectionStatusDelegate:self];
    NSString *token = kUserDefaultObject(kRCTOKEN);
    //判断是否存在token连接融云
    if (token) {
        NSDictionary *userInfo = kUserDefaultObject(kUserInfo);
        RCUserInfo *currentUserInfo = [[RCUserInfo alloc] initWithUserId:userInfo[@"phone"] name:userInfo[@"nickname"] portrait:kImgURL(userInfo[@"avatar"])];
        [RCIM sharedRCIM].currentUserInfo = currentUserInfo;
        [self loginRC:token];
    }
}

//#pragma mark - 登录融云服务器
- (void)loginRC:(NSString *)token{
    //登录融云服务器
    [[RCIM sharedRCIM] connectWithToken:token     success:^(NSString *userId) {
        NSLog(@"登陆成功。当前登录的用户ID：%@", userId);
        kUserDefaultObject(kUserID);
        kUserDefaultSynchronize;
    } error:^(RCConnectErrorCode status) {
        NSLog(@"登陆的错误码为:%ld", (long)status);
    } tokenIncorrect:^{
        //token过期或者不正确。
        //如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
        //如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。
        NSLog(@"token错误");
    }];
}

- (void)getUserInfoWithUserId:(NSString *)userId
                   completion:(void (^)(RCUserInfo *userInfo))completion
{
    NSString *currentUserID = kUserDefaultObject(kUserID);
    if ([userId isNotEmpty]) {
        NSDictionary *userInfo = kUserDefaultObject(kUserInfo);
        if ([userId isEqualToString:currentUserID]) {
            NSString *url = kImgURL(userInfo[@"avatar"]);
            return completion([[RCUserInfo alloc] initWithUserId:userId name:userInfo[@"nickname"] portrait:url]);
        }else if(![userId isEqualToString:currentUserID])
        {
            
            dispatch_group_t group = dispatch_group_create();
            
            // 获取好友信息
            dispatch_group_enter(group);
            [self getFriendData:userId requestisScu:^(BOOL isScu) {
                dispatch_group_leave(group);
            }];
            
            dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                //根据存储联系人信息的模型，通过 userId 来取得对应的name和头像url，进行以下设置（此处因为项目接口尚未实现，所以就只能这样给大家说说，请见谅）
                return completion([[RCUserInfo alloc] initWithUserId:userId name:self.friendNickName portrait:self.friendAvatar]);
            });
        }
    }else{
        return completion([[RCUserInfo alloc] initWithUserId:userId name:@"系统消息" portrait:nil]);
    }
    
    return completion(nil);
}


- (void)getFriendData:(NSString *)userID requestisScu:(void(^)(BOOL isScu))requestisScu{
    if (LOGINDATA) {
        [HttpTool getWithURL:kURL(kFriendData) params:nil haveHeader:YES success:^(id json) {
            if (json) {
                NSArray *array = [NSArray array];
                array = (NSArray *)json;
                if (array.count > 0) {
                    self.friendAvatar = kImgURL(array[0][@"avatar"]);
                    self.friendNickName = [NSString stringWithFormat:@"%@", array[0][@"nickname"]];
                    requestisScu(YES);
                }
            }
        } failure:^(NSError *error) {
            requestisScu(NO);
        }];
    }else{
        
    }
}

#pragma mark - 初始化云信
- (void)initNIM
{
    //推荐在程序启动的时候初始化 NIMSDK
    NSString *appKey        = YUNXIN_APPKEY;
    NIMSDKOption *option    = [NIMSDKOption optionWithAppKey:appKey];
    option.apnsCername      = nil;
    option.pkCername        = nil;
    [[NIMSDK sharedSDK] registerWithOption:option];
    if (LOGINDATA) {
        [self loginYunXin];
    }
}

#pragma mark - 登录云信
- (void)loginYunXin
{
    NSString *account = [NSString stringWithFormat:@"%@", kUserDefaultObject(kUserPhone)];
    NSString *token   = [NSString stringWithFormat:@"%@", kUserDefaultObject(kYunXinToken)];
    [[[NIMSDK sharedSDK] loginManager] autoLogin:account token:token];
    [[[NIMSDK sharedSDK] loginManager] login:account token:token completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"登录失败");
        }else{
            NSLog(@"登录成功");
        }
    }];
    [[NTESServiceManager sharedManager] start];
}

- (void)getAllProtocolUrl{
    [HttpTool postWithURL:kURL(kProtocolUrl) params:nil haveHeader:NO success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                kUserDefaultSetObject(dict, kProtocol);
                kUserDefaultSynchronize;
            }else{
                
            }
        }else{
            
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - 设置全局导航栏
- (void)setNavBarAppearence
{
    MainNavBarColor = kColorFromRGB(kNavColor);
    MainViewColor   = [UIColor colorWithRed:126/255.0 green:126/255.0 blue:126/255.0 alpha:1];
    
    // 设置是 广泛使用WRNavigationBar，还是局部使用WRNavigationBar，目前默认是广泛使用
    [WRNavigationBar wr_widely];
    [WRNavigationBar wr_setBlacklist:@[@"SpecialController",
                                       @"TZPhotoPickerController",
                                       @"TZGifPhotoPreviewController",
                                       @"TZAlbumPickerController",
                                       @"TZPhotoPreviewController",
                                       @"TZVideoPlayerController"]];
    
    // 设置导航栏默认的背景颜色
    [WRNavigationBar wr_setDefaultNavBarBarTintColor:MainNavBarColor];
    // 设置导航栏所有按钮的默认颜色
    [WRNavigationBar wr_setDefaultNavBarTintColor:kColorFromRGB(kTextColor)];
    // 设置导航栏标题默认颜色
    [WRNavigationBar wr_setDefaultNavBarTitleColor:kColorFromRGB(kBlack)];
    // 统一设置状态栏样式
    [WRNavigationBar wr_setDefaultStatusBarStyle:UIStatusBarStyleDefault];
    // 如果需要设置导航栏底部分割线隐藏，可以在这里统一设置
    [WRNavigationBar wr_setDefaultNavBarShadowImageHidden:YES];
}

#pragma mark-<懒加载>
-(SDCycleScrollView *)guaidView
{
    if (!_guideView) {
        //引导页vc
        self.guideArray = [NSMutableArray array];
        [self.guideArray addObject:@"guide_1.jpg"];
        [self.guideArray addObject:@"guide_2.jpg"];
        [self.guideArray addObject:@"guide_3.jpg"];
        [self.guideArray addObject:@"guide_4.jpg"];
        _guideView = [SDCycleScrollView cycleScrollViewWithFrame:self.window.bounds imageNamesGroup:self.guideArray];
        _guideView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _guideView.layer.masksToBounds = YES;
        _guideView.pageDotImage = [UIImage imageNamed:@"icon_pagedot_nor"];
        _guideView.currentPageDotImage = [UIImage imageNamed:@"icon_pagedot_sel"];
        _guideView.autoScroll = NO;
        _guideView.infiniteLoop = NO;
        _guideView.delegate = self;
    }
    return _guideView;
}

-(BaseTabBarController *)baseTabBarController
{
    if (!_baseTabBarController) {
        _baseTabBarController = [[BaseTabBarController alloc]init];
    }
    return _baseTabBarController;
}

- (NSMutableArray *)tabBarDataArray
{
    if (_tabBarDataArray == nil) {
        _tabBarDataArray = [NSMutableArray array];
    }
    return _tabBarDataArray;
}

#pragma mark - <注册第三方平台>
-(void)registerPlatform
{
    [WXApi registerApp:kWechatAPPID];
}

#pragma mark - <自定义弹窗-包括广告，隐私政策，升级提示>
-(void)showCustomWindow
{
    /*
     注意：1.每一种弹窗是模态控制器来实现，方便处理逻辑和用户操作；
     2.广告-隐私-升级（从底层控制器开始排序，最底层的由rootVC模态而出，如果“广告”没有，则“隐私”为最底层，以此类推）；
     3.目前看到的一级是由上一级模态而出（解决由同一个控制器模态不同的控制器被挤掉的问题）；
     */
    
    //请求广告接口，确定先模态哪种控制器
    kUserDefaultSetObject(@"YES", kIsShowCusWindow);
    kUserDefaultSynchronize;
    if (kUserDefaultObject(kIsShowCusWindow)) {
        NSString *show = kUserDefaultObject(kIsShowCusWindow);
        if (![show isEqualToString:@"YES"]) {
            return;
        }
    }
    NSString *token = @"";
    if (kUserDefaultObject(kUserLoginToken)) {
        token = kUserDefaultObject(kUserLoginToken);
    }else{
        [self privacyPolicyFromVC:kKeyWindow.rootViewController];
        return;
    }
    [self showADVCFromVC:kKeyWindow.rootViewController];
}

#pragma mark - <广告弹窗>
-(void)showADVCFromVC:(UIViewController *)vc
{
    //是否存在广告弹窗
    BOOL isAD = YES;
    if (!isAD) {
        [self privacyPolicyFromVC:vc];
        return;
    }
//    ADWindowViewController *adwinVC = [[ADWindowViewController alloc]initWithNibName:NSStringFromClass([ADWindowViewController class]) bundle:nil];
//    adwinVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
//    adwinVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    
//    kWeakSelf(self);
//    [vc presentViewController:adwinVC animated:NO completion:^{
//        if (![[NSUserDefaults standardUserDefaults]boolForKey:kShowPrivacyPolicy]) {
//            [weakself privacyPolicyFromVC:adwinVC];
//        }else{
//            [weakself checkTheVersionFromVC:adwinVC];
//        }
//    }];
}

#pragma mark - <IQKeyBoardManager全局键盘管理>
-(void)configIQKeyBoardManager
{
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    
    keyboardManager.enable = YES; // 控制整个功能是否启用
    
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    
    keyboardManager.shouldToolbarUsesTextFieldTintColor = YES; // 控制键盘上的工具条文字颜色是否用户自定义
    
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    
    keyboardManager.enableAutoToolbar = NO; // 控制是否显示键盘上的工具条
    
    keyboardManager.shouldShowToolbarPlaceholder = NO; // 是否显示占位文字
    
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:15]; // 设置占位文字的字体
    
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
}

#pragma mark - <创建window/引导页>
-(void)setupRootVC
{
//    self.window.rootViewController = self.baseTabBarController;
    QKTabBarController *tab = [[QKTabBarController alloc] init];
    self.window.rootViewController = tab;
    
    [self getTabBarData];
}

#pragma mark - 获取TabBar数据
- (void)getTabBarData
{
    [HttpTool postWithURL:kURL(kTabBar) params:nil haveHeader:NO success:^(id json) {
        if (json) {
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                if (dict[@"tabs"]) {
                    NSArray *array = [NSArray array];
                    array = dict[@"tabs"];
                    if (array.count > 0) {
                        for (int i = 0; i < array.count; i++) {
                            TabBarModel *model = [[TabBarModel alloc] init];
                            [model setValuesForKeysWithDictionary:array[i]];
                            [self.tabBarDataArray addObject:model];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.baseTabBarController.dataArray = self.tabBarDataArray;
                            self.window.rootViewController = self.baseTabBarController;
                            //引导页
                            //    NSString *key = @"CFBundleVersion";
                            NSString *bundleKey = @"CFBundleShortVersionString";
                            // 上一次的使用版本（存储在沙盒中的版本号）
                            NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:bundleKey];
                            // 当前软件的版本号（从Info.plist中获得）
                            NSString *currentVersion = [NSBundle mainBundle].infoDictionary[bundleKey];
                            if (![lastVersion isEqualToString:currentVersion])
                            {
                                NSArray *lastVerArray = [lastVersion componentsSeparatedByString:@"."];
                                NSArray *curVerArray = [currentVersion componentsSeparatedByString:@"."];
                                
                                if (lastVerArray.count>0 && curVerArray.count>0) {
                                    NSString *lastFirBit = lastVerArray[0];
                                    NSString *curFirBit = curVerArray[0];
                                    if (![lastFirBit isEqualToString:curFirBit]) {
                                        [self.window addSubview:self.guaidView];
                                        // 将当前的版本号存进沙盒
                                        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:bundleKey];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        
                                        kUserDefaultRemoveObject(kUserInfo);
                                        kUserDefaultRemoveObject(kUserLoginToken);
                                        kUserDefaultRemoveObject(kUserID);
                                        kUserDefaultSynchronize;
                                    }else{
                                        if (lastVerArray.count>1 && curVerArray.count>1){
                                            NSString *lastSecBit = lastVerArray[1];
                                            NSString *curSecBit = curVerArray[1];
                                            if (![lastSecBit isEqualToString:curSecBit]) {
                                                [self.window addSubview:self.guaidView];
                                                // 将当前的版本号存进沙盒
                                                [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:bundleKey];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                
                                                kUserDefaultRemoveObject(kUserInfo);
                                                kUserDefaultRemoveObject(kUserLoginToken);
                                                kUserDefaultRemoveObject(kUserID);
                                                kUserDefaultSynchronize;
                                            }else{
                                                if (lastVerArray.count>2 && curVerArray.count>2){
                                                    NSString *lastSecBit = lastVerArray[2];
                                                    NSString *curSecBit = curVerArray[2];
                                                    if (![lastSecBit isEqualToString:curSecBit]) {
                                                        [self.window addSubview:self.guaidView];
                                                        // 将当前的版本号存进沙盒
                                                        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:bundleKey];
                                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                                        
                                                        kUserDefaultRemoveObject(kUserInfo);
                                                        kUserDefaultRemoveObject(kUserLoginToken);
                                                        kUserDefaultRemoveObject(kUserID);
                                                        kUserDefaultSynchronize;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    [self.window addSubview:self.guaidView];
                                    // 将当前的版本号存进沙盒
                                    [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:bundleKey];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    kUserDefaultRemoveObject(kUserInfo);
                                    kUserDefaultRemoveObject(kUserLoginToken);
                                    kUserDefaultRemoveObject(kUserID);
                                    kUserDefaultSynchronize;
                                }
                            }
                        });
                    }
                }
            }else{
                
            }
        }else{
            
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - <全局手势滑动返回>
-(void)configPanBack
{
    //需要在设置rootVC之前设置手势返回
    [MLTransition validatePanBackWithMLTransitionGestureRecognizerType:MLTransitionGestureRecognizerTypeScreenEdgePan];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - <SDCycleScrollViewDelegate---------->
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (index == self.guideArray.count-1) {
        
        kWeakSelf(self);
        [UIView animateWithDuration:2 animations:^{
            weakself.guaidView.alpha = 0;
        } completion:^(BOOL finished) {
            [weakself.guaidView removeFromSuperview];
        }];
    }
}

#pragma mark - <隐私政策>
-(void)privacyPolicyFromVC:(UIViewController *)vc
{
    if (![[NSUserDefaults standardUserDefaults]boolForKey:kShowPrivacyPolicy]) {
        PrivacyPolicyViewController *privacyVC = [[PrivacyPolicyViewController alloc]init];
        privacyVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        privacyVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        kWeakSelf(self);
        [vc presentViewController:privacyVC animated:YES completion:^{
            [weakself checkTheVersionFromVC:privacyVC];
        }];
    }else{
        [self checkTheVersionFromVC:vc];
    }
}

#pragma mark - <版本更新检查>
-(void)checkTheVersionFromVC:(UIViewController *)vc
{
    [[DJVersion sharedInstance] checkNewVersionWithUpdateWindowFromVC:vc];
}

@end
