//
//  QKUserDefaultTool.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/3.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "QKUserDefaultTool.h"

@implementation QKUserDefaultTool

+ (void)removeAllUserDefault
{
    kUserDefaultRemoveObject(kTotalUserInfo);
    kUserDefaultRemoveObject(kUserInfo);
    kUserDefaultRemoveObject(kLoginData);
    kUserDefaultRemoveObject(kUserLoginToken);
    kUserDefaultRemoveObject(kYunXinToken);
    kUserDefaultRemoveObject(kRCTOKEN);
    kUserDefaultRemoveObject(kUserID);
    kUserDefaultRemoveObject(kRefreshToken);
    [QK_NOTICENTER postNotificationName:kLoginStatusChange object:nil userInfo:nil];
}

@end
