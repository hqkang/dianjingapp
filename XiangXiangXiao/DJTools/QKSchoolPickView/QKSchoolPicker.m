//
//  QKSchoolPicker.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/6.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "QKSchoolPicker.h"

@interface QKSchoolPicker()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,  strong) UIPickerView *pickView;
@property (nonatomic,  strong) NSArray *provinceArray;
@property (nonatomic,  strong) NSArray *schoolArray;
@property (nonatomic,  assign) NSInteger  selectRow1;
@property (nonatomic,  assign) NSInteger  selectRow2;
@end

@implementation QKSchoolPicker

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200);
    self = [super initWithFrame:frame];
    if (self) {
        _columns = 2;
        [self xjh_setupViews];
    }
    return self;
}

- (void)xjh_setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pickView];
}

- (UIPickerView *)pickView{
    if (!_pickView) {
        _pickView = [[UIPickerView alloc] init];
        _pickView.frame = self.bounds;
        _pickView.delegate = self;
    }
    return _pickView;
}

- (void)setColumns:(NSInteger)columns{
    if (_columns == 2 || _columns == 3) {
        _columns = columns;
    }
}

- (void)loadData{
    _provinceArray = [_dataArray valueForKey:@"text"];
    _schoolArray = [_dataArray[_selectRow1] valueForKeyPath:@"children.text"];
    
    [_pickView reloadAllComponents];
    [_pickView selectRow:_selectRow1 inComponent:0 animated:YES];
    [_pickView selectRow:_selectRow2 inComponent:1 animated:YES];
    
    if (_delegate && [_delegate respondsToSelector:@selector(schoolPickerDidSelectedRow:row2:)]) {
        [_delegate schoolPickerDidSelectedRow:_selectRow1 row2:_selectRow2];
    }
}

#pragma mark - UIPickerViewDataSource
/// 几列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return _columns;
}

/// 一列几行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (_dataArray.count == 0) {
        return 0;
    }
    
    if (component == 0) {
        return _provinceArray.count;
    }else if (component == 1) {
        return _schoolArray.count;
    }
    
    return 0;
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        _selectRow1 = row;
        _selectRow2 = 0;
        _schoolArray = [_dataArray[row] valueForKeyPath:@"children.text"];
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
    }else if (component == 1) {
        _selectRow2 = row;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(schoolPickerDidSelectedRow:row2:)]) {
        [_delegate schoolPickerDidSelectedRow:_selectRow1 row2:_selectRow2];
    }
}

#if 0
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (component == 0) {
        return _provinceArray[row];
    }else if (component == 1) {
        return _cityArray[row];
    }else if (component == 2) {
        return _townArray[row];
    }
    
    return @"";
}
#endif

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return CGRectGetWidth([UIScreen mainScreen].bounds)/2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view{
    
    UILabel *label = (UILabel *)view;
    if (!label) {
        label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = 1;
        //label.textColor = [UIColor whiteColor];
    }
    if (component == 0) {
        if (row < _provinceArray.count) {
            label.text = _provinceArray[row];
        }
    }else if (component == 1) {
        if (row < _schoolArray.count) {
            label.text = _schoolArray[row];
        }
    }
    //[label sizeToFit];
    return label;
}

@end
