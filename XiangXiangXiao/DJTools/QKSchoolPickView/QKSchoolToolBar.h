//
//  QKSchoolToolBar.h
//  SchoolOnline
//
//  Created by xhkj on 2018/12/6.
//  Copyright © 2018年 DD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol QKSchoolToolBarDelegate<NSObject>
/// left - 0, right - 1
- (void)toolBarDidClickButton:(NSInteger)leftOrRight;
@end

@interface QKSchoolToolBar : UIView
@property (nonatomic,  strong,  readonly) UIButton *leftButton;
@property (nonatomic,  strong,  readonly) UILabel *titleLable;
@property (nonatomic,  strong,  readonly) UIButton *rightButton;

@property (nonatomic,    weak) id<QKSchoolToolBarDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
