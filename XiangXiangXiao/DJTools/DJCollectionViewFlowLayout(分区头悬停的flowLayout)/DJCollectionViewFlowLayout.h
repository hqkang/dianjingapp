//
//  DJCollectionViewFlowLayout.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#warning **********对齐方式********
typedef NS_ENUM(NSInteger,AlignType) {
    AlignTypeWithLeft,
    AlignTypeWithCenter,
    AlignTypeWithRight
};

@interface DJCollectionViewFlowLayout : UICollectionViewFlowLayout

#warning **********对齐方式********
//需要自定义对齐的section
@property (nonatomic, assign)NSUInteger sectionNeedToCustomAlign;
//是否需要自定义对齐方式
@property (nonatomic, assign)BOOL customAlign;
//两个Cell之间的距离
@property (nonatomic,assign)CGFloat betweenOfCell;
//cell对齐方式32q
@property (nonatomic,assign)AlignType cellType;

- (instancetype)initWithType:(AlignType)cellType betweenOfCell:(CGFloat)betweenOfCell navigationHeight:(CGFloat)height sectionNeedToHover:(NSUInteger)section customAlign:(BOOL)customAlign sectionNeedToCustomAlign:(NSUInteger)sectionNeedToCustomAlign;

@end

NS_ASSUME_NONNULL_END
