//
//  ArrayToolBar.h
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/5/12.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ArrayToolBarDelegate<NSObject>
/// left - 0, right - 1
- (void)toolBarDidClickButton:(NSInteger)leftOrRight;
@end

@interface ArrayToolBar : UIView
@property (nonatomic,  strong,  readonly) UIButton *leftButton;
@property (nonatomic,  strong,  readonly) UILabel *titleLable;
@property (nonatomic,  strong,  readonly) UIButton *rightButton;

@property (nonatomic,    weak) id<ArrayToolBarDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
