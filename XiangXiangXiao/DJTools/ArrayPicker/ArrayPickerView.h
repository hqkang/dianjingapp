//
//  ArrayPickerView.h
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/5/12.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ArrayToolBar,ArrayPicker;

typedef void(^ArrayPickViewPickBlock)(NSDictionary *dic);

@interface ArrayPickerView : UIView

/// Default is 0.3.
@property (nonatomic,  assign) CGFloat  grayViewAlpha;
/// Default is 'NO'.
@property (nonatomic,  assign) BOOL  hideWhenTapGrayView;
/// Default is 3, two value to set: 2 or 3,
@property (nonatomic,  assign) NSInteger  columns;
///
@property (nonatomic,    copy) ArrayPickViewPickBlock pickBlock;
///
@property (nonatomic,  strong,  readonly) ArrayToolBar *toolBar;
///
@property (nonatomic,  strong,  readonly) ArrayPicker *picker;

@property (nonatomic, strong) NSMutableArray *dataArray;

- (void)showInView:(UIView *)view;
- (void)hide;
- (instancetype)initWithFrame:(CGRect)frame withDataArray:(NSMutableArray *)dataArray;
@end
NS_ASSUME_NONNULL_END
