//
//  IDNamePickerView.h
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/5/9.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class IDNameToolBar,IDNamePicker;

typedef void(^IDNamePickViewPickBlock)(NSDictionary *dic);

@interface IDNamePickerView : UIView

/// Default is 0.3.
@property (nonatomic,  assign) CGFloat  grayViewAlpha;
/// Default is 'NO'.
@property (nonatomic,  assign) BOOL  hideWhenTapGrayView;
/// Default is 3, two value to set: 2 or 3,
@property (nonatomic,  assign) NSInteger  columns;
///
@property (nonatomic,    copy) IDNamePickViewPickBlock pickBlock;
///
@property (nonatomic,  strong,  readonly) IDNameToolBar *toolBar;
///
@property (nonatomic,  strong,  readonly) IDNamePicker *picker;

@property (nonatomic, strong) NSMutableArray *dataArray;

- (void)showInView:(UIView *)view;
- (void)hide;
- (instancetype)initWithFrame:(CGRect)frame withDataArray:(NSMutableArray *)dataArray;
@end

NS_ASSUME_NONNULL_END
