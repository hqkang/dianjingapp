//
//  IDNameToolBar.m
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/5/9.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "IDNameToolBar.h"
#define IDNameToolBar_Button_Width 70
@interface IDNameToolBar()
@property (nonatomic,  strong) UIButton *leftButton;
@property (nonatomic,  strong) UILabel *titleLable;
@property (nonatomic,  strong) UIButton *rightButton;
@end

@implementation IDNameToolBar

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 50);
    self = [super initWithFrame:frame];
    if (self) {
        [self xjh_setupViews];
    }
    return self;
}

- (void)xjh_setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.leftButton];
    [self addSubview:self.titleLable];
    [self addSubview:self.rightButton];
}

- (UIButton *)leftButton{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _leftButton.frame = CGRectMake(0, 0, IDNameToolBar_Button_Width, 50);
        _leftButton.tag = 100;
        [_leftButton setTitle:@"取消" forState:0];
        [_leftButton addTarget:self action:@selector(buttonEvent:) forControlEvents:1<<6];
    }
    return _leftButton;
}

- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.frame = CGRectMake(IDNameToolBar_Button_Width, 0, CGRectGetWidth(self.bounds) - 2 * IDNameToolBar_Button_Width, 50);
        _titleLable.textAlignment = 1;
    }
    return _titleLable;
}

- (UIButton *)rightButton{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _rightButton.frame = CGRectMake(CGRectGetWidth(self.bounds) - IDNameToolBar_Button_Width, 0, IDNameToolBar_Button_Width, 50);
        _rightButton.tag = 200;
        [_rightButton setTitle:@"確定" forState:0];
        [_rightButton addTarget:self action:@selector(buttonEvent:) forControlEvents:1<<6];
    }
    return _rightButton;
}

- (void)buttonEvent:(UIButton *)button
{
    NSInteger type = 0;
    if (button.tag == 200) {
        type = 1;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(toolBarDidClickButton:)]) {
        [_delegate toolBarDidClickButton:type];
    }
}

@end
