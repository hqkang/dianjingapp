//
//  IDNamePicker.h
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/5/9.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol IDNamePickerDelegate <NSObject>
- (void)pickerDidSelectedRow:(NSInteger)row1;
@end

@interface IDNamePicker : UIView
/// Default is 3, two value to set: 2 or 3,
@property (nonatomic,  assign) NSInteger  columns;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic,    weak) id <IDNamePickerDelegate> delegate;

- (void)loadData;

@end

NS_ASSUME_NONNULL_END
