//
//  QKSexPickerView.h
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QKSexToolBar,QKSexPicker;

typedef void(^QKSexPickViewPickBlock)(NSDictionary *dic);

@interface QKSexPickerView : UIView

/// Default is 0.3.
@property (nonatomic,  assign) CGFloat  grayViewAlpha;
/// Default is 'NO'.
@property (nonatomic,  assign) BOOL  hideWhenTapGrayView;
/// Default is 3, two value to set: 2 or 3,
@property (nonatomic,  assign) NSInteger  columns;
///
@property (nonatomic,    copy) QKSexPickViewPickBlock pickBlock;
///
@property (nonatomic,  strong,  readonly) QKSexToolBar *toolBar;
///
@property (nonatomic,  strong,  readonly) QKSexPicker *picker;

@property (nonatomic, strong) NSMutableArray *dataArray;

- (void)showInView:(UIView *)view;
- (void)hide;
- (instancetype)initWithFrame:(CGRect)frame withDataArray:(NSMutableArray *)dataArray;
@end

NS_ASSUME_NONNULL_END
