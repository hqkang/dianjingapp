//
//  QKSexPicker.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "QKSexPicker.h"

@interface QKSexPicker()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,  strong) UIPickerView *pickView;
@property (nonatomic,  strong) NSArray *textArray;
@property (nonatomic,  assign) NSInteger  selectRow1;
@end

@implementation QKSexPicker

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200);
    self = [super initWithFrame:frame];
    if (self) {
        _columns = 1;
        [self xjh_setupViews];
    }
    return self;
}

- (void)xjh_setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pickView];
}

- (UIPickerView *)pickView{
    if (!_pickView) {
        _pickView = [[UIPickerView alloc] init];
        _pickView.frame = self.bounds;
        _pickView.delegate = self;
    }
    return _pickView;
}

- (void)setColumns:(NSInteger)columns{
    if (_columns == 1) {
        _columns = columns;
    }
}

- (void)loadData{
    _textArray = [_dataArray valueForKey:@"text"];
    
    [_pickView reloadAllComponents];
    [_pickView selectRow:_selectRow1 inComponent:0 animated:YES];
    
    if (_delegate && [_delegate respondsToSelector:@selector(sexPickerDidSelectedRow:)]) {
        [_delegate sexPickerDidSelectedRow:_selectRow1];
    }
}

#pragma mark - UIPickerViewDataSource
/// 几列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return _columns;
}

/// 一列几行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (_dataArray.count == 0) {
        return 0;
    }
    
    if (component == 0) {
        return _textArray.count;
    }
    
    return 0;
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        _selectRow1 = row;
        [pickerView selectedRowInComponent:0];
        
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(sexPickerDidSelectedRow:)]) {
        [_delegate sexPickerDidSelectedRow:_selectRow1];
    }
}

#if 0
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (component == 0) {
        return _provinceArray[row];
    }
    
    return @"";
}
#endif

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return CGRectGetWidth([UIScreen mainScreen].bounds);
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view{
    
    UILabel *label = (UILabel *)view;
    if (!label) {
        label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = 1;
        //label.textColor = [UIColor whiteColor];
    }
    if (component == 0) {
        if (row < _textArray.count) {
            label.text = _textArray[row];
        }
    }
    //[label sizeToFit];
    return label;
}

@end
