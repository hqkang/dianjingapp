//
//  QKSexToolBar.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "QKSexToolBar.h"
#define QKSexToolBar_Button_Width 70
@interface QKSexToolBar()
@property (nonatomic,  strong) UIButton *leftButton;
@property (nonatomic,  strong) UILabel *titleLable;
@property (nonatomic,  strong) UIButton *rightButton;
@end

@implementation QKSexToolBar

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 50);
    self = [super initWithFrame:frame];
    if (self) {
        [self xjh_setupViews];
    }
    return self;
}

- (void)xjh_setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.leftButton];
    [self addSubview:self.titleLable];
    [self addSubview:self.rightButton];
}

- (UIButton *)leftButton{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _leftButton.frame = CGRectMake(0, 0, QKSexToolBar_Button_Width, 50);
        _leftButton.tag = 100;
        [_leftButton setTitle:@"取消" forState:0];
        [_leftButton addTarget:self action:@selector(buttonEvent:) forControlEvents:1<<6];
    }
    return _leftButton;
}

- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.frame = CGRectMake(QKSexToolBar_Button_Width, 0, CGRectGetWidth(self.bounds) - 2 * QKSexToolBar_Button_Width, 50);
        _titleLable.textAlignment = 1;
    }
    return _titleLable;
}

- (UIButton *)rightButton{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _rightButton.frame = CGRectMake(CGRectGetWidth(self.bounds) - QKSexToolBar_Button_Width, 0, QKSexToolBar_Button_Width, 50);
        _rightButton.tag = 200;
        [_rightButton setTitle:@"确定" forState:0];
        [_rightButton addTarget:self action:@selector(buttonEvent:) forControlEvents:1<<6];
    }
    return _rightButton;
}

- (void)buttonEvent:(UIButton *)button
{
    NSInteger type = 0;
    if (button.tag == 200) {
        type = 1;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(toolBarDidClickButton:)]) {
        [_delegate toolBarDidClickButton:type];
    }
}

@end
