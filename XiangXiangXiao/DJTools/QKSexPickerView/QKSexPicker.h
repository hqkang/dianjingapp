//
//  QKSexPicker.h
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol QKSexPickerDelegate <NSObject>
- (void)sexPickerDidSelectedRow:(NSInteger)row1;
@end

@interface QKSexPicker : UIView
/// Default is 3, two value to set: 2 or 3,
@property (nonatomic,  assign) NSInteger  columns;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic,    weak) id <QKSexPickerDelegate> delegate;

- (void)loadData;

@end

NS_ASSUME_NONNULL_END
