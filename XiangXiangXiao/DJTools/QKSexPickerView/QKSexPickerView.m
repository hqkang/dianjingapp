//
//  QKSexPickerView.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "QKSexPickerView.h"
#import "QKSexToolBar.h"
#import "QKSexPicker.h"

@interface QKSexPickerView()<QKSexToolBarDelegate,QKSexPickerDelegate>
@property (nonatomic,  strong) UIView *grayView;
@property (nonatomic,  strong) UIButton *dissmissButton;
@property (nonatomic,  strong) UIView *contentView;

@property (nonatomic,  strong) QKSexToolBar *toolBar;
@property (nonatomic,  strong) QKSexPicker *picker;

@property (nonatomic,  strong) NSMutableDictionary *resultDic;

@end

@implementation QKSexPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        _grayViewAlpha = 0.3;
        _columns = 1;
        _resultDic = @{}.mutableCopy;
        [self xjh_setupViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withDataArray:(NSMutableArray *)dataArray
{
    self = [super initWithFrame:frame];
    if (self) {
        _grayViewAlpha = 0.3;
        _columns = 1;
        _resultDic = @{}.mutableCopy;
        _dataArray = dataArray;
        [self xjh_setupViews];
    }
    return self;
}

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)xjh_setupViews
{
    [self addSubview:self.grayView];
    [self addSubview:self.contentView];
    [_contentView addSubview:self.toolBar];
    [_contentView addSubview:self.picker];
}

- (UIView *)grayView{
    if (!_grayView) {
        _grayView = [[UIView alloc] init];
        _grayView.frame = self.bounds;
        _grayView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    }
    return _grayView;
}

- (UIButton *)dissmissButton{
    if (!_dissmissButton) {
        _dissmissButton = [[UIButton alloc] init];
        _dissmissButton.frame = self.bounds;
        [_dissmissButton addTarget:self action:@selector(hide) forControlEvents:1<<6];
        [_grayView addSubview:_dissmissButton];
    }
    return _dissmissButton;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.frame = CGRectMake(0, CGRectGetMaxY(self.bounds), CGRectGetWidth(self.bounds), 320);
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (QKSexToolBar *)toolBar{
    if (!_toolBar) {
        _toolBar = [[QKSexToolBar alloc] init];
        _toolBar.delegate = self;
    }
    return _toolBar;
}

- (QKSexPicker *)picker{
    if (!_picker) {
        _picker = [[QKSexPicker alloc] init];
        _picker.frame = CGRectMake(0, 50, CGRectGetWidth(self.bounds), 200);
        _picker.delegate = self;
        _picker.dataArray = _dataArray;
    }
    return _picker;
}

#pragma mark - public

- (void)setHideWhenTapGrayView:(BOOL)hideWhenTapGrayView{
    _hideWhenTapGrayView = hideWhenTapGrayView;
    if (hideWhenTapGrayView) {
        self.dissmissButton.hidden = NO;
    }else{
        self.dissmissButton.hidden = YES;
    }
}

- (void)setColumns:(NSInteger)columns{
    if (_columns == 1) {
        _columns = columns;
        _picker.columns = columns;
        [_picker loadData];
    }
}


- (void)showInView:(UIView *)view{
    if (!view) {
        return;
    }
    
    [view addSubview:self];
    WEAKSELF;
    [UIView animateWithDuration:0.25 animations:^{
        [self viewAnimation:weakSelf.grayViewAlpha height:-CGRectGetHeight(weakSelf.contentView.frame)];
    }];
}

- (void)hide{
    WEAKSELF;
    [UIView animateWithDuration:0.25 animations:^{
        [self viewAnimation:0 height:CGRectGetHeight(weakSelf.contentView.frame)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - private

- (void)viewAnimation:(CGFloat)alpha height:(CGFloat)height
{
    _grayView.backgroundColor = [UIColor colorWithWhite:0 alpha:alpha];
    
    CGRect frame = _contentView.frame;
    frame.origin.y += height;
    _contentView.frame = frame;
}

#pragma mark - QKSexToolBarDelegate
- (void)toolBarDidClickButton:(NSInteger)leftOrRight{
    // left - 0, right - 1
    if (leftOrRight == 1) {
        if (_pickBlock) {
            _pickBlock(_resultDic);
        }
    }
    [self hide];
}

#pragma mark - QKSexPickerDelegate
- (void)sexPickerDidSelectedRow:(NSInteger)row1{
    //NSLog(@"row1:%@, row2:%@, row3:%@",@(row1),@(row2),@(row3));
    
    NSString *sex = nil;
    NSString *sexValue = nil;
    NSMutableString *mstr = @"".mutableCopy;
    NSDictionary *dic1 = _dataArray[row1];
    sex = dic1[@"text"];
    sexValue = dic1[@"value"];
    
    //    [mstr appendString:mainIndustry];
    
    
    
    //    _toolBar.titleLable.text = @"选择行业";
    
    [_resultDic setValue:sex forKey:@"text"];
    [_resultDic setValue:sexValue forKey:@"textValue"];
    
    //NSLog(@"%@",_resultDic);
}

@end
