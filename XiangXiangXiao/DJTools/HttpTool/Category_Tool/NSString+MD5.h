//
//  NSString+MD5.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString *)MD5DigestLower;/**<32位MD5小写*/
- (NSString *)MD5DigestUpper;/**<32位MD5大写*/
@end
