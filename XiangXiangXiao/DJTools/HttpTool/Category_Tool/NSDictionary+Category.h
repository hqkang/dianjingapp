//
//  NSDictionary+Category.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSDictionary (Category)
/**
 *  把当前Dictionary转换为json
 *
 *  @return 转换后的json字符串
 */
- (NSString *)dictionaryToJson;

/**
 *  把指定的Dictionary转换为json
 *
 *  @param dictionary 要转换的dictionary
 *
 *  @return 转换后的json字符串，失败则为{}
 */
+ (NSString *)dictionaryToJson:(NSDictionary *)dictionary;
@end
