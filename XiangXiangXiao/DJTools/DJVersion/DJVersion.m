//
//  DJVersion.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "DJVersion.h"
#import "CustomAlert.h"
#import "NSDictionary+Category.h"
#import "UpdateWindowController/UpdateWindowController.h"

#define kiTunesConnect @"https://itunes.apple.com/lookup?id=xxxxx"
//#define kAppStoreURLFormat @"https://itunes.apple.com/app?id=1324471801"
#define kAppStoreURLFormat @"itms-apps://itunes.apple.com/cn/app/idxxxxxx?mt=8"

static NSString *const IGNORE = @"ignore";

@interface DJVersion()
//@property (nonatomic, assign)BOOL isIgnore;//是否忽略该版本
//@property (nonatomic, assign)BOOL isRemindNxt;// 是否下次提醒
@end

@implementation DJVersion


+ (void)load
{
    [self performSelectorOnMainThread:@selector(sharedInstance) withObject:nil waitUntilDone:NO];
    
    kWeakSelf(self);
    [[NSNotificationCenter defaultCenter]addObserverForName:kUpgrade object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [[weakself sharedInstance]checkNewVersionWithUpdateWindowFromVC:kKeyWindow.rootViewController];
    }];
}
+ (DJVersion *)sharedInstance
{
    static DJVersion *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[DJVersion alloc] init];
    }
    return sharedInstance;
}

-(void)checkNewVersionWithShowHud:(BOOL)isShow isEnterForeground:(BOOL)isEnterForeground
{
    NSLog(@"%@",kURL(kVersionUpdate));
    /*
     {
     "code": 0000,
     "msg": "操作成功",
     "success": true,
     "results":{
     "ios":{
     "version":"v1.1.2",
     "build":23,
     "downloadUrl":"ios下载地址",
     "forceUpgrade":false //强制更新,
     "desc":["1. Fix Bug","2.Fix Bug"]
     },
     "android":{
     "version":"v1.1.2",
     "build":35,
     "downloadUrl":"安卓下载地址",
     "forceUpgrade":false,
     "desc":["1. Fix Bug","2.Fix Bug"]
     }
     }
     }
     */
    
    [HttpTool getWithURL:kURL(kVersionUpdate) params:nil haveHeader:NO success:^(id json) {
        NSLog(@"%@",json);
        NSDictionary *dict0 = json[@"results"];
        NSString *releaseNote = dict0[@"releaseNotes"];
        NSString *version = dict0[@"version"];
        NSString *forceUpdate = dict0[@"update"];
        [self compareVersionWithVersion:version note:releaseNote force:forceUpdate showHud:isShow isEnterForeground:isEnterForeground];
    } failure:nil];
}

//对比版本号
-(void)compareVersionWithVersion:(NSString *)version note:(NSString *)note force:(NSString *)force showHud:(BOOL)isShow isEnterForeground:(BOOL)isEnterForeground//进入前台
{
    NSDictionary *infoDict = [[NSBundle mainBundle]infoDictionary];
    NSString *curVersion = infoDict[@"CFBundleShortVersionString"];
    
    NSMutableArray *arrayCurrent = [NSMutableArray arrayWithArray:[curVersion componentsSeparatedByString:@"."]];//当前版本号
    NSMutableArray *arrayLasted = [NSMutableArray arrayWithArray:[version componentsSeparatedByString:@"."]];//app store 的版本号
    
    //对比
    NSInteger lengthCur = arrayCurrent.count;
    NSInteger lengthLas = arrayLasted.count;
    //    NSInteger length = lengthCur >= lengthLas ? lengthCur:lengthLas ;
    
    //补全array便于对比
    if (lengthCur > lengthLas) {
        NSInteger tempLength = lengthCur - lengthLas;
        for (int i = 0; i < tempLength; i++) {
            [arrayLasted addObject:@"0"];
        }
    }else if (lengthCur < lengthLas){
        NSInteger tempLength = lengthLas - lengthCur;
        for (int i = 0; i < tempLength; i++) {
            [arrayCurrent addObject:@"0"];
        }
    }
    
    //开始对比
    for (int i = 0; i < arrayCurrent.count; i++) {
        NSString *itemCur = arrayCurrent[i];
        int itemCurINT = itemCur.intValue;
        NSString *itemLas = arrayLasted[i];
        int itemLasINT = itemLas.intValue;
        if (itemCurINT < itemLasINT) {
            NSLog(@"有新版本");
            if (isEnterForeground) {
                if ([force isEqualToString:@"1"]) {
                    [self showAlertWithMsg:note versionCur:curVersion versionLas:version force:force];
                }
            }else{
                [self showAlertWithMsg:note versionCur:curVersion versionLas:version force:force];
            }
            break;
        }else if (itemCurINT == itemLasINT){
            
            continue;
        }else{
            NSLog(@"当前版本大于线上版本");
            break;
        }
    }
}

-(void)showAlertWithMsg:(NSString *)msg versionCur:(NSString *)versionCur versionLas:(NSString *)versionLas force:(NSString *)force
{
    NSString *title = [NSString stringWithFormat:@"发现新版本(%@)",versionLas];
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    //修改alertvc的圆角
    for (UIView *view in alertVC.view.subviews) {
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.backgroundColor = kColorFromRGB(kWhite);
    }
    
    //立即更新
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:kAppStoreURLFormat]];
    }];
    [actionConfirm setValue:kColorFromRGB(kScoreColor) forKey:@"titleTextColor"];
    
    //下次提醒
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"下次提醒" style:UIAlertActionStyleDefault handler:nil];
    [actionCancel setValue:kColorFromRGB(kTabTextColor) forKey:@"titleTextColor"];
    
    if ([force isEqualToString:@"1"]) {
        [alertVC addAction:actionConfirm];
    }else{
        [alertVC addAction:actionCancel];
        [alertVC addAction:actionConfirm];
    }
    
    //    //跳过该版本
    //    UIAlertAction *actionIgnore = [UIAlertAction actionWithTitle:@"忽略该版本" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //        kUserDefaultSetObject(versionLas, IGNORE);
    //    }];
    //    [actionIgnore setValue:kColorFromRGB(kTabTextColor) forKey:@"titleTextColor"];
    //    [alertVC addAction:actionIgnore];
    
    [kKeyWindow.rootViewController presentViewController:alertVC animated:YES completion:nil];
}

-(void)checkNewVersionWithUpdateWindowFromVC:(UIViewController *)vc
{
    /*
     {
     "code": 0000,
     "msg": "操作成功",
     "success": true,
     "results":{
     "ios":{
     "version":"v1.1.2",
     "build":23,
     "downloadUrl":"ios下载地址",
     "forceUpgrade":false //强制更新,
     "desc":["1. Fix Bug","2.Fix Bug"]
     },
     "android":{
     "version":"v1.1.2",
     "build":35,
     "downloadUrl":"安卓下载地址",
     "forceUpgrade":false,
     "desc":["1. Fix Bug","2.Fix Bug"]
     }
     }
     }
     */
    
    if (!kUserDefaultObject(kUserLoginToken)) {
        return;
    }
    NSString *token = kUserDefaultObject(kUserLoginToken);
    NSMutableDictionary *dictParaTest = [CommonParameter commonPara];
    [dictParaTest setObject:@"IOS" forKey:@"deviceOS"];//IOS ANDROID
    [dictParaTest setObject:token forKey:@"token"];
    [HttpTool postWithURL:kURL(kUpgradeVersion) params:dictParaTest haveHeader:NO success:^(id json) {
        if (json) {
            kUserDefaultSetObject(@"NO", kIsShowCusWindow);
            kUserDefaultSynchronize;
            
            NSDictionary *dict = (NSDictionary *)json;
            NSString *codeStr = [NSString stringWithFormat:@"%@", dict[@"errcode"]];
            BOOL isEmpty = [codeStr isEmpty];
            if (isEmpty) {
                NSDictionary *ios = dict[@"results"][@"ios"];
                NSString *version = [NSString stringWithFormat:@"%@",ios[@"version"]];
                NSNumber *force = ios[@"forceUpgrade"];
                NSNumber *buildVersion = [NSNumber numberWithInt:1];
                if (ios[@"build"]) {
                    buildVersion = ios[@"build"];
                }
                
                NSArray *descArr = [NSArray array];
                if (ios[@"desc"]) {
                    descArr = ios[@"desc"];
                }
                NSMutableString *desc = [NSMutableString string];
                for (NSString *str in descArr) {
                    if (![descArr.lastObject isEqualToString:str]) {
                        [desc appendString:str];
                        [desc appendString:@"\n"];
                    }else{
                        [desc appendString:str];
                    }
                }
                
                NSMutableDictionary *dictPara = [NSMutableDictionary dictionary];
                if (![NSString isEmpty:version]) {
                    [dictPara setObject:version forKey:@"version"];
                }
                if (![NSString isEmpty:desc]) {
                    [dictPara setObject:desc forKey:@"desc"];
                }
                if (force != nil) {
                    [dictPara setObject:force forKey:@"forceUpgrade"];
                }
                if (buildVersion != nil) {
                    [dictPara setObject:buildVersion forKey:@"build"];
                }
                
                //res=1有新版本，res=-1当前版本大于线上版本,res=0版本相同
                int res = [self compareWithVersion:[NSString stringWithFormat:@"%@",buildVersion]];
                if (1==res) {
                    [self presentUpdateWindowWithPara:dictPara fromVC:vc];
                }
            }
            
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}
//根据构建号来判断是否升级
-(int)compareWithVersion:(NSString *)buildVersion
{
    NSDictionary *infoDict = [[NSBundle mainBundle]infoDictionary];
    NSString *curBuildVersion = infoDict[@"CFBundleVersion"];
    int curBuildVersionINT = [curBuildVersion intValue];
    int buildVersionINT = [buildVersion intValue];
    
    if (curBuildVersionINT > buildVersionINT) {
        return -1;
    }else if (curBuildVersionINT == buildVersionINT){
        return 0;
    }else{
        return 1;
    }
}
-(void)presentUpdateWindowWithPara:(NSDictionary *)para fromVC:(UIViewController *)vc
{
    //获取当前控制器
    while (1) {
        if ([vc isKindOfClass:[UITabBarController class]]) {
            vc = ((UITabBarController*)vc).selectedViewController;
        }
        
        if ([vc isKindOfClass:[UINavigationController class]]) {
            vc = ((UINavigationController*)vc).visibleViewController;
        }
        
        if (vc.presentedViewController) {
            vc = vc.presentedViewController;
        }else{
            break;
        }
    }
    if ([vc isKindOfClass:[UpdateWindowController class]]) {
        return;
    }
    //因为登录成功后立即dismiss掉，会影响推出升级页，所以选用登录页的父页来推出
    if ([vc isKindOfClass:NSClassFromString(@"LoginViewController")]) {
        vc = vc.presentingViewController;
    }
    
    UpdateWindowController *uWindowVC = [[UpdateWindowController alloc]initWithNibName:NSStringFromClass([UpdateWindowController class]) bundle:nil];
    uWindowVC.dictPara = para;
    uWindowVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    uWindowVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [vc presentViewController:uWindowVC animated:YES completion:nil];
}

@end
