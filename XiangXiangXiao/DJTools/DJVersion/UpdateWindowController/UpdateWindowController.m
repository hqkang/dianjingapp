//
//  UpdateWindowController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "UpdateWindowController.h"

#define kAppStoreURL @"itms-apps://itunes.apple.com/cn/app/id1324471801?mt=8"

@interface UpdateWindowController ()
@property (weak, nonatomic) IBOutlet UIView *bgWindow;
@property (weak, nonatomic) IBOutlet UIView *bgViewWeb;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbVersion;
@property (nonatomic, strong)UIButton *btnCancel;
@property (nonatomic, strong)UIButton *btnUpdate;
@property (nonatomic, strong)UIButton *btnForceUpdate;
@property (nonatomic, strong)UITextView *tvVersionDesc;
@end

@implementation UpdateWindowController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
    [self filloutData];
}

#pragma mark - <填充数据>
-(void)filloutData
{
    NSString *desc = self.dictPara[@"desc"];
    NSString *version = self.dictPara[@"version"];
    NSNumber *forceUpdate = self.dictPara[@"forceUpgrade"];
    
    self.lbVersion.text = version;
    self.tvVersionDesc.text = desc;
    if ([forceUpdate isEqualToNumber:[NSNumber numberWithInt:0]]) {
        [self.btnCancel setHidden:NO];
        [self.btnUpdate setHidden:NO];
        [self.btnForceUpdate setHidden:YES];
    }else{
        [self.btnCancel setHidden:YES];
        [self.btnUpdate setHidden:YES];
        [self.btnForceUpdate setHidden:NO];
    }
}

#pragma mark - <懒加载>
-(UIButton *)btnCancel
{
    if (!_btnCancel) {
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setTitle:@"取消" forState:UIControlStateNormal];
        [_btnCancel setTitleColor:kColorFromRGB(kTabTextColor) forState:UIControlStateNormal];
        [_btnCancel.titleLabel setFont:[UIFont systemFontOfSize:16]];
        _btnCancel.layer.cornerRadius = 22;
        _btnCancel.layer.masksToBounds  = YES;
        _btnCancel.layer.borderColor = kColorFromRGB(kTabSeperatorColor).CGColor;
        _btnCancel.layer.borderWidth = 1;
        [_btnCancel setTag:10011];
        [self.bgViewWeb addSubview:_btnCancel];
        
        [_btnCancel addTarget:self action:@selector(versionAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCancel;
}
-(UIButton *)btnUpdate
{
    if (!_btnUpdate) {
        _btnUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnUpdate setBackgroundColor:kColorFromRGB(kScoreColor)];
        [_btnUpdate setTitle:@"立即升级" forState:UIControlStateNormal];
        [_btnUpdate setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
        [_btnUpdate.titleLabel setFont:[UIFont systemFontOfSize:16]];
        _btnUpdate.layer.cornerRadius = 22;
        _btnUpdate.layer.masksToBounds  = YES;
        [_btnUpdate setTag:10012];
        [self.bgViewWeb addSubview:_btnUpdate];
        
        [_btnUpdate addTarget:self action:@selector(versionAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnUpdate;
}
-(UIButton *)btnForceUpdate
{
    if (!_btnForceUpdate) {
        _btnForceUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnForceUpdate setBackgroundColor:kColorFromRGB(kScoreColor)];
        [_btnForceUpdate setTitle:@"立即升级" forState:UIControlStateNormal];
        [_btnForceUpdate setTitleColor:kColorFromRGB(kWhite) forState:UIControlStateNormal];
        [_btnForceUpdate.titleLabel setFont:[UIFont systemFontOfSize:16]];
        _btnForceUpdate.layer.cornerRadius = 22;
        _btnForceUpdate.layer.masksToBounds  = YES;
        [_btnForceUpdate setTag:10013];
        [self.bgViewWeb addSubview:_btnForceUpdate];
        
        [_btnForceUpdate addTarget:self action:@selector(versionAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnForceUpdate;
}
-(UITextView *)tvVersionDesc
{
    if (!_tvVersionDesc) {
        _tvVersionDesc = [[UITextView alloc]initWithFrame:self.bgViewWeb.bounds];
        _tvVersionDesc.font = [UIFont systemFontOfSize:15];
        _tvVersionDesc.textColor = kColorFromRGB(kTextColor);
        _tvVersionDesc.editable = NO;
        [self.bgViewWeb addSubview:_tvVersionDesc];
    }
    return _tvVersionDesc;
}

#pragma mark - <setupUI>
-(void)setupUI
{
    kWeakSelf(self);
    [self.imgViewHeader mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
    }];
    [self.btnCancel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.bottom.mas_equalTo(-20);
        make.height.mas_equalTo(40);
        make.right.mas_equalTo(weakself.bgViewWeb.mas_centerX).with.offset(-10);
    }];
    [self.btnUpdate mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.left.mas_equalTo(weakself.bgViewWeb.mas_centerX).with.offset(10);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-20);
    }];
    [self.btnForceUpdate mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.right.mas_equalTo(-18);
        make.bottom.mas_equalTo(-20);
        make.height.mas_equalTo(40);
    }];
    [self.bgViewWeb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakself.imgViewHeader.mas_bottom).with.offset(-40);
        make.left.right.bottom.mas_equalTo(0);
    }];
    [self.tvVersionDesc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 35, 70, 35));
    }];
}

-(void)versionAction:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    switch (tag) {
        case 10011://取消升级
        {[self dismissViewControllerAnimated:NO completion:nil];}
            break;
        case 10012://立即升级
        {[self dismissViewControllerAnimated:NO completion:^{
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:kAppStoreURL]];
        }];}
            break;
        case 10013://强制升级不关闭
        {[[UIApplication sharedApplication]openURL:[NSURL URLWithString:kAppStoreURL]];}
            break;
            
        default:
            break;
    }
}
@end
