//
//  UpdateWindowController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpdateWindowController : UIViewController
@property (nonatomic, strong)NSDictionary *dictPara;
@end

NS_ASSUME_NONNULL_END
