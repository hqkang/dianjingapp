//
//  DJVersion.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DJVersion : NSObject

+ (DJVersion *)sharedInstance;
-(void)checkNewVersionWithShowHud:(BOOL)isShow isEnterForeground:(BOOL)isEnterForeground;
-(void)checkNewVersionWithUpdateWindowFromVC:(UIViewController *)vc;

@end

NS_ASSUME_NONNULL_END
