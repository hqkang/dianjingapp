//
//  RoundProgressView.h
//  HaoHuaCaiWu
//
//  Created by xhkj on 2019/6/6.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoundProgressView : UIView

@property (strong, nonatomic) UILabel *progressLabel;

/**进度条颜色*/
@property (strong, nonatomic) UIColor *progressColor;

@property (strong, nonatomic) UIColor *progressTextColor;
/**dash pattern*/
@property (strong, nonatomic) NSArray *lineDashPattern;
/**进度Label字体*/
@property (strong, nonatomic) UIFont  *progressFont;

- (void)updateProgress:(CGFloat)progress;

@end

NS_ASSUME_NONNULL_END
