//
//  QKProfessionalToolBar.h
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol QKProfessionalToolBarDelegate<NSObject>
/// left - 0, right - 1
- (void)toolBarDidClickButton:(NSInteger)leftOrRight;
@end

@interface QKProfessionalToolBar : UIView
@property (nonatomic,  strong,  readonly) UIButton *leftButton;
@property (nonatomic,  strong,  readonly) UILabel *titleLable;
@property (nonatomic,  strong,  readonly) UIButton *rightButton;

@property (nonatomic,    weak) id<QKProfessionalToolBarDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
