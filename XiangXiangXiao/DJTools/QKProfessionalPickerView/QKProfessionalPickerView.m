//
//  QKProfessionalPickerView.m
//  SchoolOnline
//
//  Created by xhkj on 2018/12/10.
//  Copyright © 2018年 DD. All rights reserved.
//

#import "QKProfessionalPickerView.h"
#import "QKProfessionalToolBar.h"
#import "QKProfessionalPicker.h"

@interface QKProfessionalPickerView()<QKProfessionalToolBarDelegate,QKProfessionalPickerDelegate>
@property (nonatomic,  strong) UIView *grayView;
@property (nonatomic,  strong) UIButton *dissmissButton;
@property (nonatomic,  strong) UIView *contentView;

@property (nonatomic,  strong) QKProfessionalToolBar *toolBar;
@property (nonatomic,  strong) QKProfessionalPicker *picker;

@property (nonatomic,  strong) NSMutableDictionary *resultDic;

@end

@implementation QKProfessionalPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        _grayViewAlpha = 0.3;
        _columns = 2;
        _resultDic = @{}.mutableCopy;
        [self xjh_setupViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withDataArray:(NSMutableArray *)dataArray
{
    self = [super initWithFrame:frame];
    if (self) {
        _grayViewAlpha = 0.3;
        _columns = 2;
        _resultDic = @{}.mutableCopy;
        _dataArray = dataArray;
        [self xjh_setupViews];
    }
    return self;
}

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)xjh_setupViews
{
    [self addSubview:self.grayView];
    [self addSubview:self.contentView];
    [_contentView addSubview:self.toolBar];
    [_contentView addSubview:self.picker];
}

- (UIView *)grayView{
    if (!_grayView) {
        _grayView = [[UIView alloc] init];
        _grayView.frame = self.bounds;
        _grayView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    }
    return _grayView;
}

- (UIButton *)dissmissButton{
    if (!_dissmissButton) {
        _dissmissButton = [[UIButton alloc] init];
        _dissmissButton.frame = self.bounds;
        [_dissmissButton addTarget:self action:@selector(hide) forControlEvents:1<<6];
        [_grayView addSubview:_dissmissButton];
    }
    return _dissmissButton;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.frame = CGRectMake(0, CGRectGetMaxY(self.bounds), CGRectGetWidth(self.bounds), 320);
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (QKProfessionalToolBar *)toolBar{
    if (!_toolBar) {
        _toolBar = [[QKProfessionalToolBar alloc] init];
        _toolBar.delegate = self;
    }
    return _toolBar;
}

- (QKProfessionalPicker *)picker{
    if (!_picker) {
        _picker = [[QKProfessionalPicker alloc] init];
        _picker.frame = CGRectMake(0, 50, CGRectGetWidth(self.bounds), 200);
        _picker.delegate = self;
        _picker.dataArray = _dataArray;
    }
    return _picker;
}

#pragma mark - public

- (void)setHideWhenTapGrayView:(BOOL)hideWhenTapGrayView{
    _hideWhenTapGrayView = hideWhenTapGrayView;
    if (hideWhenTapGrayView) {
        self.dissmissButton.hidden = NO;
    }else{
        self.dissmissButton.hidden = YES;
    }
}

- (void)setColumns:(NSInteger)columns{
    if (_columns == 2 || _columns == 3) {
        _columns = columns;
        _picker.columns = columns;
        [_picker loadData];
    }
}


- (void)showInView:(UIView *)view{
    if (!view) {
        return;
    }
    
    [view addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        [self viewAnimation:_grayViewAlpha height:-CGRectGetHeight(_contentView.frame)];
    }];
}

- (void)hide{
    [UIView animateWithDuration:0.25 animations:^{
        [self viewAnimation:0 height:CGRectGetHeight(_contentView.frame)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - private

- (void)viewAnimation:(CGFloat)alpha height:(CGFloat)height
{
    _grayView.backgroundColor = [UIColor colorWithWhite:0 alpha:alpha];
    
    CGRect frame = _contentView.frame;
    frame.origin.y += height;
    _contentView.frame = frame;
}

#pragma mark - QKProfessionalToolBarDelegate
- (void)toolBarDidClickButton:(NSInteger)leftOrRight{
    // left - 0, right - 1
    if (leftOrRight == 1) {
        if (_pickBlock) {
            _pickBlock(_resultDic);
        }
    }
    [self hide];
}

#pragma mark - QKProfessionalPickerDelegate
- (void)professionalPickerDidSelectedRow:(NSInteger)row1 row2:(NSInteger)row2{
    //NSLog(@"row1:%@, row2:%@, row3:%@",@(row1),@(row2),@(row3));
    
    NSString *mainProfessional = nil;
    NSString *mainProfessionalID = nil;
    NSString *secondProfessional = nil;
    NSString *secondProfessionalID = nil;
    NSMutableString *mstr = @"".mutableCopy;
    NSDictionary *dic1 = _dataArray[row1];
    mainProfessional = dic1[@"name"];
    mainProfessionalID = dic1[@"id"];
    
    //    [mstr appendString:mainIndustry];
    
    NSArray *array2 = [dic1 valueForKeyPath:@"children"];
    if (row2 < array2.count) {
        NSDictionary *dic2 = array2[row2];
        secondProfessional = dic2[@"name"];
        secondProfessionalID = dic2[@"id"];
        //        [mstr appendString:secondIndustry];
        
    }
    
    _toolBar.titleLable.text = @"选择专业";
    
    [_resultDic setValue:mainProfessional forKey:@"mainProfessional"];
    [_resultDic setValue:mainProfessionalID forKey:@"mainProfessionalID"];
    [_resultDic setValue:secondProfessional forKey:@"secondProfessional"];
    [_resultDic setValue:secondProfessionalID forKey:@"secondProfessionalID"];
    
    //NSLog(@"%@",_resultDic);
}

@end
