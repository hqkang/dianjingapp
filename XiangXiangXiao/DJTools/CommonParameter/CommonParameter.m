//
//  CommonParameter.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CommonParameter.h"

@implementation CommonParameter

+(CommonParameter *)shareCP
{
    static CommonParameter *sharedCommonParameterInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedCommonParameterInstance = [[self alloc] init];
    });
    return sharedCommonParameterInstance;
}
+(NSMutableDictionary *)commonPara
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    //token 权限token
    //appVersion 当前APP版本号
    //buildingCode 当前app构建号
    //deviceOs 当前系统平台,0:安卓;1:IOS
    //apiVersion api协议版本,默认为1，前端根据当前调用接口版本传输。
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    int buildCode = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]intValue];
    NSNumber *buildingCode = [NSNumber numberWithInt:buildCode];
    NSNumber *deviceOs = @(1);
    NSNumber *apiVersion = @(2);// 团购接口 协议版本2
    
    [dict setObject:appVersion forKey:@"appVersion"];
    [dict setObject:buildingCode forKey:@"buildingCode"];
    [dict setObject:deviceOs forKey:@"deviceOs"];
    [dict setObject:apiVersion forKey:@"apiVersion"];
    
    return dict;
}

@end
