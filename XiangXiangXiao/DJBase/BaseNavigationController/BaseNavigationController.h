//
//  BaseNavigationController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BaseNavigationControllerDelegate <NSObject>

- (void)baseNavigationControllerBackButtonDidClick:(UIButton *)button;

@end

typedef void(^BlockReturnBack)(void);

@interface BaseNavigationController : UINavigationController

@property (nonatomic, copy)BlockReturnBack returnBackBlock;

@property (nonatomic, weak) id<BaseNavigationControllerDelegate>baseNavDelegate;

@end

NS_ASSUME_NONNULL_END
