//
//  BaseNavigationController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self settingNavigation];
    [QK_NOTICENTER addObserver:self selector:@selector(removeBaseNavDelegate) name:kRemoveBaseNavDelegate object:nil];
}

- (void)removeBaseNavDelegate
{
    self.baseNavDelegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)settingNavigation
{
    self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = kColorFromRGB(kNavColor);
    //    self.navigationBar setBackgroundImage:[UIImage ] forBarMetrics:<#(UIBarMetrics)#>
}

#pragma mark - <重写push方法>
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
        
        //设置返回按钮
        UIButton *btnBack = [[UIButton alloc]init];
        btnBack = [self createBackButtonWithImage:@"nav_btn_back"];
        UIBarButtonItem *itemBackButton = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
        ////        //解决按钮不靠左的问题.
        //        UIBarButtonItem * spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        //        spaceItem.width = -50;
        //        viewController.navigationItem.leftBarButtonItems = @[spaceItem,itemBackButton];
        viewController.navigationItem.leftBarButtonItem = itemBackButton;
    }
    [super pushViewController:viewController animated:YES];
}

#pragma mark - <设置返回按钮>
-(UIButton *)createBackButtonWithImage:(NSString *)imageName
{
    UIImage *imgBack = [UIImage imageNamed:imageName];
    imgBack = [imgBack imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 0, 30, 30);
    [btnBack setImage:imgBack forState:UIControlStateNormal];
    //    [btnBack.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [btnBack addTarget:self action:@selector(backNormalAction) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setContentEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    return btnBack;
}

-(void)backNormalAction
{
    if (self.baseNavDelegate) {
        [QK_NOTICENTER postNotificationName:@"webBack" object:nil userInfo:nil];
    }else{
        [self popViewControllerAnimated:YES];
        if (self.returnBackBlock != nil) {
            self.returnBackBlock();
        }
    }
    
}

#pragma mark - <设置返回到rootVC按钮>
-(UIButton *)createBackButtonRootWithImage:(NSString *)imageName
{
    UIImage *imgBack = [UIImage imageNamed:imageName];
    imgBack = [imgBack imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, -15, 30, 30);
    [btnBack setImage:imgBack forState:UIControlStateNormal];
    [btnBack.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [btnBack addTarget:self action:@selector(backRooterAction) forControlEvents:UIControlEventTouchUpInside];
    return btnBack;
}
-(void)backRooterAction
{
    if (self.baseNavDelegate) {
        [QK_NOTICENTER postNotificationName:@"webBack" object:nil userInfo:nil];
    }else{
        [self popViewControllerAnimated:YES];
        if (self.returnBackBlock != nil) {
            self.returnBackBlock();
        }
    }
}

@end
