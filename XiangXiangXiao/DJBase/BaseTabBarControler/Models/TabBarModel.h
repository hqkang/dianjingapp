//
//  TabBarModel.h
//  SchoolOnline
//
//  Created by xhkj on 2019/4/8.
//  Copyright © 2019年 DD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TabBarModel : NSObject

/*
 "active_icon" = "http://api.xiangxiangxiao.cn/uploads/images/dfe56649dee75d1dfc80452a552fdfdc.png";
 icon = "http://api.xiangxiangxiao.cn/uploads/images/18cc482a198fe67b1d4b9456fac1e28f.png";
 id = 4;
 name = "\U5c0f\U89c6\U9891";
 */

@property (nonatomic, strong) NSString *active_icon;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSNumber *tabBarID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;

@end

NS_ASSUME_NONNULL_END
