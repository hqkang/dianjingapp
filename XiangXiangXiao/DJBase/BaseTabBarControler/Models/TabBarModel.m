//
//  TabBarModel.m
//  SchoolOnline
//
//  Created by xhkj on 2019/4/8.
//  Copyright © 2019年 DD. All rights reserved.
//

#import "TabBarModel.h"

@implementation TabBarModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.tabBarID = value;
    }
}

@end
