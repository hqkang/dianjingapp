//
//  BaseTabBarController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "BaseTabBarController.h"
#import "MeViewController.h"
#import "HomeViewController.h"
#import "TabBarModel.h"
#import "BaseNavigationController.h"

@interface BaseTabBarController ()

@end

@implementation BaseTabBarController

+ (void)initialize
{
    // 通过appearance统一设置所有UITabBarItem的文字属性
    // 后面带有UI_APPEARANCE_SELECTOR的方法, 都可以通过appearance对象来统一设置
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    attrs[NSForegroundColorAttributeName] = kColorFromRGB(kTextColor);
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = kColorFromRGB(kCateTitleColor);
    //    selectedAttrs[NSForegroundColorAttributeName] = [UIColor setGradualChangingColor:[UITabBarItem appearance] fromColor:@"#8652D0" toColor:@"#5239B8"];
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    [UITabBar appearance].translucent = NO;
}

- (void)setDataArray:(NSMutableArray *)dataArray
{
    if (_dataArray != dataArray) {
        _dataArray = dataArray;
    }
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tabbar" ofType:@"plist"]];
    for (int i = 0; i < dataArray.count; i++) {
        
        TabBarModel *model = [[TabBarModel alloc] init];
        model = dataArray[i];
        if([[dic allKeys] containsObject:model.code])
        {
            [self setupChildVc:[[NSClassFromString(dic[model.code]) alloc] init] title:model.name image:model.icon selectedImage:model.active_icon isHiddenNavgationBar:NO];
        }
    }
}

- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage isHiddenNavgationBar:(BOOL)isHidden
{
    // 设置文字和图片
//    vc.navigationItem.title = title;
    
    vc.tabBarItem.title = title;
    
    
    
    NSData *dataN = [NSData dataWithContentsOfURL:[NSURL URLWithString:image]];
    
    UIImage *imageN = [UIImage imageWithData:dataN];
    NSString *strnum = [NSString stringWithFormat:@"%@@3x.png", title];
    NSString *imageFilePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:strnum];
    // 将取得的图片写入本地的沙盒中，其中0.5表示压缩比例，1表示不压缩，数值越小压缩比例越大
    NSLog(@"图片路径 == %@",imageFilePath);
    BOOL success = [UIImagePNGRepresentation(imageN) writeToFile:imageFilePath atomically:YES];
    if (success){
        NSLog(@"写入本地成功");
        UIImage *savedImage = [[UIImage alloc] initWithContentsOfFile:imageFilePath]; //未选中图片
        vc.tabBarItem.image = [savedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    NSData *dataS = [NSData dataWithContentsOfURL:[NSURL URLWithString:selectedImage]];
    UIImage *imageS = [UIImage imageWithData:dataS];
    NSString *strnumS = [NSString stringWithFormat:@"%@_pre@3x.png", title];
    NSString *imageFilePathS = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:strnumS];
    // 将取得的图片写入本地的沙盒中，其中0.5表示压缩比例，1表示不压缩，数值越小压缩比例越大
    NSLog(@"图片路径 == %@",imageFilePathS);
    BOOL success1 = [UIImagePNGRepresentation(imageS) writeToFile:imageFilePathS atomically:YES];
    if (success1){
        NSLog(@"写入本地成功");
        UIImage *savedImageS = [[UIImage alloc] initWithContentsOfFile:imageFilePathS]; //未选中图片
        vc.tabBarItem.selectedImage = [savedImageS imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    vc.view.backgroundColor = [UIColor whiteColor];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    if (isHidden) {
        nav.navigationBar.hidden = YES;
    }
    [self addChildViewController:nav];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = kColorFromRGB(kWhite);
}


@end
