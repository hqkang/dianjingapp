//
//  QKTabBarController.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/19.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "QKTabBarController.h"
#import "BaseNavigationController.h"
#import "HomeViewController.h"
#import "FamilyEduViewController.h"
#import "TutorViewController.h"
#import "MyCourseViewController.h"
#import "MeViewController.h"

@interface QKTabBarController ()<UITabBarControllerDelegate,UITabBarDelegate>

@property (nonatomic, strong)NSMutableArray *vcArray;

@end

@implementation QKTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configTabBarController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - <该类初始化的时候执行，设置选中时的字体颜色>
+ (void)initialize {
    //    NSLog(@"%@",[UIFont familyNames]);
    NSDictionary *normalAttribute = @{NSForegroundColorAttributeName:kColorFromRGB(kTabTextColor),
                                      NSFontAttributeName:[UIFont boldSystemFontOfSize:12]};
    NSDictionary *selectedAttribute = @{NSForegroundColorAttributeName:kColorFromRGB(kCateTitleColor),
                                        NSFontAttributeName:[UIFont boldSystemFontOfSize:12]};
    
    [[UITabBarItem appearance] setTitleTextAttributes:normalAttribute forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:selectedAttribute forState:UIControlStateSelected];
    UIImage *tabbarBGImg = [UIImage imageNamed:@"tab_home"];
    tabbarBGImg = [tabbarBGImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UITabBar appearance]setBackgroundImage:tabbarBGImg];
    [[UITabBar appearance]setShadowImage:[[UIImage alloc]init]];
    
}

#pragma mark - <懒加载>
-(NSMutableArray *)vcArray
{
    if (!_vcArray) {
        _vcArray = [NSMutableArray array];
    }
    return _vcArray;
}

#pragma mark - <设置tabbar相关配置>
-(void)configTabBarController
{
    NSMutableArray *array = [NSMutableArray array];
    NSDictionary *dict1 = @{@"className":@"HomeViewController",
                            @"title":@"首页",
                            @"normalImage":@"tab_icon_home_nor",
                            @"selectedImage":@"tab_icon_home_sel",
                            };
    [array addObject:dict1];
    NSDictionary *dict2 = @{@"className":@"TutorViewController",
                            @"title":@"家教",
                            @"normalImage":@"tab_icon_edu_nor",
                            @"selectedImage":@"tab_icon_edu_sel",
                            };
    [array addObject:dict2];
    NSDictionary *dict3 = @{@"className":@"MyCourseViewController",
                            @"title":@"我的课程",
                            @"normalImage":@"tab_icon_course_nor",
                            @"selectedImage":@"tab_icon_course_sel",
                            };
    [array addObject:dict3];
    NSDictionary *dict4 = @{@"className":@"MeViewController", // PersonalViewController
                            @"title":@"我的",
                            @"normalImage":@"tab_icon_me_nor",
                            @"selectedImage":@"tab_icon_me_sel",
                            };
    [array addObject:dict4];
    
    for (NSDictionary *controller in array) {
        BaseNavigationController *vc = [self createViewControllerWith:controller[@"className"] withTitle:controller[@"title"] withNormalImg:controller[@"normalImage"] withSelectedImage:controller[@"selectedImage"]];
        [self.vcArray addObject:vc];
    }
    self.viewControllers = [self.vcArray copy];
    
    //背景颜色
    self.tabBar.backgroundColor = kColorFromRGB(kWhite);
    //tabbar半透明
    self.tabBar.translucent = NO;
    
    self.delegate = self;
}

#pragma mark - <创建tabBar的子控制器>
- (BaseNavigationController *)createViewControllerWith:(NSString *)vcName withTitle:(NSString *)title withNormalImg:(NSString *)normalImage withSelectedImage:(NSString *)selectedImage {
    Class cls = NSClassFromString(vcName);
    UIViewController *vc = [[cls alloc] init];
    UIImage *norImg = [UIImage imageNamed:normalImage];
    UIImage *selImg = [UIImage imageNamed:selectedImage];
    //适配iOS7以后的版本，iOS7以前默认渲染保持图片原样。
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
        norImg = [norImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        selImg = [selImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:norImg selectedImage:selImg];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    return nav;
}

#pragma mark - <选择tabBar>
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}



@end
