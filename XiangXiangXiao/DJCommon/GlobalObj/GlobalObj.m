//
//  GlobalObj.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "GlobalObj.h"

@implementation GlobalObj

+ (NSString *)imgUrlFullPath:(NSString *)imgUrl{
    if (![imgUrl isNotEmpty]) {
        return @"";
    }
    if ([imgUrl hasPrefix:@"http://"] || [imgUrl hasPrefix:@"https://"]) {
        return imgUrl;
    }
    return [NSString stringWithFormat:@"%@%@",kImgDomain,imgUrl];
}

+ (NSString *)userToken{
    NSString *token = kUserDefaultObject(kUserLoginToken);
    return token?token:@"";
}

+ (NSString *)userPhone{
    NSDictionary *userInfo = kUserDefaultObject(kUserInfo);
    return [userInfo stringForKey:@"userPhone"];
}

@end
