//
//  GlobalObj.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlobalObj : NSObject

// 加载web图片
+ (NSString *)imgUrlFullPath:(NSString *)imgUrl;

// 用户名
+ (NSString *)userPhone;

// 用户token
+ (NSString *)userToken;

@end

NS_ASSUME_NONNULL_END
