//
//  DJRichText.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TTTAttributedLabel.h>

NS_ASSUME_NONNULL_BEGIN

@interface DJRichText : NSObject

+(TTTAttributedLabel *)createRichTextLabelWithFrame:(CGRect)frame delegate:(id<TTTAttributedLabelDelegate>)delegate font:(CGFloat)font textAlignment:(NSTextAlignment)textAlignment normalTextRGBColor:(int)normalTextRGB richTextRGBColor:(int)richTextRGB textArray:(NSArray *)textArray richTextIndex:(NSArray<NSNumber *> *)richTextIndexArray underLineStyle:(CTUnderlineStyle)underLineStyle url:(NSArray *)urls;

@end

NS_ASSUME_NONNULL_END
