//
//  DJRichText.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "DJRichText.h"

@implementation DJRichText

+(TTTAttributedLabel *)createRichTextLabelWithFrame:(CGRect)frame delegate:(id<TTTAttributedLabelDelegate>)delegate font:(CGFloat)font textAlignment:(NSTextAlignment)textAlignment normalTextRGBColor:(int)normalTextRGB richTextRGBColor:(int)richTextRGB textArray:(NSArray *)textArray richTextIndex:(NSArray<NSNumber *> *)richTextIndexArray underLineStyle:(CTUnderlineStyle)underLineStyle url:(NSArray *)urls
{
    TTTAttributedLabel *labelRichText = [[TTTAttributedLabel alloc]initWithFrame:frame];
    labelRichText.numberOfLines = 0;
    labelRichText.font = [UIFont systemFontOfSize:font];
    labelRichText.lineBreakMode = NSLineBreakByCharWrapping;
    labelRichText.textColor = kColorFromRGB(normalTextRGB);
    labelRichText.textAlignment = textAlignment;
    //    //长按富文本显示的文本颜色
    //    labelService.highlightedTextColor = kColorFromRGB(highlightedTextRGB);
    labelRichText.delegate = delegate;
    
    //拼接所有文本
    NSMutableString *labelText = [NSMutableString string];
    for (int i=0; i<textArray.count; i++) {
        [labelText appendString:textArray[i]];
    }
    
    //富文本的范围数组
    NSMutableArray *rangeArray = [NSMutableArray array];
    
    //设置富文本的下划线（重点注意：设置下划线必须在setText之前）
    if (underLineStyle == kCTUnderlineStyleNone) {
        NSDictionary *attribute = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCTUnderlineStyleNone] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        [labelRichText setLinkAttributes:attribute];
    }else{
        NSDictionary *attribute = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:underLineStyle],kCTUnderlineStyleAttributeName,kColorFromRGB(richTextRGB),kCTUnderlineColorAttributeName, nil];
        [labelRichText setLinkAttributes:attribute];
    }
    
    //设置富文本
    [labelRichText setText:labelText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        //储存富文本的范围
        for (int i=0; i<textArray.count; i++) {
            for (int j=0; j<richTextIndexArray.count; j++) {
                NSNumber *numberIndex = richTextIndexArray[j];
                int index = [numberIndex intValue];
                if (i == index) {
                    NSRange range = [[mutableAttributedString string]rangeOfString:textArray[i]];
                    NSData *rangeData = [NSData dataWithBytes:&range length:sizeof(range)];
                    [rangeArray addObject:rangeData];
                    
                    //设置可点击文本的颜色
                    [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:kColorFromRGB(richTextRGB) range:range];
                }
            }
        }
        return mutableAttributedString;
    }];
    
    for (int i=0; i<rangeArray.count; i++) {
        NSData *rangeData = rangeArray[i];
        NSRange range;
        [rangeData getBytes:&range length:sizeof(range)];
        //富文本添加url
        [labelRichText addLinkToURL:urls[i] withRange:range];
    }
    
    return labelRichText;
}


@end
