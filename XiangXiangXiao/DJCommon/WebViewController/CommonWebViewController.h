//
//  CommonWebViewController.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/15.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonWebViewController : UIViewController

@property (nonatomic, strong) NSString *urlStr;

@property (nonatomic, strong) NSString *titleStr;

@property (nonatomic, strong) NSDictionary *dataDic;

@property (nonatomic, strong) id params;

@property (nonatomic, strong) id userDic;


@end

NS_ASSUME_NONNULL_END
