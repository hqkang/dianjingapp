//
//  CustomDefaultView.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomDefaultView;

@protocol CustomDefaultViewDelegate<NSObject>
@optional
-(void)didClickOperateAction:(UIButton *)sender defaultView:(CustomDefaultView *)defaultView;
@end

@interface CustomDefaultView : UIView
@property (nonatomic, weak)id<CustomDefaultViewDelegate> delegate;
@property (nonatomic, copy)NSString *imgName;
@property (nonatomic, copy)NSString *warnTitle;
@property (nonatomic, copy)NSString *buttonName;
@property (nonatomic, assign) BOOL hideButton;
@end
