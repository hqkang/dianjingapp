//
//  CustomDefaultView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/18.
//  Copyright © 2019年 xhkj. All rights reserved.
//
#import "CustomDefaultView.h"

@interface CustomDefaultView()
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *lbWarning;
@property (weak, nonatomic) IBOutlet UIButton *btnOperate;
@end

@implementation CustomDefaultView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)btnOperateAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(didClickOperateAction:defaultView:)]) {
        [self.delegate didClickOperateAction:sender defaultView:self];
    }
}

-(void)setImgName:(NSString *)imgName
{
    self.imgv.image = [UIImage imageNamed:imgName];
}
-(void)setWarnTitle:(NSString *)warnTitle
{
    if (warnTitle) {
        self.lbWarning.text = warnTitle;
    }else{
        self.lbWarning.text = @"";
    }
}
-(void)setButtonName:(NSString *)buttonName
{
    [self.btnOperate setTitle:buttonName forState:UIControlStateNormal];
}
-(void)setHideButton:(BOOL)hideButton
{
    [self.btnOperate setHidden:hideButton];
}










@end
