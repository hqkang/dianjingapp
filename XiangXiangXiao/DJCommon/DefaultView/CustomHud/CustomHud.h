//
//  CustomHud.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomHud : NSObject
+(MBProgressHUD *)createCustomHudWithMsg:(NSString *)msg duration:(CGFloat)time imgName:(NSString *)imgName addTo:(UIView *)view;
@end
