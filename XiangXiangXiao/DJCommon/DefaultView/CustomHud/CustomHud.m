//
//  CustomHud.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CustomHud.h"

@implementation CustomHud
+(MBProgressHUD *)createCustomHudWithMsg:(NSString *)msg duration:(CGFloat)time imgName:(NSString *)imgName addTo:(UIView *)view
{
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:view animated:YES];
    
    // 显示模式,改成customView,即显示自定义图片(mode设置,必须写在customView赋值之前)
    hud.mode = MBProgressHUDModeCustomView;
    
    // 设置要显示 的自定义的图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    // 显示的文字,比如:加载失败...加载中...
    hud.label.text = msg;
    hud.label.textColor = kColorFromRGB(kTextColor);
    hud.bezelView.color = kColorFromRGB(kWhite);
    hud.backgroundColor = kColorFromRGBAndAlpha(kNameColor, 0.5);
    // 标志:必须为YES,才可以隐藏,  隐藏的时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:time];
    return hud;
}
@end
