//
//  SucceedView.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "SucceedView.h"

@interface SucceedView()
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@end

@implementation SucceedView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setTitleStr:(NSString *)titleStr
{
    self.lbTitle.text = titleStr;
}

@end
