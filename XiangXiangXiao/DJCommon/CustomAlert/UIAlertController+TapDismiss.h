//
//  UIAlertController+TapDismiss.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/7/10.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (TapDismiss)
/// 弹窗点击dismiss
- (void)alertTapDismiss;

@end

NS_ASSUME_NONNULL_END
