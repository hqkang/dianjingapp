//
//  CustomAlert.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import "CustomAlert.h"

@implementation CustomAlert

#pragma mark - 只有确认按钮
+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionConfirmName:(NSString *)confirmName actionConfirm:(void (^)(void))actionConfirmBlock
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    //修改alertvc的圆角
    for (UIView *view in alertVC.view.subviews) {
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.backgroundColor = kColorFromRGB(kWhite);
    }
    
    if (confirmName) {
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:confirmName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (actionConfirmBlock != nil) {
                actionConfirmBlock();
            }
        }];
        [actionConfirm setValue:kColorFromRGB(kScoreColor) forKey:@"titleTextColor"];
        [alertVC addAction:actionConfirm];
    }
    
    return alertVC;
}

#pragma mark - 确认/取消两个按钮
+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionConfirmName:(NSString *)confirmName actionCancelName:(NSString *)cancelName actionConfirm:(void (^)(void))actionConfirmBlock actionCancel:(void (^)(void))actionCancelBlock
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    //修改alertvc的圆角
    for (UIView *view in alertVC.view.subviews) {
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.backgroundColor = kColorFromRGB(kWhite);
    }
    
    if (cancelName) {
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:cancelName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (actionCancelBlock != nil) {
                actionCancelBlock();
            }
        }];
        [actionCancel setValue:kColorFromRGB(kTabTextColor) forKey:@"titleTextColor"];
        [alertVC addAction:actionCancel];
    }
    
    if (confirmName) {
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:confirmName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (actionConfirmBlock != nil) {
                actionConfirmBlock();
            }
        }];
        [actionConfirm setValue:kColorFromRGB(kScoreColor) forKey:@"titleTextColor"];
        [alertVC addAction:actionConfirm];
    }
    
    return alertVC;
}

#pragma mark - 多个按钮
+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionName:(NSArray *)actionNameArray actionConfirm:(void (^)(int index))actionConfirmBlock
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    //修改alertvc的圆角
    for (UIView *view in alertVC.view.subviews) {
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.backgroundColor = kColorFromRGB(kWhite);
    }
    if (actionNameArray.count > 0) {
        for (int i = 0; i < actionNameArray.count; i++) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:actionNameArray[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (actionConfirmBlock != nil) {
                    actionConfirmBlock(i);
                }
            }];
            
            [action setValue:kColorFromRGB(kNameColor) forKey:@"titleTextColor"];
            [alertVC addAction:action];
        }
    }
    
    return alertVC;
}

@end
