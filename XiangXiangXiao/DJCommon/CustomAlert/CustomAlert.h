//
//  CustomAlert.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomAlert : NSObject

// 只有确认按钮
+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionConfirmName:(NSString *)confirmName actionConfirm:(void (^)(void))actionConfirmBlock;

// 确认/取消两个按钮
+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionConfirmName:(NSString *)confirmName actionCancelName:(NSString *)cancelName actionConfirm:(void (^)(void))actionConfirmBlock actionCancel:(void (^)(void))actionCancelBlock;

+(UIAlertController *)createAlertWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)style actionName:(NSArray *)actionNameArray  actionConfirm:(void (^)(int index))actionConfirmBlock;

@end
