//
//  CustomPageControl.m
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import "CustomPageControl.h"

static const CGFloat dotW = 14;
static const CGFloat dotH = 5;
static const CGFloat marginX = dotW+dotH+3;

@implementation CustomPageControl

-(void)layoutSubviews
{
    [super layoutSubviews];
    //计算圆点间距
    
    //计算整个pageControll的宽度
    CGFloat newW = (self.subviews.count - 1 ) * marginX;
    
    //设置新frame
    self.frame = CGRectMake(kScreenWidth/2-(newW + dotH)/2, self.frame.origin.y, newW + dotW, self.frame.size.height);
    
    //遍历subview,设置圆点frame
    for (int i=0; i<[self.subviews count]; i++) {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        
        if (i == self.currentPage) {
            [dot setFrame:CGRectMake(i * marginX, dot.frame.origin.y, dotW, dotH)];
        }else {
            [dot setFrame:CGRectMake(i * marginX, dot.frame.origin.y, dotW, dotH)];
        }
    }

}

@end
