//
//  CustomPageControl.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomPageControl : UIPageControl

@end

NS_ASSUME_NONNULL_END
