//
//  DJ_URL.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#ifndef DJ_URL_h
#define DJ_URL_h

#pragma mark - <域名>
#define kDomain (QK_DEV ? @"https://api.xiangxiangxiao.cn/" : @"https://api.xiangxiangxiao.com/")
#define kImgDomain (QK_DEV ? @"http://api.xiangxiangxiao.cn/file/image/" : @"https://api.xiangxiangxiao.com/file/image/")
#define H5_URL (QK_DEV ? @"http://front.xiangxiangxiao.cn/" : @"http://h5.xiangxiangxiao.com/")

#pragma mark - <url拼接>
#define kURL(field) [NSString stringWithFormat:@"%@%@",kDomain,field]
#define kImgURL(field) [GlobalObj imgUrlFullPath:field]

#pragma mark - <通用模块>
//
#define kTabBar @"api/v1/basic/config"//tabBar
#define kHomeData @"api/v2/basic/home?source=IOS"//首页
#define kPrivacyPolicy @"app/v2/privacyPolicy"//隐私政策
#define kTeacherList @"api/edu/v1/teacher"//讲师列表
#define kEduCategoryList @"api/edu/v1/basic/getTutorshipCategory"//家教类型
#define kEduList @"api/edu/v1/tutorship"//家教列表
#define kAdvList @"api/v1/basic/adv"//广告
#define kClassOrder @"api/edu/v1/center/order/course"//课堂订单
#define kOrderTutorInfo @"api/edu/v1/center/order/orderTutorInfo"//我的家教
#define kMePageData @"api/v1/center/center_icon_list"//个人页面数据
#define kProtocolUrl @"api/v1/basic/clause"//所有协议
#define kGetFeedBackData @"api/v1/suggestion/create"//获取反馈数据
#define kFeedBackUrl @"api/v1/center/app/suggestion"//提交反馈
#define kUploadUrl @"api/v1/user/file/upload"//上传文件
#define kUploadVideoUrl @"api/v1/video/upload"//上传视频

#define kSchoolDataUrl @"api/v1/basic/school"//学校信息
#define kAuthUrl @"api/v1/center/auth/store"//角色认证
#define kUpdateProfile @"api/v1/user/profile/update"//更新用户信息
#define kStudentInfo @"api/v1/user/student_info"//学生用户信息
#define kMajorInfo @"api/v1/basic/major"//专业信息
#define kCompanyInfo @"api/v1/center/company/profile"//企业用户信息
#define kSearchUrl @"api/edu/v2/search"//搜索链接

#define kVideoRoomsData @"api/v1/job_room"

#define KTestUrl @"api/v1/center/yunXinChanel/201906271043072890"
#define kFriendData @"api/v1/common/dialog/user_avatar"
//
#define kVersionUpdate @"app/index/version"//版本更新
#define kUpgradeVersion @"app/center/upgradeVersion"//新的版本更新接口
/** 获取验证码  */
#define kSendSMS @"api/v1/basic/sms/send"//获取手机验证码
// 重设密码
#define kResetPsw @"api/v1/oauth/reset"
/** 登录 */
#define kLogin @"api/v1/oauth/login"
/** 个人页面  */
#define kPersonData @"api/v1/user/profile"
/** 注册 */
#define kRegister @"api/v1/oauth/register"

#define kTokenGetUser @"app/global/tokenGetUser" //根据token 获取用户信息
#define kImageCode @"app/global/imageCode" //生成图片验证码
//#define kUpload @"app/global/upload" //上传文件到本地服务器
#define kVerifyCode @"app/global/verifyCode"//验证手机验证码(修改密码)
#define kHotSearch @"api/v1/hotword"//搜索
#define kImageUpload @"app/global/upload/oss" //文件上传至OSS

#pragma mark - <用户模块>
#define kSetPassword @"app/user/setPassword"//新用户设置密码
#define kForgetPassword @"app/user/forgetPassword"//忘记密码
#define kLoginByAccount @"app/user/login"//账号登录
#define kUpdatePhone @"app/user/updatePhone"//修改手机号码
#define kChangePassword @"app/user/changePassword"//修改密码
#define kFeedBack @"app/user/feedBack"//用户反馈
#define kLogout @"app/user/logout"//退出登录
#define kAddressList @"app/userAddress/list"//地址列表
#define kAddressInsert @"app/userAddress/insert"//新增地址
#define kAddressUpdate @"app/userAddress/updateAddress"//更新地址
#define kAddressSetDefault @"app/userAddress/setDefault"//设置默认收货地址
#define kAddressDelete @"app/userAddress/delete"//删除地址

#endif /* DJ_URL_h */
