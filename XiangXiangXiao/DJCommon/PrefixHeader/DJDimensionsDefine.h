//
//  DJDimensionsDefine.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#ifndef DJDimensionsDefine_h
#define DJDimensionsDefine_h

#pragma mark - <获取keywindow>
#define kKeyWindow [UIApplication sharedApplication].keyWindow

#pragma mark - <获取屏幕宽度与高度,屏幕bounds>
#define kScreenBounds [UIScreen mainScreen].bounds
#define kScreenWidth   [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

#pragma mark - <导航栏高度，tabbar高度，iOS11不建议使用statusBar+navigationBar=64>
#define kNavigationBarHeight ([[UINavigationController alloc]init].navigationBar.frame.size.height)
#define kTabBarHeight ([[UITabBarController alloc]init].tabBar.frame.size.height)


#pragma mark - <判断当前机型是否为iPhoneX>
#warning ******************
#define kIS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !kIS_IPAD : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !kIS_IPAD : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !kIS_IPAD : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !kIS_IPAD : NO)

//plus
#define kIsIphone4 (kScreenWidth == 320.f && kScreenHeight == 480.f)
#define kIsIphone5 (kScreenWidth == 320.f && kScreenHeight == 568.f)
#define kIsIphone6 (kScreenWidth == 375.f && kScreenHeight == 667.f)
#define kIsIphoneP (kScreenWidth == 414.f && kScreenHeight == 736.f)
// X系列 有刘海！
#define kIsIPhoneX (IS_IPHONE_X || IS_IPHONE_Xr || IS_IPHONE_Xs || IS_IPHONE_Xs_Max)

#warning ******************

#define kiPhoneX kIsIPhoneX

#pragma mark - <当前机型的状态栏高度Status bar height>
#define kStatusBarHeight (kIsIPhoneX ? 44.f : 20.f)
//安全距离
#define kTabbarSafeBottomMargin (kIsIPhoneX ? 34.f : 0.f)

#pragma mark - <页面内容左右边距>
#define kEdgeLeftAndRight (12)

#pragma mark - <banner的比例>
#define kBannerScale (343.0/175.0)

#pragma mark - <字体font>
#define kNaviFont (18)
#define kGoodNameFont (16)
#define kNormalFont (14)
#define kClassifyFont (13)
#define kTabFont (10)
#define kRecommendFont (22)

// LFH
#define RGB(r,g,b)  [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define rgba(r,g,b,a)  [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]
#define RGBS(a) RGB(a,a,a)

#define kWidth(W)   W*kScreenWidth/375.f
#define kHH(width,scale) (width)/scale

#endif /* DJDimensionsDefine_h */
