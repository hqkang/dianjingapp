//
//  DJColorDefine.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#ifndef DJColorDefine_h
#define DJColorDefine_h

#pragma mark - <RGB颜色(不含透明度)>
#define kColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#pragma mark - <RGB颜色(含透明度)>
#define kColorFromRGBAndAlpha(rgbValue,alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

#pragma mark - <十六进制颜色>
#define kBlack 0x000000
#define kWhite 0xFFFFFF
//#define kScoreColor 0xeed268
#define kScoreColor 0xffd943
//#define kPriceColor 0xe54848
#define kPriceColor 0xff5310
#define kNameColor 0x333333
#define kTextColor 0x666666
#define kTabTextColor 0x999999
#define kTabSeperatorColor 0xe3e3e3
#define kBGColor 0xf4f4f4
#define kSearchBGColor 0xf0f0f0
#define kNavColor 0xf6f6f6
#define kShadowColor 0x241a19
#define kColBGColor 0xf7f7f7
#define kCateColor 0x888888
#define kCateTitleColor 0x11C4C4
#define kLiveBGColor 0x1D1E1E

#endif /* DJColorDefine_h */
