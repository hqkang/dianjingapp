//
//  DJConfigDefine.h
//  XiangXiangXiao
//
//  Created by xhkj on 2019/6/14.
//  Copyright © 2019年 xhkj. All rights reserved.
//

#ifndef DJConfigDefine_h
#define DJConfigDefine_h

#pragma mark - <用户偏好设置>
//用户偏好设置-取object
#define kUserDefaultObject(key) [[NSUserDefaults standardUserDefaults]objectForKey:key]
//用户偏好设置-设置object
#define kUserDefaultSetObject(object,key) [[NSUserDefaults standardUserDefaults]setObject:object forKey:key]
//用户偏好设置-移除object
#define kUserDefaultRemoveObject(key) [[NSUserDefaults standardUserDefaults]removeObjectForKey:key]
//用户偏好设置-同步
#define kUserDefaultSynchronize [[NSUserDefaults standardUserDefaults]synchronize]

#define QK_NOTICENTER [NSNotificationCenter defaultCenter]

#define LOGINDATA ([[NSKeyedUnarchiver unarchiveObjectWithData:kUserDefaultObject(kLoginData)] isKindOfClass:[NSDictionary class]] ? [NSKeyedUnarchiver unarchiveObjectWithData:kUserDefaultObject(kLoginData)] : nil)

#define USERDATA ([[NSKeyedUnarchiver unarchiveObjectWithData:kUserDefaultObject(kTotalUserInfo)] isKindOfClass:[NSDictionary class]] ? [NSKeyedUnarchiver unarchiveObjectWithData:kUserDefaultObject(kTotalUserInfo)] : nil)

// 融云token
#define kRCTOKEN @"RCTOKEN"

//用户精简信息-key
#define kUserInfo @"UserInfo"

//用户全部信息-key
#define kTotalUserInfo @"TotalUserInfo"
//用户id-key
#define kUserID @"UserID"
// 刷新token
#define kRefreshToken @"refreshToken"
//用户登陆token-key
#define kUserLoginToken @"UserLoginToken"
//用户手机号码-key

// 云信token
#define kYunXinToken @"YXTOKEN"

// 登录状态变化
#define kLoginStatusChange @"loginStatusChange"

// 协议
#define kProtocol @"protocol"
// 登录信息
#define kLoginData @"loginData"

// 移除自定义导航栏协议
#define kRemoveBaseNavDelegate @"removeBaseNavDelegate"

//微信openID
#define kWXOpenID @"WXOpenID"
//极光registerID
#define kJPushRegID @"JPushRegID"

#define kUserPhone @"UserPhone"
#define kUserPsw @"UserPsw"
//是否显示引导页
#define kExistGuideView @"ableDisappearGuideView"
//JSESSIONID
#define kJSESSIONID @"JSESSIONID"
//新通知
#define kNewNotice @"NewNotice"
//临时搜索记录
#define kTempSearchRecord @"TempSearchRecord"
#define kShowPrivacyPolicy @"ShowPrivacyPolicy"
//是否已经出现广告弹窗
#define kIsShowCusWindow @"IsShowCusWindow"

//默认navigationBar的shadowImage-key
#define kNavigationBarDefaultShadowImage @"NavigationBarDefaultShadowImage"
//通知名称-商品规格数据
#define kGoodsSpecData @"GoodsSpecData"
//通知名称-商品信息详情数据
#define kGoodsDetailData @"GoodsDetailData"
//通知名称-微信登录
#define kWXLoginNoti @"WXLogin"
//通知名称-请求购物车
#define kCartData @"RefreshCartData"
//通知名称-获取购物车数量
#define kCartCountData @"RefreshCartCountData"
//通知名称-从tabbar刷新消息中心
#define kRefreshMsg @"RefreshMsg"
//通知名称-升级通知
#define kUpgrade @"Upgrade"
//通知名称-刷新个人中心数据
#define kPersonalData @"RefreshPersonalData"
//通知名称-刷新订单列表
#define kOrderListData @"RefreshOrderListData"
//通知名称-支付完成
#define kPaySuccessful @"PaySuccessful"
//通知名称-在订单详情支付完成
#define kPaySuccessfulAtOrdDetail @"PaySuccessfulAtOrdDetail"
//通知名称-支付失败
#define kPayFailure @"PayFailure"
//通知名称-修改手机后刷新用户中心
#define kModifyMobileAndPasswordSuccessful @"ModifyMobileAndPasswordSuccessful"
//通知名称-修改手机后刷新退出登录
#define kModifyMobileAndPasswordClearData @"ModifyMobileAndPasswordClearData"
//通知名称-支付成功模态成功页面
#define kPresentPaySuccessfulVC @"PresentPaySuccessfulVC"
//通知名称-计时器进入后台模拟继续计时
#define kTimerGoOn @"TimerGoOn"
//通知名称-刷新优惠券列表
#define kMyCouponssss @"MyCouponssss"
//通知名称-广告弹窗
#define kCustomWindow @"CustomWindow"
//通知名称-点击推送进入app的消息中心
#define kDidResponNotiAction @"DidResponNotiAction"

//通知名称-注册成功
#define kRegisterSucAutoLogin @"kRegisterSucAutoLoginNoti"

#endif /* DJConfigDefine_h */
